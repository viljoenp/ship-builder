package org.rcwarships.cutter;

/*
 import com.sun.org.apache.xml.internal.serialize.OutputFormat;
 import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
 import java.io.IOException;
 import javax.xml.bind.JAXBContext;
 import javax.xml.bind.JAXBException;
 import javax.xml.bind.SchemaOutputResolver;
 import javax.xml.transform.dom.DOMResult;
 import org.rcwarships.builder.ModelInput;
 import org.w3c.dom.Document;
 */
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true);
    }
//
//    public void testSchema() throws JAXBException, IOException {
//        // grab the context
//        JAXBContext context = JAXBContext.newInstance(ModelInput.class);
//
//        final java.util.List<DOMResult> results = new java.util.ArrayList<>();
//
//        // generate the schema
//        context.generateSchema(
//                // need to define a SchemaOutputResolver to store to
//                new SchemaOutputResolver() {
//                    @Override
//                    public javax.xml.transform.Result createOutput(String ns, String file)
//                    throws IOException {
//                        // save the schema to the list
//                        DOMResult result = new DOMResult();
//                        result.setSystemId(file);
//                        results.add(result);
//                        return result;
//                    }
//                });
//
//        // output schema via System.out
//        DOMResult domResult = results.get(0);
//        Document doc = (Document) domResult.getNode();
//        OutputFormat format = new OutputFormat(doc);
//        format.setIndenting(true);
//        XMLSerializer serializer = new XMLSerializer(System.out, format);
//        serializer.serialize(doc);
//    }

}
