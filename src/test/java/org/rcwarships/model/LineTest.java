package org.rcwarships.model;

/*
 import com.sun.org.apache.xml.internal.serialize.OutputFormat;
 import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
 import java.io.IOException;
 import javax.xml.bind.JAXBContext;
 import javax.xml.bind.JAXBException;
 import javax.xml.bind.SchemaOutputResolver;
 import javax.xml.transform.dom.DOMResult;
 import org.rcwarships.builder.ModelInput;
 import org.w3c.dom.Document;
 */
import javax.vecmath.Point3d;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple org.rcwarships.model.Line.
 */
public class LineTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public LineTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(LineTest.class);
    }

    /**
     * Test basic LineIntersections
     */
    public void testLineIntersectionX() {
        final Point3d p0 = new Point3d(0, 0, 0);
        final Point3d p1 = new Point3d(1, 0, 0);
        final Line line = new Line(p0, p1);

        final Line t1 = new Line(new Point3d(0.0, 0, 0), new Point3d(0.0, 1, 0));
        assertTrue("Line " + line + " should intersect line " + t1, line.getIntersection(t1, true) != null);
        assertTrue("Line " + line + " should intersect line " + t1 + " at (0,0,0)", line.getIntersection(t1, true).distance(new Point3d(0, 0, 0)) < Epsilon.E);

        final Line t2 = new Line(new Point3d(0.5, -1, 0), new Point3d(0.5, 1, 0));
        assertTrue("Line " + line + " should intersect line " + t2, line.getIntersection(t2, true) != null);
        assertTrue("Line " + line + " should intersect line " + t2 + " at (0.5,0,0)", line.getIntersection(t2, true).distance(new Point3d(0.5, 0, 0)) < Epsilon.E);

        final Line t3 = new Line(new Point3d(0.5, 0, -1), new Point3d(0.5, 0, 1));
        assertTrue("Line " + line + " should intersect line " + t3, line.getIntersection(t3, true) != null);
        assertTrue("Line " + line + " should intersect line " + t3 + " at (0.5,0,0)", line.getIntersection(t3, true).distance(new Point3d(0.5, 0, 0)) < Epsilon.E);

        final Line t4 = new Line(new Point3d(1, 0, -1), new Point3d(1, 0, 1));
        assertTrue("Line " + line + " should intersect line " + t4, line.getIntersection(t4, true) != null);
        assertTrue("Line " + line + " should intersect line " + t4 + " at (1,0,0)", line.getIntersection(t4, true).distance(new Point3d(1, 0, 0)) < Epsilon.E);

        final Line t5 = new Line(new Point3d(1, -1, 0), new Point3d(1, 1, 0));
        assertTrue("Line " + line + " should intersect line " + t5, line.getIntersection(t5, true) != null);
        assertTrue("Line " + line + " should intersect line " + t5 + " at (1,0,0)", line.getIntersection(t5, true).distance(new Point3d(1, 0, 0)) < Epsilon.E);

        final Line t6 = new Line(new Point3d(0, 0, 0), new Point3d(0, 0, 1));
        assertTrue("Line " + line + " should intersect line " + t6, line.getIntersection(t6, true) != null);
        assertTrue("Line " + line + " should intersect line " + t6 + " at (0,0,0)", line.getIntersection(t6, true).distance(new Point3d(0, 0, 0)) < Epsilon.E);

        final Line t7 = new Line(new Point3d(-0.000000001, 0, 0), new Point3d(-0.000000001, 0, 1));
        assertTrue("Line " + line + " should not intersect line " + t7, line.getIntersection(t7, true) == null);
        assertTrue("Line " + line + " should intersect (none strict) line " + t7, line.getIntersection(t7, false) != null);
        assertTrue("Line " + line + " should intersect (none strict) line " + t7 + " at (-0.000000001,0,0)", line.getIntersection(t7, false).distance(new Point3d(-0.000000001, 0, 0)) < Epsilon.E);

        final Line t8 = new Line(new Point3d(+0.000000001, +0.000010001, -1), new Point3d(+0.000000001, +0.000010001, 1));
        assertTrue("Line " + line + " should not intersect line " + t8 + " at " + line.getIntersection(t8, true), line.getIntersection(t8, true) == null);
        assertTrue("Line " + line + " should not intersect (none strict) line " + t8 + " at " + line.getIntersection(t8, false), line.getIntersection(t8, false) == null);

    }

    /**
     * Test basic LineIntersections
     */
    public void testLineIntersectionY() {
        final Point3d p0 = new Point3d(0, 0, 0);
        final Point3d p1 = new Point3d(0, 1, 0);
        final Line line = new Line(p0, p1);

        final Line t1 = new Line(new Point3d(0.0, 0, 0), new Point3d(1.0, 0, 0));
        assertTrue("Line " + line + " should intersect line " + t1, line.getIntersection(t1, true) != null);
        assertTrue("Line " + line + " should intersect line " + t1 + " at (0,0,0)", line.getIntersection(t1, true).distance(new Point3d(0, 0, 0)) < Epsilon.E);

        final Line t2 = new Line(new Point3d(-1, 0.5, 0), new Point3d(1, 0.5, 0));
        assertTrue("Line " + line + " should intersect line " + t2, line.getIntersection(t2, true) != null);
        assertTrue("Line " + line + " should intersect line " + t2 + " at (0,0.5,0)", line.getIntersection(t2, true).distance(new Point3d(0, 0.5, 0)) < Epsilon.E);

        final Line t3 = new Line(new Point3d(0, 0.5, -1), new Point3d(0, 0.5, 1));
        assertTrue("Line " + line + " should intersect line " + t3, line.getIntersection(t3, true) != null);
        assertTrue("Line " + line + " should intersect line " + t3 + " at (0,0.5,0)", line.getIntersection(t3, true).distance(new Point3d(0, 0.5, 0)) < Epsilon.E);

        final Line t4 = new Line(new Point3d(0, 1, -1), new Point3d(0, 1, 1));
        assertTrue("Line " + line + " should intersect line " + t4, line.getIntersection(t4, true) != null);
        assertTrue("Line " + line + " should intersect line " + t4 + " at (0,1,0)", line.getIntersection(t4, true).distance(new Point3d(0, 1, 0)) < Epsilon.E);

        final Line t5 = new Line(new Point3d(0, 1, -1), new Point3d(0, 1, 1));
        assertTrue("Line " + line + " should intersect line " + t5, line.getIntersection(t5, true) != null);
        assertTrue("Line " + line + " should intersect line " + t5 + " at (0,1,0)", line.getIntersection(t5, true).distance(new Point3d(0, 1, 0)) < Epsilon.E);

        final Line t6 = new Line(new Point3d(0, 0, 0), new Point3d(0, 0, 1));
        assertTrue("Line " + line + " should intersect line " + t6, line.getIntersection(t6, true) != null);
        assertTrue("Line " + line + " should intersect line " + t6 + " at (0,0,0)", line.getIntersection(t6, true).distance(new Point3d(0, 0, 0)) < Epsilon.E);

        final Line t7 = new Line(new Point3d(0, -0.000000001, 0), new Point3d(0, -0.000000001, 1));
        assertTrue("Line " + line + " should not intersect line " + t7, line.getIntersection(t7, true) == null);
        assertTrue("Line " + line + " should intersect (none strict) line " + t7, line.getIntersection(t7, false) != null);
        assertTrue("Line " + line + " should intersect (none strict) line " + t7 + " at (0,-0.000000001,0)", line.getIntersection(t7, false).distance(new Point3d(0, -0.000000001, 0)) < Epsilon.E);

        final Line t8 = new Line(new Point3d(0.000000001, -0.000000001, 0), new Point3d(-0.000000001, -0.000000001, 1));
        assertTrue("Line " + line + " should not intersect line " + t8 + " at " + line.getIntersection(t8, true), line.getIntersection(t8, true) == null);
        assertTrue("Line " + line + " should not intersect (none strict) line " + t8 + " at " + line.getIntersection(t8, false), line.getIntersection(t8, false) == null);

    }

    /**
     * Test basic LineIntersections
     */
    public void testLineIntersectionZ() {
        final Point3d p0 = new Point3d(0, 0, 0);
        final Point3d p1 = new Point3d(0, 0, 1);
        final Line line = new Line(p0, p1);

        final Line t1 = new Line(new Point3d(0.0, 0, 0), new Point3d(1.0, 0, 0));
        assertTrue("Line " + line + " should intersect line " + t1, line.getIntersection(t1, true) != null);
        assertTrue("Line " + line + " should intersect line " + t1 + " at (0,0,0)", line.getIntersection(t1, true).distance(new Point3d(0, 0, 0)) < Epsilon.E);

        final Line t2 = new Line(new Point3d(-1, 0, 0.5), new Point3d(1, 0, 0.5));
        assertTrue("Line " + line + " should intersect line " + t2, line.getIntersection(t2, true) != null);
        assertTrue("Line " + line + " should intersect line " + t2 + " at (0,0,0.5)", line.getIntersection(t2, true).distance(new Point3d(0, 0, 0.5)) < Epsilon.E);

        final Line t3 = new Line(new Point3d(0, -1, 0.5), new Point3d(0, 1, 0.5));
        assertTrue("Line " + line + " should intersect line " + t3, line.getIntersection(t3, true) != null);
        assertTrue("Line " + line + " should intersect line " + t3 + " at (0,0,0.5)", line.getIntersection(t3, true).distance(new Point3d(0, 0, 0.5)) < Epsilon.E);

        final Line t4 = new Line(new Point3d(-1, 0, 1), new Point3d(1, 0, 1));
        assertTrue("Line " + line + " should intersect line " + t4, line.getIntersection(t4, true) != null);
        assertTrue("Line " + line + " should intersect line " + t4 + " at (0,0,1)", line.getIntersection(t4, true).distance(new Point3d(0, 0, 1)) < Epsilon.E);

        final Line t5 = new Line(new Point3d(0, -1, 1), new Point3d(0, 1, 1));
        assertTrue("Line " + line + " should intersect line " + t5, line.getIntersection(t5, true) != null);
        assertTrue("Line " + line + " should intersect line " + t5 + " at (0,0,1)", line.getIntersection(t5, true).distance(new Point3d(0, 0, 1)) < Epsilon.E);

        final Line t6 = new Line(new Point3d(0, 0, 0), new Point3d(0, 1, 0));
        assertTrue("Line " + line + " should intersect line " + t6, line.getIntersection(t6, true) != null);
        assertTrue("Line " + line + " should intersect line " + t6 + " at (0,0,0)", line.getIntersection(t6, true).distance(new Point3d(0, 0, 0)) < Epsilon.E);

        final Line t7 = new Line(new Point3d(0, 0, -0.000000001), new Point3d(0, 1, -0.000000001));
        assertTrue("Line " + line + " should not intersect line " + t7, line.getIntersection(t7, true) == null);
        assertTrue("Line " + line + " should intersect (none strict) line " + t7, line.getIntersection(t7, false) != null);
        assertTrue("Line " + line + " should intersect (none strict) line " + t7 + " at (0,0,-0.000000001)", line.getIntersection(t7, false).distance(new Point3d(0, 0, -0.000000001)) < Epsilon.E);

        final Line t8 = new Line(new Point3d(1, -0.000000001, - 1), new Point3d(1, -0.000000001, -1));
        assertTrue("Line " + line + " should not intersect line " + t8 + " at " + line.getIntersection(t8, true), line.getIntersection(t8, true) == null);
        assertTrue("Line " + line + " should not intersect (none strict) line " + t8 + " at " + line.getIntersection(t8, false), line.getIntersection(t8, false) == null);
    }
    
    
     /**
     * Test basic LineIntersections
     */
    public void testLineIntersection1() {
        final Point3d p0 = new Point3d(0, 1612.12195, 2.20139);
        final Point3d p1 = new Point3d(0, 1609.22498, 0.31042);
        final Point3d p2 = new Point3d(0, 1611.19702, 1.03681);
        final Point3d p3 = new Point3d(0, 1606.31494, -0.05556);
        final Line l1 = new Line(p0, p1);
        final Line l2 = new Line(p1, p2);
        final Line l3 = new Line(p2, p3);
        
        assertTrue("Line " + l1 + " should intersect line " + l3, l1.getIntersection(l3, true) != null);
    }
        
}
