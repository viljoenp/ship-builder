/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 *
 * @author viljoenp
 */
public class MeshOptimizer {

    /**
     * Return a new mesh which is an "optimized" version of the input mesh. This
     * is done by rearranging edges to eliminate very small angles, or vertices
     * where many edges come together. The resulting mesh will generally produce
     * a better looking surface after smoothing is applied to it.
     *
     * @param mesh
     * @return
     */
    public static Mesh optimizeMesh(final Mesh mesh) {
        Face face[] = mesh.getFaces();
        Edge edge[] = mesh.getEdges();
        Vertex vertex[] = mesh.getVertexes();
        boolean candidate[] = new boolean[edge.length];
        Mesh newmesh = null;

        for (int i = 0; i < edge.length; i++) {
            candidate[i] = true;
        }
        while (true) {
            final Vector3d faceNorm[] = new Vector3d[face.length];
            final boolean onBoundary[] = new boolean[vertex.length];
            final int numEdges[] = new int[vertex.length];

            // Initialize the various arrays, and determine which edges are really candidates for optimization.
            for (int i = 0; i < face.length; i++) {
                final Face f = face[i];
                final Integer[] fEdges = f.getEdges();
                if (candidate[fEdges[0]] || candidate[fEdges[1]] || candidate[fEdges[2]]) {
                    final Point3d v1 = vertex[f.verts[0]].coord;
                    final Point3d v2 = vertex[f.verts[1]].coord;
                    final Point3d v3 = vertex[f.verts[2]].coord;
                    final Vector3d d1 = sub(v2, v1);
                    final Vector3d d2 = sub(v3, v1);
                    faceNorm[i] = cross(d1, d2);
                    double length = faceNorm[i].length();
                    if (length > 0.0) {
                        faceNorm[i].scale(1.0 / length);
                    } else if (!v1.equals(v2) && !v1.equals(v3) && !v2.equals(v3)) {
                        faceNorm[i] = null;
                    }
                }
            }
            for (int i = 0; i < edge.length; i++) {
                final Edge e = edge[i];
                numEdges[e.verts[0]]++;
                numEdges[e.verts[1]]++;
                final Integer[] eFaces = e.getFaces();
                if (eFaces.length != 2) {
                    onBoundary[e.verts[0]] = onBoundary[e.verts[1]] = true;
                    candidate[i] = false;
                } else if (candidate[i] && faceNorm[eFaces[0]] != null && faceNorm[eFaces[1]] != null && faceNorm[eFaces[0]].dot(faceNorm[eFaces[1]]) < 0.99) {
                    candidate[i] = false;
                }
            }

            // For each candidate edge, find the list of vertices and angles involved in swapping it.  The vertices
            // are ordered as follows:
            //              <-
            //              /\ 2
            //             /f1\
            //            /    \
            //          0 ------ 1
            //            \    /
            //             \f2/
            //              \/ 3
            //              ->
            final int swapVert[][] = new int[edge.length][];
            final double minAngle[][] = new double[edge.length][];
            for (int i = 0; i < edge.length; i++) {
                if (candidate[i]) {
                    // First find the vertices.

                    swapVert[i] = new int[4];
                    final Edge e = edge[i];
                    final Integer[] eFaces = e.face_keys.toArray(new Integer[2]);
                    final Face f1 = face[eFaces[0]], f2 = face[eFaces[1]];
                    if ((e.verts[0] == f1.verts[0] && e.verts[1] == f1.verts[1]) || (e.verts[0] == f1.verts[1] && e.verts[1] == f1.verts[2]) || (e.verts[0] == f1.verts[2] && e.verts[1] == f1.verts[0])) {
                        swapVert[i][0] = e.verts[0];
                        swapVert[i][1] = e.verts[1];
                    } else {
                        swapVert[i][0] = e.verts[1];
                        swapVert[i][1] = e.verts[0];
                    }
                    if (e.verts[0] != f1.verts[0] && e.verts[1] != f1.verts[0]) {
                        swapVert[i][2] = f1.verts[0];
                    } else if (e.verts[0] != f1.verts[1] && e.verts[1] != f1.verts[1]) {
                        swapVert[i][2] = f1.verts[1];
                    } else {
                        swapVert[i][2] = f1.verts[2];
                    }
                    if (e.verts[0] != f2.verts[0] && e.verts[1] != f2.verts[0]) {
                        swapVert[i][3] = f2.verts[0];
                    } else if (e.verts[0] != f2.verts[1] && e.verts[1] != f2.verts[1]) {
                        swapVert[i][3] = f2.verts[1];
                    } else {
                        swapVert[i][3] = f2.verts[2];
                    }

                    // Now calculate the angles.
                    minAngle[i] = new double[4];
                    final Vector3d d1 = sub(vertex[swapVert[i][1]].coord, vertex[swapVert[i][0]].coord);
                    final Vector3d d2 = sub(vertex[swapVert[i][2]].coord, vertex[swapVert[i][0]].coord);
                    final Vector3d d3 = sub(vertex[swapVert[i][3]].coord, vertex[swapVert[i][0]].coord);
                    final Vector3d d4 = sub(vertex[swapVert[i][2]].coord, vertex[swapVert[i][1]].coord);
                    final Vector3d d5 = sub(vertex[swapVert[i][3]].coord, vertex[swapVert[i][1]].coord);
                    final Vector3d d6 = sub(vertex[swapVert[i][3]].coord, vertex[swapVert[i][2]].coord);
                    d1.normalize();
                    d2.normalize();
                    d3.normalize();
                    d4.normalize();
                    d5.normalize();
                    d6.normalize();
                    double a1, a2;
                    a1 = Math.acos(d1.dot(d2));
                    a2 = Math.acos(d1.dot(d3));
                    if (a1 + a2 >= Math.PI) {
                        candidate[i] = false;
                        continue;
                    }
                    minAngle[i][0] = (a1 < a2 ? a1 : a2);
                    a1 = Math.acos(-d1.dot(d4));
                    a2 = Math.acos(-d1.dot(d5));
                    if (a1 + a2 >= Math.PI) {
                        candidate[i] = false;
                        continue;
                    }
                    minAngle[i][1] = (a1 < a2 ? a1 : a2);
                    a1 = Math.acos(-d6.dot(d2));
                    a2 = Math.acos(-d6.dot(d4));
                    minAngle[i][2] = (a1 < a2 ? a1 : a2);
                    a1 = Math.acos(d6.dot(d3));
                    a2 = Math.acos(d6.dot(d5));
                    minAngle[i][3] = (a1 < a2 ? a1 : a2);
                }
            }

            // Calculate scores for each candidate edge, and decide which ones to swap.
            final double score[] = new double[edge.length];
            final boolean swap[] = new boolean[edge.length];
            for (int i = 0; i < score.length; i++) {
                if (candidate[i]) {
                    score[i] = calcSwapScore(minAngle[i], swapVert[i], numEdges, onBoundary);
                }
            }
            while (true) {
                int best = -1;
                double maxScore = 0.01;
                for (int i = 0; i < candidate.length; i++) {
                    if (candidate[i] && score[i] > maxScore) {
                        best = i;
                        maxScore = score[i];
                    }
                }
                if (best == -1) {
                    break;
                }

                // Mark the edge to be swapped.  Remove it and every other edge that shares a face with it
                // from the candidate list.
                swap[best] = true;
                final Edge e = edge[best];
                final Integer[] eFaces = e.face_keys.toArray(new Integer[2]);
//                Face f = face[eFaces[0]];
                final Integer[] fEdges1 = face[eFaces[0]].getEdges();
                candidate[fEdges1[0]] = candidate[fEdges1[1]] = candidate[fEdges1[2]] = false;
                final Integer[] fEdges2 = face[eFaces[1]].getEdges();
                candidate[fEdges2[0]] = candidate[fEdges2[1]] = candidate[fEdges2[2]] = false;

                // Update the numEdges array, and recalculate scores.
                numEdges[swapVert[best][0]]--;
                numEdges[swapVert[best][1]]--;
                numEdges[swapVert[best][2]]++;
                numEdges[swapVert[best][3]]++;
                for (int i = 0; i < 4; i++) {
                    final Integer vertEdges[] = vertex[swapVert[best][i]].getEdges();
                    for (int j = 0; j < vertEdges.length; j++) {
                        if (candidate[vertEdges[j]]) {
                            score[vertEdges[j]] = calcSwapScore(minAngle[vertEdges[j]], swapVert[vertEdges[j]], numEdges, onBoundary);
                        }
                    }
                }
            }

            // We now know which edges we want to swap.  Create the new mesh.
            int newface[][] = new int[face.length][];
            int next = 0;
            for (int i = 0; i < face.length; i++) {
                final Face f = face[i];
                final Integer[] fEdges = f.getEdges();
                if (!swap[fEdges[0]] && !swap[fEdges[1]] && !swap[fEdges[2]]) {
                    newface[next++] = new int[]{f.verts[0], f.verts[1], f.verts[2]};
                }
            }
            int firstSplit = next;
            for (int i = 0; i < edge.length; i++) {
                if (swap[i]) {
                    newface[next++] = new int[]{swapVert[i][2], swapVert[i][0], swapVert[i][3]};
                    newface[next++] = new int[]{swapVert[i][2], swapVert[i][3], swapVert[i][1]};
                }
            }
            newmesh = new Mesh(mesh.name, vertex, newface);

            // Copy over edge smoothness values.
            final Vertex newvert[] = (Vertex[]) newmesh.getVertexes();
            final Edge newedge[] = newmesh.getEdges();
            for (int i = 0; i < edge.length; i++) {
                if (!swap[i] && edge[i].smoothness != 1.0) {
                    final Integer edges2[] = newvert[edge[i].verts[0]].getEdges();
                    final Edge e1 = edge[i];
                    for (int k = 0; k < edges2.length; k++) {
                        final Edge e2 = newedge[edges2[k]];
                        if ((e1.verts[0] == e2.verts[0] && e1.verts[1] == e2.verts[1]) || (e1.verts[0] == e2.verts[1] && e1.verts[1] == e2.verts[0])) {
                            e2.smoothness = e1.smoothness;
                            break;
                        }
                    }
                }
            }

            // Determine which edges are candidates for the next iteration.
            if (firstSplit == next) {
                break;
            }
            vertex = newvert;
            edge = newedge;
            face = newmesh.getFaces();
            candidate = new boolean[edge.length];
            for (int i = firstSplit; i < face.length; i++) {
                final Face f = face[i];
                final Integer fEdges[] = f.getEdges();
                candidate[fEdges[0]] = candidate[fEdges[1]] = candidate[fEdges[2]] = true;
            }
        }
        newmesh.validateSolid();
        return newmesh;
    }

    /**
     * This is a utility routine used by optimizeMesh(). It calculates the score
     * for swapping a particular edge.
     */
    private static double calcSwapScore(final double minAngle[], final int vert[], final int numEdges[], final boolean onBoundary[]) {
        final double s[] = new double[4];
        for (int i = 0; i < 4; i++) {
            final int v = vert[i];
            int ideal = (onBoundary[v] ? 4 : 6);
            if (i > 1) {
                ideal--;
            }
            s[i] = (numEdges[v] > ideal ? minAngle[i] / (numEdges[v] - ideal + 1.5) : minAngle[i]);
        }
        final double currentScore = (s[0] < s[1] ? s[0] : s[1]);
        final double swapScore = (s[2] < s[3] ? s[2] : s[3]);
        return swapScore - currentScore;
    }

    private static Vector3d sub(final Tuple3d a, final Tuple3d b) {
        final Vector3d ab = new Vector3d(a);
        ab.sub(b);
        return ab;
    }

    private static Vector3d cross(final Vector3d a, final Vector3d b) {
        final Vector3d ab = new Vector3d();
        ab.cross(a, b);
        return ab;
    }
}
