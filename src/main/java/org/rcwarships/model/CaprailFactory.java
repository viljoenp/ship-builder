/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import org.rcwarships.loaders.svg.SvgFile;
import org.rcwarships.model.clipper.Clipper;
import org.rcwarships.model.clipper.EndPointBound;
import org.rcwarships.model.extruder.Extruder;
import org.rcwarships.model.partition.OverlapPartitioner;
import org.rcwarships.model.partition.PairedHorizontalKnucklePartitioner;
import org.rcwarships.model.partition.PairedVerticalKnucklePartitioner;
import org.rcwarships.model.partition.Partition;
import org.rcwarships.model.simplifier.Simplifier;
import org.rcwarships.model.triangulation.Triangulation3d;
import org.rcwarships.model.triangulation.TriangulationPartition;
import org.rcwarships.model.utils.ProximityMap;

/**
 *
 * @author viljoenp
 */
public class CaprailFactory {

    private static final Logger LOGGER = Logger.getLogger(CaprailFactory.class.getName());
    private static boolean CUT_NOTCHES = true;

    static {
        if (System.getProperty("cut-notches") != null) {
            CUT_NOTCHES = Boolean.parseBoolean(System.getProperty("cut-notches"));
        }
    }

    /**
     *
     * FIXME , we should generalize this code to handle decks that not flat (or
     * slightly curved ), decks like Littorio / Jean Bart have 90 degree steps ,
     * (will have to make a plan with them).
     *
     *
     *
     * Static factory method to create a MESH that represents the Caprail
     * (joining the Deckplate and the ribs, of the model, some assumptions:
     *
     * The Ship is centered along the y Axis , from stern to bow ( or vice a
     * versa )
     *
     * @param hull_MESH Mesh representing the entire HULL and ONLY the hull, it
     * should not include the DECK and super structure (usually created in
     * DelftShip),
     * @param plate_width the width that the deck line on the hull model will be
     * extruded ( inwards )
     * @param plate_thickness the width of the KEEL plate
     * @param rib_positions A list of all the model Y coordinates where a RIB
     * need to be placed in the KEEL, we will cut a notch into the KEEL place so
     * the rib can fit like a puzzle piece.
     * @param maxCollisionDepth number of vertexes to backtrack in search of
     * collisions after the extrusion phase
     * @return A Mesh that represents the entire KEEL with rib notched cut into
     * it.
     */
    public static Mesh makeCaprail(Mesh hull_MESH, ModelUtils.CapRailInfo info, ModelUtils.KeelInfo keel, java.util.List<ModelUtils.RibPosition> rib_positions, final int maxCollisionDepth) {
        return makeCaprailMesh("CAPRAIL-MESH", hull_MESH, info, keel, rib_positions, maxCollisionDepth);
    }

    public static Mesh makeDeckPlate(Mesh hull_MESH, ModelUtils.DeckInfo info, ModelUtils.KeelInfo keel, final int maxCollisionDepth) {
        return makeDeckPlateMesh("DECK-MESH", hull_MESH, info, keel, maxCollisionDepth);
    }

    /**
     * To make the deck mesh.
     *
     * 1. Get p-outer-top-line all the manifold(edges with only 1 face
     * reference) edges where X >= 0 ( we first work only with the one half.
     *
     * 2. Get mirror of p-outer-top-line to get the n-outer-top-line
     *
     * 3. the p-inner-top-line : extrude p-outer-top-line inwards along the xy
     * plane by plate_width units.
     *
     * 4. cut top notches into p-inner-top-line.
     *
     * 5. the n_inner_top_line : p-inner-top-line( .x *= -1 )
     *
     */
    private static Mesh makeCaprailMesh(String name, Mesh hull_MESH, ModelUtils.CapRailInfo info, ModelUtils.KeelInfo keel, java.util.List<ModelUtils.RibPosition> rib_positions, final int maxCollisionDepth) {

        final Mesh tmp_MESH = new Mesh(name);
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Looking for points on the manifold edges  X>=0.0");
        }
        // 1. Get p-outer-lop-line all the manifold edges with only 1 face reference) edges where X >= 0 ( we first work only with the one half.
        final java.util.List<Point3d> p_outer_top_line = getCaprailTopPoints(hull_MESH);
        final Point3d keelStopPoint = p_outer_top_line.get(p_outer_top_line.size() - 1);
        final double keelStop = keel.max_y;
        final Point3d keelStartPoint = p_outer_top_line.get(0);
        final double keelStart = keel.min_y;
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Found {0} , points on manifoldedges  X>=0.0   min={1} , max={2} ", new Object[]{p_outer_top_line.size(), keelStopPoint, keelStartPoint});
            LOGGER.log(Level.INFO, "Caprail KeelNotch  {0}  and {1} ", new Object[]{keelStop, keelStart});
        }
        // Project a 2d extrusion at z=0 we will use this to build the the captrail by finding intersections with planes 
        final java.util.List<Point3d> p_inner_extrution_line1 = getInnerExtrusion(p_outer_top_line, info.l1Extrusion, maxCollisionDepth);
        final java.util.List<Point3d> p_inner_extrution_line2 = getInnerExtrusion(p_outer_top_line, info.l2Extrusion, maxCollisionDepth);

        // 3. the outer-top-line : extrude outer-bottom-line upwards in the yz plane by info.l1Extrusion units
        // 4. cut top notches into outer-top-line.
        final java.util.List<Point3d> P_outer_top_notched_line = CUT_NOTCHES ? getCutRibNotchs(ModelUtils.deepCopy(p_outer_top_line), rib_positions, keel) : ModelUtils.deepCopy(p_outer_top_line);
        // for each point in P_outer_top_notched_line (excluding the start and end) we get the rib hull out line associated with the points .y coordinate , we will later use this to get the x coordinate for a given z

        final java.util.List<Point3d> p_outer_lower_line1 = getLowerExtrusion(p_outer_top_line, info.width / 2, maxCollisionDepth);
        final java.util.List<Point3d> P_outer_lower_notched_line1 = CUT_NOTCHES ? getCutRibNotchs(mapToRibAtContour(hull_MESH, p_outer_lower_line1), rib_positions, keel) : mapToRibAtContour(hull_MESH, p_outer_lower_line1);
        final java.util.List<java.util.List<Point3d>> squares = new java.util.ArrayList<>();

        final java.util.List<ModelUtils.RibPosition> rib_clone = new java.util.ArrayList<>(rib_positions.size());
        for (ModelUtils.RibPosition pos : rib_positions) {
            ModelUtils.RibPosition clone = new ModelUtils.RibPosition(pos.y, pos.width, pos.thickness);
            clone.deck_x = Math.max(pos.deck_x - pos.thickness * .5, 0.5);
            clone.deck_z = pos.deck_z;
            clone.keel_z = pos.keel_z;
            rib_clone.add(clone);
        }
        ModelUtils.KeelInfo keel_clone = new ModelUtils.KeelInfo(keel.width, keel.thickness);
        keel_clone.max_y = keel.max_y - (keel.thickness * (1 / 3.0));
        keel_clone.min_y = keel.min_y + (keel.thickness * (1 / 3.0));
        final java.util.List<Point3d> P_outer_lower_notched_line11 = CUT_NOTCHES ? getCutRibNotchs(P_outer_lower_notched_line1, rib_positions, rib_clone, squares) : ModelUtils.deepCopy(P_outer_lower_notched_line1);
        // keelStart, keelStop, keelStartPoint.y + info.l1Extrusion, keelStopPoint.y - info.l1Extrusion
        final java.util.List<Point3d> p_outer_lower_line2 = getLowerExtrusion(p_outer_top_line, info.width, maxCollisionDepth);
        final java.util.List<Point3d> P_outer_lower_notched_line2 = CUT_NOTCHES ? getCutRibNotchs(mapToRibAtContour(hull_MESH, p_outer_lower_line2), rib_clone, keel_clone) : mapToRibAtContour(hull_MESH, p_outer_lower_line2);

        // DUMP The top Line
        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile caprailSVG = new SvgFile("caprail.svg");
                java.util.List<Point3d> mirror0 = Extruder.mirrorOnY(P_outer_top_notched_line);
                java.util.Collections.reverse(mirror0);
                java.util.List<Point3d> mirror3 = Extruder.mirrorOnY(P_outer_lower_notched_line1);
                java.util.Collections.reverse(mirror3);
                java.util.List<Point3d> mirror4 = Extruder.mirrorOnY(P_outer_lower_notched_line2);
                java.util.Collections.reverse(mirror4);
                final java.util.List<Point3d> mirror1 = Extruder.mirrorOnY(p_inner_extrution_line1);
                java.util.Collections.reverse(mirror1);
                final java.util.List<Point3d> mirror2 = Extruder.mirrorOnY(p_inner_extrution_line2);
                java.util.Collections.reverse(mirror2);

                caprailSVG.export_yz_svg("L1", "blue", 10, 160, P_outer_top_notched_line, mirror0);
                caprailSVG.export_yz_svg("L1", "pink", 10, 160, P_outer_lower_notched_line1, mirror3);
                caprailSVG.export_yz_svg("L2", "black", 10, 160, P_outer_lower_notched_line2, mirror4);
                caprailSVG.export_yz_svg("L1", "red", 10, 160, p_inner_extrution_line1, mirror1);
                caprailSVG.export_yz_svg("L1", "green", 10, 160, p_inner_extrution_line2, mirror2);

                for (java.util.List<Point3d> ribline : buildRibProfileAtPoints(hull_MESH, P_outer_top_notched_line).values()) {
                    caprailSVG.export_yz_svg("L1", "red", 10, 160, ribline, java.util.Collections.singletonList(new Point3d(0, ribline.get(0).y, ribline.get(0).z)));
                }

                caprailSVG.export_yx_svg("L1", "blue", 10, 300, P_outer_top_notched_line, mirror0);
                caprailSVG.export_yx_svg("L1", "pink", 10, 300, P_outer_lower_notched_line1, mirror3);
                caprailSVG.export_yx_svg("L1", "red", 10, 300, p_inner_extrution_line1, mirror1);
                caprailSVG.export_yx_svg("L1", "green", 10, 300, p_inner_extrution_line2, mirror2);

                caprailSVG.export_yx_svg("L2", "black", 10, 300, P_outer_lower_notched_line2, mirror4);

                java.util.List<Point3d> mirror5 = Extruder.mirrorOnY(p_outer_top_line);
                java.util.Collections.reverse(mirror5);
                caprailSVG.export_yz_svg("p_outer_top_line", "blue", 10, 600, p_outer_top_line, mirror5);
                caprailSVG.export_yx_svg("p_outer_top_line", "blue", 10, 900, p_outer_top_line, mirror5);

//                caprailSVG.export_yz_svg("extr1", "red", 10, 320, p_inner_extrution_line1, mirror1);
//                caprailSVG.export_yz_svg("extr2", "green", 10, 320, p_inner_extrution_line2, mirror2);
                caprailSVG.close();
            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump caprail.svg", ex);
            }
        }

        // Top outer level
        final java.util.List<Vertex> top_outer = ModelUtils.addMeshPoints(tmp_MESH, P_outer_top_notched_line, new Vector3d(1, 1, 1));
        final java.util.List<Point3d> top_exrusion_line = makeInnerExctrudedMap(p_outer_top_line, p_inner_extrution_line1, false);
        final java.util.List<Vertex> top_extrusion = ModelUtils.addMeshPoints(tmp_MESH, top_exrusion_line, new Vector3d(1, 1, 1));
        triangulateMappedExtrusion(tmp_MESH, top_outer, top_extrusion, false);

        // inner sidewall from top outer level to inner extrusion
        final java.util.List<Point3d> top_exrusion_line_l1 = getLowerExtrusion(top_exrusion_line, info.width / 2, maxCollisionDepth);
        final java.util.List<Vertex> top_extrusion_l1 = ModelUtils.addMeshPoints(tmp_MESH, top_exrusion_line_l1, new Vector3d(1, 1, 1));
        triangulateStripe(tmp_MESH, top_extrusion, top_extrusion_l1, false);

        // inner level top face, L1 
        final java.util.List<Point3d> top_inner_exrusion_l1_line = makeInnerExctrudedMap(top_exrusion_line_l1, p_inner_extrution_line2, false);
        final java.util.List<Vertex> top_inner_exrusion_l1 = ModelUtils.addMeshPoints(tmp_MESH, top_inner_exrusion_l1_line, new Vector3d(1, 1, 1));

        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile caprailSVG = new SvgFile("caprail-debug.svg");
                java.util.List<Vertex> mirror0 = new java.util.ArrayList<>(top_extrusion_l1);
                java.util.Collections.reverse(mirror0);
                final java.util.List<Point3d> mirror1 = new java.util.ArrayList<>(top_exrusion_line_l1);
                java.util.Collections.reverse(mirror1);

                caprailSVG.export_yz_svg(tmp_MESH, "L1", 1.0 / 144.0 / 10, 10, 160, top_inner_exrusion_l1, mirror0);
                caprailSVG.export_yz_svg("L1", "blue", 10, 160, top_inner_exrusion_l1_line, mirror1);

                for (java.util.List<Point3d> ribline : buildRibProfileAtPoints(hull_MESH, P_outer_top_notched_line).values()) {
                    caprailSVG.export_yz_svg("L1", "red", 10, 160, ribline, java.util.Collections.singletonList(new Point3d(0, ribline.get(0).y, ribline.get(0).z)));
                }

                caprailSVG.export_yx_svg(tmp_MESH, "L1", 1.0 / 144.0 / 10, 10, 300, top_inner_exrusion_l1, mirror0);
                caprailSVG.export_yx_svg("L1", "blue", 10, 300, top_inner_exrusion_l1_line, mirror1);
                caprailSVG.close();
            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump caprail.svg", ex);
            }
        }
        triangulateMappedExtrusion(tmp_MESH, top_extrusion_l1, top_inner_exrusion_l1, false);

        // inner sidewall from L1 to bottom face 
        final java.util.List<Point3d> exrusion_line_l2 = getLowerExtrusion(top_inner_exrusion_l1_line, info.width / 2, maxCollisionDepth);
        final java.util.List<Vertex> extrusion_l2 = ModelUtils.addMeshPoints(tmp_MESH, exrusion_line_l2, new Vector3d(1, 1, 1));
        triangulateStripe(tmp_MESH, top_inner_exrusion_l1, extrusion_l2, false);

        // outer sidewall from top to L1
        final java.util.List<Vertex> outer_l1 = ModelUtils.addMeshPoints(tmp_MESH, P_outer_lower_notched_line1, new Vector3d(1, 1, 1));
        triangulateOuterHull(tmp_MESH, top_outer, outer_l1, true);

        // outer sidewall L1 to L2
        final java.util.List<Vertex> outer_l11 = ModelUtils.addMeshPoints(tmp_MESH, P_outer_lower_notched_line11, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> outer_l2 = ModelUtils.addMeshPoints(tmp_MESH, P_outer_lower_notched_line2, new Vector3d(1, 1, 1));
        triangulateOuterHull(tmp_MESH, outer_l11, outer_l2, true);

        // bottom face
        triangulateMappedExtrusion(tmp_MESH, outer_l2, extrusion_l2, true);
        for (java.util.List<Point3d> square : squares) {
            triangulateSquare(tmp_MESH, ModelUtils.addMeshPoints(tmp_MESH, square, new Vector3d(1, 1, 1)), false);
        }

        tmp_MESH.mirror(new Vector3d(-1, 1, 1));
        tmp_MESH.detectManifoldEdges();
        for (Vertex vert : tmp_MESH.verts) {
            if (vert.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                LOGGER.log(Level.SEVERE, "DETECTED Degenerate edges in the resulting model. V#{0}  {1} ", new Object[]{vert.index, vert.coord});
            }
        }
        return tmp_MESH.optimizeMesh();

    }

    private static Mesh makeDeckPlateMesh(String name, Mesh hull_MESH, ModelUtils.DeckInfo info, ModelUtils.KeelInfo keel, final int maxCollisionDepth) {

        final Mesh tmp_MESH = new Mesh(name);
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Looking for points on the manifold edges  X>=0.0");
        }
        // 1. Get p-outer-lop-line all the manifold edges with only 1 face reference) edges where X >= 0 ( we first work only with the one half.
        final java.util.List<Point3d> p_outer_top_line = getCaprailTopPoints(hull_MESH);
        final Point3d keelStopPoint = p_outer_top_line.get(p_outer_top_line.size() - 1);
        final double keelStop = keel.max_y;
        final Point3d keelStartPoint = p_outer_top_line.get(0);
        final double keelStart = keel.min_y;
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "Found {0} , points on manifoldedges  X>=0.0   min={1} , max={2} ", new Object[]{p_outer_top_line.size(), keelStopPoint, keelStartPoint});
            LOGGER.log(Level.INFO, "DeckPlate  {0}  and {1} ", new Object[]{keelStop, keelStart});
        }
        // Project a 2d extrusion at z=0 we will use this to build the the captrail by finding intersections with planes 
        final java.util.List<Point3d> p_deck_extrusion_line = getInnerExtrusion(p_outer_top_line, info.extrusion, maxCollisionDepth);
        final java.util.List<Point3d> p_deck_top_line = makeInnerExctrudedMap(p_outer_top_line, p_deck_extrusion_line, false);

        final java.util.List<Point3d> p_deck_bottom_line = getLowerExtrusion(p_deck_top_line, info.thickness, maxCollisionDepth);

        // DUMP The top Line
        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile caprailSVG = new SvgFile("deck.svg");
                java.util.List<Point3d> mirror0 = Extruder.mirrorOnY(p_deck_top_line);
                java.util.Collections.reverse(mirror0);
                final java.util.List<Point3d> mirror1 = Extruder.mirrorOnY(p_deck_bottom_line);
                java.util.Collections.reverse(mirror1);

                caprailSVG.export_yz_svg("TOP", "blue", 10, 160, p_deck_top_line, mirror0);
                caprailSVG.export_yz_svg("L1", "pink", 10, 160, p_deck_bottom_line, mirror1);

                caprailSVG.export_yx_svg("TOP", "blue", 10, 300, p_deck_top_line, mirror0);
                caprailSVG.export_yx_svg("L1", "pink", 10, 300, p_deck_bottom_line, mirror1);
//                caprailSVG.export_yz_svg("extr1", "red", 10, 320, p_inner_extrution_line1, mirror1);
//                caprailSVG.export_yz_svg("extr2", "green", 10, 320, p_inner_extrution_line2, mirror2);
                caprailSVG.close();
            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump caprail.svg", ex);
            }
        }

        // Top 
        final java.util.List<Vertex> p_top_outer = ModelUtils.addMeshPoints(tmp_MESH, p_deck_top_line, new Vector3d(1, 1, 1));
        LOGGER.log(Level.SEVERE, "TopVertexList {0} from {1} points ", new Object[]{p_top_outer.size(), p_deck_top_line.size()});
        final java.util.List<Vertex> n_top_outer = ModelUtils.addMeshPoints(tmp_MESH, Extruder.mirrorOnY(p_deck_top_line), new Vector3d(1, 1, 1));
        final java.util.List<Vertex> p_bottom_extrusion = ModelUtils.addMeshPoints(tmp_MESH, p_deck_bottom_line, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> n_bottom_extrusion = ModelUtils.addMeshPoints(tmp_MESH, Extruder.mirrorOnY(p_deck_bottom_line), new Vector3d(1, 1, 1));

        // pt0 to start of p_top_outer and n_top_outer
        Vertex pt0 = p_top_outer.get(0);
        Vertex pt1 = p_top_outer.get(p_top_outer.size() - 1);
        tmp_MESH.addEdge(pt0, p_top_outer.get(1));
        tmp_MESH.addEdge(pt0, n_top_outer.get(1));
        tmp_MESH.addEdge(p_top_outer.get(1), n_top_outer.get(1));
        tmp_MESH.addFace(pt0, p_top_outer.get(1), n_top_outer.get(1));
        // pt1 to end of p_top_outer and n_top_outer
        tmp_MESH.addEdge(pt1, p_top_outer.get(p_top_outer.size() - 2));
        tmp_MESH.addEdge(pt1, n_top_outer.get(n_top_outer.size() - 2));
        tmp_MESH.addEdge(p_top_outer.get(p_top_outer.size() - 2), n_top_outer.get(n_top_outer.size() - 2));
        tmp_MESH.addFace(pt1, n_top_outer.get(n_top_outer.size() - 2), p_top_outer.get(p_top_outer.size() - 2));

        // pt0 to start of p_bottom_extrusion and n_bottom_extrusion
        Vertex pl0 = p_bottom_extrusion.get(0);
        Vertex pl1 = p_bottom_extrusion.get(p_bottom_extrusion.size() - 1);
        tmp_MESH.addEdge(pl0, p_bottom_extrusion.get(1));
        tmp_MESH.addEdge(pl0, n_bottom_extrusion.get(1));
        tmp_MESH.addEdge(p_bottom_extrusion.get(1), n_bottom_extrusion.get(1));
        tmp_MESH.addFace(pl0, n_bottom_extrusion.get(1), p_bottom_extrusion.get(1));
        // pt1 to end of p_top_outer and n_bottom_extrusion
        tmp_MESH.addEdge(pl1, p_bottom_extrusion.get(p_bottom_extrusion.size() - 2));
        tmp_MESH.addEdge(pl1, n_bottom_extrusion.get(n_bottom_extrusion.size() - 2));
        tmp_MESH.addEdge(p_bottom_extrusion.get(p_bottom_extrusion.size() - 2), n_bottom_extrusion.get(n_bottom_extrusion.size() - 2));
        tmp_MESH.addFace(pl1, p_bottom_extrusion.get(p_bottom_extrusion.size() - 2), n_bottom_extrusion.get(n_bottom_extrusion.size() - 2));

        ModelUtils.buildMeshSurfaceAlongMirroredLinesAlongYAxis(tmp_MESH, p_top_outer.subList(1, p_top_outer.size() - 1), n_top_outer.subList(1, n_top_outer.size() - 1), true);
        ModelUtils.buildMeshSurfaceAlongMirroredLinesAlongYAxis(tmp_MESH, p_bottom_extrusion.subList(1, p_bottom_extrusion.size() - 1), n_bottom_extrusion.subList(1, n_bottom_extrusion.size() - 1), false);

        triangulateStripe(tmp_MESH, p_top_outer, p_bottom_extrusion, true);
        triangulateStripe(tmp_MESH, n_top_outer, n_bottom_extrusion, false);

        tmp_MESH.detectManifoldEdges();
        for (Vertex vert : tmp_MESH.verts) {
            if (vert.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                LOGGER.log(Level.SEVERE, "DETECTED Degenerate edges in the resulting model. V#{0}  {1} ", new Object[]{vert.index, vert.coord});
            }
        }
        return tmp_MESH.optimizeMesh();
    }

    private static void triangulateMappedExtrusion(final Mesh tmp_MESH, final java.util.List<Vertex> top_outer, final java.util.List<Vertex> top_extrusion_map, boolean reverse) {
        final PairedHorizontalKnucklePartitioner partitioner = new PairedHorizontalKnucklePartitioner();
        java.util.List<Partition> partitions = partitioner.partition(new Partition(top_outer, getYZXPoints(top_outer), top_extrusion_map, getYZXPoints(top_extrusion_map)));
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "triangulateMappedExtrusion  partitioned {0} ", new Object[]{partitions.size()});
        }
        for (Partition partition : partitions) {
            final java.util.List<Vertex> line2 = new java.util.ArrayList<>(partition.line2);
            Collections.reverse(line2);
            // Hack.. FIXME the extrusion should never have generated the same Coord±!!!
            if (line2.get(0).coord.distance(partition.line1.get(partition.line1.size() - 1).coord) < 0.0000001) {
                line2.remove(0);
            }
            if (isVertical(partition)) {
                TriangulateInXZPlane.triangulate(tmp_MESH, partition.line1, line2, isAscending(partition) ? reverse : !reverse, false);
            } else {
                TriangulateInXYPlane.triangulate(tmp_MESH, partition.line1, line2, reverse, false);
            }
        }
    }

    private static void triangulateOuterHull(final Mesh tmp_MESH, final java.util.List<Vertex> top_outer, final java.util.List<Vertex> lower_outer, boolean reverse) {
        Triangulation3d triangulator = new Triangulation3d();
        final OverlapPartitioner part0 = new OverlapPartitioner();
        final PairedVerticalKnucklePartitioner partitioner = new PairedVerticalKnucklePartitioner();
        java.util.List<Partition> partitions = partitioner.partition(part0.partition(new Partition(top_outer, getXYZPoints(top_outer), lower_outer, getXYZPoints(lower_outer))));
        if (LOGGER.isLoggable(Level.INFO)) {
            LOGGER.log(Level.INFO, "triangulateOuterHull  partitioned {0} ", new Object[]{partitions.size()});
        }
        for (Partition partition : partitions) {
            int startIdx = tmp_MESH.faces.size();
            triangulator.triangulate(tmp_MESH, new TriangulationPartition(partition), reverse);
            int stopIdx = tmp_MESH.faces.size();
            int flipped = 0;
            Vector3d lastNormal = tmp_MESH.calculateNormal(tmp_MESH.faces.get(startIdx));
            for (int i = startIdx + 1; i < stopIdx; i++) {
                Vector3d normal = tmp_MESH.calculateNormal(tmp_MESH.faces.get(i));
                if (lastNormal.dot(normal) < -0.9) {
                    flipped++;
                }
                lastNormal = normal;
            }
            if (flipped > 0) {
                LOGGER.log(Level.INFO, "triangulateOuterHull faces {0} flipped from {1} face", new Object[]{flipped, stopIdx - startIdx});
                if (flipped < (stopIdx - startIdx) / 2) {
                    lastNormal = tmp_MESH.calculateNormal(tmp_MESH.faces.get(startIdx));
                    for (int i = startIdx + 1; i < stopIdx; i++) {
                        Vector3d normal = tmp_MESH.calculateNormal(tmp_MESH.faces.get(i));
                        if (lastNormal.dot(normal) < -0.9) {
                            tmp_MESH.faces.get(i).flip();
                            normal = tmp_MESH.calculateNormal(tmp_MESH.faces.get(i));
                        }
                        lastNormal = normal;
                    }
                }
            }
        }
    }

    /**
     * Points must have 1:1 mapping
     *
     * @param tmp_MESH
     * @param vertexLine1
     * @param vertexLine2
     */
    private static void triangulateSquare(final Mesh tmp_MESH, final java.util.List<Vertex> square, boolean reverse) {

        if (square.size() != 4) {
            throw new IllegalArgumentException("triangulateSquare expected four vertexes");
        }

        // The two opposite two faces should always be in the same plane.
        final Vertex vtx1 = square.get(0);
        final Vertex vtx2 = square.get(3);
        final Vertex vtx1_2 = square.get(1);
        final Vertex vtx2_2 = square.get(2);
        tmp_MESH.addEdge(vtx1, vtx2);
        tmp_MESH.addEdge(vtx2, vtx1_2);
        tmp_MESH.addEdge(vtx1_2, vtx1);
        tmp_MESH.addEdge(vtx2, vtx2_2);
        tmp_MESH.addEdge(vtx2_2, vtx1_2);
        tmp_MESH.addEdge(vtx1_2, vtx2);
        if (reverse) {
            tmp_MESH.addFace(vtx1, vtx2, vtx1_2);
            tmp_MESH.addFace(vtx2, vtx2_2, vtx1_2);
        } else {
            tmp_MESH.addFace(vtx2, vtx1, vtx1_2);
            tmp_MESH.addFace(vtx2_2, vtx2, vtx1_2);
        }
    }

    /**
     * Points must have 1:1 mapping
     *
     * @param tmp_MESH
     * @param vertexLine1
     * @param vertexLine2
     */
    private static void triangulateStripe(final Mesh tmp_MESH, final java.util.List<Vertex> vertexLine1, final java.util.List<Vertex> vertexLine2, boolean reverse) {

        if (vertexLine1.size() != vertexLine2.size()) {
            throw new IllegalArgumentException("The two vertex lines should be mirror images , and thus should have the same size");
        }

        for (int i = 0; i < vertexLine1.size() - 1; i++) {
            // The two opposite two faces should always be in the same plane.
            final Vertex vtx1 = vertexLine1.get(i);
            final Vertex vtx2 = vertexLine2.get(i);
            final Vertex vtx1_2 = vertexLine1.get(i + 1);
            final Vertex vtx2_2 = vertexLine2.get(i + 1);
            tmp_MESH.addEdge(vtx1, vtx2);
            tmp_MESH.addEdge(vtx2, vtx1_2);
            tmp_MESH.addEdge(vtx1_2, vtx1);
            tmp_MESH.addEdge(vtx2, vtx2_2);
            tmp_MESH.addEdge(vtx2_2, vtx1_2);
            tmp_MESH.addEdge(vtx1_2, vtx2);
            if (reverse) {
                tmp_MESH.addFace(vtx1, vtx2, vtx1_2);
                tmp_MESH.addFace(vtx2, vtx2_2, vtx1_2);
            } else {
                tmp_MESH.addFace(vtx2, vtx1, vtx1_2);
                tmp_MESH.addFace(vtx2_2, vtx2, vtx1_2);
            }
        }
    }

    private static java.util.List<Point3d> mapToRibAtContour(final Mesh hull_MESH, final java.util.List<Point3d> contour) {
        final java.util.List<Point3d> lines = new java.util.ArrayList<>(contour.size());
        final ProximityMap<java.util.List<Point3d>> riblines = buildRibProfileAtPoints(hull_MESH, contour);
        for (final Point3d original : contour) {
            if (riblines.containsKey(original.y)) {
                final Plane ribAtCountourZ = Plane.planeXY(original.z, true);
                final Point3d p2l = ribAtCountourZ.getIntersection(riblines.get(original.y));
                if (p2l != null) {
                    lines.add(p2l);
                }
            }
        }
        // if the end points dont end on x==0 we get the line from the Keel intersection at the corresponding z..
        if ((lines.get(0).x != 0) || (lines.get(lines.size() - 1).x != 0)) {
            final double dy0 = lines.get(0).y - lines.get(lines.size() - 1).y;
            final java.util.List<Point3d> keelContour = KeelFactory.getKeelEdgesAtX(hull_MESH, 0);
            final double dy1 = keelContour.get(0).y - keelContour.get(keelContour.size() - 1).y;
            if (Math.signum(dy1) != Math.signum(dy0)) {
                java.util.Collections.reverse(keelContour);
            }
            if (lines.get(0).x != 0) {
                final Point3d original = lines.get(0);
                final Plane ribAtCountourZ = Plane.planeXY(original.z, true);
                final Point3d p2l = ribAtCountourZ.getIntersection(keelContour.subList(0, keelContour.size() / 2));
                if (p2l != null) {
                    lines.add(0, p2l);
                }
            }
            if (lines.get(lines.size() - 1).x != 0) {
                final Point3d original = lines.get(lines.size() - 1);
                final Plane ribAtCountourZ = Plane.planeXY(original.z, true);
                final Point3d p2l = ribAtCountourZ.getIntersection(keelContour.subList(keelContour.size() / 2, keelContour.size()));
                if (p2l != null) {
                    lines.add(p2l);
                }
            }
        }

        return lines;
    }

    private static ProximityMap<java.util.List<Point3d>> buildRibProfileAtPoints(final Mesh hull_MESH, final java.util.List<Point3d> contour) {
        final ProximityMap<java.util.List<Point3d>> riblines = new ProximityMap<>(10);
        for (int i = 1; i < contour.size() - 1; i++) {
            Point3d point = contour.get(i);
            if (!riblines.containsKey(point.y)) {
                try {
                    riblines.put(point.y, RibFactory.cutRibEdgesAtRibPosition(hull_MESH, riblines.rounded(point.y)));
                } catch (Exception e) {
                    LOGGER.log(Level.SEVERE, "Ribline for pos " + point.y + " failed , we will try some perturbed lines", e);
                }
            }
        }
        return riblines;
    }

    private static boolean isAscending(Partition partition) {
        double dz1 = 0;

        for (int i = 1; i < partition.line1p.size(); i++) {
            dz1 += (partition.line1p.get(i - 1).y - partition.line1p.get(i).y);
        }
        double dz2 = 0;
        for (int i = 1; i < partition.line2p.size(); i++) {
            dz2 += (partition.line2p.get(i - 1).y - partition.line2p.get(i).y);
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.SEVERE, "Partition {0}[{5}] , {1}[{6}]  has {2},{3} is ascending {4}", new Object[]{partition.line1p.get(0).y, partition.line2p.get(0).y, dz1, dz2, (dz1 < 0 && dz2 < 0), partition.line1p.size(), partition.line2p.size()});
        }

        return (dz1 < 0 && dz2 < 0);
    }

    private static boolean isVertical(Partition partition) {
        final Point3d[] minmax1 = ModelUtils.find_min_and_max_values(partition.line1p);
        final Point3d[] minmax2 = ModelUtils.find_min_and_max_values(partition.line2p);
        final double dz1 = minmax1[1].y - minmax1[0].y, dy1 = minmax1[1].x - minmax1[0].x;  // NOTE the Transform perfomred by getPoint()..
        final double dz2 = minmax2[1].y - minmax2[0].y, dy2 = minmax2[1].x - minmax2[0].x;
        return (Math.abs(dz1) > Math.abs(dy1)) && (Math.abs(dz2) > Math.abs(dy2));
    }

    private static java.util.List<Point3d> getXYZPoints(final java.util.List<Vertex> vlist) {

        final java.util.List<Point3d> extractPoints = new java.util.ArrayList<>(vlist.size());
        for (Vertex v : vlist) {
            extractPoints.add(new Point3d(v.coord.x, v.coord.y, v.coord.z));
        }
        return extractPoints;
    }

    private static java.util.List<Point3d> getYZXPoints(final java.util.List<Vertex> vlist) {

        final java.util.List<Point3d> extractPoints = new java.util.ArrayList<>(vlist.size());
        for (Vertex v : vlist) {
            extractPoints.add(new Point3d(v.coord.y, v.coord.z, v.coord.x));
        }
        return extractPoints;
    }

    private static java.util.List<Point3d> makeInnerExctrudedMap(final java.util.List<Point3d> outer_contour, final java.util.List<Point3d> inner_extrusion_line, boolean mapMissing) {
        // for each point in P_outer_top_notched_line (excluding the start and end) we get the intersection with the extrurion_lnbe 
//        final ProximityMap<Point3d> horizontalPoints = new ProximityMap<>(10);
        final java.util.List<Point3d> extruded_points = new java.util.ArrayList<>(outer_contour.size());
        final Point3d[] minmax = ModelUtils.find_min_and_max_values(inner_extrusion_line);
//        final java.util.Set<Integer> newverts = new java.util.HashSet<>();
        for (int i = 0; i < outer_contour.size(); i++) {
            final Point3d point = outer_contour.get(i);
            if ((point.y < minmax[0].y) || (point.y > minmax[1].y)) {
                if (mapMissing) {
                    extruded_points.add(null);// no corresponding point;
                }
            } else {
                final Plane plane = Plane.planeXZ(point.y, true);
                Point3d extrPoint = plane.getIntersection(inner_extrusion_line); // get the X coordinate
                extrPoint.y = point.y;
                extrPoint.z = point.z;
                extruded_points.add(extrPoint);
            }
        }
        final Plane plane0x = Plane.planeYZ(0, true);
        if (extruded_points.get(0).x != 0) {
            final Point3d newP = new Point3d(plane0x.intersectSegment(extruded_points.get(0), extruded_points.get(1), false));
            newP.x = 0;
            extruded_points.add(0, newP);
        }
        if (extruded_points.get(extruded_points.size() - 1).x != 0) {
            final Point3d newP = new Point3d(plane0x.intersectSegment(extruded_points.get(extruded_points.size() - 1), extruded_points.get(extruded_points.size() - 2), false));
            newP.x = 0;
            extruded_points.add(newP);
        }
        return Simplifier.simplifyPolygonLineSegments(extruded_points);
    }

    private static java.util.List<Point3d> getInnerExtrusion(final java.util.List<Point3d> deck_outer_top_line, final double plate_width, final int maxCollisionDepth) {
        Transform3dToXY2d transform = new Transform3dToXY2d(0);

        java.util.List<Point3d> extruded_inner_top_line = Extruder.extrude(deck_outer_top_line, transform, -plate_width, maxCollisionDepth);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Exctruded {0} points from {1} before notches\n", new Object[]{extruded_inner_top_line.size(), deck_outer_top_line.size()});
        }
        clipEndPoints(extruded_inner_top_line, deck_outer_top_line, transform);
        return Simplifier.simplifyPolygonLineSegments(extruded_inner_top_line);
    }

    /**
     * Extrude the given list down by projecting its normal in the YZ plane
     * only.
     *
     * @param line
     * @param plate_width
     * @param maxCollisionDepth
     * @return
     */
    private static java.util.List<Point3d> getLowerExtrusion(final java.util.List<Point3d> line, final double plate_width, final int maxCollisionDepth) {

        java.util.List<Vector2d> yznormals = new java.util.ArrayList<>(line.size());
        yznormals.add(normal2d(line.get(0), line.get(1)));
        for (int i = 1; i < line.size(); i++) {
            yznormals.add(normal2d(line.get(i - 1), line.get(i)));
        }

        java.util.List<Vector2d> avgnormals = new java.util.ArrayList<>(line.size());
        for (int i = 0; i < yznormals.size(); i++) {
            Vector2d curr = findNext(yznormals, i);
            Vector2d prev = findPrev(yznormals, i);
            if (curr.dot(prev) > 0.5) {
                avgnormals.add(avagrage(curr, prev));
            } else {
                avgnormals.add(add(curr, prev));
            }
        }

        java.util.List<Point3d> extruded = new java.util.ArrayList<>(line.size());
        for (int i = 0; i < line.size(); i++) {
            Point3d point = line.get(i);
            Vector2d avg = avgnormals.get(i);
            Point3d normal = new Point3d(0, avg.y, -avg.x);
            normal.scale(plate_width);
            normal.add(point);
            extruded.add(normal);
        }
        final Plane plane0x = Plane.planeYZ(0, true);
        if (extruded.get(0).x != 0) {
            final Point3d newP = new Point3d(plane0x.intersectSegment(extruded.get(0), extruded.get(1), false));
            newP.x = 0;
            extruded.add(0, newP);
        }
        if (extruded.get(extruded.size() - 1).x != 0) {
            final Point3d newP = new Point3d(plane0x.intersectSegment(extruded.get(extruded.size() - 1), extruded.get(extruded.size() - 2), false));
            newP.x = 0;
            extruded.add(newP);
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Exctruded {0} points from {1} before notches\n", new Object[]{extruded.size(), line.size()});
        }
        // Do not smooth out the line, we expect this to be a 'mirror' , for striping 
        //return Simplifier.simplifyPolygonLineSegments(extruded);
        return extruded;
    }

    private static Vector2d add(Vector2d a, Vector2d b) {
        return new Vector2d(a.x + b.x, a.y + b.y);
    }

    private static Vector2d avagrage(Vector2d a, Vector2d b) {
        final Vector2d sum = add(a, b);
        sum.normalize();
        return sum;
    }

    private static Vector2d findPrev(java.util.List<Vector2d> yznormals, int idx) {
        for (int i = idx; i > 0; i--) {
            Vector2d vec = yznormals.get(i);
            if ((vec.lengthSquared() > 0) && (vec.x > -.5)) {
                return vec;
            }
        }
        return yznormals.get(0);

    }

    private static Vector2d findNext(java.util.List<Vector2d> yznormals, int idx) {
        for (int i = idx + 1; i < yznormals.size(); i++) {
            Vector2d vec = yznormals.get(i);
            if ((vec.lengthSquared() > 0) && (vec.x > -.5)) {
                return vec;
            }
        }
        return yznormals.get(yznormals.size() - 1);

    }

    private static Vector2d normal2d(Point3d a, Point3d b) {
        final Vector2d vec = new Vector2d(b.y - a.y, b.z - a.z);
        if (vec.lengthSquared() > 0) {
            vec.normalize();
        } else {
            vec.x = 0;
            vec.y = 0;
        }
        return vec;
    }

    private static void clipEndPoints(java.util.List<Point3d> extruded_outer_top_line, final java.util.List<Point3d> bottom_line, Transform3dToXY2d transform) {
        EndPointBound start = new EndPointBound(Plane.planeYZ(0, true));
        EndPointBound end = new EndPointBound(Plane.planeYZ(0, true));
        Clipper.clipEndPoints(extruded_outer_top_line, start, end);
    }

    /**
     * Merge the new Points for deeper notches into the
     * P_outer_lower_notched_line1.
     *
     * @param P_outer_lower_notched_line1
     * @param rib_positions
     * @param rib_clone
     * @return
     */
    private static List<Point3d> getCutRibNotchs(List<Point3d> P_outer_lower_notched_line1, List<ModelUtils.RibPosition> rib_positions, List<ModelUtils.RibPosition> rib_clone, final java.util.List<java.util.List<Point3d>> squares) {

        final java.util.List<Point3d> result = new java.util.ArrayList<>(P_outer_lower_notched_line1);

        for (int i = 0; i < rib_positions.size(); i++) {
            try {
                int location = findRibNotchInsertPoint(result, rib_positions.get(i));

                Point3d p0 = new Point3d(result.get(location - 1));
                p0.x = rib_clone.get(i).deck_x;
                Point3d p1 = new Point3d(result.get(location));
                p1.x = rib_clone.get(i).deck_x;
                squares.add(Arrays.asList(result.get(location - 1), p0, p1, result.get(location))); // we need to make two faces over this square
                result.add(location, p0);
                result.add(location + 1, p1);
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.SEVERE, "Caprail failed to complete L2 layer at " + rib_positions.get(i).y + " try to remove the rib, or move it slightly , This can happend when a rib is located inside a step of the deck, move the rib just outside of it.", e);
            }
        }
        return result;
    }

    private static int findRibNotchInsertPoint(final java.util.List<Point3d> line, ModelUtils.RibPosition pos) {
        double gap_start = pos.y - pos.width / 2.0;
        double gap_stop = pos.y + pos.width / 2.0;
        final Plane plane0 = Plane.planeXZ(pos.y, true);

        Point2d point1 = new Point2d(pos.deck_x, gap_start);
        Point2d point2 = new Point2d(pos.deck_x, gap_stop);
        for (int i = 1; i < line.size(); i++) {
            Point3d p0 = line.get(i - 1);
            Point3d p1 = line.get(i);
            if (plane0.isIntersected(p0, p1, true) && near2d(p0, point1, point2) && near2d(p1, point1, point2)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Could not locate the rib insertion point for L2 contour y=:" + pos.y + " x=:" + pos.deck_x);
    }

    private static boolean near2d(Point3d point, Point2d point1, Point2d point2) {
        Point2d location = new Point2d(point.x, point.y);
        return location.distance(point1) < 0.000001 || location.distance(point2) < 0.000001;
    }

    /**
     *
     * @param topLine
     * @param keel_x_position
     * @param plate_width
     * @param plate_thickness
     * @param rib_positions
     * @param maxCollisionDepth
     * @return
     */
    private static java.util.List<Point3d> getCutRibNotchs(final java.util.List<Point3d> topLine, final java.util.List<ModelUtils.RibPosition> rib_positions, ModelUtils.KeelInfo keel) {
        final java.util.List<Point3d> topLine0 = new java.util.ArrayList<>(topLine.subList(0, topLine.size() / 2)); // we are not allowed to change the structure of sub lists, so we make complete copy..
        final java.util.List<Point3d> topLine1 = new java.util.ArrayList<>(topLine.subList(topLine.size() / 2, topLine.size()));// we are not allowed to change the structure of sub lists, so we make complete copy.. 
        final double notchDeth = keel.width / 2.0;
        final Plane kplane0 = Plane.planeYZ(-notchDeth, true);
        final Plane kplane1 = Plane.planeYZ(notchDeth, true);
        // Cut two Notches for the Deck to fit to the Keel
        {

            Point3d intersection1 = Plane.planeXZ(keel.max_y, true).getIntersection(topLine1);
            if (intersection1 != null) {
                java.util.Collections.reverse(topLine1);
                Extruder.cutNotch(topLine1, kplane0, kplane1, new Vector3d(0, keel.max_y, intersection1.z));
                java.util.Collections.reverse(topLine1);
            } else {
                // Do we ensure x=0 ?
            }
        }
        // Cut two Notches for the Deck to fit to the Keel
        {
            Point3d intersection1 = Plane.planeXZ(keel.min_y, true).getIntersection(topLine0);
            if (intersection1 != null) {
                Extruder.cutNotch(topLine0, kplane0, kplane1, new Vector3d(0, keel.min_y, intersection1.z));
            } else {
                // Do we ensure x=0 ?
            }
        }
        final java.util.List<Point3d> result = new java.util.ArrayList<>(topLine0.size() + topLine1.size());
        result.addAll(topLine0);
        result.addAll(topLine1);
        for (java.util.Iterator< ModelUtils.RibPosition> itre = rib_positions.iterator(); itre.hasNext();) {
            ModelUtils.RibPosition pos = itre.next();
            double gap_start = pos.y - pos.width / 2.0;
            double gap_stop = pos.y + pos.width / 2.0;

            final Point3d notchCenter = Plane.planeXZ(pos.y, true).getIntersection(result);
            final Plane plane0 = Plane.planeXZ(gap_start, true);
            final Point3d notchStart = plane0.getIntersection(result);
            final Plane plane1 = Plane.planeXZ(gap_stop, false).flip();
            final Point3d notchStop = plane1.getIntersection(result);
            if ((notchCenter != null) && (notchStart != null) && (notchStop != null)) {
                Extruder.cutNotch(result, plane0, plane1, new Vector3d(pos.deck_x, 0, 0));
            } else {
                if (LOGGER.isLoggable(Level.WARNING)) {
                    LOGGER.log(Level.WARNING, "Removing rib position {0}", new Object[]{pos.y});
                }
                itre.remove();
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Exctruded {0} points from {1} after notches", new Object[]{topLine.size(), result.size()});
        }

        // FIXME , get threshhold from paramater.
        return Simplifier.simplifyPolygonLineSegments(result);
    }

    private static java.util.List<Point3d> getCaprailTopPoints(final Mesh hull_MESH) {
        java.util.List<Edge> deckEdges = getHullTopEdges(hull_MESH);

        Vertex MinMaxVertex[] = hull_MESH.findMinAndMaxVertices(deckEdges, new Vector3d(0, 1, 0));
        java.util.List<Point3d> outer_deck_line = new java.util.ArrayList<>((deckEdges.size() * 3) / 2);
        for (Vertex v : hull_MESH.traverseEdges(deckEdges, MinMaxVertex[0], MinMaxVertex[1])) {
            outer_deck_line.add(new Point3d(v.coord.x, v.coord.y, v.coord.z));
        }
        // FIXME , get threshhold from paramater.
        return Simplifier.simplifyPolygonLineSegments(outer_deck_line);
    }

    private static java.util.List<Edge> getHullTopEdges(Mesh hull_MESH) {
        /**
         * Get all the Edges with both Vets.x >= 0 , && with a Face.coord.nt of
         * 1, ie. these are the non manifold edges , on the top of hull where
         * the deck attaches to the hull. NOTE we only select on side x >= 0 ,
         * we will mirror it after all the transforms etc.
         *
         */
        java.util.Map<Integer, java.util.Set<Integer>> edgeFaceMap = ModelUtils.get_edge_face_map(hull_MESH);
        java.util.List<Edge> deckEdges = new java.util.ArrayList<>();
        for (Edge edge : hull_MESH.edges) {
            if ((hull_MESH.verts.get(edge.verts[0]).coord.x >= 0) && (hull_MESH.verts.get(edge.verts[1]).coord.x >= 0)) {
                if (!edgeFaceMap.containsKey(edge.index)) {
                    throw new RuntimeException(String.format("Invalid edge.index %d in origin edge of original hull mesh", edge.index));
                }
                //# Only interested in edges that has only 1 face. ie the edges along the 'edge' of the hull 
                if (edgeFaceMap.get(edge.index).size() == 1) {
                    deckEdges.add(edge);
                }
            }
        }
        return deckEdges;
    }

}
