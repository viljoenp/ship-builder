/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Vector2d;
import org.rcwarships.model.decompose.AbstractVertex;
import org.rcwarships.model.decompose.Decomposer;
import org.rcwarships.model.decompose.Polygon;
import org.rcwarships.model.decompose.SweepLine;

/**
 *
 * @author viljoenp
 */
public final class TriangulateInYZPlane {

    private static final Logger LOGGER = Logger.getLogger(TriangulateInYZPlane.class.getName());

    public static void triangulate(final Mesh mesh, final java.util.List<Vertex> bottomEdgeLoop, final java.util.List<Vertex> topEdgeLoop, boolean reverse) {
        triangulate(mesh, bottomEdgeLoop, topEdgeLoop, reverse, true);
    }

    /**
     * vertexEdgeLoop (bottomEdgeLoop + topEdgeLoop.reverse) must be contiguous
     * loop of vertexes in the yz plane , (a closed edge loop) , This method is
     * not concerned with other data in the Mesh, We will try to make a
     * contiguous surface/patch by triangulation.
     *
     * @param mesh The mesh containing the Vertexes in vertexEdgeLoop (we will
     * add edges and faces to the mesh on completion)
     * @param bottomEdgeLoop bottom vertex edge loop of contiguous, non-
     * intersecting 3D polygon , from the smallest Y coordinate to the largest Y
     * coordinate ( stern to bow )
     * @param topEdgeLoop bottom vertex edge loop of contiguous, non-
     * intersecting 3D polygon , from the smallest Y coordinate to the largest Y
     * coordinate ( stern to bow )
     * @param reverse
     */
    public static void triangulate(final Mesh mesh, final java.util.List<Vertex> bottomEdgeLoop, final java.util.List<Vertex> topEdgeLoop, boolean reverse, boolean check) {
        if (bottomEdgeLoop.size() + topEdgeLoop.size() < 3) {
            throw new IllegalArgumentException("vertex loop must consist of 3 or more vertexes");
        }
        if (check) {
            checkPlaneCoordinateX(mesh, bottomEdgeLoop, topEdgeLoop);
        }
//        final java.util.Deque<Vertex> stack = new java.util.ArrayDeque<>();
        SweepLine sweepline = new SweepLine();
        java.util.List<Convex> decomposed = sweepline.decompose(makeList(mesh, bottomEdgeLoop, topEdgeLoop));
        for (Convex shape : decomposed) {
            if (shape instanceof Polygon) {
                Polygon polygon = (Polygon) shape;
                Decomposer.Vertex[] vertexList = polygon.getVertices();
                for (int i = 2; i < vertexList.length; i++) {
                    mesh.addEdge(((YZVertex) vertexList[0]).vertex, ((YZVertex) vertexList[i - 1]).vertex);
                    mesh.addEdge(((YZVertex) vertexList[i - 1]).vertex, ((YZVertex) vertexList[i]).vertex);
                    mesh.addEdge(((YZVertex) vertexList[i]).vertex, ((YZVertex) vertexList[0]).vertex);
                    if (reverse) {
                        mesh.addFace(((YZVertex) vertexList[i - 1]).vertex, ((YZVertex) vertexList[0]).vertex, ((YZVertex) vertexList[i]).vertex);
                    } else {
                        mesh.addFace(((YZVertex) vertexList[0]).vertex, ((YZVertex) vertexList[i - 1]).vertex, ((YZVertex) vertexList[i]).vertex);
                    }
                }
            } else {
                throw new RuntimeException("Dont know what to do with Convex of type " + shape.getClass());
            }
        }
//                tmp_MESH.addEdge(vtx2, tmp);
//                tmp_MESH.addEdge(tmp, vtx1);
//                tmp_MESH.addFace(vtx1, vtx2, tmp);
    }

    public static class YZVertex extends AbstractVertex {

        public final Vertex vertex;

        public YZVertex(final Vertex vertex) {
            this.vertex = vertex;
        }

        @Override
        public double getX() {
            return vertex.coord.z;
        }

        @Override
        public double getY() {
            return vertex.coord.y;
        }

        @Override
        public Vector2d getTuple2d() {
            return new Vector2d(vertex.coord.z, vertex.coord.y);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("YZVertex[Point=[").append(getX()).append(",").append(getY()).append(",").append(vertex.coord.x).append("]").append("|  Type=").append(this.type).append(" index=").append(vertex.index).append("]");
            return sb.toString();
        }
    }

    private static java.util.List<Decomposer.Vertex> makeList(Mesh mesh, java.util.List<Vertex>... points) {
        int size = 0;
        for (java.util.List<Vertex> list : points) {
            size += list.size();
        }
        final java.util.List<Decomposer.Vertex> list = new java.util.ArrayList<>(size);
        for (java.util.List<Vertex> pointList : points) {
            for (Vertex vert : pointList) {
                list.add(new YZVertex(vert));
            }
        }
        if (LOGGER.isLoggable(Level.FINEST)) {
            for (Decomposer.Vertex vert : list) {
                System.err.printf(" %4.6f , %4.6f  idx=%d\n", vert.getX(), vert.getY(), ((YZVertex) vert).vertex.index);
            }
            LOGGER.log(Level.INFO, "makeList size {0}", list.size());
        }
        return list;
    }

    /**
     * All the vertexes should have the same X coordinate ( or very close to the
     * same ) JAVA8 this should be doable with a lambda.
     *
     * @param mesh
     * @param vertexEdgeLoop
     */
    private static void checkPlaneCoordinateX(final Mesh mesh, final java.util.List<Vertex> bottomEdgeLoop, final java.util.List<Vertex> topEdgeLoop) {
        double planeC = bottomEdgeLoop.get(0).coord.x;
        for (int i = 1; i < bottomEdgeLoop.size(); i++) {
            Vertex tmpV = bottomEdgeLoop.get(i);
            if (Math.abs(planeC - tmpV.coord.x) > Mesh.MIN_VERTEX_PLANE_DISTANCE) {
                throw new IllegalArgumentException("bottom vertex number " + i + " #" + tmpV.index + " : (" + String.format("%9.5f,%9.5f,%9.5f", tmpV.coord.x, tmpV.coord.y, tmpV.coord.z) + ") deviates from the planeX : " + String.format("%9.5f", planeC));
            }
        }
        for (int i = 1; i < topEdgeLoop.size(); i++) {
            Vertex tmpV = topEdgeLoop.get(i);
            if (Math.abs(planeC - tmpV.coord.x) > Mesh.MIN_VERTEX_PLANE_DISTANCE) {
                throw new IllegalArgumentException("top vertex number " + i + " #" + tmpV.index + " : (" + String.format("%9.5f,%9.5f,%9.5f", tmpV.coord.x, tmpV.coord.y, tmpV.coord.z) + ") deviates from the planeX : " + String.format("%9.5f", planeC));
            }
        }
    }
}
