/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package org.rcwarships.model;

import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 * represents a Line segment in 3d space
 *
 * @author viljoenp
 * @param <F>
 */
public class Line<F extends Tuple3d> {

    private static final Logger LOGGER = Logger.getLogger(Line.class.getName());
    /**
     * Parametric equation for a line p0,p1 is p0 + (p1-p0), p0 + u
     */
    private final F p0;
    private final F p1;
    private final Vector3d u;
//    private final double s;

    public Line(final F p0, final F p1) {
        this.p0 = copy(p0);
        this.p1 = copy(p1);
        final Vector3d p1p0 = new Vector3d(p1);
        p1p0.sub(p0);
        this.u = p1p0;
//        this.s = p1p0.length();
//        if (s > Epsilon.E) {
//            p1p0.scale(1 / s);
//        }
    }

    private F copy(F point) {
        if (point instanceof Point3d) {
            return (F) new Point3d(point);
        } else if (point instanceof Vector3d) {
            return (F) (new Vector3d(point));
        } else {
            F newEl = (F) point.clone();
            newEl.set(point);
            return newEl;
        }
    }

    /**
     * To calculate the intersection of two lines we have to solve the equation
     * l1.p0 +s*l1.u = l2.p0 +t*l2.u ( we only need to use two of the dimensions
     * x,y) to solve the two variable s&t
     *
     * Solve the eqation by setting the x's and y's equal, leaving us with
     *
     * l1.p0.x +s*l1.u.x = l2.p0.x +t*l2.u.x
     *
     * l1.p0.y +s*l1.u.y = l2.p0.y +t*l2.u.y
     *
     * l1.p0.z +s*l1.u.z = l2.p0.z +t*l2.u.z
     *
     *
     * this leaves us with two equation and two variables s & t;
     *
     * we can further reduce the two equation to
     *
     * s*l1.u.x - t*l2.u.x = l2.p0.x - l1.p0.x
     *
     * s*l1.u.y - t*l2.u.y = l2.p0.y - l1.p0.y
     *
     * s*l1.u.z - t*l2.u.z = l2.p0.z - l1.p0.z
     *
     * to solve s we need to get the cooificient of t the same ( but negative )
     * in any combination of two of the three equations.
     *
     * case xy :
     *
     * s*l1.u.x*l2.u.y - t*l2.u.x*l2.u.y = (l2.p0.x - l1.p0.x)*l2.u.y
     *
     * -s*l1.u.y*l2.u.x + t*l2.u.y*l2.u.x = -(l2.p0.y - l1.p0.y)*l2.u.x
     *
     * this reduces to
     *
     * s*(l1.u.x*l2.u.y - l1.u.y*l2.u.x) = (l2.p0.x - l1.p0.x)*l2.u.y -(l2.p0.y
     * - l1.p0.y)*l2.u.x
     *
     * reduces to
     *
     * s = ((l2.p0.x - l1.p0.x)*l2.u.y -(l2.p0.y - l1.p0.y)*l2.u.x ) /
     * (l1.u.x*l2.u.y - l1.u.y*l2.u.x)
     *
     *
     * case yz :
     *
     * s*l1.u.y - t*l2.u.y = l2.p0.y - l1.p0.y
     *
     * s*l1.u.z - t*l2.u.z = l2.p0.z - l1.p0.z
     *
     * s*l1.u.y*l2.u.z - t*l2.u.y*l2.u.z = (l2.p0.y - l1.p0.y)*l2.u.z
     *
     * -s*l1.u.z*l2.u.y - t*l2.u.z*l2.u.y = (l2.p0.z - l1.p0.z)*l2.u.y
     *
     * reduces to
     *
     * s = ((l2.p0.y - l1.p0.y)*l2.u.y -(l2.p0.y - l1.p0.y)*l2.u.x ) /
     * (l1.u.x*l2.u.y - l1.u.y*l2.u.x)
     *
     * plug s into the original equation
     *
     * ( s*l1.u.x - t*l2.u.x = l2.p0.x - l1.p0.x ) to solve t
     *
     * s*l1.u.x - t*l2.u.x = l2.p0.x - l1.p0.x
     *
     * reduces to
     *
     * t*l2.u.x = -s*l1.u.x + l1.p0.x - l2.p0.x
     *
     * reduces to
     *
     * t = ( -s*l1.u.x + l1.p0.x - l2.p0.x ) / l2.u.x
     *
     * For a strict line intersections ie the intersection in l1 should be
     * between l1.p0 and l1.p1 and in l2 should be between l2.p0 and l2.p1
     *
     * s and t must respectively be greater than or equal to 0 and less than or
     * equal to 1 || ( 0 &lt;= s &lt;= 1 ) && ( 0 &lt;= t &lt;= 1 )
     *
     * @param line
     * @param strict treat the lines as a finite line segment , so we only want
     * the intersection if it is between this.p0,this.p1 and line.p0 , line.p1
     * @return true if this line intersects the given line
     */
    public boolean intersects(final Line line, final boolean strict) {
        return getIntersection(line, strict) != null;
    }

    public boolean intersects(final Tuple3d p0, final Tuple3d p1, final boolean strict) {
        return Line.this.intersects(new Line(p0, p1), strict);
    }

    /**
     * get the point where line intersects with this line
     *
     * @param line
     * @param strict treat the lines as a finite line segment , so we only want
     * the point if it is between this.p0,this.p1 and line.p0 , line.p1
     * @return the point where the two lines intersect or null if not
     * intersection was found
     */
    public Point3d getIntersection(final Line line, final boolean strict) {
        final Point3d p0d = new Point3d(line.p0);
        p0d.sub(this.p0);
        final double s = solveIntersectionS(line, p0d);
        if (Double.isNaN(s)) {
            // The line is parallel with this line
            return null;
        }
        final double t = solveIntersectionT(line, p0d, s);
        if (Double.isNaN(t)) {
            return null;
        }
        if ((strict) && ((t < 0) || (t > 1) || (s < 0) || (s > 1))) {
            return null;
        }
        final Point3d thisP = getParametricPoint(s);
        final Point3d lineP = line.getParametricPoint(t);

        if (thisP.distanceSquared(lineP) > Epsilon.E) {
            // Throw
            return null;
        }
        return thisP;
    }

    private double dotxy2d(final Tuple3d a, final Tuple3d b) {
        return a.x * b.y - a.y * b.x;
    }

    private double dotyz2d(final Tuple3d a, final Tuple3d b) {
        return a.y * b.z - a.z * b.y;
    }

    private double dotxz2d(final Tuple3d a, final Tuple3d b) {
        return a.x * b.z - a.z * b.x;
    }

    private double solveIntersectionS(final Line line, final Point3d p0d) {
        final double dotxy = dotxy2d(this.u, line.u);
        if (Math.abs(dotxy) < Epsilon.E) {
            final double dotxz = dotxz2d(this.u, line.u);
            if (Math.abs(dotxz) < Epsilon.E) {
                final double dotyz = dotyz2d(this.u, line.u);
                if (Math.abs(dotyz) < Epsilon.E) {
                    return Double.NaN;
                } else {
                    return dotyz2d(p0d, line.u) / dotyz;
                }
            } else {
                return dotxz2d(p0d, line.u) / dotxz;
            }
        } else {
            return dotxy2d(p0d, line.u) / dotxy;
        }
    }

    private double solveIntersectionT(final Line line, final Point3d p0d, final double s) {
        if (Math.abs(line.u.x) < Epsilon.E) { // Cannot use x substitution , we use y or z
            if (Math.abs(line.u.y) < Epsilon.E) {
                if (Math.abs(line.u.z) < Epsilon.E) {
                    return Double.NaN;
                } else {
                    // s*l1.u.z - t*l2.u.z = l2.p0.z - l1.p0.z ====>  t = -(((l2.p0.z - l1.p0.z ) - s*l1.u.z) / l2.u.z
                    return -(p0d.z - s * this.u.z) / line.u.z;
                }
            } else {
                // s*l1.u.y - t*l2.u.y = l2.p0.y - l1.p0.y ====>  t = -(((l2.p0.y - l1.p0.y ) - s*l1.u.y) / l2.u.y
                return -(p0d.y - s * this.u.y) / line.u.y;
            }
        } else {
            // s*l1.u.x - t*l2.u.x = l2.p0.x - l1.p0.x  ====>  t = -(((l2.p0.x - l1.p0.x ) - s*l1.u.x ) / l2.u.x
            return -(p0d.x - s * this.u.x) / line.u.x;
        }
    }

    /**
     * Apply s to the parametric equation to get a Point on the line
     *
     * @param s
     * @return
     */
    public Point3d getParametricPoint(final double s) {
        final Point3d p = new Point3d(u);
        p.scale(s);
        p.add(p0);
        return p;
    }

    @Override
    public String toString() {
        return String.format("(%.3f, %+.3f, %+.3f) + s(%.3f, %+.3f, %+.3f)", p0.x, p0.y, p0.z, u.x, u.y, u.z);
    }

    public F getP0() {
        return p0;
    }

    public F getP1() {
        return p1;
    }

}
