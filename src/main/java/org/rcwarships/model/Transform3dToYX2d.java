/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple2d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector2d;

/**
 * An implementation to convert the 3d coords of a Point3d to a Point2d in the
 * YZ Plane
 *
 * @author viljoenp
 */
public class Transform3dToYX2d implements Transform3dTo2d {

    double xPos;

    public Transform3dToYX2d() {
    }

    public Transform3dToYX2d(final double xPos) {
        this();
        this.xPos = xPos;
    }

    public double getxPos() {
        return xPos;
    }

    public void setxPos(final double xPos) {
        this.xPos = xPos;
    }

    @Override
    public double getX(final Tuple3d point) {
        return point.y;
    }

    @Override
    public double getY(final Tuple3d point) {
        return point.z;
    }

    @Override
    public Point2d get2dCoord(final Tuple3d point) {
        return new Point2d(point.y, point.z);
    }

    @Override
    public Vector2d getNormal(final Tuple3d point1, final Tuple3d point2) {
        final Vector2d normal = new Vector2d(getX(point1) - getX(point2), getY(point1) - getY(point2));
        if (normal.lengthSquared() > 0) {
            normal.normalize();
        } else {
            normal.x = 0;
            normal.y = 0;
        }
        return normal;
    }

    @Override
    public Point3d get3dCoord(final Tuple2d point) {
        return new Point3d(xPos, point.x, point.y);
    }

}
