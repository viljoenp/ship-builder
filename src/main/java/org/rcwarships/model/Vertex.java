/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 *
 * @author viljoenp
 */
public class Vertex {

    public enum Flag {

        MANIFOLD_EDGE(1);
        public final int bit;

        Flag(int bit) {
            this.bit = bit;
        }
    };

    public Vertex(Point3d _coord, Vector3d _normal) {
        this.coord = _coord;
        this.normal = _normal;
        // Never have -0 as a coordinate 
        if (coord.x == -0) {
            coord.x = 0;
        }
        if (coord.y == -0) {
            coord.y = 0;
        }
        if (coord.z == -0) {
            coord.z = 0;
        }
    }

    public Vertex(Point3d _coord) {
        this(_coord, new Vector3d(0, 0, 0));
    }

    public Vertex() {
        this(new Point3d(0, 0, 0), new Vector3d(0, 0, 0));
    }

    public boolean isOnX(double x_position) {
        return Math.abs(coord.x - x_position) < Epsilon.E;
//        return coord.x == x_position;
//        return Math.abs(Math.abs(coord.x) - x_position) < Mesh.MIN_VERTEX_PLANE_DISTANCE;
    }

    public boolean isOnY(double y_position) {
        return Math.abs(coord.y - y_position) < Epsilon.E;
//        return coord.y == y_position;
//        return Math.abs(Math.abs(coord.y) - y_position) < Mesh.MIN_VERTEX_PLANE_DISTANCE;
    }

    public boolean isOnZ(double z_position) {
        return Math.abs(coord.z - z_position) < Epsilon.E;
//        return coord.z == z_position;
//        return Math.abs(Math.abs(coord.z) - z_position) < Mesh.MIN_VERTEX_PLANE_DISTANCE;
    }

    public boolean isSet(Flag flag) {
        return (flags & flag.bit) != 0;
    }

    public void set(Flag flag) {
        flags |= flag.bit;
    }
    public int index = -1;
    private int flags = 0;

    public Integer[] getEdges() {
        return edge_keys.toArray(new Integer[edge_keys.size()]);
    }

    public Integer[] getFaces() {
        return face_keys.toArray(new Integer[face_keys.size()]);

    }
    public final java.util.Set<Integer> edge_keys = new ArraySet<>();
    public final java.util.Set<Integer> face_keys = new ArraySet<>();
    public final javax.vecmath.Point3d coord;
    public final javax.vecmath.Vector3d normal;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertex other = (Vertex) obj;
        return this.index == other.index;
    }

}
