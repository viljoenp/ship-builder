/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import org.rcwarships.loaders.svg.SvgFile;
import org.rcwarships.model.clipper.Clipper;
import org.rcwarships.model.clipper.EndPointBound;
import org.rcwarships.model.extruder.Extruder;
import org.rcwarships.model.simplifier.Simplifier;
import org.rcwarships.model.triangulation.Triangulation3d;

/**
 *
 * @author viljoenp
 */
public class RibFactory {

    private static final Logger LOGGER = Logger.getLogger(RibFactory.class.getName());
    private static boolean CUT_NOTCHES = true;

    static {
        if (System.getProperty("cut-notches") != null) {
            CUT_NOTCHES = Boolean.parseBoolean(System.getProperty("cut-notches"));
        }
    }

    /**
     * Static factory method to create a MESH that represents the a RIB at the
     * specific position, of the model, some assumptions:
     *
     * The Ship is centered along the y Axis , from stern to bow ( or vice a
     * versa )
     *
     * @param hull_MESH Mesh representing the entire HULL and ONLY the hull, it
     * should not include the DECK and super structure (usually created in
     * DelftShip),
     * @param plate_width the width the rib will be extruded ( up and inwards )
     * @param plate_thickness the width of the KEEL plate
     * @param rib_position Data for the placement and width of this rib. need to
     * be placed in the KEEL, we will cut a notch into the KEEL place so the rib
     * can fit like a puzzle piece.
     * @param maxCollisionDepth number of vertexes to backtrack in search of
     * collisions after the extrusion phase
     * @return A Mesh that represents the entire KEEL with rib notched cut into
     * it.
     */
    public static Mesh makeRib(Mesh hull_MESH, double plate_width, double plate_thickness, ModelUtils.RibPosition rib_position, final int maxCollisionDepth, boolean keelNotch) {
        return makeRibMeshesh(String.format("RIB-MESH-%.0f", rib_position.y), hull_MESH, plate_width, plate_thickness, rib_position, maxCollisionDepth, keelNotch);
    }

    /**
     * Static factory method to create a MESH that represents the a RIB at the
     * specific position, of the model, some assumptions:
     *
     * The Ship is centered along the y Axis , from stern to bow ( or vice a
     * versa )
     *
     * @param hull_MESH Mesh representing the entire HULL and ONLY the hull, it
     * should not include the DECK and super structure (usually created in
     * DelftShip),
     * @param plate_width the width the rib will be extruded ( up and inwards )
     * @param plate_thickness the width of the KEEL plate
     * @param rib_position Data for the placement and width of this rib. need to
     * be placed in the KEEL, we will cut a notch into the KEEL place so the rib
     * can fit like a puzzle piece.
     * @param maxCollisionDepth number of vertexes to backtrack in search of
     * collisions after the extrusion phase
     * @return A Mesh that represents the entire KEEL with rib notched cut into
     * it.
     */
    public static Mesh makeSolidRib(Mesh hull_MESH, double plate_width, double plate_thickness, ModelUtils.RibPosition rib_position, final int maxCollisionDepth) {
        return makeSolidRibMeshesh(String.format("RIB-MESH-%.0f", rib_position.y), hull_MESH, plate_width, plate_thickness, rib_position, maxCollisionDepth);
    }

    /**
     * To make the rib mesh.
     *
     * 1. bottom-line-1 : cut/draw a line across the hull at y = rib_position.y
     * - rib_position.width/2
     *
     * 2. bottom-line-2 : cut/draw a line across the hull at y = rib_position.y
     * + rib_position.width/2
     *
     * 3. top-line-1 : extrude bottom-line-1 upwards in the xz plane
     *
     * 4. top-line-2 : extrude bottom-line-2 upwards in the xz plane
     *
     * 5. cut keel notches into bottom-line-1
     *
     * 5.1 cut Stringer notches into bottom-line-1
     *
     * 6. cut keel notches into bottom-line-2
     *
     * 6.1 cut Stringer notches into bottom-line-2
     *
     * 7. cut deck notches into top-line-1
     *
     * 8. cut deck notches into top-line-2
     *
     * then form a solid by building faces ( right hand orientation ) from
     *
     * 9. triangulate bottom-line-1 and top-line-1 to for front surface
     *
     * 10. triangulate bottom-line-2 and top-line-2 to for rear surface
     *
     * 10. triangulate bottom-line-1 and bottom-line-1 to for outer (only half)
     * surface
     *
     * 11. triangulate top-line-1 and top-line-1 to for outer (other half)
     * surface
     *
     *
     * @param name
     * @param hull_MESH
     * @param plate_width
     * @param plate_thickness
     * @param rib_positions
     * @param keel_notch_heights
     * @return
     */
    private static Mesh makeRibMeshesh(String name, Mesh hull_MESH, double plate_width, double plate_thickness, ModelUtils.RibPosition rib_position, int maxCollisionDepth, boolean keelNotch) {

        final Mesh ribMesh = new Mesh(name);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Drawing rib at Y position = {0}", rib_position.y);
        }
        final java.util.List<Point3d> bottom_line_center = cutRibEdgesAtRibPosition(hull_MESH, rib_position.y);
        rib_position.deck_x = bottom_line_center.get(0).x - (rib_position.thickness * (3 / 4.0));
        rib_position.deck_z = bottom_line_center.get(0).z - rib_position.thickness / 2.0; // FIXME This should really be the Caprail thickness !!!

        // 1. bottom-line-1 : cut/draw a line across the hull at y = rib_position.y - rib_position.width/2
        final double line_1_y = rib_position.y - rib_position.width / 2;
        final java.util.List<Point3d> bottom_line_1 = cutRibEdgesAtRibPosition(hull_MESH, line_1_y);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} , points intersecting the Y = {1}", new Object[]{bottom_line_1.size(), String.format("%12.8f", line_1_y)});
        }

        // 2. bottom-line-2 : cut/draw a line across the hull at y = rib_position.y + rib_position.width/2
        final double line_2_y = rib_position.y + rib_position.width / 2;
        final java.util.List<Point3d> bottom_line_2 = cutRibEdgesAtRibPosition(hull_MESH, line_2_y);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} , points intersecting the Y = {1}", new Object[]{bottom_line_1.size(), String.format("%12.8f", line_2_y)});
        }

        // 3. top-line-1 : extrude bottom-line-1 upwards in the xz plane
        // 5. cut keel notches into bottom-line-1
        // 5.1 cut Stringer notches into bottom-line-1
        // 7. cut deck notches into top-line-1
        final java.util.List<Point3d> top_line_1 = getTopNotchedLine(bottom_line_1, plate_thickness / 2.f, plate_width, plate_thickness, rib_position, maxCollisionDepth, keelNotch);
        final double line_1_area = computeRemovedArea(top_line_1);
        final Tuple3d line_1_avg = ModelUtils.average(top_line_1);
        // 4. top-line-2 : extrude bottom-line-2 upwards in the xz plane
        // 6. cut keel notches into bottom-line-2
        // 6.1 cut Stringer notches into bottom-line-2
        // 8. cut deck notches into top-line-2
        final java.util.List<Point3d> top_line_2 = getTopNotchedLine(bottom_line_2, plate_thickness / 2.f, plate_width, plate_thickness, rib_position, maxCollisionDepth, keelNotch);
        final double line_2_area = computeRemovedArea(top_line_2);
        final Tuple3d line_2_avg = ModelUtils.average(top_line_2);

        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Found averages of extruded lines 1 -- {0}, {1}   2 -- {2}, {3}  for y={4}", new Object[]{line_1_avg, line_1_area, line_2_avg, line_2_area, rib_position.y});
        }

        // to make the extruded (inner sides) of the rib uniform , we will select the one side  that has the smallest area.
        if (line_1_area < line_2_area) {
            cloneLine(top_line_1, top_line_2, line_2_y);
        } else {
            cloneLine(top_line_2, top_line_1, line_1_y);
        }

        if (LOGGER.isLoggable(Level.FINEST)) {
            for (int i = top_line_1.size() - 1; i >= 0; i--) {
                LOGGER.log(Level.INFO, "tl1  v:{0}   {1}", new Object[]{i, top_line_1.get(i)});
            }
            for (int i = 0; i < bottom_line_1.size(); i++) {
                LOGGER.log(Level.INFO, "bl1  v:{0}   {1}", new Object[]{i, bottom_line_1.get(i)});
            }
            LOGGER.log(Level.INFO, "      -------");
            for (int i = top_line_2.size() - 1; i >= 0; i--) {
                LOGGER.log(Level.INFO, "tl2  v:{0}   {1}", new Object[]{i, top_line_2.get(i)});
            }
            for (int i = 0; i < bottom_line_2.size(); i++) {
                LOGGER.log(Level.INFO, "bl2  v:{0}   {1}", new Object[]{i, bottom_line_2.get(i)});
            }

        }

// DUMP line1
        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile line1 = new SvgFile(name + ".svg");
                java.util.List<Point3d> vertexLine1 = new java.util.ArrayList<>(top_line_1);
                java.util.Collections.reverse(vertexLine1);
                line1.export_xz_svg(name + "-1-" + (vertexLine1.size() + bottom_line_1.size()), "blue", 120, 160, bottom_line_1, vertexLine1);
                java.util.List<Point3d> vertexLine2 = new java.util.ArrayList<>(top_line_2);
                java.util.Collections.reverse(vertexLine2);
                line1.export_xz_svg(name + "-2", "green", 120, 160, bottom_line_2, vertexLine2);
                line1.close();

            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump " + name + ".svg", ex);
            }
        }

        final java.util.List<Vertex> bottom1 = ModelUtils.addMeshPoints(ribMesh, bottom_line_1, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> top1 = ModelUtils.addMeshPoints(ribMesh, top_line_1, new Vector3d(1, 1, 1));

        final java.util.List<Vertex> bottom2 = ModelUtils.addMeshPoints(ribMesh, bottom_line_2, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> top2 = ModelUtils.addMeshPoints(ribMesh, top_line_2, new Vector3d(1, 1, 1));
        // 7. triangulate outer-bottom-line and outer-top-line to for side surface
        ModelUtils.buildMeshSurfaceAlongXAxis(ribMesh, bottom1, top1, false);

        // 8. triangulate mirrored-bottom-line and mirrored-top-line to for opposite side surface
        ModelUtils.buildMeshSurfaceAlongXAxis(ribMesh, bottom2, top2, true);

        ModelUtils.buildMeshSurfaceAlongMirroredLinesAlongXAxis(ribMesh, top1, top2, false);

        // Close the TOP hole by making a square  top1 , bottom1 ,bottom2 , top2
        ribMesh.addEdge(top1.get(0), bottom2.get(0));
        ribMesh.addEdge(top1.get(0), bottom1.get(0));
        ribMesh.addEdge(top2.get(0), bottom2.get(0));
        ribMesh.addEdge(bottom1.get(0), bottom2.get(0));
        ribMesh.addEdge(top1.get(0), top2.get(0));
        ribMesh.addFace(top1.get(0), bottom1.get(0), bottom2.get(0));
        ribMesh.addFace(top1.get(0), bottom2.get(0), top2.get(0));

        Triangulation3d triangulator = new Triangulation3d();
        triangulator.triangulate(ribMesh, bottom1, bottom2, true);
        ribMesh.mirror(new Vector3d(-1, 1, 1));
        Mesh result = ribMesh.optimizeMesh();
        if (result.validateSolid()) {
            return result;
        } else {
            return ribMesh;
        }
    }

    /**
     * To make the rib mesh.
     *
     * 1. bottom-line-1 : cut/draw a line across the hull at y = rib_position.y
     * - rib_position.width/2
     *
     * 2. bottom-line-2 : cut/draw a line across the hull at y = rib_position.y
     * + rib_position.width/2
     *
     * then form a solid by building faces ( right hand orientation ) from
     *
     * 9. triangulate bottom-line-1 and top-line-1 to for front surface
     *
     * 10. triangulate bottom-line-2 and top-line-2 to for rear surface
     *
     * 10. triangulate bottom-line-1 and bottom-line-1 to for outer (only half)
     * surface
     *
     * 11. triangulate top-line-1 and top-line-1 to for outer (other half)
     * surface
     *
     *
     * @param name
     * @param hull_MESH
     * @param plate_width
     * @param plate_thickness
     * @param rib_positions
     * @param keel_notch_heights
     * @return
     */
    private static Mesh makeSolidRibMeshesh(String name, Mesh hull_MESH, double plate_width, double plate_thickness, ModelUtils.RibPosition rib_position, int maxCollisionDepth) {

        final Mesh ribMesh = new Mesh(name);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Drawing rib at Y position = {0}", rib_position.y);
        }
        final java.util.List<Point3d> bottom_line_center = cutRibEdgesAtRibPosition(hull_MESH, rib_position.y);
        rib_position.deck_x = bottom_line_center.get(0).x - (rib_position.thickness * (2 / 3.0));
        rib_position.deck_z = bottom_line_center.get(0).z - rib_position.thickness / 2.0; // FIXME This should really be the Caprail thickness !!!

        // 1. bottom-line-1 : cut/draw a line across the hull at y = rib_position.y - rib_position.width/2
        final double line_1_y = rib_position.y - rib_position.width / 2;
        final java.util.List<Point3d> bottom_line_1 = cutRibEdgesAtRibPosition(hull_MESH, line_1_y);
        final java.util.List<Point3d> top_line_1 = java.util.Collections.singletonList(new Point3d(0, line_1_y, bottom_line_1.get(0).z));
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} , points intersecting the Y = {1}", new Object[]{bottom_line_1.size(), String.format("%12.8f", line_1_y)});
        }

        // 2. bottom-line-2 : cut/draw a line across the hull at y = rib_position.y + rib_position.width/2
        final double line_2_y = rib_position.y + rib_position.width / 2;
        final java.util.List<Point3d> bottom_line_2 = cutRibEdgesAtRibPosition(hull_MESH, line_2_y);
        final java.util.List<Point3d> top_line_2 = java.util.Collections.singletonList(new Point3d(0, line_2_y, bottom_line_2.get(0).z));
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} , points intersecting the Y = {1}", new Object[]{bottom_line_1.size(), String.format("%12.8f", line_2_y)});
        }

// DUMP line1
        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile line1 = new SvgFile(name + "-solid.svg");
                java.util.List<Point3d> vertexLine1 = new java.util.ArrayList<>(top_line_1);
                java.util.Collections.reverse(vertexLine1);
                line1.export_xz_svg(name + "-1", "blue", 120, 160, bottom_line_1, vertexLine1);
                java.util.List<Point3d> vertexLine2 = new java.util.ArrayList<>(top_line_2);
                java.util.Collections.reverse(vertexLine2);
                line1.export_xz_svg(name + "-2", "green", 120, 160, bottom_line_2, vertexLine2);
                line1.close();
            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump " + name + "-solid.svg", ex);
            }
        }

        final java.util.List<Vertex> bottom1 = ModelUtils.addMeshPoints(ribMesh, bottom_line_1, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> top1 = ModelUtils.addMeshPoints(ribMesh, top_line_1, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> bottom2 = ModelUtils.addMeshPoints(ribMesh, bottom_line_2, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> top2 = ModelUtils.addMeshPoints(ribMesh, top_line_2, new Vector3d(1, 1, 1));
        // 7. triangulate outer-bottom-line and outer-top-line to for side surface
        ModelUtils.buildMeshSurfaceAlongXAxis(ribMesh, bottom1, top1, false);

        // 8. triangulate mirrored-bottom-line and mirrored-top-line to for opposite side surface
        ModelUtils.buildMeshSurfaceAlongXAxis(ribMesh, bottom2, top2, true);

        ribMesh.addEdge(top1.get(0), bottom2.get(0));
        ribMesh.addEdge(top1.get(0), top2.get(0));
        ribMesh.addEdge(bottom1.get(0), bottom2.get(0));
        ribMesh.addFace(top1.get(0), bottom2.get(0), top2.get(0));
        ribMesh.addFace(top1.get(0), bottom1.get(0), bottom2.get(0));
        Triangulation3d triangulator = new Triangulation3d();
        triangulator.triangulate(ribMesh, bottom1, bottom2, true);
        ribMesh.mirror(new Vector3d(-1, 1, 1));
//        return ribMesh.optimizeMesh();
        Mesh result = ribMesh.optimizeMesh();
        if (result.validateSolid()) {
            return result;
        } else {
            return ribMesh;
        }
    }

    private static void cloneLine(final java.util.List<Point3d> src, final java.util.List<Point3d> dst, double y) {
        double firstZ = dst.get(0).z;
        dst.clear();
        for (Point3d p : src) {
            dst.add(new Point3d(p.x, y, p.z));
        }
        dst.get(0).z = firstZ;
    }

    private static double computeRemovedArea(java.util.List<Point3d> extruded_outer_top_line) {
        double area = 0;
        final Point3d topLeftCorner = new Point3d(0, extruded_outer_top_line.get(0).y, extruded_outer_top_line.get(0).z);
        area += (topLeftCorner.x + extruded_outer_top_line.get(0).x) * (topLeftCorner.z - extruded_outer_top_line.get(0).z);
        for (int i = 0; i < extruded_outer_top_line.size() - 1; i++) {
            final Point3d p1 = extruded_outer_top_line.get(i);
            final Point3d p2 = extruded_outer_top_line.get(i + 1);
            area += (p1.x + p2.x) * (p1.z - p2.z);
        }
        area *= 0.5;
        return area;
    }

    private static void clipEndPoints(java.util.List<Point3d> extruded_outer_top_line, final java.util.List<Point3d> bottom_line, Transform3dToXZ2d transform, final ModelUtils.RibPosition rib_position) {
        EndPointBound start = new EndPointBound(Plane.planeYZ(rib_position.deck_x, false), Plane.planeXY(rib_position.deck_z, false));
        // Note we start to provide for the Deck notch here !!
        EndPointBound end = new EndPointBound(Plane.planeYZ(0, true));
        Clipper.clipEndPoints(extruded_outer_top_line, start, end);
    }

    private static void cutKeelNotch(final java.util.List<Point3d> bottom_line, Transform3dToXZ2d transform, final ModelUtils.RibPosition rib_position, final double plate_thickness) {
        EndPointBound start = new EndPointBound();
        EndPointBound end = new EndPointBound(Plane.planeYZ(plate_thickness / 2.0, true));
        Clipper.clipEndPoints(bottom_line, start, end);
        bottom_line.add(new Point3d(plate_thickness / 2.0, bottom_line.get(0).y, rib_position.keel_z));
        bottom_line.add(new Point3d(0, bottom_line.get(0).y, rib_position.keel_z));

    }

    private static java.util.List<Point3d> getTopNotchedLine(final java.util.List<Point3d> bottom_line, final double keel_x_position, final double plate_width, final double plate_thickness, final ModelUtils.RibPosition rib_position, final int maxCollisionDepth, boolean keelNotch) {
        final Transform3dToXZ2d transform = new Transform3dToXZ2d(bottom_line.get(0).y);

        java.util.List<Point3d> extruded_outer_top_line = Extruder.extrude(bottom_line, transform, plate_width, maxCollisionDepth);
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Exctruded {0} points from {1} before notches\n", new Object[]{extruded_outer_top_line.size(), bottom_line.size()});
        }
        clipEndPoints(extruded_outer_top_line, bottom_line, transform, rib_position);

        if (keelNotch && CUT_NOTCHES) {
            cutKeelNotch(bottom_line, transform, rib_position, plate_thickness);
        }

        // cutDeckNotch
        if (CUT_NOTCHES) {
            extruded_outer_top_line.add(0, new Point3d(rib_position.deck_x, transform.getyPos(), bottom_line.get(0).z));
            extruded_outer_top_line.add(1, new Point3d(rib_position.deck_x, transform.getyPos(), rib_position.deck_z));
        }
        // avoid vertical lines at x==0
        Plane plane = Plane.planeYZ(0, true);
        for (java.util.ListIterator<? extends Tuple3d> iter = extruded_outer_top_line.listIterator(); iter.hasNext();) {

            final Tuple3d cur = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d next = iter.next();
            iter.previous();

            if (plane.isCoPlanar(cur, next)) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.INFO, "REMOVING Coplanar edge vertex {0}", new Object[]{next});
                }
                iter.remove();
                iter.previous(); // Start again at the last CoPlanar Point
            }
        }
        // remove Vetexes with X < 0
        for (Point3d point : extruded_outer_top_line) {
            if (point.x < 0.000005) {
                point.x = 0.0;
            }
        }

        return Simplifier.simplifyPolygonLineSegments(extruded_outer_top_line);
    }

    /**
     * get the vertices for the Rib section from the hull_MESH at rib position.
     * 1. Get rib-faces: the list of faces that cover the area of the rib from
     * the hull.
     *
     * 2. Get highest-face : face with the highest vertex.
     *
     * 3. Get highest-edge : edge with the highest vertex from the highest-face
     * , this should also be a manifold edge.
     *
     * 4. Traverse the rib-faces from the highest-face to the next face
     * selecting the other edge that crossed the rib-position. Any given face
     * can have at most two points that cross the rib position, && at the least
     * it only has one point (this would not be often) && this means that we can
     * ignore the face , since bordering && sharing the same point must be
     * another face that will cross the rib
     *
     */
    public static java.util.List<Point3d> cutRibEdgesAtRibPosition(final Mesh hull_MESH, final double rib_y_position) {
        Plane plane = Plane.planeXZ(rib_y_position, true);

        // Accumalate the points for the rib , by calculating the intersection of the edge with the rib_y_position
        final java.util.List<Point3d> ribPoints = ModelTracer.traceProfileLine(hull_MESH, plane, new ModelTracer.StartSelector() {

            @Override
            public Face selectStartFace(Mesh hull_MESH, List<Face> faces, Plane plane) {
                return getHighestFace(hull_MESH, faces, plane);
            }

            @Override
            public Edge selectStartEdge(Mesh hull_MESH, Face startFace, Plane plane) {
                return getHighestEdge(hull_MESH, startFace, plane);
            }

            @Override
            public Point3d selectStartPoint(Mesh hull_MESH, Edge startEdge, Plane plane) {
                return getHighestPoint(hull_MESH, startEdge, plane);
            }
        });
        // We only want the area where x > 0
//        final Transform3dToXZ2d transform = new Transform3dToXZ2d(ribPoints.get(0).y);
        EndPointBound start = new EndPointBound();// Empty set , no Clipping
        EndPointBound end = new EndPointBound(Plane.planeYZ(0, true));
        Clipper.clipEndPoints(ribPoints, start, end);
//        if (Math.abs(ribPoints.get(ribPoints.size() - 1).x) > 0.00001) {
// FIXME If we dont end at x=0 ?? 
//        }
        return Simplifier.simplifyPolygonLineSegments(ribPoints);
    }

    /**
     * Get The face with the highest Z coord , from the given MeshFaceList ,
     * NOTE, we only want the face with the highest edge crossing rib_position
     * on the manifold edge.
     *
     */
    private static Face getHighestFace(Mesh hull_MESH, java.util.List<Face> old_rib_face_list, Plane plane) {
        Face high_face = old_rib_face_list.get(0);
        Face manifoldFace = null;
        double manifold_edge_z = -Double.MAX_VALUE;
        double high_edge_z = -Double.MAX_VALUE;
        for (Face face : old_rib_face_list) {
            Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
            Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
            if (plane.isIntersected(vert_1.coord, vert_2.coord, true) && (vert_1.coord.x >= 0) && (vert_2.coord.x >= 0)) {
                if (vert_1.coord.z > high_edge_z) {
                    high_face = face;
                    high_edge_z = vert_1.coord.z;
                }
                if (vert_2.coord.z > high_edge_z) {
                    high_face = face;
                    high_edge_z = vert_2.coord.z;
                }
            }

            vert_1 = hull_MESH.verts.get(face.verts[1]);
            vert_2 = hull_MESH.verts.get(face.verts[2]);
            if (plane.isIntersected(vert_1.coord, vert_2.coord, true) && (vert_1.coord.x >= 0) && (vert_2.coord.x >= 0)) {
                if (vert_1.coord.z > high_edge_z) {
                    high_face = face;
                    high_edge_z = vert_1.coord.z;
                }
                if (vert_2.coord.z > high_edge_z) {
                    high_face = face;
                    high_edge_z = vert_2.coord.z;
                }
            }
            vert_1 = hull_MESH.verts.get(face.verts[2]);
            vert_2 = hull_MESH.verts.get(face.verts[0]);
            if (plane.isIntersected(vert_1.coord, vert_2.coord, true) && (vert_1.coord.x >= 0) && (vert_2.coord.x >= 0)) {
                if (vert_1.coord.z > high_edge_z) {
                    high_face = face;
                    high_edge_z = vert_1.coord.z;
                }
                if (vert_2.coord.z > high_edge_z) {
                    high_face = face;
                    high_edge_z = vert_2.coord.z;
                }
            }
            for (int edgeId : face.edge_keys) {
                Edge edge = hull_MESH.edges.get(edgeId);
                vert_1 = hull_MESH.verts.get(edge.verts[0]);
                vert_2 = hull_MESH.verts.get(edge.verts[1]);
                if (edge.isManifoldEdge() && plane.isIntersected(vert_1.coord, vert_2.coord, true) && (vert_1.coord.x >= 0) && (vert_2.coord.x >= 0)) {
                    if (vert_1.coord.z > manifold_edge_z) {
                        manifoldFace = face;
                        manifold_edge_z = vert_1.coord.z;
                    }
                    if (vert_2.coord.z > manifold_edge_z) {
                        manifoldFace = face;
                        manifold_edge_z = vert_2.coord.z;
                    }
                }

            }
        }
        if (manifoldFace != null) {
            return manifoldFace;
        }
        return high_face;
    }

    private static Point3d getHighestPoint(final Mesh hull_MESH, final Edge edge, Plane plane) {
        Plane.EdgeIntersectType type = ModelTracer.getIntersectType(hull_MESH, edge, plane);
        switch (type) {
            case COPLANAR_EDGE:
                final Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
                final Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
                if (vert_1.coord.z > vert_2.coord.z) {
                    return new Point3d(vert_1.coord);
                } else {
                    return new Point3d(vert_2.coord);
                }
            case COPLANAR_VERTEX:
                return new Point3d(ModelTracer.selectCoPlanarVertex(hull_MESH, edge, plane).coord);
            case INTERSECT:
                return new Point3d(ModelTracer.getIntersection(hull_MESH, edge, plane));
            default:
                throw new IllegalArgumentException("Edge #" + edge.index + " does not intercect plane " + plane);
        }
    }

    private static Edge getHighestEdge(final Mesh hull_MESH, final Face face, Plane plane) {
        /**
         * Get The edge with the highest Z coord , from the given face , NOTE,
         * we only want the highest edge crossing rib_position
         *
         */
        //# note we start with invalid edge index
        int highestEdge = -1;
        double high_edge_z = -Double.MAX_VALUE;
        for (int edgeIdx : face.edge_keys) {
            final Edge edge = hull_MESH.edges.get(edgeIdx);
            final Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
            final Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
            if ((plane.isIntersected(vert_1.coord, vert_2.coord, true) && (vert_1.coord.x >= 0) && (vert_2.coord.x >= 0))) {
                final Tuple3d ints = plane.intersectSegment(vert_1.coord, vert_2.coord, true);
                if (high_edge_z < ints.z) {
                    highestEdge = edge.index;
                    high_edge_z = ints.z;
                }
            }
        }
        if (highestEdge < 0) {
            System.err.printf("FACE %d ribPosition:%s  :  %s\n", face.index, plane, face.edge_keys);
            throw new RuntimeException(String.format("Could not get highest edge of face %d", face.index));
        }
        return hull_MESH.edges.get(highestEdge);
    }

}
