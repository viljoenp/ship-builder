/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import org.rcwarships.model.decompose.Shape;

/**
 *
 * @author viljoenp
 */
public interface Convex extends Shape {
//
//    /**
//     * Returns an array of separating axes to test for this {@link Shape}.
//     * <p>
//     * Given some points, this method will return the separating axes for this
//     * {@link Shape}'s voronoi regions also. The voronoi axes will not be
//     * included if the foci array is null.
//     * <p>
//     * The points in the foci array are assumed to be in world space.
//     * <p>
//     * The returned axes are normalized.
//     *
//     * @param foci the world space points representing foci of curved
//     * {@link Shape}s; can be null
//     * @param transform the local to world space {@link Transform} of this
//     * {@link Convex} {@link Shape}
//     * @return {@link Tuple2d}[]
//     */
//    public abstract Tuple2d[] getAxes(Tuple2d[] foci, Transform transform);
//
//    /**
//     * Returns an array of world space foci points for circular curved edges.
//     * <p>
//     * This method returns null if the {@link Shape} has zero curved edges.
//     *
//     * @param transform the local to world space {@link Transform} of this
//     * {@link Convex} {@link Shape}
//     * @return {@link Tuple2d}[]
//     */
//    public abstract Tuple2d[] getFoci(Transform transform);
//
//    /**
//     * Returns the feature farthest in the direction of n.
//     *
//     * @param n the direction
//     * @param transform the local to world space {@link Transform} of this
//     * {@link Convex} {@link Shape}
//     * @return {@link Feature}
//     */
//    public abstract Feature getFarthestFeature(Tuple2d n, Transform transform);
//
//    /**
//     * Returns the point farthest in the direction of n. If two points are
//     * equally distant along the given {@link Tuple2d} the first one in the
//     * vertex list is used.
//     * <p>
//     * The returned point is in world space.
//     *
//     * @param n the direction
//     * @param transform the local to world space {@link Transform} of this
//     * {@link Convex} {@link Shape}
//     * @return {@link Tuple2d}
//     */
//    public abstract Tuple2d getFarthestPoint(Tuple2d n, Transform transform);
}
