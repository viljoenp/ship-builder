/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import javax.vecmath.Point3f;

/**
 *
 * @author viljoenp
 */
public class Face {

    public Face(int v1_index, int v2_index, int v3_index, Point3f _normal) {
        this.verts[0] = v1_index;
        this.verts[1] = v2_index;
        this.verts[2] = v3_index;
        this.normal = _normal;
    }

    public Face(int v1_index, int v2_index, int v3_index) {
        this(v1_index, v2_index, v3_index, new Point3f(0, 0, 0));
    }

    public Face(Vertex v1, Vertex v2, Vertex v3) {
        this(v1.index, v2.index, v3.index);
    }

    public Face() {
        this(-1, -1, -1);
    }

    public Integer[] getEdges() {
        return edge_keys.toArray(new Integer[3]);
    }

    public java.util.List<Integer> getSharedEdges(final Vertex v1) {
        final java.util.List<Integer> shared = new java.util.ArrayList<>(2);
        for (int eIdx : this.edge_keys) {
            if (v1.edge_keys.contains(eIdx)) {
                shared.add(eIdx);
            }
        }
        return shared;
    }

    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder();
        buf.append("F(").append(index).append(",  v(").append(verts[0]).append(",").append(verts[1]).append(",").append(verts[2]).append(")  e:(").append(edge_keys).append("))");
        return buf.toString();
    }

    public void flip() {
        int v2 = verts[2];
        verts[2] = verts[1];
        verts[1] = v2;
        normal.scale(-1);
    }
    public final int verts[] = new int[]{-1, -1, -1};
    public int index = -1;
    public final javax.vecmath.Point3f normal;
    public final java.util.Set<Integer> edge_keys = new ArraySet<>();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Face other = (Face) obj;
        return this.index == other.index;
    }

}
