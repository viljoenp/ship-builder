/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.utils;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author viljoenp
 * @param <T>
 */
public class ProximityMap<T> implements java.util.Map<Double, T> {

    final java.util.HashMap<Long, T> proximity;

    final double multiplier;

    public ProximityMap(final double multiplier) {
        this.multiplier = multiplier;
        this.proximity = new java.util.HashMap<>();
    }

    @Override
    public T get(Object key) {
        return proximity.get(convertKey(key)); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T put(Double key, T value) {
        return proximity.put(convertKey(key), value); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void putAll(Map<? extends Double, ? extends T> m) {
        for (Map.Entry<? extends Double, ? extends T> entry : m.entrySet()) {
            proximity.put(convertKey(entry.getKey()), entry.getValue());
        }
    }

    @Override
    public T remove(final Object key) {
        return proximity.remove(convertKey(key)); //To change body of generated methods, choose Tools | Templates.
    }

    private Object convertKey(final Object key) {
        if (key instanceof Double) {
            return convertKey((Double) key);
        } else {
            return key;
        }
    }

    private Long convertKey(final Double key) {
        final Double keyVal =  key;
        return Math.round(keyVal * multiplier);
    }

//    private Double invertKey(final Long key) {
//        return key.doubleValue() / multiplier;
//    }

    public double rounded(double key) {
        return ((double) Math.round(key * multiplier)) / multiplier;
    }

    @Override
    public boolean containsKey(Object key) {
        return proximity.containsKey(convertKey(key)); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int size() {
        return proximity.size();
    }

    @Override
    public boolean isEmpty() {
        return proximity.isEmpty();
    }

    @Override
    public boolean containsValue(Object value) {
        return proximity.containsValue(value);
    }

    @Override
    public void clear() {
        proximity.clear();
    }

    @Override
    public Set<Double> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<T> values() {
        return proximity.values();
    }

    @Override
    public Set<Entry<Double, T>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
