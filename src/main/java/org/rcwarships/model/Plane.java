/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 * Represents a plane in 3d space.
 *
 *
 * @author viljoenp
 */
public class Plane {

    public enum FaceIntersectType {

        NONE, COPLANAR_EDGE, COPLANAR_VERTEX, TWO_EDGES, INTERSECT_AND_COPLANAR_VERTEX;
    }

    public enum EdgeIntersectType {

        NONE, COPLANAR_EDGE, COPLANAR_VERTEX, INTERSECT;
    }

    private static final Logger LOGGER = Logger.getLogger(Plane.class.getName());

    public static Plane planeYZ(final double x, final boolean positive) {
        return new Plane(new Point3d(x, 0, 0), new Vector3d(positive ? 1 : -1, 0, 0));
    }

    public static Plane planeXZ(final double y, final boolean positive) {
        return new Plane(new Point3d(0, y, 0), new Vector3d(0, positive ? 1 : -1, 0));
    }

    public static Plane planeXY(final double z, final boolean positive) {
        return new Plane(new Point3d(0, 0, z), new Vector3d(0, 0, positive ? 1 : -1));
    }

    /**
     * normal form/algebraic form of the plane
     */
    final Point3d pointInPlane;
    final Vector3d normal;

    /**
     * normalized form's distance from the pointInPlane.
     */
    double distance;

    public Plane(final Tuple3d pointInPlane, final Vector3d normal) {
        this.pointInPlane = new Point3d(pointInPlane);
        this.normal = new Vector3d(normal);
        this.normal.normalize();
        this.distance = -this.normal.dot(new Vector3d(pointInPlane)); // 
    }

    public Plane flip() {
        return new Plane(pointInPlane, new Vector3d(-normal.x, -normal.y, -normal.z));
    }

    public double parametricDistance(final Tuple3d point) {
        return normal.dot(new Vector3d(point)) + distance;
    }

    /**
     * the unit vector in the direction of p0 to p1
     *
     * @param p0
     * @param p1
     * @return
     */
    private Vector3d lineUnitVector(final Tuple3d p0, final Tuple3d p1) {
        final Vector3d p0_p1_unitVector = new Vector3d(p1);
        p0_p1_unitVector.sub(p0);
        //  p0_p1_unitVector.normalize(); // We should not have any duplicate points in our polygon, so this should always be a none 0 length vector
        return p0_p1_unitVector;
    }

    /**
     * return the point where the line segment p0 - p1 will intersect the plane
     *
     * @param p0
     * @param p1
     * @param strict treat p0 - p1 as a finite line segment, else we will return
     * the point where the infinite line p0-p1 will intersect the plane
     * @return
     */
    public Tuple3d intersectSegment(final Tuple3d p0, final Tuple3d p1, final boolean strict) {
        final Vector3d p0_p1_unitVector = lineUnitVector(p0, p1);
        if (Math.abs(normal.dot(p0_p1_unitVector)) < Epsilon.E * 6) { // Line segment is paralell to plane
            if (Math.abs(parametricDistance(p0)) < Epsilon.E * 3) {  // if p0 is in the plane we return p0
                return new Point3d(p0);
            }
            throw new IllegalArgumentException("Line segment " + p0 + " - " + p1 + " is paralell with Plane " + this);
        } else {
            double scale = -parametricDistance(p0) / (normal.dot(p0_p1_unitVector));
            p0_p1_unitVector.scaleAdd(scale, p0);
            if (strict && (scale < 0 || scale > 1)) {
                throw new IllegalArgumentException("Line segment " + p0 + " - " + p1 + " does not intercect the Plane " + this + "  strict: scale:" + scale);
            }
            return p0_p1_unitVector;
        }
    }

    public boolean isCoPlanar(final Tuple3d p0) {
        return Math.abs(parametricDistance(p0)) < Epsilon.E * 3;
    }

    public boolean isCoPlanar(final Tuple3d p0, final Tuple3d p1) {
        return isIntersected(p0, p1, true) && Math.abs(parametricDistance(p0)) < Epsilon.E * 3 && Math.abs(parametricDistance(p1)) < Epsilon.E * 3;
    }

    /**
     * return the if the line segment p0 - p1 will intersect the plane
     *
     * @param p0
     * @param p1
     * @param strict treat p0 - p1 as a finite line segment, treated as an
     * infinite line p0-p1
     * @return
     */
    public boolean isIntersected(final Tuple3d p0, final Tuple3d p1, final boolean strict) {
        final Vector3d p0_p1_unitVector = lineUnitVector(p0, p1);
        if (Math.abs(normal.dot(p0_p1_unitVector)) < Epsilon.E * 6) {
            return Math.abs(parametricDistance(p0)) < Epsilon.E * 3;
        } else {
            double scale = -parametricDistance(p0) / (normal.dot(p0_p1_unitVector));
            return !strict || (scale >= 0 && scale <= 1);
        }
    }

    public EdgeIntersectType getIntersectType(final Tuple3d p0, final Tuple3d p1, final boolean strict) {
        final Vector3d p0_p1_unitVector = lineUnitVector(p0, p1);
        final double d0 = parametricDistance(p0);
        if (Math.abs(normal.dot(p0_p1_unitVector)) < Epsilon.E * 6) {
            return Math.abs(d0) < Epsilon.E * 3 ? EdgeIntersectType.COPLANAR_EDGE : EdgeIntersectType.NONE;
        } else {
            final double scale = -d0 / (normal.dot(p0_p1_unitVector));
            if (!strict || (scale >= 0 && scale <= 1)) {
                final double d1 = parametricDistance(p1);
                if ((Math.abs(d0) < Epsilon.E * 3) || (Math.abs(d1) < Epsilon.E * 3)) {
                    return EdgeIntersectType.COPLANAR_VERTEX;
                } else {
                    return EdgeIntersectType.INTERSECT;
                }
            } else {
                return EdgeIntersectType.NONE;
            }
        }
    }

    public FaceIntersectType getIntersectType(final Tuple3d p0, final Tuple3d p1, final Tuple3d p2) {
        EdgeIntersectType e0 = getIntersectType(p0, p1, true);
        EdgeIntersectType e1 = getIntersectType(p1, p2, true);
        EdgeIntersectType e2 = getIntersectType(p2, p0, true);
        if ((e0 == EdgeIntersectType.COPLANAR_EDGE) || (e1 == EdgeIntersectType.COPLANAR_EDGE) || (e2 == EdgeIntersectType.COPLANAR_EDGE)) {
            // ANY Edge is CoPlanar
            return FaceIntersectType.COPLANAR_EDGE;
        } else if (((e0 == EdgeIntersectType.COPLANAR_VERTEX) && (e1 == EdgeIntersectType.COPLANAR_VERTEX) && (e2 == EdgeIntersectType.NONE))
                || ((e1 == EdgeIntersectType.COPLANAR_VERTEX) && (e2 == EdgeIntersectType.COPLANAR_VERTEX) && (e0 == EdgeIntersectType.NONE))
                || ((e2 == EdgeIntersectType.COPLANAR_VERTEX) && (e0 == EdgeIntersectType.COPLANAR_VERTEX) && (e1 == EdgeIntersectType.NONE))) {
            // Two edges has a CoPlanar VERTEX
            return FaceIntersectType.COPLANAR_VERTEX;
        } else if (((e0 == EdgeIntersectType.COPLANAR_VERTEX) && (e1 == EdgeIntersectType.COPLANAR_VERTEX) && (e2 == EdgeIntersectType.INTERSECT))
                || ((e1 == EdgeIntersectType.COPLANAR_VERTEX) && (e2 == EdgeIntersectType.COPLANAR_VERTEX) && (e0 == EdgeIntersectType.INTERSECT))
                || ((e2 == EdgeIntersectType.COPLANAR_VERTEX) && (e0 == EdgeIntersectType.COPLANAR_VERTEX) && (e1 == EdgeIntersectType.INTERSECT))) {
            // Two edges has a CoPlanar VERTEX
            return FaceIntersectType.INTERSECT_AND_COPLANAR_VERTEX;
        } else if (((e0 == EdgeIntersectType.INTERSECT) && (e1 == EdgeIntersectType.INTERSECT))
                || ((e1 == EdgeIntersectType.INTERSECT) && (e2 == EdgeIntersectType.INTERSECT))
                || ((e2 == EdgeIntersectType.INTERSECT) && (e0 == EdgeIntersectType.INTERSECT))) {
            // Two edges has a clean intersection
            return FaceIntersectType.TWO_EDGES;
        } else if ((e0 == EdgeIntersectType.NONE) && (e1 == EdgeIntersectType.NONE) && (e2 == EdgeIntersectType.NONE)) {
            // No intersection
            return FaceIntersectType.NONE;
        } else {
            throw new IllegalArgumentException("Invalid intersection combination for face and plane (" + e0 + "," + e1 + "," + e2 + ")");
        }

    }

    /**
     * find the first point on the Plane where the polygon formed from list
     * originalPoints will intersect.
     *
     * @param originalPoints
     * @return
     */
    public Point3d getIntersection(final java.util.List<? extends Tuple3d> originalPoints) {
        Tuple3d last_vert = originalPoints.get(0);
        double last_d = parametricDistance(last_vert);

        for (int i = 1; i < originalPoints.size(); i++) {
            final Tuple3d curr_vert = originalPoints.get(i);
            final double curr_d = parametricDistance(curr_vert);
            final double last_curr_d = curr_d * last_d;

            // If last_vert and curr_vert is on oposite sides of the Plane  
            if ((last_curr_d < 0) && (Math.abs(last_curr_d) > Epsilon.EE)) {
                return new Point3d(intersectSegment(last_vert, curr_vert, true));
            } else if (Math.abs(curr_d) <= Epsilon.E && Math.abs(last_d) >= Epsilon.E) { // Only curr_vert is in the plane
                return new Point3d(curr_vert);
            } else if (Math.abs(last_d) <= Epsilon.E) { // last_vert is in the plane ( or last_vert AND cur_vert )
                return new Point3d(last_vert);
            } else {
                last_vert = curr_vert;
                last_d = curr_d;
            }
        }

        if (LOGGER.isLoggable(Level.SEVERE)) {
            last_vert = originalPoints.get(0);
            last_d = parametricDistance(last_vert);
            for (int i = 1; i < originalPoints.size(); i++) {
                final Tuple3d curr_vert = originalPoints.get(i);
                final double curr_d = parametricDistance(curr_vert);
                final double last_curr_d = curr_d * last_d;
                LOGGER.log(Level.SEVERE, String.format("Plane::getIntersection failed with %s  (%5.2f, %5.2f , %f   -   %5.2f, %5.2f , %f)   last_d=%.5f , curr_d=%.5f last_curr_d=%.5f\n", toString(), last_vert.x, last_vert.y, last_vert.z, curr_vert.x, curr_vert.y, curr_vert.z, last_d, curr_d, last_curr_d));
                last_vert = curr_vert;
                last_d = curr_d;
            }
        }
        LOGGER.log(Level.SEVERE, "No intersection could be found with plane {0}", toString());
        //throw new IllegalArgumentException("No intersection could be found with plane " + toString());
        return null;
    }

    public double distance(final Plane other) {

        final Vector3d u = new Vector3d();
        u.cross(normal, other.normal); // Pn1.n * Pn2.n;          // cross product

        double ax = (u.x >= 0 ? u.x : -u.x);
        double ay = (u.y >= 0 ? u.y : -u.y);
        double az = (u.z >= 0 ? u.z : -u.z);

        // test if the two planes are parallel
        if ((ax + ay + az) < Epsilon.E * 6) {        // Pn1 and Pn2 are near parallel
            return -parametricDistance(other.pointInPlane);  //  NOTE this might return 0 if other coincide with this.   < 0 if this plane is on left of the other plane
        } else {
            return 0;// The planes cross
        }
    }

    @Override
    public String toString() {
        return String.format("%.3fx %+.3fy %+.3fz = %.3f  %s ", normal.x, normal.y, normal.z, -distance, pointInPlane);
    }

}
