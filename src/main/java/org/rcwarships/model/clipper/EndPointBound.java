/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.clipper;

import org.rcwarships.model.Plane;

/**
 *
 * @author viljoenp
 */
public class EndPointBound {

    public EndPointBound(Plane... constraints) {
        this.planes = new java.util.ArrayList<>(java.util.Arrays.asList(constraints));
    }

    // List of planes that define the constaints to be enforces
    final java.util.List<Plane> planes;

    @Override
    public String toString() {
        StringBuilder sbuf = new StringBuilder();
        sbuf.append("(");
        for (Plane plane : planes) {
            sbuf.append(plane.toString());
            sbuf.append(" , ");
        }
        sbuf.append(" )");
        return sbuf.toString(); //To change body of generated methods, choose Tools | Templates.
    }

}
