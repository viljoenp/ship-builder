/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.clipper;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Tuple3d;
import org.rcwarships.model.Plane;

/**
 *
 * @author viljoenp
 */
public class Clipper {

    private static final Logger LOGGER = Logger.getLogger(Clipper.class.getName());

    public static void clipEndPoints(final java.util.List<? extends Tuple3d> originalPoints, final EndPointBound start, final EndPointBound end) {
        if (originalPoints.size() < 2) {
            return;
        }
        if (!start.planes.isEmpty()) {
            clipBoundry(originalPoints, start);
        }
        if (!end.planes.isEmpty()) {
            java.util.Collections.reverse(originalPoints);
            clipBoundry(originalPoints, end);
            java.util.Collections.reverse(originalPoints);
        }
    }

    public static void clipBoundry(final java.util.List<? extends Tuple3d> originalPoints, final EndPointBound boundry) {

        for (Plane plane : boundry.planes) {

            // Vector3d startYClip = ModelUtils.vector(null, null)
            int minIDX = findMinima(originalPoints, plane);
            // Clip Start Point
            // Always clip the end point
            boolean clipped = true;
            for (java.util.ListIterator<? extends Tuple3d> iter = originalPoints.listIterator(); iter.hasNext() && (iter.nextIndex() <= minIDX || clipped);) {
                clipped = false;
                final Tuple3d cur = iter.next();
                final double curD = plane.parametricDistance(cur);
                if (!iter.hasNext()) {
                    break;
                }
                final Tuple3d next = iter.next();
                final double nextD = plane.parametricDistance(next);
                iter.previous();

                // Check if we need to clip Y ( either y< start.bounds.y or y > start.bounds.y )
                if (curD < 0 || nextD < 0) {
                    // Check if the next point also need to be clipped
                    if (plane.isIntersected(cur, next, true)) {
                        cur.set(plane.intersectSegment(cur, next, true));
                        clipped = true;
                    } else if (curD < 0 && nextD < 0) {
                        // Both points are beyond the clipping bounds, we remove cur
                        iter.previous();
                        iter.remove(); // Remove Cur
                        clipped = true;
                    } else {
                        LOGGER.log(Level.WARNING, "Clipper Cur {0}:{1}  , Next {2}:{3}  one is neg, but the edge is neither completely neg, or intersected by plane : {4}", new Object[]{curD, cur, nextD, next, plane});
                    }
                }

            }
        }
    }

    private static int findMinima(final java.util.List<? extends Tuple3d> originalPoints, Plane plane) {
        double curmin = Double.MAX_VALUE;
        double curmax = -Double.MAX_VALUE;
        int cntBelow = 0, cntAbove = 0;
        int curMinidx = 0;
        int curMaxidx = 0;
        for (int i = 0; i < originalPoints.size(); i++) {
            final double curY = plane.parametricDistance(originalPoints.get(i));
            if (curY < curmin) {
                curmin = curY;
                curMinidx = i;
            }
            if (curY > curmax) {
                curmax = curY;
                curMaxidx = i;
            }
            if (curY < 0) {
                cntBelow++;
            } else if (curY > 0) {
                cntAbove++;
            }
        }
        if (cntBelow < cntAbove) {
            return curMinidx;
        } else {
            return curMaxidx;
        }
    }
}
