/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import org.rcwarships.model.simplifier.Simplifier;

/**
 * Utility to get the line that is the intersection of a Plane and the Model
 *
 * @author viljoenp
 */
public class ModelTracer {

    private static final Logger LOGGER = Logger.getLogger(ModelTracer.class.getName());

    /**
     * Provided by the Caller to select the start Points of the trace
     */
    public interface StartSelector extends java.io.Serializable {

        /**
         *
         * @param hull_MESH
         * @param faces The faces that was selected from the mesh , all these
         * faces has edges that intersects the plane ( or a single CoPlanar
         * Vertex )
         * @param plane The plane that is used to trace the new Polygon
         * @return
         */
        Face selectStartFace(Mesh hull_MESH, java.util.List<Face> faces, Plane plane);

        /**
         *
         * @param hull_MESH
         * @param startFace The Start Face
         * @param plane The plane that is used to trace the new Polygon
         * @return
         */
        Edge selectStartEdge(Mesh hull_MESH, Face startFace, Plane plane);

        /**
         *
         * @param hull_MESH
         * @param startEdge The Start Edge
         * @param plane The plane that is used to trace the new Polygon
         * @return
         */
        Point3d selectStartPoint(Mesh hull_MESH, Edge startEdge, Plane plane);
    }

    /**
     * get the vertices for the Rib section from the hull_MESH at rib position.
     * 1. Get rib-faces: the list of faces that cover the area of the rib from
     * the hull.
     *
     * 2. Get highest-face : face with the highest vertex.
     *
     * 3. Get highest-edge : edge with the highest vertex from the highest-face
     * , this should also be a manifold edge.
     *
     * 4. Traverse the rib-faces from the highest-face to the next face
     * selecting the other edge that crossed the rib-position. Any given face
     * can have at most two points that cross the rib position, && at the least
     * it only has one point (this would not be often) && this means that we can
     * ignore the face , since bordering && sharing the same point must be
     * another face that will cross the rib
     *
     * @param hull_MESH
     * @param plane
     * @param selector StartSelector to select the start point of the trace
     * @return the list of Points that constitute the contiguous polygon (
     * profile) that defines the intersection of the Hull with the given plane
     */
    public static java.util.List<Point3d> traceProfileLine(final Mesh hull_MESH, final Plane plane, final StartSelector selector) {
        // 1. Get the list of faces that cover the area of the intersection with the hull.
        final java.util.List<Face> intFaces = getFacesSpanningPlane(hull_MESH, plane);
        if (intFaces.isEmpty()) {
            throw new IllegalArgumentException("No mesh intersection intersection at " + plane);
        }
        // This we should be able to optimize by only using the allowes faces
        final java.util.List<Edge> intEdges = getHullEdgesSpanningPlane(hull_MESH, plane, intFaces);
        // Make a list of Vertexes that is exactly ON the plane
        final java.util.List<Vertex> intVerts = getCoPlanarVertexes(hull_MESH, plane, intEdges);

        // Make set of all the face id's 
        final java.util.Set<Integer> allowedFaceIdxList = new java.util.HashSet<>(intFaces.size());
        for (Face face : intFaces) {
            allowedFaceIdxList.add(face.index);
        }
        // Make set of all the face id's 
        final java.util.Set<Integer> allowedEdgeIdxList = new java.util.HashSet<>(intEdges.size());
        for (Edge edge : intEdges) {
            allowedEdgeIdxList.add(edge.index);
        }
        // Make set of all the Vertex on the Plane
        final java.util.Set<Integer> allowedVertexIdxList = new java.util.HashSet<>(intVerts.size());
        for (Vertex vert : intVerts) {
            allowedVertexIdxList.add(vert.index);
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Detected {0} faces , {1} edges , {2} vertexes on {3}", new Object[]{allowedFaceIdxList.size(), allowedEdgeIdxList.size(), allowedVertexIdxList.size(), plane});
        }

        // We need some start point for the algorithm , and we always start from the manifold edge and in the case of ribs ribs we start with the highest fase (it should also be the only face with an edge on the manifold edge).
        // 2. Get highest-face : face with the highest vertex.
        Face cur_face = selector.selectStartFace(hull_MESH, intFaces, plane);

        // 3. Get highest-edge : edge with the highest vertex from the highest-face, this should also be a manifold edge.
        Edge cur_edge = selector.selectStartEdge(hull_MESH, cur_face, plane);
        if (!cur_edge.isManifoldEdge()) {
            LOGGER.log(Level.WARNING, "Detected start edge {0}\n on start face {1}\n is not a manifold edge on {2}", new Object[]{ModelUtils.dumpEdge(hull_MESH, cur_edge), ModelUtils.dumpFace(hull_MESH, cur_face), plane});
            for (int faceid : cur_edge.face_keys) {
                if (faceid != cur_face.index) {
                    LOGGER.log(Level.WARNING, "Other face {0}\n", new Object[]{ModelUtils.dumpFace(hull_MESH, hull_MESH.faces.get(faceid))});
                }
            }
        }
        Point3d cur_point = selector.selectStartPoint(hull_MESH, cur_edge, plane);

        // 4. Traverse the faces from the start to the next face selecting the other edge that crossed the plane
        java.util.Set<Integer> visited_face_index = new java.util.HashSet<>(allowedFaceIdxList.size());
        java.util.Set<Integer> visited_edge_index = new java.util.HashSet<>(allowedEdgeIdxList.size());
        java.util.Set<Integer> visited_vert_index = new java.util.HashSet<>(allowedVertexIdxList.size());

        final java.util.List<Point3d> points = new java.util.ArrayList<>(allowedFaceIdxList.size() * 2);

        // Accumalate the points for the intersection , by calculating the intersection of the edge with the plane
        while ((cur_face != null) && (cur_edge != null)) {
            switch (getIntersectType(hull_MESH, cur_face, plane)) {
                case COPLANAR_EDGE: {
                    // One (or more) edges CoPlanar with plane 
                    cur_edge = selectCoPlanarEdge(hull_MESH, cur_face, plane);
                    final Vertex v1 = getNearestVertex(hull_MESH, cur_edge, cur_point); // Get the vertex nearest to cur_point
                    final Vertex v2 = getOppositeVertex(hull_MESH, cur_edge, v1);
                    if (!visited_vert_index.contains(v1.index)) {
                        points.add(new Point3d(v1.coord));
                    }
                    if (!visited_vert_index.contains(v2.index)) {
                        points.add(new Point3d(v2.coord));
                    }
                    visited_edge_index.addAll(cur_face.edge_keys);
                    visited_face_index.add(cur_face.index);
                    visited_vert_index.add(v1.index);
                    visited_vert_index.add(v2.index);
                }
                break;
                case COPLANAR_VERTEX: {
                    // A 
                    final Vertex v1 = selectCoPlanarVertex(hull_MESH, cur_face, plane);
                    if (!visited_vert_index.contains(v1.index)) {
                        points.add(new Point3d(v1.coord));
                    }
                    visited_edge_index.addAll(cur_face.getSharedEdges(v1));
                    visited_face_index.add(cur_face.index);
                    visited_vert_index.add(v1.index);
                    if (cur_face.edge_keys.contains(cur_face.index)) {
                        cur_edge = getOppositeEdge(hull_MESH, cur_face, cur_edge, plane, Plane.FaceIntersectType.COPLANAR_VERTEX);
                    } else {
                        // get The first COPLANAR_VERTEX edge in the face 
                        boolean foundEdge = false;
                        for (int edgeIdx : cur_face.edge_keys) {
                            final Edge tmpEdge = hull_MESH.edges.get(edgeIdx);
                            if (getIntersectType(hull_MESH, tmpEdge, plane) == Plane.EdgeIntersectType.COPLANAR_VERTEX) {
                                foundEdge = true;
                                cur_edge = tmpEdge;
                                break;
                            }
                        }
                        if (!foundEdge) {
                            final Vertex vf_1 = hull_MESH.verts.get(cur_face.verts[0]);
                            final Vertex vf_2 = hull_MESH.verts.get(cur_face.verts[1]);
                            final Vertex vf_3 = hull_MESH.verts.get(cur_face.verts[2]);

                            for (final int eIdx : cur_face.edge_keys) {
                                final Edge otherEdge = hull_MESH.edges.get(eIdx);
                                final Vertex vert_1 = hull_MESH.verts.get(otherEdge.verts[0]);
                                final Vertex vert_2 = hull_MESH.verts.get(otherEdge.verts[1]);
                                LOGGER.log(Level.SEVERE, "Face #{0}==>{5} Edge #{1} hasIntersectType {2}   {3} - {4}", new Object[]{cur_face.index, otherEdge.index, plane.getIntersectType(vert_1.coord, vert_2.coord, true), vert_1.coord, vert_2.coord, plane.getIntersectType(vf_1.coord, vf_2.coord, vf_3.coord)});
                            }

                            throw new IllegalArgumentException("The Edge #" + cur_edge.index + " does not have an opposite edge with type " + Plane.EdgeIntersectType.COPLANAR_VERTEX + " in face #" + cur_face.index + " [" + cur_face.edge_keys + "]");
                        }
                    }
                }
                break;
                case INTERSECT_AND_COPLANAR_VERTEX: {
                    if (getIntersectType(hull_MESH, cur_edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                        visited_face_index.add(cur_face.index);
                        // Add Intersect Point
                        points.add(new Point3d(getIntersection(hull_MESH, cur_edge, plane)));

                        // Add CoPlanar Point
                        final Vertex v1 = getOppositeVertex(hull_MESH, cur_face, cur_edge);
                        if (!visited_vert_index.contains(v1.index)) {
                            points.add(new Point3d(v1.coord));
                        }
                        visited_vert_index.add(v1.index);
                        visited_edge_index.addAll(cur_face.edge_keys);
                        cur_edge = getOppositeEdge(hull_MESH, cur_face, cur_edge, plane, Plane.FaceIntersectType.INTERSECT_AND_COPLANAR_VERTEX);
                    } else {
                        visited_face_index.add(cur_face.index);
                        // Add CoPlanar Point
                        final Vertex v1 = selectCoPlanarVertex(hull_MESH, cur_face, plane);
                        if (!visited_vert_index.contains(v1.index)) {
                            points.add(new Point3d(v1.coord));
                        }
                        visited_vert_index.add(v1.index);
                        // Add Intersect Point
                        cur_edge = getOppositeEdge(hull_MESH, cur_face, cur_edge, plane, Plane.FaceIntersectType.INTERSECT_AND_COPLANAR_VERTEX);
                        if (!visited_edge_index.contains(cur_edge.index)) {
                            points.add(new Point3d(getIntersection(hull_MESH, cur_edge, plane)));
                        }
                        visited_edge_index.addAll(cur_face.edge_keys);
                    }
                }
                break;
                case TWO_EDGES: {
                    if (!visited_edge_index.contains(cur_edge.index)) {
                        points.add(new Point3d(getIntersection(hull_MESH, cur_edge, plane)));
                    }
                    visited_edge_index.add(cur_edge.index);
                    cur_edge = getOppositeEdge(hull_MESH, cur_face, cur_edge, plane, Plane.FaceIntersectType.TWO_EDGES);
                    if (!visited_edge_index.contains(cur_edge.index)) {
                        points.add(new Point3d(getIntersection(hull_MESH, cur_edge, plane)));
                    }
                    visited_edge_index.add(cur_edge.index);
                    visited_face_index.add(cur_face.index);
                }
                break;
                default:
                    if (visited_face_index.size() < intFaces.size()) {
                        LOGGER.log(Level.SEVERE, "Stopping after {0} procced faces from {1}  plane:{2}", new Object[]{visited_face_index.size(), intFaces.size(), plane});
                        LOGGER.log(Level.SEVERE, "Stopping faceIdx :{0} is has no edges crossing the plane at plane:{2}, but it should", new Object[]{cur_face.index, plane});
                        LOGGER.log(Level.SEVERE, "Stopping faceIdxList:{0}", new Object[]{allowedFaceIdxList});
                        LOGGER.log(Level.SEVERE, "Stopping visited_face_index:{0}", new Object[]{visited_face_index});
                        allowedFaceIdxList.removeAll(visited_face_index);
                        LOGGER.log(Level.SEVERE, "Stopping visited_face_index:{0}", new Object[]{allowedFaceIdxList});
                        throw new IllegalArgumentException("Could not trace the intersection profile at " + plane);
                    }
            }
            cur_point = points.get(points.size() - 1);
            Face next_face = _find_next_face(hull_MESH, cur_face, cur_edge, allowedFaceIdxList, visited_face_index, visited_edge_index, plane);
            if (next_face == null && visited_face_index.size() < intFaces.size()) {
                next_face = _search_nearests_intersect_face(hull_MESH, cur_face, allowedFaceIdxList, visited_face_index, plane, points.get(points.size() - 1));
                cur_edge = _search_nearest_edge(hull_MESH, cur_face, plane, points.get(points.size() - 1));
            }
            cur_face = next_face;

//            if (cur_face != null) {
//
//            }

        }
        if (visited_face_index.size() < intFaces.size()) {
            LOGGER.log(Level.SEVERE, "Stopping after {0} procced faces from {1}  plane:{2}", new Object[]{visited_face_index.size(), intFaces.size(), plane});
            LOGGER.log(Level.SEVERE, "Stopping faceIdxList:{0}", new Object[]{allowedFaceIdxList});
            LOGGER.log(Level.SEVERE, "Stopping visited_face_index:{0}", new Object[]{visited_face_index});
            allowedFaceIdxList.removeAll(visited_face_index);
            LOGGER.log(Level.SEVERE, "Stopping visited_face_index:{0}", new Object[]{allowedFaceIdxList});
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Precessed {0}:{1} faces , {2}:{3} edges , {4}:{5} vertexes at manifold plane:{6}", new Object[]{allowedFaceIdxList.size(), visited_face_index.size(), allowedEdgeIdxList.size(), visited_edge_index.size(), allowedVertexIdxList.size(), visited_vert_index.size(), plane});
        }

        return Simplifier.simplifyPolygonLineSegments(points);
    }

    /**
     * Get all the faces with an edge that intersects the Plane
     *
     * @param hull_MESH the Mesh we are going to calculate the intersection
     * with.
     * @param plane the Plane that defines the intersection.
     * @return a List of all the faces that somehow intersects the Plane
     */
    public static java.util.List<Face> getFacesSpanningPlane(final Mesh hull_MESH, final Plane plane) {
        final java.util.List<Face> faces = new java.util.ArrayList<>(500);
        for (final Face face : hull_MESH.faces) {
            final Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
            final Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
            final Vertex vert_3 = hull_MESH.verts.get(face.verts[2]);
            if ((vert_1.coord.x < 0) && (Math.abs(vert_1.coord.x) > Epsilon.E) && (vert_2.coord.x < 0) && (Math.abs(vert_2.coord.x) > Epsilon.E) && (vert_3.coord.x < 0) && (Math.abs(vert_3.coord.x) > Epsilon.E)) {
                continue;
            }
            final Plane.FaceIntersectType type = plane.getIntersectType(vert_1.coord, vert_2.coord, vert_3.coord);
            if (type != Plane.FaceIntersectType.NONE) {
                faces.add(face);
            }
        }
        return faces;
    }

    /**
     * Get all the edges of the given faces that intersects the plane .
     *
     */
    private static java.util.List<Edge> getHullEdgesSpanningPlane(final Mesh hull_MESH, final Plane plane, final java.util.Collection<Face> faces) {
        final java.util.List<Edge> edges = new java.util.ArrayList<>(faces.size() * 3); // maximum number of edges that can all be in the same plane
        for (final Face face : faces) {
            for (final Integer edgeIdx : face.edge_keys) {
                final Edge edge = hull_MESH.edges.get(edgeIdx);
                final Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
                final Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
                if ((vert_1.coord.x < 0) && (Math.abs(vert_1.coord.x) > Epsilon.E) && (vert_2.coord.x < 0) && (Math.abs(vert_2.coord.x) > Epsilon.E)) {
                    continue;
                }
                final Plane.EdgeIntersectType type = plane.getIntersectType(vert_1.coord, vert_2.coord, true);
                if (type != Plane.EdgeIntersectType.NONE) {
                    edges.add(edge);
                }
            }
        }
        return edges;
    }

    /**
     * Get all the vertexes of the given edges that are coplanar with plane.
     */
    private static java.util.List<Vertex> getCoPlanarVertexes(final Mesh hull_MESH, final Plane plane, final java.util.Collection<Edge> edges) {
        final java.util.List<Vertex> verts = new java.util.ArrayList<>(edges.size() + 2); // Max number of vertexes that can all be in the same plane
        for (final Edge edge : edges) {
            for (final Integer vertIdx : edge.verts) {
                final Vertex vert = hull_MESH.verts.get(vertIdx);
                if ((vert.coord.x < 0) && (Math.abs(vert.coord.x) > Epsilon.E)) {
                    continue;
                }
                double dist = plane.parametricDistance(vert.coord);
                if (Math.abs(dist) < Epsilon.E * 3) {
                    verts.add(vert);
                }
            }
        }
        return verts;
    }

    /**
     * @param hull_MESH
     * @param edge
     * @param plane
     * @return the Point that represents the Intersection of the edge with the
     * Plane
     */
    public static Tuple3d getIntersection(final Mesh hull_MESH, final Edge edge, final Plane plane) {
        Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
        Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
        return plane.intersectSegment(vert_1.coord, vert_2.coord, true);
    }

    /**
     * Filter out the faces that is on the positive side if the plane
     *
     * @param hull_MESH
     * @param faces
     * @param plane
     * @return
     */
    public static java.util.List<Face> filterFaces(final Mesh hull_MESH, java.util.List<Face> faces, final Plane plane) {
        for (java.util.Iterator<Face> iter = faces.iterator(); iter.hasNext();) {
            final Face face = iter.next();
            final Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
            final Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
            final Vertex vert_3 = hull_MESH.verts.get(face.verts[2]);
            if (plane.parametricDistance(vert_1.coord) < -Epsilon.E || plane.parametricDistance(vert_2.coord) < -Epsilon.E || plane.parametricDistance(vert_3.coord) < -Epsilon.E) {
                iter.remove();
            }
        }
        return faces;
    }

    /**
     *
     * @param hull_MESH
     * @param edge
     * @param plane
     * @return the Edge Vertex opposite the given vertex
     */
    private static Vertex getOppositeVertex(final Mesh hull_MESH, final Edge edge, Vertex v1) {
        if (edge.verts[0] == v1.index) {
            return hull_MESH.verts.get(edge.verts[1]);
        } else if (edge.verts[1] == v1.index) {
            return hull_MESH.verts.get(edge.verts[0]);
        } else {
            throw new IllegalArgumentException("The vertex #" + v1.index + " is not part of edge #" + edge.index + "[" + edge.verts[0] + "," + edge.verts[1] + "]");
        }
    }

    /**
     *
     * @param hull_MESH
     * @param edge
     * @param plane
     * @return the Edge Vertex opposite the given vertex
     */
    private static Vertex getNearestVertex(final Mesh hull_MESH, final Edge edge, Point3d coord) {
        final Vertex vert_0 = hull_MESH.verts.get(edge.verts[0]);
        final Vertex vert_1 = hull_MESH.verts.get(edge.verts[1]);

        double d0 = vert_0.coord.distanceSquared(coord);
        double d1 = vert_1.coord.distanceSquared(coord);
        if (d0 < d1) {
            return vert_0;
        } else {
            return vert_1;
        }
        // What a the degenerate case d0==d1 ? 
    }

    public static Vertex getOppositeVertex(final Mesh hull_MESH, final Face face, final Edge edge) {
        if ((face.verts[0] != edge.verts[0]) && (face.verts[0] != edge.verts[1])) {
            return hull_MESH.verts.get(face.verts[0]);
        } else if ((face.verts[1] != edge.verts[0]) && (face.verts[1] != edge.verts[1])) {
            return hull_MESH.verts.get(face.verts[1]);
        } else {
            return hull_MESH.verts.get(face.verts[2]);
        }
    }
//
//    private static Edge getOppositeEdge(final Mesh hull_MESH, final Face face, final Edge edge, final Plane plane) {
//        for (final int eIdx : face.edge_keys) {
//
//            if (eIdx != edge.index) {
//                final Edge otherEdge = hull_MESH.edges.get(eIdx);
//                final Vertex vert_1 = hull_MESH.verts.get(otherEdge.verts[0]);
//                final Vertex vert_2 = hull_MESH.verts.get(otherEdge.verts[1]);
//                if (plane.getIntersectType(vert_1.coord, vert_2.coord, true) == Plane.EdgeIntersectType.INTERSECT) {
//                    return otherEdge;
//                }
//            }
//        }
//        throw new IllegalArgumentException("The Edge #" + edge.index + " does not have an opposite edge with type INTERSECT in face #" + face.index + " [" + face.edge_keys + "]");
//    }

    private static Edge getOppositeEdge(final Mesh hull_MESH, final Face face, final Edge edge, final Plane plane, Plane.FaceIntersectType faceType) {
        final Vertex vx_1 = hull_MESH.verts.get(edge.verts[0]);
        final Vertex vx_2 = hull_MESH.verts.get(edge.verts[1]);
        final Plane.EdgeIntersectType edge_type = plane.getIntersectType(vx_1.coord, vx_2.coord, true);

        final Plane.EdgeIntersectType searchType;
        switch (faceType) {
            case INTERSECT_AND_COPLANAR_VERTEX:
                if (edge_type == Plane.EdgeIntersectType.INTERSECT) {
                    searchType = Plane.EdgeIntersectType.COPLANAR_VERTEX;
                } else {
                    searchType = Plane.EdgeIntersectType.INTERSECT;
                }
                break;
            case COPLANAR_VERTEX:
            case TWO_EDGES:
            default:
                searchType = edge_type;
        }

        for (final int eIdx : face.edge_keys) {
            if (eIdx != edge.index) {
                final Edge otherEdge = hull_MESH.edges.get(eIdx);
                final Vertex vert_1 = hull_MESH.verts.get(otherEdge.verts[0]);
                final Vertex vert_2 = hull_MESH.verts.get(otherEdge.verts[1]);
                if (plane.getIntersectType(vert_1.coord, vert_2.coord, true) == searchType) {
                    return otherEdge;
                }
            }
        }
        final Vertex vf_1 = hull_MESH.verts.get(face.verts[0]);
        final Vertex vf_2 = hull_MESH.verts.get(face.verts[1]);
        final Vertex vf_3 = hull_MESH.verts.get(face.verts[2]);

        for (final int eIdx : face.edge_keys) {
            final Edge otherEdge = hull_MESH.edges.get(eIdx);
            final Vertex vert_1 = hull_MESH.verts.get(otherEdge.verts[0]);
            final Vertex vert_2 = hull_MESH.verts.get(otherEdge.verts[1]);
            LOGGER.log(Level.SEVERE, "Face #{0}==>{5} Edge #{1} hasIntersectType {2}   {3} - {4}", new Object[]{face.index, otherEdge.index, plane.getIntersectType(vert_1.coord, vert_2.coord, true), vert_1.coord, vert_2.coord, plane.getIntersectType(vf_1.coord, vf_2.coord, vf_3.coord)});
        }

        throw new IllegalArgumentException("The Edge #" + edge.index + " does not have an opposite edge with type " + searchType + " in face #" + face.index + " [" + face.edge_keys + "]");
    }

    public static Plane.EdgeIntersectType getIntersectType(final Mesh hull_MESH, final Edge edge, final Plane plane) {
        final Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
        final Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
        return plane.getIntersectType(vert_1.coord, vert_2.coord, true);
    }

    public static Plane.FaceIntersectType getIntersectType(final Mesh hull_MESH, final Face face, final Plane plane) {
        final Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
        final Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
        final Vertex vert_3 = hull_MESH.verts.get(face.verts[2]);
        return plane.getIntersectType(vert_1.coord, vert_2.coord, vert_3.coord);
    }

    public static Vertex selectCoPlanarVertex(final Mesh hull_MESH, final Edge edge, final Plane plane) {
        final Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
        final Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
        if (plane.isCoPlanar(vert_1.coord)) {
            return vert_1;
        } else if (plane.isCoPlanar(vert_2.coord)) {
            return vert_2;
        } else {
            throw new IllegalArgumentException("Edge #" + edge.index + " has not coplanar vertex to plane " + plane);
        }
    }

    public static Vertex selectCoPlanarVertex(final Mesh hull_MESH, final Face face, final Plane plane) {
        final Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
        final Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
        final Vertex vert_3 = hull_MESH.verts.get(face.verts[2]);
        if (plane.isCoPlanar(vert_1.coord)) {
            return vert_1;
        } else if (plane.isCoPlanar(vert_2.coord)) {
            return vert_2;
        } else if (plane.isCoPlanar(vert_3.coord)) {
            return vert_3;
        } else {
            throw new IllegalArgumentException("Face #" + face.index + " has not coplanar vertex to plane " + plane);
        }
    }

    public static Edge selectCoPlanarEdge(final Mesh hull_MESH, final Face face, final Plane plane) {
        final Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
        final Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
        final Vertex vert_3 = hull_MESH.verts.get(face.verts[2]);
        final boolean coPlane1 = plane.isCoPlanar(vert_1.coord);
        final boolean coPlane2 = plane.isCoPlanar(vert_2.coord);
        final boolean coPlane3 = plane.isCoPlanar(vert_3.coord);
        // Degenerate case , all three points are in the plane, we select the longest edge 
        if (coPlane1 && coPlane2 && coPlane3) {
            final double d12 = vert_1.coord.distanceSquared(vert_2.coord);
            final double d23 = vert_2.coord.distanceSquared(vert_3.coord);
            final double d31 = vert_3.coord.distanceSquared(vert_1.coord);
            if ((d12 > d23) && (d12 > d31)) {
                return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(vert_1, vert_2));
            } else if ((d23 > d12) && (d23 > d31)) {
                return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(vert_2, vert_3));
            } else {
                return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(vert_3, vert_1));
            }
        } else if (coPlane1 && coPlane2) {
            return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(vert_1, vert_2));
        } else if (coPlane2 && coPlane3) {
            return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(vert_2, vert_3));
        } else if (coPlane3 && coPlane1) {
            return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(vert_3, vert_1));
        } else {
            throw new IllegalArgumentException("Face #" + face.index + " is not coplanar with Plane:" + plane);
        }
    }

    private static Face _find_next_face(Mesh hull_MESH, Face face, Edge edge, java.util.Set<Integer> allowedFaceIdxList, java.util.Set<Integer> visited_face_index, java.util.Set<Integer> visited_edge_index, Plane plane) {
        // shared edges
        for (int face_idx : edge.face_keys) {
            if (allowedFaceIdxList.contains(face_idx) && !visited_face_index.contains(face_idx)) {
                return hull_MESH.faces.get(face_idx);
            }
        }
        // if none of the shares edges leads to an allowed face , it can be that one of our vertexes is on the Edge 
        final Vertex vx1 = hull_MESH.verts.get(edge.verts[0]);
        for (int face_idx : vx1.face_keys) {
            if (allowedFaceIdxList.contains(face_idx) && !visited_face_index.contains(face_idx)) {
                return hull_MESH.faces.get(face_idx);
            }
        }
        final Vertex vx2 = hull_MESH.verts.get(edge.verts[1]);
        for (int face_idx : vx2.face_keys) {
            if (allowedFaceIdxList.contains(face_idx) && !visited_face_index.contains(face_idx)) {
                return hull_MESH.faces.get(face_idx);
            }
        }
        // if none of our vertexes lead to an allowed face, it can be that we need to process another edge ?
        for (int edgeIdx : face.edge_keys) {
            if (edgeIdx == edge.index) {
                continue;
            }
            final Edge edge1 = hull_MESH.edges.get(edgeIdx);
            for (int face_idx : edge1.face_keys) {
                if (allowedFaceIdxList.contains(face_idx) && !visited_face_index.contains(face_idx)) {
                    return hull_MESH.faces.get(face_idx);
                }
            }
            Vertex vex1 = hull_MESH.verts.get(edge1.verts[0]);
            for (int face_idx : vex1.face_keys) {
                if (allowedFaceIdxList.contains(face_idx) && !visited_face_index.contains(face_idx)) {
                    return hull_MESH.faces.get(face_idx);
                }
            }
            Vertex vex2 = hull_MESH.verts.get(edge1.verts[1]);
            for (int face_idx : vex2.face_keys) {
                if (allowedFaceIdxList.contains(face_idx) && !visited_face_index.contains(face_idx)) {
                    return hull_MESH.faces.get(face_idx);
                }
            }
            // We go back and visit an previously visited face.
//            if (plane.isIntersected(vex1.coord, vex2.coord, true) && !visited_edge_index.contains(edge1.index)) {
//                for (int face_idx : edge1.face_keys) {
//                    if (allowedFaceIdxList.contains(face_idx) && face_idx != face.index) {
//                        return hull_MESH.faces.get(face_idx);
//                    }
//                }
//            }
        }

        if (LOGGER.isLoggable(Level.FINE)) {

            System.err.printf("_find_next_face could not continue %d face_edges   : %s  curEdge : %d\n", face.index, face.edge_keys, edge.index);
            for (int edgIdx : face.edge_keys) {
                Edge e = hull_MESH.edges.get(edgIdx);
                Vertex v1 = hull_MESH.verts.get(e.verts[0]);
                Vertex v2 = hull_MESH.verts.get(e.verts[1]);
                System.err.printf("_find_next_face could not continue %d edge_verts   : %d:(%8.3f,%8.3f,%8.3f)  %d:(%8.3f,%8.3f,%8.3f) visited:%b  faces:%s\n", e.index, e.verts[0], v1.coord.x, v1.coord.y, v1.coord.z, e.verts[1], v2.coord.x, v2.coord.y, v2.coord.z, visited_edge_index.contains(e.index), e.face_keys);
                for (int faceIdx : e.face_keys) {
                    System.err.printf("                                   %d : face:%d  Allowed:%b , Visited:%b\n", e.index, faceIdx, allowedFaceIdxList.contains(faceIdx), visited_face_index.contains(faceIdx));
                }
            }
            for (int vertIdx : face.verts) {
                Vertex v1 = hull_MESH.verts.get(vertIdx);
                System.err.printf("_find_next_face could not continue %d :(%8.3f,%8.3f,%8.3f) faces:%s\n", v1.index, v1.coord.x, v1.coord.y, v1.coord.z, v1.face_keys);
                for (int faceIdx : v1.face_keys) {
                    System.err.printf("                                   %d : face:%d  Allowed:%b , Visited:%b\n", v1.index, faceIdx, allowedFaceIdxList.contains(faceIdx), visited_face_index.contains(faceIdx));
                }

            }
            System.err.printf("_find_next_face could not continue ,  edge_face_list:%s\nallowd:%s\nvisited_face_index:%s\nvisited_edge_index:%s\n", edge.face_keys, allowedFaceIdxList, visited_face_index, visited_edge_index);

            LOGGER.log(Level.SEVERE, "", new RuntimeException(String.format("_find_next_face could not continue ,  edge_face_list:%s  allowd:%s visited_face_index:%s", edge.face_keys, allowedFaceIdxList, visited_face_index)));
        }
        return null;
    }

    private static Face _search_nearests_intersect_face(Mesh hull_MESH, Face cur_face, java.util.Set<Integer> allowedFaceIdxList, Set<Integer> visited_face_index, Plane plane, Point3d last) {
        Face nearestFace = null;
        double lastDistance = Double.MAX_VALUE;
        for (int faceIdx : allowedFaceIdxList) {
            if (!visited_face_index.contains(faceIdx)) {
                final Face face = hull_MESH.faces.get(faceIdx);
                final Vertex vert_1 = hull_MESH.verts.get(face.verts[0]);
                final Vertex vert_2 = hull_MESH.verts.get(face.verts[1]);
                final Vertex vert_3 = hull_MESH.verts.get(face.verts[2]);
                if (plane.isIntersected(vert_1.coord, vert_2.coord, true)) {
                    Tuple3d interSect = plane.intersectSegment(vert_1.coord, vert_2.coord, true);
                    double dist = last.distanceSquared(new Point3d(interSect));
                    if (dist < lastDistance) {
                        nearestFace = face;
                        lastDistance = dist;
                    }
                }
                if (plane.isIntersected(vert_2.coord, vert_3.coord, true)) {
                    Tuple3d interSect = plane.intersectSegment(vert_2.coord, vert_3.coord, true);
                    double dist = last.distanceSquared(new Point3d(interSect));
                    if (dist < lastDistance) {
                        nearestFace = face;
                        lastDistance = dist;
                    }
                }
                if (plane.isIntersected(vert_3.coord, vert_1.coord, true)) {
                    Tuple3d interSect = plane.intersectSegment(vert_3.coord, vert_1.coord, true);
                    double dist = last.distanceSquared(new Point3d(interSect));
                    if (dist < lastDistance) {
                        nearestFace = face;
                        lastDistance = dist;
                    }
                }
            }
        }
        return nearestFace;
    }

    private static Edge _search_nearest_edge(Mesh hull_MESH, Face cur_face, Plane plane, Point3d last) {

        Edge nearestEdge = null;
        if (cur_face == null) {
            return nearestEdge;
        }
        double lastDistance = Double.MAX_VALUE;
        for (final int eIdx : cur_face.edge_keys) {
            final Edge otherEdge = hull_MESH.edges.get(eIdx);
            final Vertex vert_1 = hull_MESH.verts.get(otherEdge.verts[0]);
            final Vertex vert_2 = hull_MESH.verts.get(otherEdge.verts[1]);
            if (plane.isIntersected(vert_1.coord, vert_2.coord, true)) {
                Tuple3d interSect = plane.intersectSegment(vert_1.coord, vert_2.coord, true);
                double dist = last.distanceSquared(new Point3d(interSect));
                if (dist < lastDistance) {
                    nearestEdge = otherEdge;
                    lastDistance = dist;
                }
            }
        }
        return nearestEdge;
    }
}
