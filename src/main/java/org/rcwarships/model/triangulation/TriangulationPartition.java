/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.triangulation;

import javax.vecmath.Point3d;
import org.rcwarships.model.partition.Partition;

/**
 *
 * @author viljoenp
 */
public class TriangulationPartition extends Partition {

    public TriangulationPartition(final Partition partition) {
        super(partition.line1, partition.line1p, partition.line2, partition.line2p);

        this.line1Distance = calcDistance(line1p);
        this.line2Distance = calcDistance(line2p);
    }

    public double getLine1Distance() {
        return line1Distance.get(line1Distance.size() - 1);
    }

    public double getLine2Distance() {
        return line2Distance.get(line2Distance.size() - 1);
    }

    private static java.util.List<Double> calcDistance(final java.util.List<Point3d> line) {
        final java.util.List<Double> distance = new java.util.ArrayList<>(line.size());
        double currDist = 0.0;
        distance.add(currDist);

        for (int i = 1; i < line.size(); i++) {
            Point3d prev = line.get(i - 1);
            Point3d curr = line.get(i);
            currDist += +curr.distance(prev);// Sinse all points in line is transformed to a single value of Z this is the same a 2d distance
            distance.add(currDist);
        }
        return distance;
    }

    public final java.util.List<Double> line1Distance;
    public final java.util.List<Double> line2Distance;

}
