/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.triangulation;

import java.util.logging.Logger;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import org.rcwarships.model.Mesh;
import org.rcwarships.model.Vertex;
import org.rcwarships.model.partition.Partition;
import org.rcwarships.model.partition.PartitionFactory;

/**
 *
 * @author viljoenp
 */
public class Triangulation3d {

    private final static Logger LOGGER = Logger.getLogger(Triangulation3d.class.getName());

    /**
     * Triangulate two polygon lines ( these lines are parallel to one another
     * with respect of one of the three axis ). The goal is to create triangles
     * by inserting edges between the lines.
     *
     * @param mesh
     * @param line1
     * @param line2
     */
    public void triangulate(final Mesh mesh, final java.util.List<Vertex> line1, final java.util.List<Vertex> line2, boolean reverse) {
        triangulate(mesh, PartitionFactory.partition(line1, line2), reverse);
    }

    private void triangulate(final Mesh mesh, final java.util.List<Partition> partitions, boolean reverse) {
        for (Partition partition : partitions) {
            triangulate(mesh, new TriangulationPartition(partition), reverse);
        }
    }

    public void triangulate(final Mesh mesh, final TriangulationPartition partition, boolean reverse) {
        // Simple case 
        if ((partition.line1.size() == 2) && (partition.line2.size() == 1)) {
            triangulateClipEar1p(mesh, partition, 0, 0, reverse);
            return;
        } else if ((partition.line2.size() == 2) && (partition.line1.size() == 1)) {
            triangulateClipEar2p(mesh, partition, 0, 0, reverse);
            return;
        }
        contitueTrinagulationP(mesh, partition, 0, 0, reverse);
    }

    double distance2d(final Point3d p1, final Point3d p2) {
        final Point2d p21 = new Point2d(p1.x, p1.y);
        final Point2d p22 = new Point2d(p2.x, p2.y);
        return p21.distance(p22);
    }

    private void contitueTrinagulationP(final Mesh mesh, final TriangulationPartition partition, final int line1Start, final int line2Start, boolean reverse) {
        if ((line1Start < partition.line1.size() - 1) && (line2Start == partition.line2.size() - 1)) { // l2Curr is last point , but l1Curr is not yet last point
            // is l1Next the second last point in line1  AND l2Next the last point in line2 ?
            triangulateClipEar1p(mesh, partition, line1Start, line2Start, reverse);
        } else if ((line2Start < partition.line2.size() - 1) && (line1Start == partition.line1.size() - 1)) {  // l1Curr is last point , but l2Curr is not yet last point
            // is l2Next the second last point in line2  AND l1Next the last point in line2 ?
            triangulateClipEar2p(mesh, partition, line1Start, line2Start, reverse);
        } else if (((line1Start < partition.line1.size()) && (line2Start < partition.line2.size() - 1)) || ((line2Start < partition.line2.size()) && (line1Start < partition.line1.size() - 1))) {
            double dist1 = partition.line1Distance.get(line1Start + 1) / partition.getLine1Distance(); // Relative portion of loop that will be completed at line1Start+1
            double dist2 = partition.line2Distance.get(line2Start + 1) / partition.getLine2Distance(); // Relative portion of loop that will be completed at line2Start+1
            // Continue , we are not at the end yet
            if (dist1 <= dist2) {
                triangulateClipEar1p(mesh, partition, line1Start, line2Start, reverse);
            } else {
                triangulateClipEar2p(mesh, partition, line1Start, line2Start, reverse);
            }
        }
    }
    
    
//
//    private Vector3d sub(final Point3d p0, final Point3d p1) {
//        final Vector3d p01 = new Vector3d(p0);
//        p01.sub(p1);
//        return p01;
//    }
//
//    /**
//     * Calculates cross-product with vector v. The resulting vector is
//     * perpendicular to both the current and supplied vector and overrides the
//     * current.
//     *
//     * @param v the v
//     *
//     * @return itself
//     */
//    private final javax.vecmath.Vector3d crossSelf(final javax.vecmath.Vector3d a, javax.vecmath.Vector3d v) {
//        final double cx = a.y * v.z - v.y * a.z;
//        final double cy = a.z * v.x - v.z * a.x;
//        a.z = a.x * v.y - v.x * a.y;
//        a.x = cx;
//        a.y = cy;
//        return a;
//    }

//    private double winding(final Point3d p0, final Point3d p1, final Point3d p2) {
//        final Vector3d p01 = sub(p0, p1);
//        final Vector3d p02 = sub(p0, p2);
//        return p01.z;
//    }
//
//    private double winding1(final Point3d p0, final Point3d p1, final Point3d p2) {
//        final Vector3d p01 = sub(p0, p1);
//        final Vector3d p02 = sub(p0, p2);
//        return p01.y;
//
//    }
//
//    private double winding2(final Point3d p0, final Point3d p1, final Point3d p2) {
//        final Vector3d p01 = sub(p0, p1);
//        final Vector3d p02 = sub(p0, p2);
//        return p01.x;
//    }

    private void triangulateClipEar1p(final Mesh mesh, final TriangulationPartition partition, final int line1Start, final int line2Start, boolean reverse) {
        mesh.addEdge(partition.line1.get(line1Start), partition.line1.get(line1Start + 1));
        mesh.addEdge(partition.line1.get(line1Start + 1), partition.line2.get(line2Start));
        mesh.addEdge(partition.line2.get(line2Start), partition.line1.get(line1Start));
        final Point3d earStart = partition.line1p.get(line1Start); // We will clip this 'ear' point 
//        final Point3d l1Next = partition.line1p.get(line1Start + 1);
        final Point3d l2Next = partition.line2p.get(line2Start);
        //
//        Vector3d normal = new Vector3d(earStart.x- l2Next.x,earStart.y- l2Next.y,earStart.z - l2Next.z);// crossSelf(sub(l2Next, earStart), sub(l1Next, earStart));
//        if (winding >= 0) {
//            mesh.addFace(partition.line1.get(line1Start), partition.line2.get(line2Start), partition.line1.get(line1Start + 1));
//        } else {
//            mesh.addFace(partition.line1.get(line1Start), partition.line1.get(line1Start + 1), partition.line2.get(line2Start));
//        }
        if (earStart.z > l2Next.z) {
            if (reverse) {
                mesh.addFace(partition.line1.get(line1Start), partition.line2.get(line2Start), partition.line1.get(line1Start + 1));
            } else {
                mesh.addFace(partition.line1.get(line1Start), partition.line1.get(line1Start + 1), partition.line2.get(line2Start));
            }
        } else {
            if (reverse) {
                mesh.addFace(partition.line1.get(line1Start), partition.line1.get(line1Start + 1), partition.line2.get(line2Start));
            } else {
                mesh.addFace(partition.line1.get(line1Start), partition.line2.get(line2Start), partition.line1.get(line1Start + 1));
            }
        }
        contitueTrinagulationP(mesh, partition, line1Start + 1, line2Start, reverse);
    }

    private void triangulateClipEar2p(final Mesh mesh, final TriangulationPartition partition, final int line1Start, final int line2Start, boolean reverse) {
        mesh.addEdge(partition.line2.get(line2Start), partition.line2.get(line2Start + 1));
        mesh.addEdge(partition.line2.get(line2Start + 1), partition.line1.get(line1Start));
        mesh.addEdge(partition.line1.get(line1Start), partition.line2.get(line2Start));
        final Point3d earStart = partition.line2p.get(line2Start); // We will clip this 'ear' point 
        final Point3d l1Next = partition.line1p.get(line1Start);
//        final Point3d l2Next = partition.line2p.get(line2Start + 1);
        // 
//        Vector3d normal = new Vector3d(earStart.x- l1Next.x,earStart.x - l1Next.x ,earStart.z - l1Next.z);//crossSelf(sub(l2Next, earStart), sub(l1Next, earStart));
//        if (winding >= 0) {
//            mesh.addFace(partition.line2.get(line2Start), partition.line2.get(line2Start + 1), partition.line1.get(line1Start));
//        } else {
//            mesh.addFace(partition.line2.get(line2Start), partition.line1.get(line1Start), partition.line2.get(line2Start + 1));
//        }
        if (earStart.z > l1Next.z) {
            if (reverse) {
                mesh.addFace(partition.line2.get(line2Start), partition.line1.get(line1Start), partition.line2.get(line2Start + 1));
            } else {
                mesh.addFace(partition.line2.get(line2Start), partition.line2.get(line2Start + 1), partition.line1.get(line1Start));
            }
        } else {
            if (reverse) {
                mesh.addFace(partition.line2.get(line2Start), partition.line2.get(line2Start + 1), partition.line1.get(line1Start));
            } else {
                mesh.addFace(partition.line2.get(line2Start), partition.line1.get(line1Start), partition.line2.get(line2Start + 1));
            }
        }
        contitueTrinagulationP(mesh, partition, line1Start, line2Start + 1, reverse);
    }

}
