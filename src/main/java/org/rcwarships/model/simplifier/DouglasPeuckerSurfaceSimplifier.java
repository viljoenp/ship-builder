/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.simplifier;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import org.rcwarships.collections.IdentitySet;
import org.rcwarships.model.Mesh;
import org.rcwarships.model.Vertex;

/**
 * A description of an algorithm to simplify a surface .
 *
 * http://libraries.maine.edu/Spatial/gisweb/spatdb/egis/eg94036.html
 *
 * A new algorithm for DTM generalization The suggested method is suitable for
 * any set of Cartesian points, irregular, random orgrid sets. The basic method
 * could be used with different coordinate systems, although the calculations
 * would be different.
 *
 * Given a set of DTM points ( x, y, z ), the algorithm first creates four
 * anchor points which are the four corners of a bounding in the x y plane
 * rectangle of the DTM points, all these four points are assigned a value,
 * calculated by subtracting twice the tolerance from the lowest z height (this
 * extra factor allows the outer boundary points to be maintained).The highest
 * point of the DTM set is selected to be the fifth anchor point this creates a
 * four sided pyramid made up of four triangular surfaces. (Figure 2.) ￼
 *
 * For each triangular surface, the sub-set of points which lie within the
 * triangles x,y limits belong to that triangle. For all these points the
 * vertical distance of the point from the triangular surface is calculated
 * (i.e. the distance separating the point and the point directly below it in
 * the plane with the same x and y coordinates).
 *
 * If all these distances are within the tolerance or there are no points lying
 * inside this triangle, the triangular surface is considered adequate to
 * describe these points and this sub-set of points are ignored. If the vertical
 * height above or below the plane of any of these DTM points is greater than
 * the tolerance, then theDTM point which is furthest away from the triangle
 * becomes a new anchor point with three new triangular surfaces replacing the
 * old one. This process is carried out recursively for all triangular surfaces
 * until a set of anchor points and triangular surfaces have been created for
 * which all of the original points lie within the tolerance distance from these
 * triangular surfaces.Then the four original anchor point are deleted from the
 * set. The advantage of this algorithm over some more simple generalizations is
 * that it produces a better caricature while taking only slightly longer time.
 * The absolute difference must be computed between the DTM point and the
 * intersection point which is defined by the vertical line and the triangle.
 * Here it was not necessary to calculate perpendicular distance.
 *
 * @author viljoenp
 */
public class DouglasPeuckerSurfaceSimplifier {

    private static final Logger LOGGER = Logger.getLogger(DouglasPeuckerSurfaceSimplifier.class.getName());

    /**
     *
     * 1 Transform ( side way view ) the data of the mesh. remove all points
     * with z &lt; 0 ( we are only interested in the one half of the hull ) by
     * transforming we now have a convex hull surface. and we should be able to
     * apply the algorithm described above to the data. We will apply this
     * algorithm to the data to get a set of candidate vertexes that should be
     * retained.
     *
     * Once we have the list of vertexes we want to retain, we will apply the
     * edge-breaker algorithm to the original Mesh and retain only these
     * vertexes.
     *
     * @param mesh
     * @return
     */
    public static Mesh simplifyModel(final Mesh mesh, double tolerance) {
        // Generate a new Mesh by simplification of the surface 
        if (mesh.faces.size() <= 1) {
            return mesh;
        }

        final java.util.Map<Point3d, Vertex> subHull = new java.util.IdentityHashMap<>(mesh.verts.size());

        for (Vertex vert : mesh.verts) {
            Point3d transformed = applySideViewTransform(vert);
            if (transformed.z >= 0.0) {
                // Make a map to the original vertexes ( not this is an Identity Map ) .equals is not applied ! , 
                // this map will allow us to identify the original vertex we want to retain
                subHull.put(transformed, vert);
            }
        }

        final java.util.Collection<Point3d> simplified = simplifySurface(subHull.keySet(), tolerance);
        LOGGER.log(Level.INFO, "simplifyModel reduced SubHull {0} vertexes to {1} vetexes with tolerance {2}", new Object[]{subHull.size(), simplified.size(), tolerance});
        final java.util.Set<Integer> retainedVertexes = new java.util.HashSet<>(simplified.size());
        for (Point3d point : simplified) {
            if (subHull.containsKey(point)) {
                retainedVertexes.add(subHull.get(point).index);
            } else {
                LOGGER.log(Level.SEVERE, "simplifyModel simplified set contains a point that is not in the original set  {0}", new Object[]{point});
            }
        }
        return edgeBreakerSimplification(mesh, retainedVertexes);

    }

    private static Point3d applySideViewTransform(Vertex vertex) {
        return new Point3d(vertex.coord.y, vertex.coord.z, vertex.coord.x);
    }

    private static java.util.Collection<Point3d> simplifySurface(Set<Point3d> keySet, double tolerance) {

        /*
         * Given a set of DTM points ( x, y, z ), the algorithm first creates four
         * anchor points which are the four corners of a bounding in the x y plane
         * rectangle of the DTM points, all these four points are assigned a value,
         * calculated by subtracting twice the tolerance from the lowest z height (this
         * extra factor allows the outer boundary points to be maintained).The highest
         * point of the DTM set is selected to be the fifth anchor point this creates a
         * four sided pyramid made up of four triangular surfaces. (Figure 2.) ￼
         *
         */
        final Point3d minMax[] = new Point3d[2];
        minMax[0] = new Point3d(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        minMax[1] = new Point3d(-Double.MAX_VALUE, -Double.MAX_VALUE, -Double.MAX_VALUE);
        Point3d maxZ = null;
        for (Point3d vert : keySet) {
            // Min Values
            if (minMax[0].x > vert.x) {
                minMax[0].x = vert.x;
            }
            if (minMax[0].y > vert.y) {
                minMax[0].y = vert.y;
            }
            if (minMax[0].z > vert.z) {
                minMax[0].z = vert.z;
            }
            if (minMax[1].x < vert.x) {
                minMax[1].x = vert.x;
            }
            if (minMax[1].y < vert.y) {
                minMax[1].y = vert.y;
            }
            if (minMax[1].z < vert.z) {
                minMax[1].z = vert.z;
                maxZ = vert;
            }
        }

        final Point3d[] cornerPoints = new Point3d[]{
            new Point3d(minMax[0].x, minMax[0].y, minMax[0].z - 2 * tolerance),
            new Point3d(minMax[0].x, minMax[1].y, minMax[0].z - 2 * tolerance),
            new Point3d(minMax[1].x, minMax[1].y, minMax[0].z - 2 * tolerance),
            new Point3d(minMax[1].x, minMax[0].y, minMax[0].z - 2 * tolerance)};

        final TriangularPiramid t0 = new TriangularPiramid(cornerPoints[0], maxZ, cornerPoints[1], keySet.size() / 2);
        final TriangularPiramid t1 = new TriangularPiramid(cornerPoints[1], maxZ, cornerPoints[2], keySet.size() / 2);
        final TriangularPiramid t2 = new TriangularPiramid(cornerPoints[2], maxZ, cornerPoints[3], keySet.size() / 2);
        final TriangularPiramid t3 = new TriangularPiramid(cornerPoints[3], maxZ, cornerPoints[0], keySet.size() / 2);
        for (final Point3d p : keySet) {
            if (t0.accuratePointInTriangle(p)) {
                t0.add(p);
            } else if (t1.accuratePointInTriangle(p)) {
                t1.add(p);
            } else if (t2.accuratePointInTriangle(p)) {
                t2.add(p);
            } else {
                t3.add(p);
            }
        }
        final IdentitySet<Point3d> retain = new IdentitySet<>(keySet.size());
        t0.simplifySurface(retain, tolerance);
        t1.simplifySurface(retain, tolerance);
        t2.simplifySurface(retain, tolerance);
        t3.simplifySurface(retain, tolerance);

        LOGGER.log(Level.SEVERE, "simplifyModel remove corner  {0}", cornerPoints[0]);
        retain.remove(cornerPoints[0]);
        LOGGER.log(Level.SEVERE, "simplifyModel remove corner  {0}", cornerPoints[1]);
        retain.remove(cornerPoints[1]);
        LOGGER.log(Level.SEVERE, "simplifyModel remove corner  {0}", cornerPoints[2]);
        retain.remove(cornerPoints[2]);
        LOGGER.log(Level.SEVERE, "simplifyModel remove corner  {0}", cornerPoints[3]);
        retain.remove(cornerPoints[3]);
        return retain;
    }

    /**
     * Reconstruct original mesh, but keep only the points that is to be
     * retained
     *
     * @param mesh
     * @param retainedVertexes
     * @return
     */
    private static Mesh edgeBreakerSimplification(Mesh mesh, Set<Integer> retainedVertexes) {
        int vertex_capacity = retainedVertexes.size();
        int edges_capacity = 0;
        int faces_capacity = 0;
        final Mesh newMesh = new Mesh(mesh.name, vertex_capacity, edges_capacity, faces_capacity);

        return newMesh;
    }

}
