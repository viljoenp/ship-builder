/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.simplifier;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

import static org.rcwarships.model.ModelUtils.distanceSquared;
import static org.rcwarships.model.ModelUtils.vector;

/**
 *
 * @author viljoenp
 */
class DouglasPeuckerSimplifier {

    private static final Logger LOGGER = Logger.getLogger(DouglasPeuckerSimplifier.class.getName());

    public static <F extends Tuple3d> java.util.List<F> simplifyDouglasPeucker(final java.util.List<F> polygon, double margin) {
        // validate input and check if simplification required
        if (polygon.size() < 3 || margin <= 0) {
            return polygon;
        }
        java.util.List<F> result = new java.util.LinkedList<>();
        final boolean keys[] = approximate(polygon, margin);
        for (int i = 0; i < keys.length; i++) {
            if (keys[i]) {
                result.add(polygon.get(i));
            }
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "simplifyDouglasPeucker  reduced to  {0}  from {1} points,  delta={2}  tolerance:{3}", new Object[]{result.size(), polygon.size(), polygon.size() - result.size(), String.format("%.8f", margin)});
        }
        return result;
    }

    private static class KeyInfo {

        int index;
        double distance;
    }

    private static class SubPoly {

        int first;//! coord index of the first point
        int last;     //! coord index of the last point

        private SubPoly(final int first, final int last) {
            this.first = first;
            this.last = last;
        }
    }

    /**
     * Finds the key for the given sub polygon.
     *
     * Finds the point in the range [first, last] that is furthest away from the
     * segment (first, last). This point is called the key.
     *
     * @param polygon the polygon to search for a key
     * @param first the first coordinate of the first polygon point
     * @param last the first coordinate of the last polygon point
     * @returns the index of the key and its distance, or last when a key could
     * not be found
     */
    private static <F extends Tuple3d> KeyInfo findKey(final java.util.List<F> polygon, int first, int last) {
        final F firstP = polygon.get(first);
        final F lastP = polygon.get(last);

        final Tuple3d first_to_last = new Point3d(lastP);
        first_to_last.sub(firstP);

        final Vector3d v = vector(firstP, lastP);
        final double cv = v.lengthSquared();   // squared length of v
        final KeyInfo keyInfo = new KeyInfo();
        keyInfo.index = -1;
        keyInfo.distance = -Double.MAX_VALUE;
        for (int i = first + 1; i < last; i++) {
            final F current = polygon.get(i);
            final Vector3d w = vector(firstP, current);
            final double cw = v.dot(w);   // project w onto v
            if (cw <= 0) {
                // projection of w lies to the left of s1
                final double d2 = distanceSquared(current, firstP);
                if (d2 >= keyInfo.distance) {
                    keyInfo.distance = d2;
                    keyInfo.index = i;
                }
                continue;
            }
            if (cv <= cw) {
                // projection of w lies to the right of s2
                final double d2 = distanceSquared(current, lastP);
                if (d2 >= keyInfo.distance) {
                    keyInfo.distance = d2;
                    keyInfo.index = i;
                }
                continue;
            }
            // Beware divide by 0
            final double fraction = cw / cv;

            final Tuple3d proj = new Point3d(first_to_last);
            proj.scale(fraction);
            proj.add(firstP);

            final double d2 = distanceSquared(current, proj);
//            double d2 = Simplifier.segmentDistanceSquared(firstP, lastP, polygon.get(i));
            if (d2 >= keyInfo.distance) {
                keyInfo.distance = d2;
                keyInfo.index = i;
            }
        }
        return keyInfo;
    }

    /**
     * Performs Douglas-Peucker approximation.
     *
     * @param <F> Type of Tuple3d in polygon
     * @param polygon the Polygon to perform on
     */
    private static <F extends Tuple3d> boolean[] approximate(final java.util.List<F> polygon, final double margin) {
        final double tolSquared = margin * margin;
        final boolean keys[] = new boolean[polygon.size()];
        java.util.Arrays.fill(keys, false);
        keys[0] = true;                       // the first point is always a key
        keys[polygon.size() - 1] = true;      // the last point is always a key

        final java.util.Deque<SubPoly> stack = new java.util.LinkedList<>();
        stack.push(new SubPoly(0, polygon.size() - 1));  // add complete poly

        while (!stack.isEmpty()) {
            final SubPoly subPoly = stack.removeFirst();     // take a sub poly , and find its key
            KeyInfo keyInfo = findKey(polygon, subPoly.first, subPoly.last);
            if ((keyInfo.index >= 0) && tolSquared < keyInfo.distance) {
                // store the key if valid
                keys[keyInfo.index] = true;
                // split the polygon at the key and recurse
                stack.addFirst(new SubPoly(keyInfo.index, subPoly.last));
                stack.addFirst(new SubPoly(subPoly.first, keyInfo.index));
            }
        }
        return keys;
    }
}
