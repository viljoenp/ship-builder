/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.simplifier;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import org.rcwarships.model.Epsilon;
import org.rcwarships.model.Line;
import org.rcwarships.model.ModelUtils;

/**
 *
 * @author viljoenp
 */
public class Simplifier {

    private static final Logger LOGGER = Logger.getLogger(Simplifier.class.getName());
    public static final double STRAIGHT_LINE_ANGLE_MARGIN = Math.PI / 1000.; //  WARNING This constant might need some tweeking
    public static final double SPIKE_ANGLE_MARGIN = Math.PI - Math.PI / 60.; //  WARNING This constant might need some tweeking       
    public static double DOUGLASPEUCKERSIMPLIFIER_TOLERANCE = 0.1;//
    public static double SANDWITCHED_SHORT_SEGMENT_MARGIN = 0.1; // WARNING This constant might need some tweeking
    public static double SIMPLIFY_TOLERANCE = 0.0045;
    public static double LONG_SEGMENT_LENGTH = 76.2; // 3" in mm

    static {
        if (System.getProperty("simplify-tolerance") != null) {
            SIMPLIFY_TOLERANCE = Double.parseDouble(System.getProperty("simplify-tolerance"));
        }
        if (System.getProperty("sandwitched-short-segment-margin") != null) {
            SANDWITCHED_SHORT_SEGMENT_MARGIN = Double.parseDouble(System.getProperty("sandwitched-short-segment-margin"));
        }
        if (System.getProperty("douglaspeucker-simplifier-tolerance") != null) {
            DOUGLASPEUCKERSIMPLIFIER_TOLERANCE = Double.parseDouble(System.getProperty("douglaspeucker-simplifier-tolerance"));
        }
        if (System.getProperty("long-segment-margin") != null) {
            LONG_SEGMENT_LENGTH = Double.parseDouble(System.getProperty("long-segment-margin"));
        }
    }

    /**
     *
     * @param <F> The type of Tuples that makes the polygon
     * @param polygon The polygon that will be simplified
     * @return a new polygon that represents the contour of the original
     */
    public static <F extends Tuple3d> java.util.List<F> simplifyPolygonLineSegments(final java.util.List<F> polygon) {
        if (polygon.size() < 3) {
            return polygon;
        }
        int originalSize = polygon.size();
        final java.util.List<F> tmp = new java.util.LinkedList<>(polygon);
        reduceRadialDistance(tmp, Epsilon.E);
        removeVeryShortSegments(tmp, SANDWITCHED_SHORT_SEGMENT_MARGIN);
        mergeStraightLines(tmp, STRAIGHT_LINE_ANGLE_MARGIN);
        removeSpikeSegments(tmp, SPIKE_ANGLE_MARGIN);
        reduceRadialDistance(tmp, Epsilon.E);
        final java.util.List<F> result = DouglasPeuckerSimplifier.simplifyDouglasPeucker(tmp, DOUGLASPEUCKERSIMPLIFIER_TOLERANCE);

        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "simplifyPolygonLineSegments - {0} points from {1} points", new Object[]{result.size(), originalSize});
        }
        // Now one last thing , we dont want VERY long segments either.
        return new java.util.ArrayList<>(removeIntersectionLoops(splitVeryLongSegments(result, LONG_SEGMENT_LENGTH)));// We need list that work fast with indexes
    }

    public static <F extends Tuple3d> java.util.List<F> removeIntersectionLoops(final java.util.List<F> polygon) {
        final java.util.List<Line<F>> lines = new java.util.ArrayList<>(polygon.size());
        lines.add(new Line<>(polygon.get(0), polygon.get(1)));
        for (int i = 2; i < polygon.size(); i++) {
            int size = lines.size();
            Line<F> newLine;
            // The new line segment we add is the last tuple in checked and  polygon.get(i)
            while (removeIntersectionLoops(lines, newLine = new Line<>(lines.get(lines.size() - 1).getP1(), polygon.get(i)))) {
                if (LOGGER.isLoggable(Level.INFO)) {
                    LOGGER.log(Level.WARNING, "simplifyPolygonLineSegments - removed IntersectionLoop of {0} points", new Object[]{size - lines.size()});
                }
            }
            lines.add(newLine);
        }

        // Reconstruct the polygon from the given lineSegments
        return buildPolygon(lines);
    }

    private static <F extends Tuple3d> java.util.List<F> buildPolygon(final java.util.List<Line<F>> lines) {

        final java.util.List<F> checked = new java.util.ArrayList<>(lines.size() + 1);
        if (!lines.isEmpty()) {
            checked.add(lines.get(0).getP0());
        }
        for (int i = 0; i < lines.size(); i++) {
            checked.add(lines.get(i).getP1());
        }

        return checked;
    }

    private static <F extends Tuple3d> boolean removeIntersectionLoops(final java.util.List<Line<F>> lines, final Line<F> newLine) {

        // We start from the second last element since the last Shall always have an intersection exactly at newLine.p0  
        for (int i = lines.size() - 2; i > 0 && i >= 0; i--) {
            final Line<F> line = lines.get(i);
            Point3d intersection = line.getIntersection(newLine, true);
            if (intersection != null) {
                LOGGER.log(Level.WARNING, "Found collision  for  {0} - {1} and {2} - {3}  -- collision {4}  at {5}  size {6} ", new Object[]{line.getP0(), line.getP1(), newLine.getP0(), newLine.getP1(), intersection, i, lines.size()});
                for (int j = lines.size() - 1; j >= i; j--) {
                    lines.remove(j);
                }
                // Resplace the last line with the line.p0 , intersection
                lines.add(new Line<>(line.getP0(), makeTuple(line.getP1(), intersection)));
                return true;
            }
        }
        return false;
    }

    /**
     * After polygon simplyfication we can get problems with the triangulation
     * when we have a very long segment . Iterate through all the line segments
     * , and subdivide lineSegments that is very long.
     *
     */
    private static <F extends Tuple3d> java.util.List<F> splitVeryLongSegments(java.util.List<F> polygon, double margin) {
        if (polygon.size() < 3 || margin <= 0) {
            return polygon;
        }
        final int originsSize = polygon.size();
        for (final java.util.ListIterator<F> iter = polygon.listIterator(); iter.hasNext();) {
            final F p0 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final F p1 = iter.next();
            iter.previous(); // next loop should start with p1 again.
            splitSegment(iter, p0, p1, margin);
        }

        if (LOGGER.isLoggable(Level.FINER) && originsSize != polygon.size()) {
            LOGGER.log(Level.FINER, "splitVeryLongSegments  increased to  {0}  from {1} points,  delta={2}  tolerance:{3}", new Object[]{polygon.size(), originsSize, polygon.size() - originsSize, String.format("%.8f", margin)});
            LOGGER.log(Level.FINER, "splitVeryLongSegments  {0}", new Object[]{polygon});
        }
        return polygon;
    }

    private static <F extends Tuple3d> boolean splitSegment(final java.util.ListIterator<F> iter, final F p0, final F p1, final double margin) {
        final double dist = ModelUtils.distance(p0, p1);

        if (dist > margin + Epsilon.E * 3) {

            // sub devide a new point margin distance away from p0
            final Vector3d pp0 = new Vector3d(p1);
            pp0.sub(p0);
            pp0.normalize();
            pp0.scale(dist / 2);
            pp0.add(p0);
            F split = makeTuple(p0, pp0);
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, "splitVeryLongSegments  {0}  --   {1}    distance:{2}   margin:{3}   newPoint:{4}", new Object[]{p0, p1, dist, margin, split});
            }
            splitSegment(iter, p0, split, margin);
            iter.add(split);
            splitSegment(iter, split, p1, margin);
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private static <F extends Tuple3d> F makeTuple(F element, Tuple3d tuple) {
        if (element instanceof Point3d) {
            return (F) (new Point3d(tuple));
        } else if (element instanceof Vector3d) {
            return (F) (new Vector3d(tuple));
        } else {
            F newEl = (F) element.clone();
            newEl.set(tuple);
            return newEl;
        }
    }

    /**
     * RD is a brute-force O(n) algorithm for polygon simplification. It reduces
     * successive vertices that are clustered too closely to a single vertex,
     * called a key. The resulting keys form the simplified polygon.
     *
     * @param <F> Type of Tuple3d
     * @param polygon polygon that will be reduced
     * @param margin margin/tolerance ( distance between points )
     * @return return the list with reduced points so that no two adjacent
     * points have a distance less than margin from each other
     */
    private static <F extends Tuple3d> java.util.List<F> reduceRadialDistance(java.util.List<F> polygon, double margin) {
        if (polygon.size() < 3 || margin <= 0) {
            return polygon;
        }
        final int originsSize = polygon.size();
        // Remove very short edges, ie the distance betweeb two points are very short , but adjasent edges are not we merge the two points into one
        for (final java.util.ListIterator<F> iter = polygon.listIterator(); iter.hasNext();) {
            final Tuple3d p0 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p1 = iter.next();
            final boolean isEnd = !iter.hasNext();
            iter.previous(); // next loop should start with p1 again.
            if (ModelUtils.distance(p0, p1) < margin) {
                if (isEnd) {
                    iter.previous(); // remove p0 instead of p1
                }
                if (LOGGER.isLoggable(Level.FINER)) {
                    LOGGER.log(Level.FINER, "reduceRadialDistance  {0}  --   {1}    distance:{2}   margin:{3}", new Object[]{p0, p1, ModelUtils.distance(p0, p1), margin});
                }
                iter.remove(); // Remove current point                
            }

        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "reduceRadialDistance  reduced to  {0}  from {1} points,  delta={2}  tolerance:{3}", new Object[]{polygon.size(), originsSize, originsSize - polygon.size(), String.format("%.8f", margin)});
        }
        return polygon;
    }

    private static <F extends Tuple3d> java.util.List<F> removeVeryShortSegments(java.util.List<F> polygon, double margin) {
        if (polygon.size() < 4 || margin <= 0) {
            return polygon;
        }
        final int originsSize = polygon.size();
        final double longMargin = 5 * margin;
        // Remove very short edges, ie the distance betweeb two points are very short , but adjasent edges are not we merge the two points into one
        for (final java.util.ListIterator<F> iter = polygon.listIterator(); iter.hasNext();) {
            final Tuple3d p0 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p1 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p2 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p3 = iter.next();
            final Vector3d v12 = ModelUtils.vector(p1, p2);
            final double d1 = v12.length();

            // rewind
            iter.previous();
            iter.previous();
            iter.previous();
            if (d1 < margin) {
                final Vector3d v01 = ModelUtils.vector(p0, p1);
                final double d0 = v01.length();
                final Vector3d v23 = ModelUtils.vector(p2, p3);
                final double d2 = v23.length();
                if ((d2 > longMargin) && (d0 > longMargin)) {  // If this short line segment is between two longer segments we remove the short one.
                    p2.add(p1);
                    p2.scale(1.0 / 2.0);
                    if (LOGGER.isLoggable(Level.FINER)) {
                        LOGGER.log(Level.FINER, "removeVeryShortSegments  d0:{0}  d1:{1}  d2:{2}    angle 0-2: {3}    angle 0-1: {4} ,   angle 1-2: {5}   tolerance:{6}", new Object[]{String.format("%.8f", d0), String.format("%.8f", d1), String.format("%.8f", d2), v01.angle(v23), v01.angle(v12), v12.angle(v23), margin});
                    }
                    iter.remove();// remove p1
                }
            }
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "removeVeryShortSegments  reduced to  {0}  from {1} points,  delta={2}  tolerance:{3}", new Object[]{polygon.size(), originsSize, originsSize - polygon.size(), String.format("%.8f", margin)});
        }
        return polygon;
    }

    private static <F extends Tuple3d> java.util.List<F> removeSpikeSegments(java.util.List<F> polygon, double angle) {
        if (polygon.size() < 3 || angle <= 0) {
            return polygon;
        }
        final int originsSize = polygon.size();
        // straight 
        // Remove very short edges, ie the distance betweeb two points are very short , but adjasent edges are not we merge the two points into one
        for (final java.util.ListIterator<F> iter = polygon.listIterator(); iter.hasNext();) {
            final Tuple3d p0 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p1 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p2 = iter.next();

            iter.previous(); // Step back to p2
            iter.previous(); // Step back to p1
            final Vector3d v01 = ModelUtils.vector(p0, p1);
            final Vector3d v12 = ModelUtils.vector(p1, p2);

            double p02 = ModelUtils.distance(p0, p2);
            double p01 = ModelUtils.distance(p0, p1);
            if (v01.angle(v12) > angle) {
//                LOGGER.log(Level.INFO, "removeSpikeSegments  ===== angle 0-2: {0}  distances : {1} : {2} {3}", new Object[]{String.format("%.8f", v01.angle(v12)), p02, p01, p01 / p02});
                if (v01.angle(v12) > angle) {
                    if (p01 / p02 < 5) {
                        if (LOGGER.isLoggable(Level.FINER)) {
                            LOGGER.log(Level.FINER, "removeSpikeSegments  angle 0-2: {0}  distances : {1} : {2} ratio {3}", new Object[]{String.format("%.8f", v01.angle(v12)), p02, p01, p01 / p02});
                        }
                    } else if (LOGGER.isLoggable(Level.FINE)) {
                        if (LOGGER.isLoggable(Level.FINE)) {
                            LOGGER.log(Level.FINE, "removeSpikeSegments  angle 0-2: {0}  distances : {1} : {2} ratio {3}", new Object[]{String.format("%.8f", v01.angle(v12)), p02, p01, p01 / p02});
                        }
                    }
                    iter.remove();// remove p1
                }
            }
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "removeSpikeSegments  reduced to  {0}  from {1} points,  delta={2}  tolerance:{3}", new Object[]{polygon.size(), originsSize, originsSize - polygon.size(), String.format("%.8f", angle)});
        }

        return polygon;
    }

    private static <F extends Tuple3d> java.util.List<F> mergeStraightLines(java.util.List<F> polygon, double margin) {
        if (polygon.size() < 3 || margin <= 0) {
            return polygon;
        }
        final int originsSize = polygon.size();
        // straight 
        // Remove very short edges, ie the distance betweeb two points are very short , but adjasent edges are not we merge the two points into one
        for (final java.util.ListIterator<F> iter = polygon.listIterator(); iter.hasNext();) {
            final Tuple3d p0 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p1 = iter.next();
            if (!iter.hasNext()) {
                break;
            }
            final Tuple3d p2 = iter.next();

            iter.previous(); // Step back to p2
            iter.previous(); // Step back to p1
            final Vector3d v01 = ModelUtils.vector(p0, p1);
            final Vector3d v12 = ModelUtils.vector(p1, p2);
            if (v01.angle(v12) < margin) {
                if (LOGGER.isLoggable(Level.FINER)) {
                    LOGGER.log(Level.FINER, "mergeStraightLines  angle 0-2: {0}   margin:{1}", new Object[]{String.format("%.8f", v01.angle(v12)), String.format("%.8f", margin)});
                }
                iter.remove();// remove p1
            }
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "mergeStraightLines  reduced to  {0}  from {1} points,  delta={2}  tolerance:{3}", new Object[]{polygon.size(), originsSize, originsSize - polygon.size(), String.format("%.8f", margin)});
        }
        return polygon;
    }

}
