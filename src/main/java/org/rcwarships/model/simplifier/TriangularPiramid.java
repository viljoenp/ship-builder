/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.simplifier;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import org.rcwarships.model.Epsilon;

/**
 *
 * @author viljoenp
 */
public class TriangularPiramid {

    private static final double EPSILON = 0.001;
    private static final double EPSILON_SQUARE = EPSILON * EPSILON;

    private static final Vector3d UP_NORMAL = new Vector3d(0., 0., 1.);
    private double tolarance = 0;
    private Point3d highest = null;
    private final Point3d p0, p1, p2;
    private final double xMin, xMax, yMin, yMax, zMin, zMax;
    private final Vector3d normal;
    private final java.util.List<Point3d> points;
    private final boolean degenerate;
    private final double area;
    private final double p0_p1_squareLength;
    private final double p1_p2_squareLength;
    private final double p2_p0_squareLength;

    public TriangularPiramid(Point3d p0, Point3d p1, Point3d p2, int capacity) {
        this.p0 = p0;
        this.p1 = p1;
        this.p2 = p2;
//        this.highest = z;
        this.xMin = Math.min(p0.x, Math.min(p1.x, p2.x)) - EPSILON;
        this.xMax = Math.max(p0.x, Math.max(p1.x, p2.x)) + EPSILON;
        this.yMin = Math.min(p0.y, Math.min(p1.y, p2.y)) - EPSILON;
        this.yMax = Math.max(p0.y, Math.max(p1.y, p2.y)) + EPSILON;
        this.zMin = Math.min(p0.z, Math.min(p1.z, p2.z)) - EPSILON;
        this.zMax = Math.max(p0.z, Math.max(p1.z, p2.z)) + EPSILON;
        this.points = new java.util.ArrayList<>(capacity);
        final Vector3d p1_p0 = sub(p1, p0);             //  T.V1 - T.V0;
        final Vector3d p2_p0 = sub(p2, p0);             //  T.V2 - T.V0;
        final Vector3d p2_p1 = sub(p2, p0);
        normal = new Vector3d();
        normal.cross(p1_p0, p2_p0);                     // p1_p0*p2_p0 cross product
        degenerate = (normal.lengthSquared() <= Epsilon.E);
        area = -p1.y * p2.x + p0.y * (p2.x - p1.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y;
        p0_p1_squareLength = lenSqrt2d(p0, p1);
        p1_p2_squareLength = lenSqrt2d(p1, p2);
        p2_p0_squareLength = lenSqrt2d(p2, p0);
    }

    private double lenSqrt2d(final Point3d p0, final Point3d p1) {
        final double dx = p1.x - p0.x;
        final double dy = p1.y - p0.y;
        return (dx * dx) + (dy * dy);
    }

    /**
     * is the point p in the 2d (xy) triangle of th three points p1,p1,p2
     *
     * @param p
     * @return
     */
    public boolean pointInTriangle(final Point3d p) {
        double s = p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y;
        double t = p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y;
        if (Math.signum(s) != Math.signum(t)) {
            return false;
        }
        double sumA = area - (s + t);
        return Math.signum(s) == Math.signum(sumA);
    }

    public boolean pointInTriangleSlow(final Point3d p) {
        double s = p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y;
        double t = p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y;

        if ((s < 0) != (t < 0)) {
            return false;
        }

        double A = -p1.y * p2.x + p0.y * (p2.x - p1.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y;
        if (A < 0.0) {
            s = -s;
            t = -t;
            A = -A;
        }
        return s > 0 && t > 0 && (s + t) < A;
    }

    private boolean pointInTriangleBoundingBox(final Point3d p) {
        return p.x >= xMin && xMax >= p.x && p.y >= yMin && yMax >= p.y;
    }

    private double distanceSquarePointToSegment(final Point3d p0, final Point3d p1, final double seg_squareLength, final Point3d p) {
//        final double p1_p2_squareLength = (p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y);
        final double dotProduct = ((p.x - p0.x) * (p1.x - p0.x) + (p.y - p0.y) * (p1.y - p0.y)) / seg_squareLength;
        if (dotProduct < 0) {
            return (p.x - p0.x) * (p.x - p0.x) + (p.y - p0.y) * (p.y - p0.y);
        } else if (dotProduct <= 1) {
            double p_p1_squareLength = (p0.x - p.x) * (p0.x - p.x) + (p0.y - p.y) * (p0.y - p.y);
            return p_p1_squareLength - dotProduct * dotProduct * seg_squareLength;
        } else {
            return (p.x - p1.x) * (p.x - p1.x) + (p.y - p1.y) * (p.y - p1.y);
        }
    }

    public boolean accuratePointInTriangle(final Point3d p) {
        if (!pointInTriangleBoundingBox(p)) {
            return false;
        }
        if (pointInTriangle(p)) {
            if (!pointInTriangleSlow(p)) {
                pointInTriangleSlow(p);
                pointInTriangle(p);

                throw new RuntimeException();
            }
            return true;
        }
        if (pointInTriangleSlow(p)) {
            pointInTriangleSlow(p);
            pointInTriangle(p);
            throw new RuntimeException();
        }
        if (distanceSquarePointToSegment(p0, p1, p0_p1_squareLength, p) <= EPSILON_SQUARE) {
            return true;
        }
        if (distanceSquarePointToSegment(p1, p2, p1_p2_squareLength, p) <= EPSILON_SQUARE) {
            return true;
        }
        return distanceSquarePointToSegment(p2, p0, p2_p0_squareLength, p) <= EPSILON_SQUARE;
    }

    /**
     * For each triangular surface, the sub-set of points which lie within the
     * triangles x,y limits belong to that triangle. For all these points the
     * vertical distance of the point from the triangular surface is calculated
     * (i.e. the distance separating the point and the point directly below it
     * in the plane with the same x and y coordinates).
     *
     * @param p
     */
    public void add(final Point3d p) {
        if ((highest == null) || highest.z < p.z) {
            highest = p;
        }
        points.add(p);
        final double height = Math.abs(intersectZ(p));
        if (height > tolarance) {
            tolarance = height;
        }
    }

    public void simplifySurface(java.util.Set<Point3d> retain, double tolerance) {
        if (points.isEmpty() || this.tolarance < tolerance) {
            retain.add(p0);
            retain.add(p1);
            retain.add(p2);
        } else {
            final TriangularPiramid t0 = new TriangularPiramid(p0, highest, p1, points.size() / 2);
            final TriangularPiramid t1 = new TriangularPiramid(p1, highest, p2, points.size() / 2);
            final TriangularPiramid t2 = new TriangularPiramid(p2, highest, p0, points.size() / 2);
            for (final Point3d p : points) {
                if (t0.accuratePointInTriangle(p)) {
                    t0.add(p);
                } else if (t1.accuratePointInTriangle(p)) {
                    t1.add(p);
                } else {
                    t2.add(p);
                }
            }
            t0.simplifySurface(retain, tolerance);
            t1.simplifySurface(retain, tolerance);
            t2.simplifySurface(retain, tolerance);
        }
    }

    public java.util.List<TriangularPiramid> partition(final Point3d newPoint) {
        java.util.List<TriangularPiramid> partitions = new java.util.ArrayList<>(3);
        final TriangularPiramid t0;
        partitions.add(t0 = new TriangularPiramid(p0, newPoint, p1, points.size() / 2));
        final TriangularPiramid t1;
        partitions.add(t1 = new TriangularPiramid(p1, newPoint, p2, points.size() / 2));
        final TriangularPiramid t2;
        partitions.add(t2 = new TriangularPiramid(p2, newPoint, p0, points.size() / 2));

        for (final Point3d p : points) {
            if (t0.accuratePointInTriangle(p)) {
                t0.add(p);
            } else if (t1.accuratePointInTriangle(p)) {
                t1.add(p);
            } else {
                t2.add(p);
            }
        }
        return partitions;
    }

    private Vector3d sub(final Tuple3d a, final Tuple3d b) {
        final Vector3d ab = new Vector3d(a);
        ab.sub(b);
        return ab;
    }

    /**
     * For all these points the vertical distance of the point from the
     * triangular surface is calculated (i.e. the distance separating the point
     * and the point directly below it in the plane with the same x and y
     * coordinates).
     *
     * @param point0
     * @return
     */
    double intersectZ(final Point3d point0) {
        final Vector3d w0 = sub(point0, p0);                // R.P0 - T.V0
        double a = -normal.dot(w0);
        double b = normal.z;
        if (Math.abs(b) < Epsilon.E) {                      // ray is  parallel to triangle plane
            if (Math.abs(a) < Epsilon.E) {                  // ray lies in triangle plane            
                return 0;
            } else {
                return Double.MAX_VALUE; // return 0;       // ray disjoint from plane
            }
        }

        // get intersect point of ray with triangle plane
        return a / b;                                      // point0 + r * dir : intersect point of ray and plane  , then height is  intersect - point0 , thus we teturn r*dir , and dir is up.
    }

    public double getTolarance() {
        return tolarance;
    }

    public boolean isDegenerate() {
        return degenerate;
    }

    public boolean isEmpty() {
        return points.isEmpty();
    }

}
