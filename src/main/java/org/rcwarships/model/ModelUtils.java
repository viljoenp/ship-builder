/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;
import org.rcwarships.builder.Caprail;
import org.rcwarships.builder.Deck;
import org.rcwarships.builder.Keel;
import org.rcwarships.builder.ModelInput;
import org.rcwarships.builder.Rib;

/**
 *
 * @author viljoenp
 */
public class ModelUtils {

    private static final Logger LOGGER = Logger.getLogger(ModelUtils.class.getName());
    // Comply with BIG GUN COMBAT WARSHIPS INTERNATIONAL  construction rules 
    // Section 5. HULL CONSTRUCTION  
    // Sub-Section A. Frames (ribs)
    public static final double SANDWITCHED_SHORT_SEGMENT_MARGIN = 0.01; // WARNING This  
    public static final double STRAIGHT_LINE_ANGLE_MARGIN = Math.PI / 10000.F; // 
    public static final double RIB_WIDTH_PER_INCH_SPACING = 3.175;  // in mm    1/8th Inch
    public static final double INCH_IN_MM = 25.4;  // in mm

    public static double scale_mm_to_model_m(double mm, double scale) {
        /**
         * assuming the model was built with 1 unit = 1 m (realWorld) , scale is
         * the fraction of realWorld / modelWorld typical 1/144 mm is the
         * realWorld mm as measured on the model this method will calculate
         * units ( in realWorld ie model units ) for the given mm
         */
        //  1000/scale  should return 1 .
        return (mm / 1000.00) / scale;
    }

    public static double model_m_to_scale_mm(double m, double scale) {
        /**
         * assuming the model was built with 1 unit = 1 m (realWorld) , scale is
         * the fraction of realWorld / modelWorld typical 1/144 mm is the
         * realWorld mm as measured on the model this method will calculate
         * units ( in realWorld ie model units ) for the given mm
         *
         */
        //#  1000/scale  should return 1 .
        return (m * 1000.00) * scale;
    }

    public static Tuple3d average(final java.util.List<? extends Tuple3d> originalPoints) {
        Point3d result = new Point3d(0, 0, 0);
        for (Tuple3d p1 : originalPoints) {
            result.add(p1);
        }
        result.scale(1.0 / (double) originalPoints.size());
        return result;
    }

    public static ModelInput generateModelInput(final Mesh hull_MESH) {
        final Vertex[] minMax = hull_MESH.findMinAndMaxVertices_verts(hull_MESH.verts, new Vector3d(0, 1, 0));
        //# we will populate this list with the y Coordinates of where we want to place ribs. this allows us to costomize rib placement
        //# to customise the rib positions. print out the values and initialize it here
        //# we default to have ribs separated by 50.8 mm on the final scaled mode. note the first 3 sections and last 3 sections has more detail            
        final java.util.List<ModelUtils.RibPosition> rib_positions = ModelUtils.generate_rib_positions(hull_MESH, ModelUtils.INCH_IN_MM * 3 + 0.000001, ModelUtils.INCH_IN_MM * 2 + 0.000001, 9, 5, 10);

        // Add some super detail ribs..
        double dpos = rib_positions.get(0).y - ModelUtils.INCH_IN_MM;
        while (minMax[0].coord.y < dpos - ModelUtils.INCH_IN_MM / 4) {
            rib_positions.add(0, new ModelUtils.RibPosition(dpos, ModelUtils.RIB_WIDTH_PER_INCH_SPACING, 10));   // 10 = 10 mm
            dpos -= ModelUtils.INCH_IN_MM;
        }
        double dpos1 = rib_positions.get(rib_positions.size() - 1).y + ModelUtils.INCH_IN_MM;
        while (minMax[1].coord.y > dpos1 + ModelUtils.INCH_IN_MM / 4) {
            rib_positions.add(new ModelUtils.RibPosition(dpos1, ModelUtils.RIB_WIDTH_PER_INCH_SPACING, 10));
            dpos1 += ModelUtils.INCH_IN_MM;
        }
        for (ModelUtils.RibPosition pos : rib_positions) {
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.log(Level.FINE, "Rib position y={0} , width={1}", new Object[]{pos.y, pos.width});
            }
        }

        final ModelInput input = new ModelInput();
        input.setCaprailInfo(new Caprail());
        input.getCaprailInfo().setL1Extrusion(13);
        input.getCaprailInfo().setL2Extrusion(22);
        input.getCaprailInfo().setWidth(ModelUtils.RIB_WIDTH_PER_INCH_SPACING * 3); // 3/8th Inch
        input.setKeelInfo(new Keel());
        input.getKeelInfo().setThickness(10);
        input.getKeelInfo().setWidth(ModelUtils.RIB_WIDTH_PER_INCH_SPACING * 3); // 3/8th Inch
        input.setDeckPlate(new Deck());
        input.getDeckPlate().setExtrusion(10.5);
        input.getDeckPlate().setThickness((ModelUtils.RIB_WIDTH_PER_INCH_SPACING * 3) / 2);

        input.setRibs(new java.util.ArrayList<Rib>(rib_positions.size()));
        for (ModelUtils.RibPosition pos : rib_positions) {
            Rib rib = new Rib();
            rib.setPosition(pos.y);
            rib.setThickness(pos.thickness);
            rib.setWidth(pos.width);
            input.getRibs().add(rib);
        }
        return input;
    }

    /**
     *
     */
    public static class KeelInfo {

        public KeelInfo(final double width, final double thickness) {
            this.width = width;
            this.thickness = thickness;
        }
        /**
         * Width of the impenetrable area exposed to the hull
         */
        public final double width;
        /**
         * Extrusion extrusion inward from the hull
         */
        public final double thickness;
        // Deck Notch at max y
        public double max_z;
        // Deck Notch at max y
        public double max_y;
        // Deck Notch at min y
        public double min_z;
        // Deck Notch at min y
        public double min_y;
    }

    /**
     *
     */
    public static class CapRailInfo {

        public CapRailInfo(final double width, final double l1Extrusion, final double l2Extrusion) {
            this.width = width;
            this.l1Extrusion = l1Extrusion;
            this.l2Extrusion = l2Extrusion;
        }
        /**
         * Width of the impenetrable area exposed to the hull
         */
        public final double width;
        /**
         * Level1 Extrusion extrusion inward from the hull
         */
        public final double l1Extrusion;
        public final double l2Extrusion;
        // Deck Notch at max y
        public double max_z;
        // Deck Notch at max y
        public double max_y;
        // Deck Notch at min y
        public double min_z;
        // Deck Notch at min y
        public double min_y;
    }

    /**
     *
     */
    public static class DeckInfo {

        public DeckInfo(final double thickness, final double extrusion) {
            this.thickness = thickness;
            this.extrusion = extrusion;
        }
        /**
         * Width of the impenetrable area exposed to the hull
         */
        public final double thickness;
        /**
         * Extrusion extrusion inward from the hull
         */
        public final double extrusion;
        // Deck Notch at max y
        public double max_z;
        // Deck Notch at max y
        public double max_y;
        // Deck Notch at min y
        public double min_z;
        // Deck Notch at min y
        public double min_y;
    }

    /**
     *
     */
    public static class RibPosition {

        public RibPosition(final double y, final double width, final double thickness) {
            this.y = y;
            this.width = width;
            this.thickness = thickness;
        }
        /**
         * Position
         */
        public final double y;
        /**
         * Width of the impenetrable area exposed to the hull
         */
        public final double width;
        /**
         * Extrusion extrusion inward from the hull
         */
        public final double thickness;
        // Keel Notch
        public double keel_z;
        // Deck Notch
        public double deck_x;
        // Deck Notch
        public double deck_z;
    }

    public static java.util.List<RibPosition> generate_rib_positions(Mesh hull_MESH, double normal_rib_spacing, double detail_rib_spacing, double bow_detailed_sections, double stern_detailed_sections, double extrusion) {
        /**
         * hull_MESH the Hull mesh we have to generate rib positions for
         * rib_spacing_model_mm is the distance between ribs as it will be after
         * scaling and measured in mm (millimeters)
         */
        final Vertex[] min_max_y = hull_MESH.findMinAndMaxVertices_verts(hull_MESH.verts, new Vector3d(0, 1, 0));

        // Comply with BIG GUN COMBAT WARSHIPS INTERNATIONAL  construction rules 
        // Section 5. HULL CONSTRUCTION  
        // Sub-Section A. Frames (ribs)
        double detail_spacing = Math.max(INCH_IN_MM, detail_rib_spacing);
        LOGGER.log(Level.INFO, "generate_rib_positions - Detail Spacing will be {0}mm, {1}\"", new Object[]{detail_spacing, Math.floor(Math.max(INCH_IN_MM, detail_rib_spacing) / INCH_IN_MM)});
        double normal_spacing = Math.max(INCH_IN_MM, normal_rib_spacing);
        LOGGER.log(Level.INFO, "generate_rib_positions - Normal Spacing will be {0}mm, {1}\"", new Object[]{normal_spacing, Math.floor(Math.max(INCH_IN_MM, normal_rib_spacing) / INCH_IN_MM)});

        double detail_rib_thickness = Math.min(RIB_WIDTH_PER_INCH_SPACING * 3, RIB_WIDTH_PER_INCH_SPACING * Math.floor(Math.max(INCH_IN_MM, detail_rib_spacing) / INCH_IN_MM));
        LOGGER.log(Level.INFO, "generate_rib_positions - Detail ribs will be {0}mm, {1}units", new Object[]{detail_rib_thickness, detail_rib_thickness / RIB_WIDTH_PER_INCH_SPACING});
        double normal_rib_thickness = Math.min(RIB_WIDTH_PER_INCH_SPACING * 3, RIB_WIDTH_PER_INCH_SPACING * Math.floor(Math.max(INCH_IN_MM, normal_rib_spacing) / INCH_IN_MM));
        LOGGER.log(Level.INFO, "generate_rib_positions - Normal ribs will be {0}mm, {1}units", new Object[]{normal_rib_thickness, normal_rib_thickness / RIB_WIDTH_PER_INCH_SPACING});

        double start_y = min_max_y[0].coord.y; //# usualy 0.0 , this is the stern (or the bow)..
        double end_y = min_max_y[1].coord.y;
        java.util.List<RibPosition> rib_positions = new java.util.ArrayList<>();
        double start_detail_end = start_y + (stern_detailed_sections * detail_spacing);
        double end_detail_start = end_y - (bow_detailed_sections * detail_spacing);
        // # we start with some high detail rib positions. every world_spacing/4  units.
        while (start_y <= start_detail_end) {
            start_y = start_y + detail_spacing;
            rib_positions.add(new RibPosition(start_y, detail_rib_thickness, extrusion));
        }
        //# the main/normal detail sections
        start_y = start_y + normal_spacing;
        do {
            rib_positions.add(new RibPosition(start_y, normal_rib_thickness, extrusion));
            start_y = start_y + normal_spacing;
        } while (start_y <= end_detail_start);

        //# we end with some high detail rib positions. every world_spacing/4  units.
        while (start_y < end_y - detail_spacing) {
            rib_positions.add(new RibPosition(start_y, detail_rib_thickness, extrusion));
            start_y = start_y + detail_spacing;
        }
        return rib_positions;
    }

    public static java.util.List<Vertex> makeList(Mesh mesh, java.util.List<Vertex>... points) {
        int size = 0;
        for (java.util.List<Vertex> list : points) {
            size += list.size();
        }
        final java.util.List<Vertex> list = new java.util.ArrayList<>(size);
        for (java.util.List<Vertex> pointList : points) {
            if (!list.isEmpty() && !pointList.isEmpty() && pointList.get(0).index == list.get(list.size() - 1).index) {
                list.remove(list.size() - 1);
            }
            list.addAll(pointList);
        }
        if (!list.isEmpty() && list.get(0).index == list.get(list.size() - 1).index) {
            list.remove(list.size() - 1);
        }
        return list;
    }

    public static <F extends Tuple3d> F copy(final F point) {
        if (point instanceof Point3d) {
            return (F) new Point3d(point);
        } else if (point instanceof Vector3d) {
            return (F) (new Vector3d(point));
        } else {
            F newEl = (F) point.clone();
            newEl.set(point);
            return newEl;
        }
    }

    public static Vector3d vector(final Tuple3d p1, final Tuple3d p2) {
        final Vector3d vdist = new Vector3d(p2);
        vdist.sub(p1);
        return vdist;
    }

    public static double distance(final Tuple3d p1, final Tuple3d p2) {
        return vector(p1, p2).length();
    }

    public static double distanceSquared(final Tuple3d p1, final Tuple3d p2) {
        return vector(p1, p2).lengthSquared();
    }

    /**
     * Computes the squared distance between a line segment (s1, s2) and a point
     * p
     *
     * @param s1 the first coordinate of the start point of the segment
     * @param s2 the first coordinate of the end point of the segment
     * @param p the first coordinate of the test point
     * @return the squared distance
     */
    public static double segmentDistanceSquared(Tuple3d s1, Tuple3d s2, Tuple3d p) {
        final Vector3d v = vector(s1, s2);
        final Vector3d w = vector(s1, p);
        final double cw = w.dot(v);   // project w onto v
        if (cw <= 0) {
            // projection of w lies to the left of s1
            return distanceSquared(p, s1);
        }
        final double cv = v.lengthSquared();   // squared length of v
        if (cv <= cw) {
            // projection of w lies to the right of s2
            return distanceSquared(p, s2);
        }
        // Beware divide by 0
        final double fraction = cw / cv;
        final Tuple3d proj = interpolate(s1, s2, fraction);
        return distanceSquared(p, proj);
    }

    public static Tuple3d interpolate(final Tuple3d p1, final Tuple3d p2, double fraction) {
        final Tuple3d v = new Point3d(p2);
        v.sub(p1);
        v.scale(fraction);
        v.add(p1);
        return v;
    }

//
//    public static double[] get_mesh_min_max_y(Mesh hull_MESH) {
//        /**
//         * hull_MESH is the bpy.types.Mesh Object of the ship hull we will
//         * return an list of two floats , the min value of any Vertex on the y
//         * axis and the max value of ant Vertex on the y axis
//         *
//         */
//        double minY = Double.MAX_VALUE;
//        double maxY = Double.MIN_VALUE;
//        for (Vertex v : hull_MESH.verts) {
//            double val = v.coord.y;
//            if (val < minY) {
//                minY = val;
//            }
//            if (val > maxY) {
//                maxY = val;
//            }
//        }
//
//        return new double[]{minY, maxY};
//    }
    public static java.util.Map<Integer, java.util.Set<Integer>> get_edge_face_map(Mesh hull_MESH) {
        /**
         * return the reverse lookup map for edge to face list of the given Mesh
         *
         */
        java.util.Map<Integer, java.util.Set<Integer>> edgeFaceMap = new java.util.HashMap<>();

        for (Edge edge : hull_MESH.edges) {
            edgeFaceMap.put(edge.index, new java.util.HashSet<Integer>());
        }

        //# now if the edge indexies are correct we have an entry in the edgeFaceList for everty edge.index
        for (Face face : hull_MESH.faces) {
            for (int edge_key : face.edge_keys) {
                if ((!edgeFaceMap.containsKey(edge_key)) || (edge_key < 0)) {
                    throw new RuntimeException(String.format("Invalid edge_key %d in Face %d of original hull mesh", edge_key, face.index));
                }
                edgeFaceMap.get(edge_key).add(face.index);
            }

        }
        return edgeFaceMap;
    }

    public static java.util.Map<Integer, java.util.Set<Integer>> get_vertex_edge_map(Mesh hull_MESH) {
        /*
         return the reverse lookup map for vertex to edge list of the given Mesh
         */
        java.util.Map<Integer, java.util.Set<Integer>> vertexEdgeMap = new java.util.HashMap<>();
        for (Vertex vert : hull_MESH.verts) {
            vertexEdgeMap.put(vert.index, new java.util.HashSet<Integer>());
        }
        //
        // Before we can select the needed edges , we need to count the number of faces that reference each edge
        for (Edge edge : hull_MESH.edges) {
            for (int vert : edge.verts) {
                if ((!vertexEdgeMap.containsKey(vert)) || (vert < 0)) {
                    throw new RuntimeException(String.format("Invalid vert %d in edge %d of original hull mesh", vert, edge.index));
                }
                vertexEdgeMap.get(vert).add(edge.index);
                //#
            }
        }
        return vertexEdgeMap;
    }

    public static int _get_shared_edge(Vertex v1, Vertex v2) {
        for (int edge_idx : v1.edge_keys) {
            if (v2.edge_keys.contains(edge_idx)) {
                return edge_idx;
                //
            }
            //
        }
        throw new RuntimeException(String.format("Given vertex lists %d:%s  and  %d:%s does not share an edge", v1.index, v1.edge_keys, v2.index, v2.edge_keys));
    }

    public static Point3d[] find_min_and_max_vertices(java.util.List<Point3d> vert_list, Vector3d axis) {
        Point3d MinMaxVertex[] = new Point3d[2];
        MinMaxVertex[0] = vert_list.get(0);
        MinMaxVertex[1] = vert_list.get(0);
        double dot1 = -Double.MAX_VALUE;
        double dot0 = Double.MAX_VALUE;

        for (Point3d vert : vert_list) {
            double vdot = axis.dot(new Vector3d(vert));
            if (vdot > dot1) {
                MinMaxVertex[1] = vert;
                dot1 = vdot;
            }
            if (vdot < dot0) {
                MinMaxVertex[0] = vert;
                dot0 = vdot;
            }
        }

        return MinMaxVertex;

    }

    public static Point3d[] find_min_and_max_values(java.util.Collection<Point3d> vert_list) {
        Point3d minMaxVertex[] = new Point3d[2];
        minMaxVertex[0] = new Point3d(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        minMaxVertex[1] = new Point3d(-Double.MAX_VALUE, -Double.MAX_VALUE, -Double.MAX_VALUE);
        for (Point3d vert : vert_list) {
            // Min Values
            if (minMaxVertex[0].x > vert.x) {
                minMaxVertex[0].x = vert.x;
            }
            if (minMaxVertex[0].y > vert.y) {
                minMaxVertex[0].y = vert.y;
            }
            if (minMaxVertex[0].z > vert.z) {
                minMaxVertex[0].z = vert.z;
            }
            if (minMaxVertex[1].x < vert.x) {
                minMaxVertex[1].x = vert.x;
            }
            if (minMaxVertex[1].y < vert.y) {
                minMaxVertex[1].y = vert.y;
            }
            if (minMaxVertex[1].z < vert.z) {
                minMaxVertex[1].z = vert.z;
            }
        }
        return minMaxVertex;
    }

    /**
     * Build a single surface between the two lines. line1 , and line2 //
     * http://www.cgal.org
     * http://groups.csail.mit.edu/graphics/classes/6.838/F01/lectures/PolygonTriangulation/index.html
     * http://cse.wustl.edu/Research/Lists/Technical%20Reports/Attachments/993/pringleSGP.pdf
     * in 1990 a very complicated deterministic linear (O(n)) polygon
     * triangulation algorithm was given by Chazelle
     *
     * @param tmp_MESH
     * @param vertexLine1
     * @param vertexLine2
     * @param reverse reverse the face orientation
     */
    public static void buildMeshSurfaceAlongYAxis(final Mesh tmp_MESH, final java.util.List<Vertex> vertexLine1, final java.util.List<Vertex> vertexLine2, boolean reverse) {
        // NOTE if we add more vertexes to any of these lines , we have to subdivide any face that already share the 'edege' where we insert lines...
        final java.util.List<Vertex> vertexLine2r = new java.util.ArrayList<>(vertexLine2);
        java.util.Collections.reverse(vertexLine2r);
        TriangulateInYZPlane.triangulate(tmp_MESH, vertexLine1, vertexLine2r, reverse);
    }

    /**
     * Build a single surface between the two lines. line1 , and line2 //
     * http://www.cgal.org
     * http://groups.csail.mit.edu/graphics/classes/6.838/F01/lectures/PolygonTriangulation/index.html
     * http://cse.wustl.edu/Research/Lists/Technical%20Reports/Attachments/993/pringleSGP.pdf
     * in 1990 a very complicated deterministic linear (O(n)) polygon
     * triangulation algorithm was given by Chazelle
     *
     * @param tmp_MESH
     * @param vertexLine1
     * @param vertexLine2
     * @param reverse reverse the face orientation
     */
    public static void buildMeshSurfaceAlongXAxis(final Mesh tmp_MESH, final java.util.List<Vertex> vertexLine1, final java.util.List<Vertex> vertexLine2, boolean reverse) {
        // NOTE if we add more vertexes to any of these lines , we have to subdivide any face that already share the 'edege' where we insert lines...
        final java.util.List<Vertex> vertexLine2r = new java.util.ArrayList<>(vertexLine2);
        java.util.Collections.reverse(vertexLine2r);
        TriangulateInXZPlane.triangulate(tmp_MESH, vertexLine1, vertexLine2r, reverse);
    }
//
//    /**
//     * Build a single surface between the two lines. line1 , and line2 //
//     * http://www.cgal.org
//     * http://groups.csail.mit.edu/graphics/classes/6.838/F01/lectures/PolygonTriangulation/index.html
//     * http://cse.wustl.edu/Research/Lists/Technical%20Reports/Attachments/993/pringleSGP.pdf
//     * in 1990 a very complicated deterministic linear (O(n)) polygon
//     * triangulation algorithm was given by Chazelle
//     *
//     * @param tmp_MESH
//     * @param vertexLine1
//     * @param vertexLine2
//     * @param reverse reverse the face orientation
//     */
//    public static void buildMeshSurfaceAlongXAxis(final Mesh tmp_MESH, final java.util.List<Vertex> vertexLine1,final java.util.List<Vertex> vertexLine2, boolean reverse) {
//        final java.util.List<Vertex> vertexLine2r = new java.util.ArrayList<Vertex>(vertexLine2);
//        java.util.Collections.reverse(vertexLine2r);
//        TriangulateInXZPlane.triangulate(tmp_MESH, vertexLine1, vertexLine2r,reverse);
//    }

    /**
     * Add a point to the mesh after applying the mirror to it, mirror should be
     * a unary vertex
     *
     * @param tmp_MESH the Mesh object where the vertex will be added to.
     * @param line The list of Points to be added as vertexes
     * @param mirror Apply this mirror vertex before adding the point
     * @return The list of Mesh Vertexes as they where added.
     */
    public static java.util.List<Vertex> addMeshPoints(final Mesh tmp_MESH, final java.util.List<Point3d> line, final Vector3d mirror) {
        final java.util.List<Vertex> vertexLine = new java.util.ArrayList<>(line.size());
        for (Point3d point : line) {
            vertexLine.add(tmp_MESH.addVertex(new Point3d(point.x * mirror.x, point.y * mirror.y, point.z * mirror.z)));
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Added {0} vertexies te mesh from {1} point", new Object[]{vertexLine.size(), line.size()});
        }
        return vertexLine;
    }

    public static java.util.List<Point3d> deepCopy(final java.util.List<Point3d> line) {
        final java.util.List<Point3d> copy = new java.util.ArrayList<>(line.size());
        for (Point3d point : line) {
            copy.add(new Point3d(point));
        }
        return copy;
    }

    /**
     * Build a single surface between the two lines. line1 , and line2
     *
     * @param tmp_MESH
     * @param vertexLine1
     * @param vertexLine2
     * @param reverse reverse the face orientation
     */
    public static void buildMeshSurfaceAlongMirroredLinesAlongYAxis(final Mesh tmp_MESH, final java.util.List<Vertex> vertexLine1, final java.util.List<Vertex> vertexLine2, boolean reverse) {
        // NOTE if we add more vertexes to any of these lines , we have to subdivide any face that already share the 'edege' where we insert lines...

        if (vertexLine1.size() != vertexLine2.size()) {
            throw new IllegalArgumentException("The two vertex lines should be mirror images , and thus should have the same size  l1:" + vertexLine1.size() + " l2:" + vertexLine2.size());
        }

        for (int i = 0; i < vertexLine1.size() - 1; i++) {
            // The two opposite two faces should always be in the same plane.
            final Vertex vtx1 = vertexLine1.get(i);
            final Vertex vtx2 = vertexLine2.get(i);
            if (Math.abs(vtx1.coord.x + vtx2.coord.x) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + i + " does not seem to be mirror images.   v1:" + vtx1.coord + " v2:" + vtx2.coord + " ");
            }
            if (Math.abs(vtx1.coord.y - vtx2.coord.y) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + i + " does not seem to be mirror images.   v1:" + vtx1.coord + " v2:" + vtx2.coord + " ");
            }
            if (Math.abs(vtx1.coord.z - vtx2.coord.z) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + i + " does not seem to be mirror images.   v1:" + vtx1.coord + " v2:" + vtx2.coord + " ");
            }
            final Vertex vtx1_2 = vertexLine1.get(i + 1);
            final Vertex vtx2_2 = vertexLine2.get(i + 1);
            if (Math.abs(vtx1_2.coord.x + vtx2_2.coord.x) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + (i + 1) + " does not seem to be mirror images.   v1:" + vtx1_2.coord + " v2:" + vtx2_2.coord + " ");
            }
            if (Math.abs(vtx1_2.coord.y - vtx2_2.coord.y) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + (i + 1) + " does not seem to be mirror images.   v1:" + vtx1_2.coord + " v2:" + vtx2_2.coord + " ");
            }
            if (Math.abs(vtx1_2.coord.z - vtx2_2.coord.z) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + (i + 1) + " does not seem to be mirror images.   v1:" + vtx1_2.coord + " v2:" + vtx2_2.coord + " ");
            }
            tmp_MESH.addEdge(vtx1, vtx2);
            tmp_MESH.addEdge(vtx2, vtx1_2);
            tmp_MESH.addEdge(vtx1_2, vtx1);
            tmp_MESH.addEdge(vtx2, vtx2_2);
            tmp_MESH.addEdge(vtx2_2, vtx1_2);
            tmp_MESH.addEdge(vtx1_2, vtx2);
            if (reverse) {
                tmp_MESH.addFace(vtx2, vtx1, vtx1_2);
                tmp_MESH.addFace(vtx2_2, vtx2, vtx1_2);
            } else {
                tmp_MESH.addFace(vtx1, vtx2, vtx1_2);
                tmp_MESH.addFace(vtx2, vtx2_2, vtx1_2);
            }
        }
    }

    /**
     * Build a single surface between the two lines. line1 , and line2
     *
     * @param tmp_MESH
     * @param vertexLine1
     * @param vertexLine2
     * @param reverse reverse the face orientation
     */
    public static void buildMeshSurfaceAlongMirroredLinesAlongXAxis(final Mesh tmp_MESH, final java.util.List<Vertex> vertexLine1, final java.util.List<Vertex> vertexLine2, boolean reverse) {
        // NOTE if we add more vertexes to any of these lines , we have to subdivide any face that already share the 'edege' where we insert lines...

        if (vertexLine1.size() != vertexLine2.size()) {
            throw new IllegalArgumentException("The two vertex lines should be mirror images , and thus should have the same size");
        }

        for (int i = 0; i < vertexLine1.size() - 1; i++) {
            // The two opposite two faces should always be in the same plane.
            final Vertex vtx1 = vertexLine1.get(i);
            final Vertex vtx2 = vertexLine2.get(i);
            if (Math.abs(vtx1.coord.x - vtx2.coord.x) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + i + " does not seem to be mirror images.   v1:" + vtx1.coord + " v2:" + vtx2.coord + " ");
            }
//            if (Math.abs(vtx1.coord.y - vtx2.coord.y) > Epsilon.E) {
//                throw new IllegalArgumentException("The two opposite vertexes at index " + i + " does not seem to be mirror images.   v1:" + vtx1.coord + " v2:" + vtx2.coord + " ");
//            }
            // NOTE we exlude mirror check for 1st vertex ( Ribs, the Z  does not always match
            if ((Math.abs(vtx1.coord.z - vtx2.coord.z) > Epsilon.E) && (i != 0)) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + i + " does not seem to be mirror images.   v1:" + vtx1.coord + " v2:" + vtx2.coord + " ");
            }
            final Vertex vtx1_2 = vertexLine1.get(i + 1);
            final Vertex vtx2_2 = vertexLine2.get(i + 1);
            if (Math.abs(vtx1_2.coord.x - vtx2_2.coord.x) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + (i + 1) + " does not seem to be mirror images.   v1:" + vtx1_2.coord + " v2:" + vtx2_2.coord + " ");
            }
//            if (Math.abs(vtx1_2.coord.y - vtx2_2.coord.y) > Epsilon.E) {
//                throw new IllegalArgumentException("The two opposite vertexes at index " + (i + 1) + " does not seem to be mirror images.   v1:" + vtx1_2.coord + " v2:" + vtx2_2.coord + " ");
//            }
            if (Math.abs(vtx1_2.coord.z - vtx2_2.coord.z) > Epsilon.E) {
                throw new IllegalArgumentException("The two opposite vertexes at index " + (i + 1) + " does not seem to be mirror images.   v1:" + vtx1_2.coord + " v2:" + vtx2_2.coord + " ");
            }
            tmp_MESH.addEdge(vtx1, vtx2);
            tmp_MESH.addEdge(vtx2, vtx1_2);
            tmp_MESH.addEdge(vtx1_2, vtx1);
            tmp_MESH.addEdge(vtx2, vtx2_2);
            tmp_MESH.addEdge(vtx2_2, vtx1_2);
            tmp_MESH.addEdge(vtx1_2, vtx2);
            if (reverse) {
                tmp_MESH.addFace(vtx2, vtx1, vtx1_2);
                tmp_MESH.addFace(vtx2_2, vtx2, vtx1_2);
            } else {
                tmp_MESH.addFace(vtx1, vtx2, vtx1_2);
                tmp_MESH.addFace(vtx2, vtx2_2, vtx1_2);
            }
        }
    }

    public static String dumpEdge(final Mesh hull_MESH, final Edge edge) {
        return dumpEdge(hull_MESH, edge, 0);
    }

    public static String dumpFace(final Mesh hull_MESH, final Face face) {
        return dumpFace(hull_MESH, face, 0);
    }

    private static String indent(int indent) {
        final StringBuilder sbuf = new StringBuilder();
        for (int i = 0; i < indent + 1; i++) {
            sbuf.append("\t");
        }
        return sbuf.toString();
    }

    public static String dumpVertex(final Mesh hull_MESH, final Vertex vertex, int indent) {
        final StringBuilder sbuf = new StringBuilder();
        sbuf.append("V(\n");
        sbuf.append(indent(indent)).append("index:").append(vertex.index).append("\n");
        sbuf.append(indent(indent)).append("x:").append(vertex.coord.x).append("\n");
        sbuf.append(indent(indent)).append("y:").append(vertex.coord.y).append("\n");
        sbuf.append(indent(indent)).append("z:").append(vertex.coord.z).append("\n");
        sbuf.append(indent(indent)).append("edges:").append(vertex.edge_keys).append("\n");
        sbuf.append(indent(indent)).append("faces:").append(vertex.face_keys).append("\n");
        sbuf.append(indent(indent)).append("flags:");
        for (Vertex.Flag flag : Vertex.Flag.values()) {
            if (vertex.isSet(flag)) {
                sbuf.append(flag).append(" ");
            }
        }
        sbuf.append("\n");
        sbuf.append(indent(indent)).append(")");
        return sbuf.toString();
    }

    public static String dumpEdge(final Mesh hull_MESH, final Edge edge, int indent) {
        final StringBuilder sbuf = new StringBuilder();
        sbuf.append("E(\n");
        sbuf.append(indent(indent)).append("index:").append(edge.index).append("\n");
        sbuf.append(indent(indent)).append("vert[0]:").append(dumpVertex(hull_MESH, hull_MESH.verts.get(edge.verts[0]), indent + 1)).append("\n");
        sbuf.append(indent(indent)).append("vert[1]:").append(dumpVertex(hull_MESH, hull_MESH.verts.get(edge.verts[1]), indent + 1)).append("\n");
        sbuf.append(indent(indent)).append("faces:").append(edge.face_keys).append("\n");
        sbuf.append(indent(indent)).append("flags:");
        if (edge.isManifoldEdge()) {
            sbuf.append("MANIFOLD_EDGE ");
        }
        sbuf.append("\n");
        sbuf.append(indent(indent)).append(")");
        return sbuf.toString();
    }

    public static String dumpFace(final Mesh hull_MESH, final Face face, int indent) {
        final StringBuilder sbuf = new StringBuilder();
        sbuf.append("F(\n");
        sbuf.append(indent(indent)).append("index:").append(face.index).append("\n");
        sbuf.append(indent(indent)).append("vert[0]:").append(dumpVertex(hull_MESH, hull_MESH.verts.get(face.verts[0]), indent + 1)).append("\n");
        sbuf.append(indent(indent)).append("vert[1]:").append(dumpVertex(hull_MESH, hull_MESH.verts.get(face.verts[1]), indent + 1)).append("\n");
        sbuf.append(indent(indent)).append("vert[2]:").append(dumpVertex(hull_MESH, hull_MESH.verts.get(face.verts[2]), indent + 1)).append("\n");
        sbuf.append(indent(indent)).append("edges:").append(face.edge_keys).append("\n");
        sbuf.append(indent(indent)).append(")");
        return sbuf.toString();
    }
}
