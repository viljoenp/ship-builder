/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

/**
 *
 * @author viljoenp
 */
public class Edge {

    public Edge(int v1_index, int v2_index) {
        this.verts[0] = v1_index;
        this.verts[1] = v2_index;
    }

    public Edge(Vertex v1, Vertex v2) {
        this(v1.index, v2.index);
    }

    public Edge() {
        this(-1, -1);
    }

    public long makeVertexHash() {
        return makeVertexHash(this.verts[0], this.verts[1]);
    }

    public static long makeVertexHash(int idx0, int idx1) {
        if (idx0 < idx1) {
            return (((long) idx0) << 32) | ((long) idx1);
        } else {
            return (((long) idx1) << 32) | ((long) idx0);
        }
    }

    /**
     * check if this edge is on the edge of a manifold
     *
     * @return (if this edge is used by a face and only by one face )
     */
    public boolean isManifoldEdge() {
        return face_keys.size() > 0 && face_keys.size() < 2;
    }

    public Integer[] getFaces() {
        return face_keys.toArray(new Integer[face_keys.size()]);
    }

    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder();
        buf.append("E(").append(index).append(",  v(").append(verts[0]).append(",").append(verts[1]).append(")  f:(").append(face_keys).append("))");
        return buf.toString();
    }
    public final int verts[] = new int[]{-1, -1};
    public int index = -1;
    public double smoothness = -1;
    public final java.util.Set<Integer> face_keys = new ArraySet<>();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Edge other = (Edge) obj;
        return this.index == other.index;
    }

}
