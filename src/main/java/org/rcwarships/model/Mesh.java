/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3d;

/**
 *
 * @author viljoenp
 */
public class Mesh {

    private static final Logger LOGGER = Logger.getLogger(Mesh.class.getName());

    public Mesh(final String _name) {
        this(_name, 1000, 1000, 1000);
    }

    public Mesh(final String _name, final int vertex_capacity, final int edges_capacity, final int faces_capacity) {
        this.name = _name;
        this.faces = new java.util.ArrayList<>(faces_capacity);
        this.edges = new java.util.ArrayList<>(edges_capacity);
        this.verts = new java.util.ArrayList<>(vertex_capacity);
        this.edgeHashIDX = new java.util.HashMap<>(edges_capacity);
    }

    private static int getMaxIdx(final Vertex[] verts) {
        int maxIdx = Integer.MIN_VALUE;
        for (Vertex vert : verts) {
            maxIdx = Math.max(maxIdx, vert.index);
        }
        return maxIdx;
    }

    public Mesh(final String _name, final Vertex[] verts, int[][] faces) {
        this(_name, verts.length, faces.length * 3 / 2, faces.length);
        final int[] vertMap = new int[getMaxIdx(verts) + 1];
        for (Vertex vert : verts) {
            vertMap[vert.index] = this.verts.size();
            Vertex newVertex = new Vertex(vert.coord, vert.normal);
            newVertex.index = this.verts.size();
            this.verts.add(newVertex);

        }
        for (final int[] faceverts : faces) {
            Vertex v0 = this.verts.get(vertMap[faceverts[0]]);
            Vertex v1 = this.verts.get(vertMap[faceverts[1]]);
            Vertex v2 = this.verts.get(vertMap[faceverts[2]]);
            addEdge(v0, v1);
            addEdge(v1, v2);
            addEdge(v2, v0);
            addFace(v0, v1, v2);
        }
    }

    public void removeVertex(final Vertex tmpVertex) {
        if ((this.verts.size() < tmpVertex.index) || (tmpVertex.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("vertex index is out of sync with vertex list v.index:%d this.verts.size():%d ", tmpVertex.index, this.verts.size()));
        }
        // 1st remove all faces that reference this vertex
        final java.util.Set<Face> faces_to_remove = new java.util.HashSet<>();
        for (int face_key : tmpVertex.face_keys) {
            faces_to_remove.add(this.faces.get(face_key));
        }
        for (final Face face : faces_to_remove) {
            this.removeFace(face);
        }
        // 2nd remove all edges that reference this vertex
        final java.util.Set<Edge> edges_to_remove = new java.util.HashSet<>();
        for (int edge_key : tmpVertex.edge_keys) {
            edges_to_remove.add(this.edges.get(edge_key));
        }
        for (final Edge edge : edges_to_remove) {
            this.removeEdge(edge);
        }

        int index_to_drop = tmpVertex.index;
        synchronized (this) {
            // Patch faces that reference vertecis with index greater than index_to_drop
            for (final Face face : this.faces) {
                if (face.verts[0] > index_to_drop) {
                    face.verts[0] = face.verts[0] - 1;
                }
                if (face.verts[1] > index_to_drop) {
                    face.verts[1] = face.verts[1] - 1;
                }
                if (face.verts[2] > index_to_drop) {
                    face.verts[2] = face.verts[2] - 1;
                }
            }
            // Patch edges that reference vertecis with index greater than index_to_drop
            for (final Edge edge : this.edges) {
                if (edge.verts[0] > index_to_drop) {
                    edge.verts[0] = edge.verts[0] - 1;
                }
                if (edge.verts[1] > index_to_drop) {
                    edge.verts[1] = edge.verts[1] - 1;
                }
            }
            // Patch vertex indexes greater than index_to_drop
            for (int i = index_to_drop + 1; i < this.verts.size(); i++) {
                final Vertex v = this.verts.get(i);
                v.index = v.index - 1;
            }

            // finaly remove vertex index_to_drop
            this.verts.remove(index_to_drop);
        }
    }

    //
    public void removeEdge(final Edge tmpEdge) {
        if ((this.edges.size() < tmpEdge.index) || (tmpEdge.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("edge index is out of sync with vertex list e.index:%d  this.edges.size():%d ", tmpEdge.index, this.edges.size()));
        }
    }

    //
    public void removeFace(final Face tmpFace) {
        if ((this.faces.size() < tmpFace.index) || (tmpFace.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("face index is out of sync with vertex list f.index:%d  this.faces.size():%d ", tmpFace.index, this.faces.size()));
        }
    }

    public boolean isDisconnected(final Face face) {
        for (int edge_key : face.edge_keys) {
            final Edge edge = edges.get(edge_key);
            if (edge.face_keys.size() > 1) {
                return false;
            }
        }
        return true;
    }

    //
    public Vertex addVertex(final Point3d co) {
        return addVertex(co, new Vector3d(0, 0, 0));
    }

    public Vertex addVertex(final Point3d co, final Vector3d normal) {
        for (final Vertex vert : this.verts) { // THIS should be done with the spacialPartition
            if (vert.coord.distance(co) < MIN_VERTEX_DISTANCE) {
                return vert;
            }
        }

        synchronized (this) {
            final Vertex newVertex = new Vertex(co, normal);
            newVertex.index = this.verts.size();
            this.verts.add(newVertex);
            return newVertex;
        }
    }
    //

    public final Edge addEdge(final Vertex v1, final Vertex v2) {
        if ((this.verts.size() < v1.index) || (v1.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("Vertex [0] index :%d is invalid must be between 0 - %d", v1.index, this.verts.size()));
        }
        if ((this.verts.size() < v2.index) || (v2.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("Vertex [1] index :%d is invalid must be between 0 - %d", v2.index, this.verts.size()));
        }
        if (v1.index == v2.index) {
            throw new RuntimeException(String.format("Cannot add an edge  from vertex %d to vertex %d ", v1.index, v2.index));
        }

        final long hash = Edge.makeVertexHash(v1.index, v2.index);
        if (edgeHashIDX.containsKey(hash)) {
            return edgeHashIDX.get(hash);
        } else {
            final Edge newEdge = new Edge();
            newEdge.verts[0] = v1.index;
            newEdge.verts[1] = v2.index;

            synchronized (this) {
                newEdge.index = this.edges.size();
                v1.edge_keys.add(newEdge.index);
                v2.edge_keys.add(newEdge.index);
                this.edges.add(newEdge);
                this.edgeHashIDX.put(hash, newEdge);
            }
            return newEdge;
        }
    }
    //

    public int findCommonEdgeKey(final Vertex v1, final Vertex v2) {
        int answer = -1;
        for (int edge_key : v1.edge_keys) {
            if (v2.edge_keys.contains(edge_key)) {
//                if (edge_key == 46618) {
//                    final RuntimeException ex = new RuntimeException(String.format("debug::  v1.edges:%s  v2.edges:%s", v1.edge_keys, v2.edge_keys));
//                    if (ex.getStackTrace()[1].getMethodName().equals("_get_keel_faces_at_x")) {
//                        throw ex;
//                    }
//                }
                if (answer == -1) {
                    answer = edge_key;
                } else {
                    throw new RuntimeException(String.format("v1 has more than 1 edge in common with v2  v1.edges:%s  v2.edges:%s", v1.edge_keys, v2.edge_keys));
                }
            }
        }
        if (answer == -1) {
            throw new RuntimeException(String.format("v1 does not have edges in common with v2  v1.edges:%s  v2.edges:%s", v1.edge_keys, v2.edge_keys));
        } else {
            return answer;
        }
    }

    public final Face addFace(final Vertex v1, final Vertex v2, final Vertex v3) {
        final Face newFace = new Face();
        if ((this.verts.size() < v1.index) || (v1.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("Vertex [0] index :%d is invalid must be between 0 - %d", v1.index, this.verts.size()));
        }
        if ((this.verts.size() < v2.index) || (v2.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("Vertex [1] index :%d is invalid must be between 0 - %d", v2.index, this.verts.size()));
        }
        if ((this.verts.size() < v3.index) || (v3.index < 0)) {
            throw new IndexOutOfBoundsException(String.format("Vertex [2] index :%d is invalid must be between 0 - %d", v3.index, this.verts.size()));
        }

        newFace.verts[0] = v1.index;
        newFace.verts[1] = v2.index;
        newFace.verts[2] = v3.index;

        //
        // We must have common edges between the given verteces
        int edgeIdx;
        newFace.edge_keys.add(edgeIdx = this.findCommonEdgeKey(v1, v2));
        final Edge e1 = this.edges.get(edgeIdx);
        newFace.edge_keys.add(edgeIdx = this.findCommonEdgeKey(v2, v3));
        final Edge e2 = this.edges.get(edgeIdx);
        newFace.edge_keys.add(edgeIdx = this.findCommonEdgeKey(v3, v1));
        final Edge e3 = this.edges.get(edgeIdx);
        //
        synchronized (this) {
            newFace.index = this.faces.size();
            this.faces.add(newFace);
            v1.face_keys.add(newFace.index);
            v2.face_keys.add(newFace.index);
            v3.face_keys.add(newFace.index);
            e1.face_keys.add(newFace.index);
            e2.face_keys.add(newFace.index);
            e3.face_keys.add(newFace.index);
        }
        // only add this face to the face_keys of the v4 if it is not False
        return newFace;
    }

    /**
     * check if the face is on the edge of a manifold.
     *
     * @param face
     * @return true if any of the face's edges is a manifold edge ( is used by
     * only 1 face )
     */
    public boolean isManifoldEdgeFace(final Face face) {
        for (int edge_key : face.edge_keys) {
            if (this.edges.get(edge_key).isManifoldEdge()) {
                return true;
            }
        }
        return false;
    }

    /**
     * get an edge this is a manifoldEdge,
     *
     * @param face
     * @return
     */
    public Edge getManifoldEdge(final Face face) {
        for (int edge_key : face.edge_keys) {
            final Edge edge = this.edges.get(edge_key);
            if (edge.isManifoldEdge()) {
                return edge;
            }
        }
        throw new RuntimeException(String.format("Face %d is not on the manifold edge, all its edges is shared with other faces", face.index));
    }

    public java.util.List<Vertex> traverseEdges(final java.util.List<Edge> edge_line_list, final Vertex v_first, final Vertex v_last) {
        return traverseEdges(edge_line_list, v_first, v_last, false);
    }

    public java.util.List<Vertex> traverseEdges(final java.util.List<Edge> edge_line_list, final Vertex v_first, final Vertex v_last, final boolean non_manifold) {
        /**
         * Return the list of vertices that define the line specified by edges ,
         * the vertices will be returned in the correct order from v_first to
         * v_last, v_first & v_last MUST be the two end points of the line, all
         * the edges MUST be connected
         */
        final java.util.List<Vertex> ordred_vertex_list = new java.util.ArrayList<>(edge_line_list.size() + 16);
        ordred_vertex_list.add(v_first);
        Vertex cur_vertex = v_first;
        final java.util.Set<Integer> visited_edge_index = new java.util.HashSet<>(edge_line_list.size() + 16);
        final java.util.Set<Integer> edge_list_indexies = new java.util.HashSet<>(edge_line_list.size() + 16);
        for (final Edge edge : edge_line_list) {
            edge_list_indexies.add(edge.index);
        }
//        java.util.Set<Integer> edge_key_set
        //
        while ((cur_vertex.index != v_last.index) || (visited_edge_index.size() < edge_line_list.size())) {
            final Edge next_edge = this.findLineEdge(cur_vertex, edge_list_indexies, visited_edge_index, non_manifold);
            if (next_edge != null) {
                visited_edge_index.add(next_edge.index);
                // If the edge returned is not in the original list of edges. we skip this edge.
                if (!edge_list_indexies.contains(next_edge.index)) {
                    System.err.printf("WARNING traverse_edges skipping next_edge %d, is was not in original line edge list %s\n", next_edge.index, edge_list_indexies);
                    continue;
                }
                final Vertex next_vertex = this.findOtherVertex(cur_vertex, next_edge);
                ordred_vertex_list.add(next_vertex);
                cur_vertex = next_vertex;
                if (cur_vertex.index == v_last.index) {
                    break;
                }
            } else { // Step back .
                // Remove last vertex from ordred_vertex_list , reset the cur_vertex to the top vertex
//                System.err.printf("traverse_edges reversing cur_vertex %d\n", cur_vertex.index);
                ordred_vertex_list.remove(ordred_vertex_list.size() - 1);
                cur_vertex = ordred_vertex_list.get(ordred_vertex_list.size() - 1);
            }
        }
        //
        return ordred_vertex_list;
    }

    private Edge findLineEdge(final Vertex vertex, final java.util.Set<Integer> selected_edge_index, final java.util.Set<Integer> visited_edge_index, final boolean non_manifold) {
        for (int edgeidx : vertex.edge_keys) {
            if (visited_edge_index.contains(edgeidx)) {
                continue;
            }
            if (!selected_edge_index.contains(edgeidx)) {
                continue;
            }
            Edge edge = this.edges.get(edgeidx);
            Vertex other_vert = this.findOtherVertex(vertex, edge);
            if ((non_manifold != false) && (other_vert.edge_keys.size() > 2)) {
                System.err.printf("WARNING _find_line_edge V:%d skipping other_vert %d, it has more than 2 edgeKeys %s \n", vertex.index, other_vert.index, other_vert.edge_keys);
                continue; // this is a shared vertex
            }
            return edge;
        }
        //
        return null;
//        throw new RuntimeException(String.format("findLineEdge V:%d could not continue , edgeKeys=%s visited_edge_index:%s", vertex.index, vertex.edge_keys, visited_edge_index));
    }
    //

    private Vertex findOtherVertex(final Vertex vertex, final Edge edge) {
        if (edge.verts[0] == vertex.index) {
            return this.verts.get(edge.verts[1]);
        } else {
            return this.verts.get(edge.verts[0]);
        }
    }

    public Vertex[] findMinAndMaxVertices(final java.util.List<Edge> edge_list) {
        final java.util.List<Vertex> MinMaxVertex = new java.util.ArrayList<>(2);

        final int vertexReferenceCounts[] = new int[this.verts.size()];
        java.util.Arrays.fill(vertexReferenceCounts, 0);
        for (Edge edge : edge_list) {
            int idx0 = edge.verts[0];
            int idx1 = edge.verts[1];
            vertexReferenceCounts[idx0] = vertexReferenceCounts[idx0] + 1;
            vertexReferenceCounts[idx1] = vertexReferenceCounts[idx1] + 1;
        }
        //
        for (int i = 0; i < this.verts.size(); i++) {
            if (vertexReferenceCounts[i] == 1) {
                MinMaxVertex.add(this.verts.get(i));
            } else if (vertexReferenceCounts[i] > 2) {
//                String prefix = this.name;
//                for (Edge edge : edge_list) {
//                    int idx0 = edge.verts[0];
//                    int idx1 = edge.verts[1];
//                    Vertex vert_1 = this.verts.get(idx0);
//                    Vertex vert_2 = this.verts.get(idx1);
//                    System.err.printf("%s %d [%d] (%5.3f , %5.3f , %5.3f)  -  [%d] (%5.3f , %5.3f , %5.3f)\n", prefix, edge.index, vert_1.index, vert_1.coord.x, vert_1.coord.y, vert_1.coord.z, vert_2.index, vert_2.coord.x, vert_2.coord.y, vert_2.coord.z);
//                    System.err.printf("%s (%5.3f , %5.3f , %5.3f) [%d ]  refs: %d\n", prefix, vert_1.coord.x, vert_1.coord.y, vert_1.coord.z, vert_1.index, vertexReferenceCounts[idx0]);
//                    System.err.printf("%s (%5.3f , %5.3f , %5.3f) [%d ]  refs: %d\n", prefix, vert_2.coord.x, vert_2.coord.y, vert_2.coord.z, vert_2.index, vertexReferenceCounts[idx1]);
//                }
                System.err.println(String.format("WARNING Found loops in the Edge list references Vertex:%d", i));
            }
        }

        //
        //
        if (MinMaxVertex.size() != 2) {
            final String prefix = this.name;
            for (Edge edge : edge_list) {
                int idx0 = edge.verts[0];
                int idx1 = edge.verts[1];
                Vertex vert_1 = this.verts.get(idx0);
                Vertex vert_2 = this.verts.get(idx1);
                System.err.printf("%s %d [%d] (%5.3f , %5.3f , %5.3f)  -  [%d] (%5.3f , %5.3f , %5.3f)\n", prefix, edge.index, vert_1.index, vert_1.coord.x, vert_1.coord.y, vert_1.coord.z, vert_2.index, vert_2.coord.x, vert_2.coord.y, vert_2.coord.z);
                System.err.printf("%s (%5.3f , %5.3f , %5.3f) [%d ]  refs: %d\n", prefix, vert_1.coord.x, vert_1.coord.y, vert_1.coord.z, vert_1.index, vertexReferenceCounts[idx0]);
                System.err.printf("%s (%5.3f , %5.3f , %5.3f) [%d ]  refs: %d\n", prefix, vert_2.coord.x, vert_2.coord.y, vert_2.coord.z, vert_2.index, vertexReferenceCounts[idx1]);
            }
            throw new RuntimeException(String.format("Expected exactly 2 min/max points (vertxies with only 1 edge ref) , instead we got %d", MinMaxVertex.size()));
        }
        return MinMaxVertex.toArray(new Vertex[2]);
    }

    public Vertex[] findMinAndMaxVertices(final java.util.List<Edge> edge_list, final Vector3d axis) {
        final Vertex MinMaxVertex[] = findMinAndMaxVertices(edge_list);
        final Vertex result[] = new Vertex[2];

        if (axis.dot(new Vector3d(MinMaxVertex[0].coord)) > axis.dot(new Vector3d(MinMaxVertex[1].coord))) {
            result[0] = MinMaxVertex[1];
            result[1] = MinMaxVertex[0];
        } else {
            result[0] = MinMaxVertex[0];
            result[1] = MinMaxVertex[1];
        }
        //
        return result;
    }
    //

    public Vertex[] findMinAndMaxVertices_verts(final java.util.List<Vertex> vert_list) {
        return findMinAndMaxVertices_verts(vert_list, new Vector3d(1, 1, 1));
    }

    public Vertex[] findMinAndMaxVertices_verts(final java.util.List<Vertex> vert_list, final Vector3d axis) {
        final Vertex MinMaxVertex[] = new Vertex[2];
        MinMaxVertex[0] = vert_list.get(0);
        MinMaxVertex[1] = vert_list.get(0);
        double dot1 = axis.dot(new Vector3d(MinMaxVertex[1].coord));
        double dot0 = axis.dot(new Vector3d(MinMaxVertex[0].coord));

        for (Vertex vert : vert_list) {
            final double vdot = axis.dot(new Vector3d(vert.coord));
            if (vdot > dot1) {
                MinMaxVertex[1] = vert;
                dot1 = vdot;
            }
            if (vdot < dot0) {
                MinMaxVertex[0] = vert;
                dot0 = vdot;
            }
        }

        return MinMaxVertex;
    }

    public void clear() {
        synchronized (this) {
            this.edgeHashIDX.clear();
            for (Face f : this.faces) {
                f.edge_keys.clear();
            }
            this.faces.clear();
            for (Edge e : this.edges) {
                e.face_keys.clear();
            }
            this.edges.clear();
            for (Vertex v : this.verts) {
                v.edge_keys.clear();
                v.face_keys.clear();
            }
            this.verts.clear();
        }
    }

    public Vertex mirror(final Vertex vert, final Vector3d mirror) {
        return addVertex(new Point3d(vert.coord.x * mirror.x, vert.coord.y * mirror.y, vert.coord.z * mirror.z), new Vector3d(vert.normal.x * mirror.x, vert.normal.y * mirror.y, vert.normal.z * mirror.z));
    }

    public void mirror(final Vector3d mirror) {
        //mirror.normalize(); // ensure it is normalized
        final java.util.List<Face> copiedListofFaces = new java.util.ArrayList<>(faces);

        for (Face face : copiedListofFaces) {
            Vertex v0 = verts.get(face.verts[0]);
            Vertex v1 = verts.get(face.verts[1]);
            Vertex v2 = verts.get(face.verts[2]);
            Vertex m0 = mirror(v2, mirror);
            Vertex m1 = mirror(v1, mirror);
            Vertex m2 = mirror(v0, mirror);
            addEdge(m0, m1);
            addEdge(m1, m2);
            addEdge(m2, m0);
            addFace(m0, m1, m2);
        }
    }

    /**
     * check if the line between vert_1 and vert_2 intersects YZ plane at
     * x_position
     *
     * @param vert_1
     * @param vert_2
     * @param x_position
     * @return true if between vert_1 and vert_2 intersects YZ plane at
     * x_position
     */
    public boolean intersects_X(Vertex vert_1, Vertex vert_2, double x_position) {
        return (((vert_1.coord.x <= x_position) && (vert_2.coord.x >= x_position)) || ((vert_2.coord.x <= x_position) && (vert_1.coord.x >= x_position)));
    }

    /**
     * check if the line between vert_1 and vert_2 intersects XZ plane at
     * y_position
     *
     * @param vert_1
     * @param vert_2
     * @param y_position
     * @return true if between vert_1 and vert_2 intersects XZ plane at
     * y_position
     */
    public boolean intersects_Y(Vertex vert_1, Vertex vert_2, double y_position) {
        return (((vert_1.coord.y <= y_position) && (vert_2.coord.y >= y_position)) || ((vert_2.coord.y <= y_position) && (vert_1.coord.y >= y_position)));
    }

    /**
     * check if the line between vert_1 and vert_2 intersects XY plane at
     * z_position
     *
     * @param vert_1
     * @param vert_2
     * @param z_position
     * @return true if between vert_1 and vert_2 intersects XY plane at
     * z_position
     */
    public boolean intersects_Z(Vertex vert_1, Vertex vert_2, double z_position) {
        return (((vert_1.coord.z <= z_position) && (vert_2.coord.z >= z_position)) || ((vert_2.coord.z <= z_position) && (vert_1.coord.z >= z_position)));
    }

    /**
     * check if the edge intersects YZ plane at x_position
     *
     * @param edge
     * @param x_position
     * @return true if edge intersects YZ plane at x_position
     */
    public boolean intersects_X(Edge edge, double x_position) {
        final Vertex vert_1 = verts.get(edge.verts[0]);
        final Vertex vert_2 = verts.get(edge.verts[1]);
        return intersects_X(vert_1, vert_2, x_position);
    }

    /**
     * count how many edges of face intersects YZ plane at x_position
     *
     * @param face
     * @param x_position
     * @return many edges of face intersects YZ plane at x_position
     */
    public int intersects_X(Face face, double x_position) {
        final Vertex vert_1 = verts.get(face.verts[0]);
        final Vertex vert_2 = verts.get(face.verts[1]);
        final Vertex vert_3 = verts.get(face.verts[2]);
        int intersects = 0;
        if (intersects_X(vert_1, vert_2, x_position)) {
            intersects++;
        }
        if (intersects_X(vert_2, vert_3, x_position)) {
            intersects++;
        }
        if (intersects_X(vert_3, vert_1, x_position)) {
            intersects++;
        }
        return intersects;
    }

    /**
     * check if the given edge has both vertexes on the x_position
     *
     * @param edge
     * @param x_position
     * @return true if both vertexes isOnX(x_position)
     */
    public boolean isOnX(final Edge edge, double x_position) {
        return verts.get(edge.verts[0]).isOnX(x_position) && verts.get(edge.verts[1]).isOnX(x_position);
    }

    /**
     * check if the given edge has both vertexes on the y_position
     *
     * @param edge
     * @param y_position
     * @return true if both vertexes isOnY(y_position)
     */
    public boolean isOnY(final Edge edge, double y_position) {
        return verts.get(edge.verts[0]).isOnY(y_position) && verts.get(edge.verts[1]).isOnY(y_position);
    }

    /**
     * check if the given edge has both vertexes on the z_position
     *
     * @param edge
     * @param z_position
     * @return true if both vertexes isOnY(z_position)
     */
    public boolean isOnZ(final Edge edge, double z_position) {
        return verts.get(edge.verts[0]).isOnZ(z_position) && verts.get(edge.verts[1]).isOnZ(z_position);
    }

    /**
     * check if all vertexes of the given face has coord.x >= x_position
     *
     * @param face
     * @param x_position
     * @return true if all vertexes of the given face has coord.x >= x_position
     */
    public boolean face_ge_X(final Face face, final double x_position) {
        final Vertex v1 = verts.get(face.verts[0]);
        final Vertex v2 = verts.get(face.verts[1]);
        final Vertex v3 = verts.get(face.verts[2]);
        return v1.coord.x >= x_position && v2.coord.x >= x_position && v3.coord.x >= x_position;
    }

    /**
     * return the number of vertexes of given edge that isOnX(keel_x_position)
     *
     * @param edge
     * @param keel_x_position
     * @return number of vertexes of given face that isOnX(keel_x_position)
     */
    public int touches_X(final Edge edge, final double keel_x_position) {
        final Vertex v1 = verts.get(edge.verts[0]);
        final Vertex v2 = verts.get(edge.verts[1]);
        int touchCount = 0;
        if (v1.isOnX(keel_x_position)) {
            touchCount += 1;
        }
        if (v2.isOnX(keel_x_position)) {
            touchCount += 1;
        }
        return touchCount;
    }

    /**
     * return the number of vertexes of given face that isOnX(keel_x_position)
     *
     * @param face
     * @param keel_x_position
     * @return number of vertexes of given face that isOnX(keel_x_position)
     */
    public int touches_X(final Face face, final double keel_x_position) {
        final Vertex v1 = verts.get(face.verts[0]);
        final Vertex v2 = verts.get(face.verts[1]);
        final Vertex v3 = verts.get(face.verts[2]);
        int touchCount = 0;
        if (v1.isOnX(keel_x_position)) {
            touchCount += 1;
        }
        if (v2.isOnX(keel_x_position)) {
            touchCount += 1;
        }
        if (v3.isOnX(keel_x_position)) {
            touchCount += 1;
        }
        return touchCount;
    }

    /**
     * Mark all the Vertexes that form part of the Manifolds edge
     */
    public void detectManifoldEdges() {
        for (Face face : faces) {
            if (isManifoldEdgeFace(face)) {
                for (int edgeIdx : face.edge_keys) {
                    final Edge edge = edges.get(edgeIdx);
                    if (edge.isManifoldEdge()) {
                        verts.get(edge.verts[0]).set(Vertex.Flag.MANIFOLD_EDGE);
                        verts.get(edge.verts[1]).set(Vertex.Flag.MANIFOLD_EDGE);
                    }
                }
            }
        }
    }

    /**
     * Returns the minimum x value of this edge.
     *
     * @return double
     */
    public double getMinX(final Edge edge) {
        return Math.min(verts.get(edge.verts[0]).coord.x, verts.get(edge.verts[1]).coord.x);
    }

    /**
     * Returns the maximum x value of this edge.
     *
     * @return double
     */
    public double getMaxX(final Edge edge) {
        return Math.max(verts.get(edge.verts[0]).coord.x, verts.get(edge.verts[1]).coord.x);
    }

    /**
     * Returns the minimum y value of this edge.
     *
     * @return double
     */
    public double getMinY(final Edge edge) {
        return Math.min(verts.get(edge.verts[0]).coord.y, verts.get(edge.verts[1]).coord.y);
    }

    /**
     * Returns the maximum y value of this edge.
     *
     * @return double
     */
    public double getMaxY(final Edge edge) {
        return Math.max(verts.get(edge.verts[0]).coord.y, verts.get(edge.verts[1]).coord.y);
    }

    /**
     * Returns the minimum z value of this edge.
     *
     * @return double
     */
    public double getMinZ(final Edge edge) {
        return Math.min(verts.get(edge.verts[0]).coord.z, verts.get(edge.verts[1]).coord.z);
    }

    /**
     * Returns the maximum z value of this edge.
     *
     * @return double
     */
    public double getMaxZ(final Edge edge) {
        return Math.max(verts.get(edge.verts[0]).coord.z, verts.get(edge.verts[1]).coord.z);
    }

    /**
     * Returns true if the interior of the polygon is to the right of this edge.
     * <p>
     * Given that the polygon's vertex winding is Counter- Clockwise, if the
     * vertices that make this edge decrease along the y axis then the interior
     * of the polygon is to the right, otherwise its to the left.
     *
     * @return boolean
     */
    public boolean isInteriorRight(final Edge edge) {
        double diff = verts.get(edge.verts[0]).coord.y - verts.get(edge.verts[1]).coord.y;
        // check if the points have nearly the
        // same x value
        if (Math.abs(diff) <= Epsilon.E) {
            // if they do, is the vector of the
            // two points to the right or to the left
            if (verts.get(edge.verts[0]).coord.x < verts.get(edge.verts[1]).coord.x) {
                return true;
            } else {
                return false;
            }
            // otherwise just compare the y values
        } else if (diff > 0.0) {
            return true;
        } else {
            return false;
        }
    }

    public double getArea(final Face face) {
        Point3d v0 = verts.get(face.verts[0]).coord;
        Point3d v1 = verts.get(face.verts[1]).coord;
        Point3d v2 = verts.get(face.verts[2]).coord;
        Vector3d w = sub(v1, v0);
        Vector3d v = sub(v2, v0);
        return cross(w, v).length() / 2;
    }

    public boolean validateSolid() {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "validateSolid::{0}, verts:{1},  edges:{2},  faces:{3}", new Object[]{name, verts.size(), edges.size(), faces.size()});
        }
        int fc = faces.size();
        boolean valid = true;
        if (((fc / 2) * 2) != fc) {
            valid = false;
            LOGGER.log(Level.WARNING, "validateSolid::{0}, faces:{1} is not even", new Object[]{name, faces.size()});
        }
// number of edges must be a multiple of 3
        int ec = edges.size();
        if (((ec / 3) * 3) != ec) {
            valid = false;
            LOGGER.log(Level.WARNING, "validateSolid::{0}, edges:{1} is not a multiple of 3", new Object[]{name, edges.size()});
        }

        // number of edges must be 2/3 number of faces
        if (2 * ec != 3 * fc) {
            valid = false;
            LOGGER.log(Level.WARNING, "validateSolid::{0}, number of faces:{1} is not 2/3 the number of edges:{2}", new Object[]{name, faces.size(), edges.size()});
        }
        // must be a non-manifold solid, according to Euler's Law
        int vc = verts.size();
        if (fc - ec + vc != 2) {
            valid = false;
            LOGGER.log(Level.WARNING, "validateSolid::{0}, not a Eulers non-manifold solid because faces:{1} - edges:{2} +vertices:{3} != 2", new Object[]{name, faces.size(), edges.size(), verts.size()});
        }
        for (Face face : faces) {
            boolean faceValid = true;
            for (int edgeIdx : face.edge_keys) {
                // Each edge must have exactly 2 face references
                final Edge edge = edges.get(edgeIdx);

                if (edge.face_keys.size() != 2) {
                    Vertex ve0 = verts.get(edge.verts[0]);
                    Vertex ve1 = verts.get(edge.verts[1]);
                    LOGGER.log(Level.WARNING, "validateSolid::{0}, Edge idx:{1} has:{2} faceReferneces   : {3}", new Object[]{name, edgeIdx, edge.face_keys.size(), edge.face_keys});
                    LOGGER.log(Level.WARNING, "validateSolid::{0}, Edge idx:{1} v1:#{2}:{3} , v2:#{4}:{5}", new Object[]{name, edgeIdx, ve0.index, ve0.coord, ve1.index, ve1.coord});
                    valid = faceValid = false;
                }
            }
            if (!faceValid) {
                Vertex vf0 = verts.get(face.verts[0]);
                Vertex vf1 = verts.get(face.verts[1]);
                Vertex vf2 = verts.get(face.verts[2]);
                LOGGER.log(Level.WARNING, "validateSolid::{0}, Face idx:{1} invalid edges {2} ", new Object[]{name, face.index, face.edge_keys.size()});
                LOGGER.log(Level.WARNING, "validateSolid::{0}, Face idx:{1} v1:#{2}:{3} , v2:#{4}:{5} , v2:#{6}:{7}", new Object[]{name, face.index, vf0.index, vf0.coord, vf1.index, vf1.coord, vf2.index, vf2.coord});
            }
        }

        // Check for degenerate faces
        for (Face face : faces) {

            final double size = getArea(face);
            if (size < 0.00001) {
                LOGGER.log(Level.WARNING, "validateSolid::{0}, Face idx:{1} has: SMALL area {2}", new Object[]{name, face.index, size});
                valid = false;
            }
        }

        // Check overlapping triangles
        // http://stackoverflow.com/questions/7113344/find-whether-two-triangles-intersect-or-not
        // http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.131.9981&rep=rep1&type=pdf
        // http://stackoverflow.com/questions/4226319/collision-with-shapes-other-than-rectangles/4228915#4228915
        return valid;
    }

    /**
     * return true if the Vertex is to the left of Edge , on the YZ plane
     *
     * @param edge
     * @param vertex
     */
    public boolean isLeftYZ(Edge edge, Vertex vertex) {

        // attempt the simple comparison first
        if (vertex.coord.z < getMinZ(edge)) {
            return true;
        } else if (vertex.coord.x > getMaxX(edge)) {
            return false;
        }
        // its in between the min and max x so we need to 
        // do a side of line test
        double location = getLocationYZ(vertex.coord, verts.get(edge.verts[0]).coord, verts.get(edge.verts[1]).coord);
        return location < 0.0;

    }

    Vector3d sub(Tuple3d a, Tuple3d b) {
        Vector3d ab = new Vector3d(a);
        ab.sub(b);
        return ab;
    }

    Vector3d cross(Vector3d a, Vector3d b) {
        Vector3d ab = new Vector3d();
        ab.cross(a, b);
        return ab;
    }

    /**
     * Automatically select smoothness values for all edges in the mesh. This is
     * done based on the angle between the two faces sharing the edge. If it is
     * greater than the specified cutoff, the edge smoothness is set to 0.
     * Otherwise, it is set to 1.
     *
     * @param angle the cutoff angle, in radians
     */
    public void autosmoothMeshEdges(double angle) {
        double cutoff = Math.cos(angle);
        for (int i = 0; i < edges.size(); i++) {
            Edge edge = edges.get(i);
            if (edge.face_keys.size() != 2) {
                edge.smoothness = 1.0f;
                continue;
            }
            final Integer[] eFaces = edge.face_keys.toArray(new Integer[2]);
            Face f1 = faces.get(eFaces[0]);
            Face f2 = faces.get(eFaces[1]);
            Vector3d norm1 = cross(sub(verts.get(f1.verts[0]).coord, verts.get(f1.verts[1]).coord), sub(verts.get(f1.verts[0]).coord, verts.get(f1.verts[2]).coord));
            Vector3d norm2 = cross(sub(verts.get(f2.verts[0]).coord, verts.get(f2.verts[1]).coord), sub(verts.get(f2.verts[0]).coord, verts.get(f2.verts[2]).coord));
            norm1.normalize();
            norm2.normalize();
            if (norm1.dot(norm2) < cutoff) {
                edge.smoothness = 0.0f;
            } else {
                edge.smoothness = 1.0f;
            }
        }
    }

    /**
     * Return a new mesh which is an "optimized" version of the input mesh. This
     * is done by rearranging edges to eliminate very small angles, or vertices
     * where many edges come together. The resulting mesh will generally produce
     * a better looking surface after smoothing is applied to it.
     */
    public Mesh optimizeMesh() {
        autosmoothMeshEdges(Math.PI / 10);
        return MeshOptimizer.optimizeMesh(this);
    }

    public Face[] getFaces() {
        return faces.toArray(new Face[faces.size()]);
    }

    public Edge[] getEdges() {
        return edges.toArray(new Edge[edges.size()]);
    }

    public Vertex[] getVertexes() {
        return verts.toArray(new Vertex[verts.size()]);
    }

    /**
     * Determines where the point is relative to the given line.
     * <pre>
     * Set L = linePoint2 - linePoint1
     * Set P = point - linePoint1
     * location = L.cross(P)
     * </pre> Returns 0 if the point lies on the line created from the line
     * segment.<br />
     * Assuming a right handed coordinate system:<br />
     * Returns < 0 if the point lies on the right side of the line<br /> Returns
     * > 0 if the point lies on the left side of the line
     * <p>
     * Assumes all points are in world space.
     *
     * @param point the point
     * @param linePoint1 the first point of the line
     * @param linePoint2 the second point of the line
     * @return double
     */
    public static double getLocationYZ(Tuple3d point, Tuple3d linePoint1, Tuple3d linePoint2) {
        return (linePoint2.z - linePoint1.z) * (point.y - linePoint1.y) - (point.z - linePoint1.z) * (linePoint2.y - linePoint1.y);
    }

    public void scale(final Tuple3d scale) {
        for (Vertex vert : verts) {
            vert.coord.set(vert.coord.x * scale.x, vert.coord.y * scale.y, vert.coord.z * scale.z);
        }
    }

    private Vector3d sub(final Point3d p0, final Point3d p1) {
        final Vector3d p01 = new Vector3d(p0);
        p01.sub(p1);
        return p01;
    }

    /**
     * Calculates cross-product with vector v. The resulting vector is
     * perpendicular to both the current and supplied vector and overrides the
     * current.
     *
     * @param v the v
     *
     * @return itself
     */
    public final javax.vecmath.Vector3d crossSelf(final javax.vecmath.Vector3d a, javax.vecmath.Vector3d v) {
        final double cx = a.y * v.z - v.y * a.z;
        final double cy = a.z * v.x - v.z * a.x;
        a.z = a.x * v.y - v.x * a.y;
        a.x = cx;
        a.y = cy;
        return a;
    }

    public javax.vecmath.Vector3d calculateNormal(Face face) {
        final Point3d a = verts.get(face.verts[0]).coord;
        final Point3d b = verts.get(face.verts[1]).coord;
        final Point3d c = verts.get(face.verts[2]).coord;

        final javax.vecmath.Vector3d normal = crossSelf(sub(b, a), sub(c, a));
        normal.normalize();
        face.normal.set(normal);
        return normal;
    }

    public Face getLastFace() {
        return faces.get(faces.size() - 1);
    }

    public final java.util.List<Face> faces;
    public final java.util.List<Edge> edges;
    public final java.util.List<Vertex> verts;
    public final java.util.Map<Long, Edge> edgeHashIDX;
    public final String name;

    public static final double MIN_VERTEX_DISTANCE = 0.00001;
    public static final double MIN_VERTEX_PLANE_DISTANCE = 0.00000001;
}
