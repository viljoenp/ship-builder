/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple2d;
import javax.vecmath.Vector3d;
import org.rcwarships.loaders.svg.SvgFile;
import org.rcwarships.model.clipper.Clipper;
import org.rcwarships.model.clipper.EndPointBound;
import org.rcwarships.model.extruder.Extruder;
import org.rcwarships.model.simplifier.Simplifier;
import org.rcwarships.model.triangulation.Triangulation3d;

/**
 * Factory to 'cut'/make the KEEL MESH from the HULL MESH,
 *
 * @author viljoenp
 */
public final class KeelFactory {

    private static final Logger LOGGER = Logger.getLogger(KeelFactory.class.getName());
    private static boolean CUT_NOTCHES = true;

    static {
        if (System.getProperty("cut-notches") != null) {
            CUT_NOTCHES = Boolean.parseBoolean(System.getProperty("cut-notches"));
        }
    }

    /**
     * Static factory method to create a MESH that represents the KEEL, of the
     * model, some assumptions:
     *
     * The Ship is centered along the y Axis , from stern to bow ( or vice a
     * versa )
     *
     * @param hull_MESH Mesh representing the entire HULL and ONLY the hull, it
     * should not include the DECK and super structure (usually created in
     * DelftShip),
     * @param info Extrusion info for the keel
     * @param rib_positions A list of all the model Y coordinates where a RIB
     * need to be placed in the KEEL, we will cut a notch into the KEEL place so
     * the rib can fit like a puzzle piece.
     * @param maxCollisionDepth number of vertexes to backtrack in search of
     * collisions after the extrusion phase
     * @return A Mesh that represents the entire KEEL with rib notched cut into
     * it.
     */
    public static Mesh make_keel(final Mesh hull_MESH, final ModelUtils.KeelInfo info, final java.util.List<ModelUtils.RibPosition> rib_positions, final int maxCollisionDepth) {
        return _make_keel_mesh("KEEL-MESH", hull_MESH, info, rib_positions, maxCollisionDepth);
    }

    /**
     * To make the keel mesh.
     *
     * 1. the center-bottom-line : edges of the hull at x=0
     *
     * 2. the outer-bottom-line : edges of the hull at x=(plate_width/2)
     *
     * 3. the mirrored-bottom-line : outer-bottom-line( .x *= -1 )
     *
     * 4. the outer-top-line : extrude outer-bottom-line upwards in the yz plane
     * by plate_width units
     *
     * 5. cut top notches into outer-top-line.
     *
     * 6. the mirrored-top-line : outer-top-line( .x *= -1 )
     *
     * then form a solid by building faces ( right hand orientation ) from
     *
     * 7. triangulate outer-bottom-line and outer-top-line to for side surface
     *
     * 8. triangulate mirrored-bottom-line and mirrored-top-line to for opposite
     * side surface
     *
     * 9. triangulate mirrored-top-line and outer-top-line + bottom_center.first
     * + bottom_center.last ( top surface )
     *
     * 10. triangulate outer-bottom-line and center-bottom-line ( 1/2 bottom
     * surface )
     *
     * 11. triangulate center-bottom-line and mirrored-bottom-line ( 1/2 bottom
     * surface )
     *
     *
     * @param name
     * @param hull_MESH
     * @param info Extrusion info for the keel
     * @param rib_positions
     * @param keel_notch_heights
     * @return
     */
    private static Mesh _make_keel_mesh(final String name, final Mesh hull_MESH, final ModelUtils.KeelInfo info, final java.util.List<ModelUtils.RibPosition> rib_positions, final int maxCollisionDepth) {

        final Mesh tmp_MESH = new Mesh(name);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Looking for points intersecting the YZ plane at X=0.0");
        }
        // 1. the center-bottom-line : edges of the hull at x=0
        final java.util.List<Point3d> bottom_center_line = getKeelEdgesAtX(hull_MESH, 0.0);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} , points intersecting the YZ plane at X={1}", new Object[]{bottom_center_line.size(), String.format("%12.8f", 0.0)});
        }
//        final java.util.List<Point3d> top_center_line = getTopNotchedLine(bottom_center_line, 0.0, info.thickness, keel_positions);

        // 2. the outer-bottom-line : edges of the hull at x=(info.width/2)
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Looking for points intersecting the YZ plane at X={0}", String.format("%12.8f", info.width / 2.f));
        }
        final java.util.List<Point3d> bottom_outer_line = getKeelEdgesAtX(hull_MESH, info.width / 2.f);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} , points intersecting the YZ plane at X={1}", new Object[]{bottom_outer_line.size(), String.format("%12.8f", info.width / 2.f)});
        }
//        // DUMP The CenterLine
//        if (LOGGER.isLoggable(Level.INFO)) {
//            try {
//                SvgFile keelCenter = new SvgFile("keel.svg");
//
//                keelCenter.export_yz_svg("extrusion", "red", 10, 160, bottom_outer_line);
//
//                keelCenter.export_yz_svg("keelCenter", "green", 10, 160, bottom_center_line);
//
//                keelCenter.export_yz_svg("keelOutter", "blue", 10, 160, bottom_outer_line);
//
//                keelCenter.close();
//            } catch (java.io.IOException ex) {
//                LOGGER.log(Level.INFO, "Failed to dump keelCenter.svg", ex);
//            }
//        }
        // 3. the mirrored-bottom-line : outer-bottom-line( .x *= -1 )
        final java.util.List<Point3d> bottom_mirrored_line = Extruder.mirrorOnY(bottom_outer_line);

        // 4. the outer-top-line : extrude outer-bottom-line upwards in the yz plane by info.thickness units
        // 5. cut top notches into outer-top-line.
        final java.util.List<Point3d> top_extruded_line = getExtrusion(bottom_outer_line, info.width / 2.f, info.thickness, maxCollisionDepth);
        final java.util.List<Point3d> top_outer_line = CUT_NOTCHES ? getNotchedLine(ModelUtils.deepCopy(top_extruded_line), bottom_outer_line, info.width / 2.f, info, rib_positions) : ModelUtils.deepCopy(top_extruded_line);

        // 6. the mirrored-top-line : outer-top-line( .x *= -1 )
        final java.util.List<Point3d> top_mirrored_line = Extruder.mirrorOnY(top_outer_line);

        final java.util.List<Vertex> bottom_center = ModelUtils.addMeshPoints(tmp_MESH, bottom_center_line, new Vector3d(1, 1, 1));
//        final java.util.List<Vertex> top_center = addMeshPoints(tmp_MESH, top_center_line, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> bottom_outer = ModelUtils.addMeshPoints(tmp_MESH, bottom_outer_line, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> top_outer = ModelUtils.addMeshPoints(tmp_MESH, top_outer_line, new Vector3d(1, 1, 1));

        final java.util.List<Vertex> bottom_mirrored = ModelUtils.addMeshPoints(tmp_MESH, bottom_mirrored_line, new Vector3d(1, 1, 1));
        final java.util.List<Vertex> top_mirrored = ModelUtils.addMeshPoints(tmp_MESH, top_mirrored_line, new Vector3d(1, 1, 1));

        // DUMP The CenterLine
        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile keelCenter = new SvgFile("keel.svg");

                java.util.List<Point3d> vertexLine0 = new java.util.ArrayList<>(top_extruded_line);
                java.util.Collections.reverse(vertexLine0);
                keelCenter.export_yz_svg("extrusion", "red", 10, 160, bottom_outer_line, vertexLine0);

                java.util.List<Point3d> vertexLine2 = new java.util.ArrayList<>(top_outer_line);
                java.util.Collections.reverse(vertexLine2);
                keelCenter.export_yz_svg("keelCenter", "green", 10, 160, bottom_center_line, vertexLine2);

                java.util.List<Vertex> vertexLine3 = new java.util.ArrayList<>(top_outer);
                java.util.Collections.reverse(vertexLine3);
                keelCenter.export_yz_svg(tmp_MESH, "keelOutter", 1.0 / 144.0 / 10, 10, 160, bottom_outer, vertexLine3);

                keelCenter.close();
            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump keelCenter.svg", ex);
            }
        }

        // 7. triangulate outer-bottom-line and outer-top-line to for side surface
        ModelUtils.buildMeshSurfaceAlongYAxis(tmp_MESH, bottom_outer, top_outer, true);

        // 8. triangulate mirrored-bottom-line and mirrored-top-line to for opposite side surface
        ModelUtils.buildMeshSurfaceAlongYAxis(tmp_MESH, bottom_mirrored, top_mirrored, false);

        // 9. triangulate mirrored-top-line and outer-top-line + bottom_center.first + bottom_center.last ( top surface ) 
        // mirrored-top-line and outer-top-line is mirror images , it should be easy to triangulate this surface by 
        // building quads in the same plane from opposite vertexes..
        ModelUtils.buildMeshSurfaceAlongMirroredLinesAlongYAxis(tmp_MESH, top_outer, top_mirrored, false);
        // Join the bottom_center with the bottom_outer and bottom_mirrored.
        tmp_MESH.addEdge(bottom_outer.get(0), bottom_mirrored.get(0));
        tmp_MESH.addEdge(bottom_mirrored.get(0), bottom_center.get(0));
        tmp_MESH.addEdge(bottom_center.get(0), bottom_outer.get(0));
        tmp_MESH.addFace(bottom_outer.get(0), bottom_center.get(0), bottom_mirrored.get(0));
        tmp_MESH.addEdge(bottom_outer.get(bottom_outer.size() - 1), bottom_mirrored.get(bottom_mirrored.size() - 1));
        tmp_MESH.addEdge(bottom_mirrored.get(bottom_mirrored.size() - 1), bottom_center.get(bottom_center.size() - 1));
        tmp_MESH.addEdge(bottom_center.get(bottom_center.size() - 1), bottom_outer.get(bottom_outer.size() - 1));
        tmp_MESH.addFace(bottom_outer.get(bottom_outer.size() - 1), bottom_mirrored.get(bottom_mirrored.size() - 1), bottom_center.get(bottom_center.size() - 1));
        // Complete the top by closing the last an first vertexes of bottom_outer , top_outer ,  bottom_mirrored and top_mirrored
        tmp_MESH.addEdge(top_outer.get(0), bottom_mirrored.get(0));
        tmp_MESH.addFace(top_outer.get(0), bottom_mirrored.get(0), top_mirrored.get(0));
        tmp_MESH.addFace(top_outer.get(0), bottom_outer.get(0), bottom_mirrored.get(0));

        tmp_MESH.addEdge(bottom_mirrored.get(bottom_mirrored.size() - 1), top_outer.get(top_outer.size() - 1));
        tmp_MESH.addFace(bottom_mirrored.get(bottom_mirrored.size() - 1), top_outer.get(top_outer.size() - 1), top_mirrored.get(top_mirrored.size() - 1));
        tmp_MESH.addFace(bottom_outer.get(bottom_outer.size() - 1), top_outer.get(top_outer.size() - 1), bottom_mirrored.get(bottom_mirrored.size() - 1));

        // 10. triangulate outer-bottom-line and center-bottom-line ( 1/2 bottom surface ) , here we hit the shit creek 
        // This is a hard problem to solve in general because these two lines are not in the same plane, however we have an aid, and that is the original hull, 
        // All the vertexes in these two lines will match an edge on the original hull, in the case of the center-bottom-line  the vertcies will match actual 
        // vertexes on the original hull, we might need to prepare these lines so they will match more easily.
        // 11. triangulate center-bottom-line and mirrored-bottom-line ( 1/2 bottom surface ), here we hit the shit creek 
        // FIXME : For Now we ignore the bottom Center line , we will somply build a stripe across the mirrored lines
//        ModelUtils.buildMeshSurfaceAlongMirroredLinesAlongYAxis(tmp_MESH, bottom_outer, bottom_mirrored, true);
        Triangulation3d triangulator = new Triangulation3d();
//        java.util.List<Vertex> loop = new java.util.ArrayList<>(bottom_outer.size() + bottom_center.size() + 2);
//        loop.addAll(bottom_outer);
        LOGGER.log(Level.INFO, "KEEL bottom_outer  {0} Vertexes", new Object[]{bottom_outer.size()});

//        java.util.Collections.reverse(loop);
//        loop.addAll(bottom_center);
        LOGGER.log(Level.INFO, "KEEL bottom_center {0} Vertexes", new Object[]{bottom_center.size()});
//        LOGGER.log(Level.INFO, "KEEL loop          {0} Vertexes", new Object[]{loop.size()});

        triangulator.triangulate(tmp_MESH, bottom_center, bottom_outer, false);
        triangulator.triangulate(tmp_MESH, bottom_center, bottom_mirrored, false);
        LOGGER.log(Level.INFO, "Completed KEEL generation {0} Vertexes, {1} Edges, {2} Faces", new Object[]{tmp_MESH.verts.size(), tmp_MESH.edges.size(), tmp_MESH.faces.size()});
        return tmp_MESH.optimizeMesh();
    }

    private static void clipEndPoints(java.util.List<Point3d> extruded_outer_top_line, final java.util.List<Point3d> bottom_line, Transform3dToYZ2d transform) {
        EndPointBound start = new EndPointBound(Plane.planeXY(bottom_line.get(0).z, false));
        EndPointBound end = new EndPointBound(Plane.planeXY(bottom_line.get(bottom_line.size() - 1).z, false));
        Clipper.clipEndPoints(extruded_outer_top_line, start, end);
    }

    private static java.util.List<Point3d> getNotchedLine(java.util.List<Point3d> extruded_outer_top_line, final java.util.List<Point3d> bottom_line, final double keel_x_position, final ModelUtils.KeelInfo info, final java.util.List<ModelUtils.RibPosition> rib_positions) {
        Transform3dToYZ2d transform = new Transform3dToYZ2d(keel_x_position);

        if (LOGGER.isLoggable(Level.INFO)) {
            try {
                SvgFile keelCenter = new SvgFile("keel-" + keel_x_position + ".svg");
                java.util.List<Point3d> vertexLine2 = new java.util.ArrayList<>(extruded_outer_top_line);
                java.util.Collections.reverse(vertexLine2);
                keelCenter.export_yz_svg("keel-" + keel_x_position, "green", 10, 160, bottom_line, vertexLine2);
                keelCenter.close();
            } catch (java.io.IOException ex) {
                LOGGER.log(Level.INFO, "Failed to dump keel-line.svg", ex);
            }
        }

        boolean reverse = Extruder.xDelta(extruded_outer_top_line, transform) > 0;
        if (reverse) {
            java.util.Collections.reverse(extruded_outer_top_line);
        }

        for (java.util.Iterator< ModelUtils.RibPosition> itre = rib_positions.iterator(); itre.hasNext();) {
            ModelUtils.RibPosition pos = itre.next();
            double gap_start = pos.y - pos.width / 2.0;
            double gap_stop = pos.y + pos.width / 2.0;

            final Point3d notchCenter = Plane.planeXZ(pos.y, true).getIntersection(bottom_line);
            final Plane plane0 = Plane.planeXZ(gap_start, true);
            final Point3d notchStart = plane0.getIntersection(bottom_line);
            final Plane plane1 = Plane.planeXZ(gap_stop, false).flip();
            final Point3d notchStop = plane1.getIntersection(bottom_line);

//            Tuple2d notchCenter = Extruder.getIntersection(bottom_line, transform, pos.y);
//            Tuple2d notchStart = Extruder.getIntersection(bottom_line, transform, gap_start);
//            Tuple2d notchStop = Extruder.getIntersection(bottom_line, transform, gap_stop);
            if ((notchCenter != null) && (notchStart != null) && (notchStop != null)) {
                double netchDepth = Math.max(Math.max(notchCenter.z + (info.thickness / 2.0), notchStart.z + (info.thickness / 2.0)), notchStop.z + (info.thickness / 2.0));
                pos.keel_z = Extruder.cutNotch(extruded_outer_top_line, plane0, plane1, new Vector3d(0, 0, netchDepth));
            } else {
                if (LOGGER.isLoggable(Level.WARNING)) {
                    LOGGER.log(Level.WARNING, "Removing rib position {0}", new Object[]{pos.y});
                }
                itre.remove();
            }
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Exctruded {0} points from {1} after notches", new Object[]{extruded_outer_top_line.size(), bottom_line.size()});
        }

        // FIXME Find a solution to search for the gap_stop 
        // Cut two Notches for the Deck to fit to the Keel
        {
            Tuple2d notchStop = transform.get2dCoord(bottom_line.get(0));
            double gap_stop = notchStop.x - (info.thickness * (2 / 3.0));
            double notchDeth = notchStop.y - info.width / 2.0;
            for (int i = extruded_outer_top_line.size() - 1; i > 0 && transform.getY(extruded_outer_top_line.get(i)) > notchDeth; i--) {
                Tuple2d notchFix = transform.get2dCoord(extruded_outer_top_line.get(i));
                notchFix.y = notchDeth;
                // FIXME perhaps move the x coordinate too 
                extruded_outer_top_line.get(i).set(transform.get3dCoord(notchFix));
            }
            if (bottom_line.get(0).y > bottom_line.get(bottom_line.size() - 1).y) {
                info.max_y = gap_stop;
                info.max_z = notchDeth;
            } else {
                info.min_y = gap_stop;
                info.min_z = notchDeth;
            }
            extruded_outer_top_line.add(transform.get3dCoord(new Point2d(gap_stop, notchDeth)));
            extruded_outer_top_line.add(transform.get3dCoord(new Point2d(gap_stop, notchStop.y)));
        }

        // Cut two Notches for the Deck to fit to the Keel
        {
            Tuple2d notchStart = transform.get2dCoord(bottom_line.get(bottom_line.size() - 1));
            double gap_start = notchStart.x + (info.thickness * (2 / 3.0));
            double notchDeth = notchStart.y - info.width / 2.0;
            for (int i = 0; i < (extruded_outer_top_line.size() - 1) && transform.getY(extruded_outer_top_line.get(i)) > notchDeth; i++) {
                Tuple2d notchFix = transform.get2dCoord(extruded_outer_top_line.get(i));
                notchFix.y = notchDeth;
                // FIXME perhaps move the x coordinate too 
                extruded_outer_top_line.get(i).set(transform.get3dCoord(notchFix));
            }
            if (bottom_line.get(0).y > bottom_line.get(bottom_line.size() - 1).y) {
                info.min_y = gap_start;
                info.min_z = notchDeth;
            } else {
                info.max_y = gap_start;
                info.max_z = notchDeth;
            }
            extruded_outer_top_line.add(0, transform.get3dCoord(new Point2d(gap_start, notchDeth)));
            extruded_outer_top_line.add(0, transform.get3dCoord(new Point2d(gap_start, notchStart.y)));
        }
        if (reverse) {
            java.util.Collections.reverse(extruded_outer_top_line);
        }

        return Simplifier.simplifyPolygonLineSegments(extruded_outer_top_line);
    }

    private static java.util.List<Point3d> getExtrusion(final java.util.List<Point3d> bottom_line, final double keel_x_position, final double plate_width, final int maxCollisionDepth) {
        Transform3dToYZ2d transform = new Transform3dToYZ2d(keel_x_position);

        java.util.List<Point3d> extruded_outer_top_line = Extruder.extrude(bottom_line, transform, plate_width, maxCollisionDepth);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Exctruded {0} points from {1} before notches\n", new Object[]{extruded_outer_top_line.size(), bottom_line.size()});
        }
        clipEndPoints(extruded_outer_top_line, bottom_line, transform);
        return Simplifier.simplifyPolygonLineSegments(extruded_outer_top_line);
    }
//

    public static java.util.List<Point3d> getKeelEdgesAtX(final Mesh hull_MESH, double keel_x_position) {
        final Plane plane = Plane.planeYZ(keel_x_position, true);

        // Accumalate the points for the intersection , by calculating the intersection of the edge with the keel_x_position
        final java.util.List<Point3d> stern_to_bow_vertices = ModelTracer.traceProfileLine(hull_MESH, plane, new ModelTracer.StartSelector() {

            @Override
            public Face selectStartFace(Mesh hull_MESH, java.util.List<Face> faces, Plane plane) {
                return getSternFace(hull_MESH, faces, plane);
            }

            @Override
            public Edge selectStartEdge(Mesh hull_MESH, Face startFace, Plane plane) {
                return getSternEdge(hull_MESH, startFace, plane);
            }

            @Override
            public Point3d selectStartPoint(Mesh hull_MESH, Edge startEdge, Plane plane) {
                return getSternPoint(hull_MESH, startEdge, plane);
            }

        });
//        java.util.List<Point3d> stern_to_bow_vertices = getKeelEdgesAt_x_position(hull_MESH, keel_x_position);
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "Found {0} raw points intersecting the YZ plane at X={1}", new Object[]{stern_to_bow_vertices.size(), String.format("%12.8f", keel_x_position)});
        }
        // FIXME , get threshhold from paramater.
        return Simplifier.simplifyPolygonLineSegments(stern_to_bow_vertices);
    }

    private static Face getSternFace(final Mesh hull_MESH, final java.util.List<Face> faces, final Plane plane) {
        final java.util.Set<Edge> surfaceEdges = new java.util.HashSet<>();
        for (Face face : faces) {
            Plane.FaceIntersectType type = ModelTracer.getIntersectType(hull_MESH, face, plane);
            switch (type) {
                case TWO_EDGES: { // No vertex touched xz plane at keel_x_position , but face has vertexes on both sides of YZ plane at keel_x_position
                    // detect the potential start edge
                    final Vertex v1 = hull_MESH.verts.get(face.verts[0]);
                    final Vertex v2 = hull_MESH.verts.get(face.verts[1]);
                    final Vertex v3 = hull_MESH.verts.get(face.verts[2]);
                    if (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) && v2.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                        Edge edge = hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v2));
                        if (ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                            surfaceEdges.add(edge);
                        }
                    } else if (v2.isSet(Vertex.Flag.MANIFOLD_EDGE) && v3.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                        Edge edge = hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v2, v3));
                        if (ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                            surfaceEdges.add(edge);
                        }
                    } else if (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) && v3.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                        Edge edge = hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v3));
                        if (ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                            surfaceEdges.add(edge);
                        }
                    }
                }
                break;
                case COPLANAR_VERTEX: { // one vertex touched xz plane at keel_x_position
                    // need to detect special case of the ManifoldEdge , we dont want the face, but we want the othe face and edges on the shared vertex at keel_x_position
                    final Vertex v1 = ModelTracer.selectCoPlanarVertex(hull_MESH, face, plane);
                    if (v1.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                        for (int edgeIdx : v1.edge_keys) {
                            Edge edge = hull_MESH.edges.get(edgeIdx);
                            if (edge.isManifoldEdge() && ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.COPLANAR_VERTEX) {
                                surfaceEdges.add(edge);
                                break;
                            }
                        }
                    }
                }
                break;
                case COPLANAR_EDGE: { // two vertexes touched xz plane at keel_x_position
                    //    We dont want the coplanar edge, we are looking for one of the oposite edges that are a manifolde edge 
                    if (hull_MESH.face_ge_X(face, plane.distance)) {
                        // detect the potential start edge
                        final Edge edge = ModelTracer.selectCoPlanarEdge(hull_MESH, face, plane);
                        Vertex v1 = ModelTracer.getOppositeVertex(hull_MESH, face, edge);
                        for (int edgeIndex : face.getSharedEdges(v1)) {
                            final Edge otherEdge = hull_MESH.edges.get(edgeIndex);
                            if (otherEdge.isManifoldEdge() && ModelTracer.getIntersectType(hull_MESH, otherEdge, plane) != Plane.EdgeIntersectType.COPLANAR_EDGE) {
                                surfaceEdges.add(otherEdge);
                            }
                        }
                    }
//                        final Vertex v1 = hull_MESH.verts.get(face.verts[0]);
//                        final Vertex v2 = hull_MESH.verts.get(face.verts[1]);
//                        final Vertex v3 = hull_MESH.verts.get(face.verts[2]);
//                        final boolean v1_onx = v1.isOnX(plane.distance);
//                        final boolean v2_onx = v2.isOnX(plane.distance);
//                        final boolean v3_onx = v3.isOnX(plane.distance);
//                        if (v1_onx && v2_onx && (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) || v2.isSet(Vertex.Flag.MANIFOLD_EDGE))) {
//                            surfaceEdges.add(hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v2)));
//                        } else if (v2_onx && v3_onx && (v2.isSet(Vertex.Flag.MANIFOLD_EDGE) || v3.isSet(Vertex.Flag.MANIFOLD_EDGE))) {
//                            surfaceEdges.add(hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v2, v3)));
//                        } else if (v1_onx && v3_onx && (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) || v3.isSet(Vertex.Flag.MANIFOLD_EDGE))) {
//                            surfaceEdges.add(hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v3)));
//                        }                    
                }
                break;
                case INTERSECT_AND_COPLANAR_VERTEX: { // one vertex touched xz plane at keel_x_position , and have two vertexes on either side  of YZ plane at keel_x_position

                    final Vertex v1 = hull_MESH.verts.get(face.verts[0]);
                    final Vertex v2 = hull_MESH.verts.get(face.verts[1]);
                    final Vertex v3 = hull_MESH.verts.get(face.verts[2]);
                    final boolean v1_onx = v1.isOnX(plane.distance);
                    final boolean v2_onx = v2.isOnX(plane.distance);
                    final boolean v3_onx = v3.isOnX(plane.distance);
                    if ((v1_onx || v2_onx) && (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) || v2.isSet(Vertex.Flag.MANIFOLD_EDGE)) && (v1.coord.x >= plane.distance && v2.coord.x >= plane.distance)) {
                        surfaceEdges.add(hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v2)));
                    } else if ((v3_onx || v2_onx) && (v3.isSet(Vertex.Flag.MANIFOLD_EDGE) || v2.isSet(Vertex.Flag.MANIFOLD_EDGE)) && (v3.coord.x >= plane.distance && v2.coord.x >= plane.distance)) {
                        surfaceEdges.add(hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v2, v3)));
                    } else if ((v1_onx || v3_onx) && (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) || v3.isSet(Vertex.Flag.MANIFOLD_EDGE)) && (v1.coord.x >= plane.distance && v3.coord.x >= plane.distance)) {
                        surfaceEdges.add(hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v3)));
                    }
                }
                break;
                default: { // No vertex touched xz plane at keel_x_position , or the Face is IN the xz plane
                    // This is only a problem when x=0, it should be quite possible for the face to be in the xz plane for other values of x, we should handle this
                    System.err.printf("Currupt Face %d  type:%s plane:%s \n", face.index, type, plane);
                    for (int edgIdx : face.edge_keys) {
                        Edge e = hull_MESH.edges.get(edgIdx);
                        Vertex v1 = hull_MESH.verts.get(e.verts[0]);
                        Vertex v2 = hull_MESH.verts.get(e.verts[1]);
                        System.err.printf("\t Currupt Face %d keelPosition:%.2f  edge:%d   %d:%b:(%9.5f,%9.5f,%9.5f)  %d:%b:(%9.5f,%9.5f,%9.5f) faces:%s  type:%s\n", face.index, plane.distance, e.index, e.verts[0], v1.isSet(Vertex.Flag.MANIFOLD_EDGE), v1.coord.x, v1.coord.y, v1.coord.z, e.verts[1], v2.isSet(Vertex.Flag.MANIFOLD_EDGE), v2.coord.x, v2.coord.y, v2.coord.z, e.face_keys, ModelTracer.getIntersectType(hull_MESH, e, plane));
                    }
                    // System.err.printf("\t Current Result[%d]\n%s\n\n", result.faces.size(), result.faces);
                    // throw new RuntimeException(String.format("Hull_MESH seems to have corrupt face %d , touchesX:%d , intersectsX:%d", face.index, touchesX, intersectsX));
                }
            }
        }
        if (surfaceEdges.size() == 2) {
            Face stern_face;
            Edge stern_edge;
            Vertex stern_vert;// if stern edge is onAxis this will be the vert on the edge
            Face bow_face = null;
            Edge bow_edge;
            Vertex bow_vert; //  if bow edge is onAxis this will be the vert on the edge
            java.util.List<Edge> surfaceList = new java.util.ArrayList<>(surfaceEdges);
            Edge edge0 = surfaceList.get(0);
            Vertex vert_e0_0 = hull_MESH.verts.get(edge0.verts[0]);
//            Vertex vert_e0_1 = hull_MESH.verts.get(edge0.verts[1]);
            Edge edge1 = surfaceList.get(1);
            Vertex vert_e1_0 = hull_MESH.verts.get(edge1.verts[0]);
//            Vertex vert_e1_1 = hull_MESH.verts.get(edge1.verts[1]);
            if (vert_e0_0.coord.y < vert_e1_0.coord.y) { // FIXME this is a very simplistic version...
                bow_edge = edge1;
                stern_edge = edge0;
            } else {
                bow_edge = edge0;
                stern_edge = edge1;
            }
            for (int faceIdx : bow_edge.face_keys) {
                if (faces.contains(hull_MESH.faces.get(faceIdx))) {
                    return hull_MESH.faces.get(faceIdx);
                }
            }
//            if (hull_MESH.verts.get(bow_edge.verts[0]).isSet(Vertex.Flag.MANIFOLD_EDGE)) {
//                bow_vert = hull_MESH.verts.get(bow_edge.verts[0]);
//            }
//            if (hull_MESH.verts.get(bow_edge.verts[1]).isSet(Vertex.Flag.MANIFOLD_EDGE)) {
//                bow_vert = hull_MESH.verts.get(bow_edge.verts[1]);
//            }
//            for (int faceIdx : stern_edge.face_keys) {
//                if (faces.contains(hull_MESH.faces.get(faceIdx))) {
//                    return hull_MESH.faces.get(faceIdx);
//                }
//            }
//            if (hull_MESH.verts.get(stern_edge.verts[0]).isSet(Vertex.Flag.MANIFOLD_EDGE)) {
//                stern_vert = hull_MESH.verts.get(stern_edge.verts[0]);
//            }
//            if (hull_MESH.verts.get(stern_edge.verts[1]).isSet(Vertex.Flag.MANIFOLD_EDGE)) {
//                stern_vert = hull_MESH.verts.get(stern_edge.verts[1]);
//            }
            throw new IllegalArgumentException("Could not find Keel start face at plane: " + plane);
        } else {
            System.err.printf("Found a non continuous face list . following edges have problems [%d] : %s \n", surfaceEdges.size(), surfaceEdges);
            for (Edge e : surfaceEdges) {
                Vertex v1 = hull_MESH.verts.get(e.verts[0]);
                Vertex v2 = hull_MESH.verts.get(e.verts[1]);
                System.err.printf("\t _get_keel_faces_at_x surfaceEdges e[%d]  verts  : %d:%b:(%9.5f,%9.5f,%9.5f)  %d:%b:(%9.5f,%9.5f,%9.5f) faces:%s type:%s\n", e.index, e.verts[0], v1.isSet(Vertex.Flag.MANIFOLD_EDGE), v1.coord.x, v1.coord.y, v1.coord.z, e.verts[1], v2.isSet(Vertex.Flag.MANIFOLD_EDGE), v2.coord.x, v2.coord.y, v2.coord.z, e.face_keys, ModelTracer.getIntersectType(hull_MESH, e, plane));
                for (int fidx : e.face_keys) {
                    Face face = hull_MESH.faces.get(fidx);
                    final int intersectsX = hull_MESH.intersects_X(face, plane.distance);
                    int touchesX = hull_MESH.touches_X(face, plane.distance);
                    System.err.printf("\t\tCurrupt surface Edges face %d  touchesX:%d  intersectsX:%d  plane:%s  type:%s\n", face.index, touchesX, intersectsX, plane, ModelTracer.getIntersectType(hull_MESH, face, plane));
                    for (int edgIdx : face.edge_keys) {
                        Edge fe = hull_MESH.edges.get(edgIdx);
                        Vertex ev1 = hull_MESH.verts.get(fe.verts[0]);
                        Vertex ev2 = hull_MESH.verts.get(fe.verts[1]);
                        System.err.printf("\t\t\t Currupt Face %d plane:%s edge:%d   %d:%b:(%9.5f,%9.5f,%9.5f)  %d:%b:(%9.5f,%9.5f,%9.5f) faces:%s type:%s\n", face.index, plane, fe.index, fe.verts[0], ev1.isSet(Vertex.Flag.MANIFOLD_EDGE), ev1.coord.x, ev1.coord.y, ev1.coord.z, fe.verts[1], ev2.isSet(Vertex.Flag.MANIFOLD_EDGE), ev2.coord.x, ev2.coord.y, ev2.coord.z, fe.face_keys, ModelTracer.getIntersectType(hull_MESH, fe, plane));
                    }
                }
            }
            throw new IllegalArgumentException(String.format("Found a non continuous face list . following edges have only 1 face references... %s ", surfaceEdges));
        }
    }

    private static Edge getSternEdge(final Mesh hull_MESH, final Face startFace, final Plane plane) {
        Plane.FaceIntersectType type = ModelTracer.getIntersectType(hull_MESH, startFace, plane);
        final Vertex v1 = hull_MESH.verts.get(startFace.verts[0]);
        final Vertex v2 = hull_MESH.verts.get(startFace.verts[1]);
        final Vertex v3 = hull_MESH.verts.get(startFace.verts[2]);
        final boolean v1_onx = v1.isOnX(plane.distance);
        final boolean v2_onx = v2.isOnX(plane.distance);
        final boolean v3_onx = v3.isOnX(plane.distance);
        switch (type) {
            case TWO_EDGES: {
                if (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) && v2.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                    Edge edge = hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v2));
                    if (ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                        return edge;
                    }
                }
                if (v2.isSet(Vertex.Flag.MANIFOLD_EDGE) && v3.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                    Edge edge = hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v2, v3));
                    if (ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                        return edge;
                    }
                }
                if (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) && v3.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                    Edge edge = hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v3));
                    if (ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.INTERSECT) {
                        return edge;
                    }
                }
                throw new IllegalArgumentException(String.format("Startface #%d does not seem to be on the manifolde edge at plane : %s  intersectionType:%s", startFace.index, plane, ModelTracer.getIntersectType(hull_MESH, startFace, plane)));
            }
            case COPLANAR_EDGE: {
                // detect the potential start edge
                final Edge edge = ModelTracer.selectCoPlanarEdge(hull_MESH, startFace, plane);
                Vertex opposite = ModelTracer.getOppositeVertex(hull_MESH, startFace, edge);
                for (int edgeIndex : startFace.getSharedEdges(opposite)) {
                    final Edge otherEdge = hull_MESH.edges.get(edgeIndex);
                    if (otherEdge.isManifoldEdge() && ModelTracer.getIntersectType(hull_MESH, otherEdge, plane) != Plane.EdgeIntersectType.COPLANAR_EDGE) {
                        return otherEdge;
                    }
                }
                throw new IllegalArgumentException(String.format("Startface #%d does not seem to be on the manifolde edge at plane : %s  intersectionType:%s", startFace.index, plane, ModelTracer.getIntersectType(hull_MESH, startFace, plane)));
            }
            case COPLANAR_VERTEX: {
                final Vertex coplanarvertex = ModelTracer.selectCoPlanarVertex(hull_MESH, startFace, plane);
                if (coplanarvertex.isSet(Vertex.Flag.MANIFOLD_EDGE)) {
                    for (int edgeIdx : coplanarvertex.edge_keys) {
                        Edge edge = hull_MESH.edges.get(edgeIdx);
                        if (edge.isManifoldEdge() && ModelTracer.getIntersectType(hull_MESH, edge, plane) == Plane.EdgeIntersectType.COPLANAR_VERTEX) {
                            return edge;
                        }
                    }
                }
                throw new IllegalArgumentException(String.format("Startface #%d does not seem to be on the manifolde edge at plane : %s  intersectionType:%s", startFace.index, plane, ModelTracer.getIntersectType(hull_MESH, startFace, plane)));
            }

            case INTERSECT_AND_COPLANAR_VERTEX:

                if ((v1_onx || v2_onx) && (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) || v2.isSet(Vertex.Flag.MANIFOLD_EDGE)) && (v1.coord.x >= plane.distance && v2.coord.x >= plane.distance)) {
                    return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v2));
                } else if ((v3_onx || v2_onx) && (v3.isSet(Vertex.Flag.MANIFOLD_EDGE) || v2.isSet(Vertex.Flag.MANIFOLD_EDGE)) && (v3.coord.x >= plane.distance && v2.coord.x >= plane.distance)) {
                    return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v2, v3));
                } else if ((v1_onx || v3_onx) && (v1.isSet(Vertex.Flag.MANIFOLD_EDGE) || v3.isSet(Vertex.Flag.MANIFOLD_EDGE)) && (v1.coord.x >= plane.distance && v3.coord.x >= plane.distance)) {
                    return hull_MESH.edges.get(hull_MESH.findCommonEdgeKey(v1, v3));
                }
                throw new IllegalArgumentException(String.format("Startface #%d does not seem to be on the manifolde edge at plane : %s  intersectionType:%s", startFace.index, plane, ModelTracer.getIntersectType(hull_MESH, startFace, plane)));
            default:
                throw new IllegalArgumentException(String.format("Startface #%d does not seem to be on the manifolde edge at plane : %s  intersectionType:%s", startFace.index, plane, ModelTracer.getIntersectType(hull_MESH, startFace, plane)));
        }

    }

    private static Point3d getSternPoint(final Mesh hull_MESH, final Edge edge, final Plane plane) {
        Plane.EdgeIntersectType type = ModelTracer.getIntersectType(hull_MESH, edge, plane);
        switch (type) {
            case COPLANAR_EDGE:
                final Vertex vert_1 = hull_MESH.verts.get(edge.verts[0]);
                final Vertex vert_2 = hull_MESH.verts.get(edge.verts[1]);
                if (vert_1.coord.z > vert_2.coord.z) {
                    return new Point3d(vert_1.coord);
                } else {
                    return new Point3d(vert_2.coord);
                }
            case COPLANAR_VERTEX:
                return new Point3d(ModelTracer.selectCoPlanarVertex(hull_MESH, edge, plane).coord);
            case INTERSECT:
                return new Point3d(ModelTracer.getIntersection(hull_MESH, edge, plane));
            default:
                throw new IllegalArgumentException("Edge #" + edge.index + " does not intersect plane " + plane + "\n" + ModelUtils.dumpEdge(hull_MESH, edge));
        }
    }
}
