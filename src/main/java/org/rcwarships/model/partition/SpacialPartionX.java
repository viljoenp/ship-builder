/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import javax.vecmath.Tuple3d;
import org.rcwarships.model.Epsilon;

/**
 *
 * @author viljoenp
 * @param <F>
 */
public class SpacialPartionX<F extends Tuple3d, V> {

    double epsilon = Epsilon.E;
    int size = 0;
    final double partitionSize;
    final double minX;
    final double maxX;
    final double minY;
    final double maxY;

    // Each bucket is is futher divided by a Y partition
    final SpacialPartionY<F, V>[] yPartitions;

    // if a bucket gets to many points we will split the contents of the yPartition into a finerX partition
    final SpacialPartionX<F, V>[] finerPartitions;

    SpacialPartionX(final double minX, final double maxX, final int partitionCount, double minY, double maxY) {
        if (minX > maxX) {
            throw new IllegalArgumentException("Min X (" + minX + ") should be less that Max X (" + maxX + ") ");
        }
        if (minY > maxY) {
            throw new IllegalArgumentException("Min Y (" + minY + ") should be less that Max Y (" + maxY + ") ");
        }
        if (partitionCount < 1) {
            throw new IllegalArgumentException("partitionCount (" + partitionCount + ") should be greater than 1");
        }
        this.partitionSize = Math.abs((maxX - minX) / (double) partitionCount);

        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.yPartitions = new SpacialPartionY[partitionCount];
        this.finerPartitions = new SpacialPartionX[partitionCount];
        java.util.Arrays.fill(yPartitions, null);
        java.util.Arrays.fill(finerPartitions, null);
    }

    public double getEpsilon() {
        return epsilon;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
        for (int i = 0; i < yPartitions.length; i++) {
            if (finerPartitions[i] != null) {
                finerPartitions[i].setEpsilon(epsilon);
            } else if (yPartitions[i] != null) {
                yPartitions[i].setEpsilon(epsilon);
            }
        }
    }

    public Entry<F, V> locate(F tuple) {
        if (tuple.x < minX) {
            return null;
        }
        if (tuple.x > maxX) {
            return null;
        }
        int partitionIndex = (int) Math.floor(Math.abs(tuple.x - minX) / partitionSize);
        // first check if we have further x Partitions..
        if (finerPartitions[partitionIndex] != null) {
            return finerPartitions[partitionIndex].locate(tuple);
        } else if (yPartitions[partitionIndex] != null) {
            return yPartitions[partitionIndex].locate(tuple);
        }
        return null;

    }

    public void addAll(final java.util.Collection<Entry<F, V>> values) {
        for (Entry<F, V> entry : values) {
            add(entry);
        }
    }

    public java.util.List<Entry<F, V>> getAll(final java.util.List<Entry<F, V>> result) {
        for (int i = 0; i < yPartitions.length; i++) {
            if (finerPartitions[i] != null) {
                finerPartitions[i].getAll(result);
            } else if (yPartitions[i] != null) {
                yPartitions[i].getAll(result);
            }
        }
        return result;
    }

    public void add(final F tuple, V value) {
        add(new Entry<>(tuple, value));
    }

    public void add(final Entry<F, V> entry) {
        if (entry.tuple.x < minX) {
            throw new IllegalArgumentException("Min X value allowed in spacial Partion = " + minX + " trying to add tuple :" + entry.tuple);
        }
        if (entry.tuple.x > maxX) {
            throw new IllegalArgumentException("Max X value allowed in spacial Partion = " + maxX + " trying to add tuple :" + entry.tuple);
        }
        int partitionIndex = (int) Math.floor(Math.abs(entry.tuple.x - minX) / partitionSize);
        // first check if we have further x Partitions..
        if (finerPartitions[partitionIndex] != null) {
            finerPartitions[partitionIndex].add(entry);
        } else {
            if (yPartitions[partitionIndex] == null) {
                yPartitions[partitionIndex] = new SpacialPartionY<>(minY, maxY, yPartitions.length);
                yPartitions[partitionIndex].setEpsilon(epsilon);
            }
            yPartitions[partitionIndex].add(entry);
            // Check point Distribution... and make a finer SpacialPartionX for the slot if needed

            if (yPartitions[partitionIndex].size() >= 8) {
                double finerMinX = minX + (partitionSize * (double) partitionIndex);
                finerPartitions[partitionIndex] = new SpacialPartionX<>(finerMinX, finerMinX + partitionSize, yPartitions.length, minY, maxY);
                finerPartitions[partitionIndex].setEpsilon(epsilon);
                java.util.List<Entry<F, V>> all = new java.util.ArrayList<>(yPartitions[partitionIndex].size());
                yPartitions[partitionIndex].getAll(all);
                finerPartitions[partitionIndex].addAll(all);
                yPartitions[partitionIndex].clear();
                yPartitions[partitionIndex] = null;
            }
        }
        size++;
    }

    int size() {
        return size;
    }

    private static class SpacialPartionY<F extends Tuple3d, V> {

        double epsilon = Epsilon.E;
        int size = 0;
        final double minY;
        final double maxY;
        final double partitionSize;

        // Each bucket contains a list of Tuples
        final java.util.List<Entry<F, V>>[] rawPartitions;
        // if a bucket gets to many points we will split the contents of the rawPartition into a finerY partition
        final SpacialPartionY<F, V>[] finerPartitions;

        public SpacialPartionY(final double minY, final double maxY, final int partitionCount) {
            if (minY > maxY) {
                throw new IllegalArgumentException("Min Y (" + minY + ") should be less that Max Y (" + maxY + ") ");
            }
            this.minY = minY;
            this.maxY = maxY;
            this.partitionSize = (maxY - minY) / (double) partitionCount;
            this.rawPartitions = new java.util.List[partitionCount];
            this.finerPartitions = new SpacialPartionY[partitionCount];
            java.util.Arrays.fill(rawPartitions, null);
            java.util.Arrays.fill(finerPartitions, null);

        }

        public double getEpsilon() {
            return epsilon;
        }

        public void setEpsilon(double epsilon) {
            this.epsilon = epsilon;
            for (SpacialPartionY<F, V> finerPartition : finerPartitions) {
                if (finerPartition != null) {
                    finerPartition.setEpsilon(epsilon);
                }
            }
        }

        public void clear() {
            for (int i = 0; i < rawPartitions.length; i++) {
                if (finerPartitions[i] != null) {
                    finerPartitions[i].clear();
                } else if (rawPartitions[i] != null) {
                    rawPartitions[i].clear();
                }
            }
            java.util.Arrays.fill(rawPartitions, null);
            java.util.Arrays.fill(finerPartitions, null);
            size = 0;
        }

        public java.util.List<Entry<F, V>> getAll(final java.util.List<Entry<F, V>> result) {
            for (int i = 0; i < rawPartitions.length; i++) {
                if (finerPartitions[i] != null) {
                    finerPartitions[i].getAll(result);
                } else if (rawPartitions[i] != null) {
                    result.addAll(rawPartitions[i]);
                }
            }
            return result;
        }

        public void addAll(final java.util.Collection<Entry<F, V>> values) {
            for (Entry<F, V> entry : values) {
                add(entry);
            }
        }

        public Entry<F, V> locate(F tuple) {
            if (tuple.y < minY) {
                return null;
            }
            if (tuple.y > maxY) {
                return null;
            }
            int partitionIndex = (int) Math.floor(Math.abs(tuple.y - minY) / partitionSize);
            // first check if we have further x Partitions..
            if (finerPartitions[partitionIndex] != null) {
                return finerPartitions[partitionIndex].locate(tuple);
            } else if (rawPartitions[partitionIndex] != null) {
                for (Entry<F, V> entry : rawPartitions[partitionIndex]) {
                    if ((Math.abs(entry.tuple.x - tuple.x) <= epsilon) && (Math.abs(entry.tuple.y - tuple.y) <= epsilon)) {
                        return entry;
                    }
                }
            }
            return null;
        }

        public void add(final Entry<F, V> entry) {
            if (entry.tuple.y < minY) {
                throw new IllegalArgumentException("Min Y value allowed in spacial Partion = " + minY + " trying to add tuple :" + entry.tuple);
            }
            if (entry.tuple.y > maxY) {
                throw new IllegalArgumentException("Max Y value allowed in spacial Partion = " + maxY + " trying to add tuple :" + entry.tuple);
            }
            int partitionIndex = (int) Math.floor(Math.abs(entry.tuple.y - minY) / partitionSize);
            // first check if we have further x Partitions..
            if (finerPartitions[partitionIndex] != null) {
                finerPartitions[partitionIndex].add(entry);
            } else {
                if (rawPartitions[partitionIndex] == null) {
                    rawPartitions[partitionIndex] = new java.util.ArrayList<>(8);
                }
                rawPartitions[partitionIndex].add(entry);
                // Check point Distribution... and make a finer SpacialPartionY for the slot if needed
                if (rawPartitions[partitionIndex].size() >= 8) {
                    double finerMinY = minY + (partitionSize * (double) partitionIndex);
                    finerPartitions[partitionIndex] = new SpacialPartionY<>(finerMinY, finerMinY + partitionSize, rawPartitions.length);
                    finerPartitions[partitionIndex].setEpsilon(epsilon);
                    finerPartitions[partitionIndex].addAll(rawPartitions[partitionIndex]);
                    rawPartitions[partitionIndex].clear();
                    rawPartitions[partitionIndex] = null;
                }
            }
            size++;
        }

        int size() {
            return size;
        }
    }

    public static class Entry<F, V> {

        public final F tuple;
        public final V value;

        public Entry(final F tuple, final V value) {
            this.tuple = tuple;
            this.value = value;
        }
    }

}
