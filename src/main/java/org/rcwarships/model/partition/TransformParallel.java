/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.rcwarships.model.Vertex;

/**
 *
 * @author viljoenp
 */
public interface TransformParallel {

    Point3d worldToPlane(final Point3d tuple);

    Vector3d worldToPlane(final Vector3d tuple);

    Point3d planeToPlane(final Point3d tuple);

    Vector3d planeToPlane(final Vector3d tuple);

    java.util.List<Point3d> transformToPlane(final java.util.List<Vertex> line);
}
