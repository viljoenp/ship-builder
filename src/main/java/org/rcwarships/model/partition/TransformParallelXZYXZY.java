/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 *
 * @author viljoenp
 */
class TransformParallelXZYXZY extends AbstractTransform {

    final double base;

    public TransformParallelXZYXZY(final double base) {
        this.base = base;
    }

    /**
     * transform x=>z , y=>x , z=>y (y,z,x-base)
     */
    @Override
    public Point3d worldToPlane(final Point3d tuple) {
        return new Point3d(tuple.y, tuple.z, tuple.x - base);
    }

    /**
     * transform x=>z , y=>x , z=>y (y,z,x-base)
     */
    @Override
    public Vector3d worldToPlane(final Vector3d tuple) {
        return new Vector3d(tuple.y, tuple.z, tuple.x - base);
    }

    /**
     * inverse transform x=>z , y=>x , z=>y (z+base,x,y)
     */
    @Override
    public Point3d planeToPlane(final Point3d tuple) {
        return new Point3d(tuple.z + base, tuple.x, tuple.y);
    }

    /**
     * inverse transform x=>z , y=>x , z=>y (z+base,x,y)
     */
    @Override
    public Vector3d planeToPlane(final Vector3d tuple) {
        return new Vector3d(tuple.z + base, tuple.x, tuple.y);
    }

    
    
}
