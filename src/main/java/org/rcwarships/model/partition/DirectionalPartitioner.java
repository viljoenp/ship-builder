/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 *
 * @author viljoenp
 */
public class DirectionalPartitioner extends AbstractPartitioner {

    private final static Logger LOGGER = Logger.getLogger(DirectionalPartitioner.class.getName());

    @Override
    public java.util.List<Partition> partition(final Partition partition) {
        final java.util.List<Partition> partitions = new java.util.ArrayList<>();
// Filter only direction changes a corresponding  direction changes in the visinity
        final java.util.List<Integer> line1 = scanDirectionChange(partition.line1p);
        final java.util.List<Integer> line2 = scanDirectionChange(partition.line2p);
        // Filter only direction changes a corresponding  direction changes in the visinity
        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        filterVicinity(partition, line1, line2, true);
        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        if (line1.size() != line2.size()) {
            // FIXME, we should try to find nearest Matches 
            LOGGER.log(Level.WARNING, "The Direction Change Partitions have un-even number of knucles");
        }
        int line1Start = 0, line2Start = 0;
        for (int i = 0; i < Math.max(line1.size(), line2.size()); i++) { // Ensure we get Index out of bounds if line1.size != line2.size
            final Partition tmp = new Partition(
                    partition.line1.subList(line1Start, line1.get(i) + 1),
                    partition.line1p.subList(line1Start, line1.get(i) + 1),
                    partition.line2.subList(line2Start, line2.get(i) + 1),
                    partition.line2p.subList(line2Start, line2.get(i) + 1));
            partitions.add(tmp);

            line1Start = line1.get(i);
            line2Start = line2.get(i);
        }
        final Partition tmp = new Partition(
                partition.line1.subList(line1Start, partition.line1.size()),
                partition.line1p.subList(line1Start, partition.line1p.size()),
                partition.line2.subList(line2Start, partition.line2.size()),
                partition.line2p.subList(line2Start, partition.line2p.size()));
        partitions.add(tmp);
        return partitions;
    }

    private java.util.List<Integer> scanDirectionChange(final java.util.List<Point3d> line) {
        final java.util.List<Integer> directionChanges = new java.util.ArrayList<>();
        double[] crossz = new double[line.size()];
        java.util.Arrays.fill(crossz, 0);
        if (line.size() > 1) {
            Vector3d v01 = new Vector3d();
            v01.sub(line.get(0), line.get(1));
            for (int i = 1; i < line.size() - 1; i++) {
                final Vector3d v12 = new Vector3d();
                v12.sub(line.get(i), line.get(i + 1));
                final Vector3d cross = new Vector3d();
                cross.cross(v01, v12);
                crossz[i] = cross.z;
                v01 = v12;
            }
        }
        for (int i = 2; i < line.size() - 1; i++) {
            if (Math.signum(crossz[i - 1]) != Math.signum(crossz[i])) {
                directionChanges.add(i);
            }
        }
        return directionChanges;
    }
}
