/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point2d;

/**
 *
 * @author viljoenp
 */
public abstract class AbstractPartitioner implements Partitioner {

    private final static Logger LOGGER = Logger.getLogger(AbstractPartitioner.class.getName());
    protected static double MAX_PAIRED_DISTANCE = 3.8;

    static {
        if (System.getProperty("max-paired-distance") != null) {
            MAX_PAIRED_DISTANCE = Double.parseDouble(System.getProperty("max-paired-distance"));
        }
    }

    @Override
    public java.util.List<Partition> partition(final java.util.List<Partition> partitions) {
        final java.util.List<Partition> results = new java.util.ArrayList<>();
        for (Partition partition : partitions) {
            results.addAll(partition(partition));
        }
        return results;
    }

    protected String toString(double[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("( ");
        for (int i = 0; i < arr.length; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(arr[i]);
        }
        sb.append(" )");
        return sb.toString();
    }

    /**
     * Remove entries in line that has no counter part knuckle in its vicinity
     *
     * @param partition
     * @param line1
     * @param line2
     */
    protected void filterVicinity(final Partition partition, final java.util.List<Integer> line1, final java.util.List<Integer> line2, final boolean inVicinity) {

        final double[] minDistances12 = new double[line1.size()];
        final int[] nearest12 = new int[line1.size()];
        java.util.Arrays.fill(minDistances12, Double.MAX_VALUE);
        final double[] minDistances21 = new double[line2.size()];
        final int[] nearest21 = new int[line2.size()];
        java.util.Arrays.fill(minDistances21, Double.MAX_VALUE);
        for (int i = 0; i < line1.size(); i++) {
            final Point2d p1 = new Point2d(partition.line1p.get(line1.get(i)).x, partition.line1p.get(line1.get(i)).y);
            for (int j = 0; j < line2.size(); j++) {
                final Point2d p2 = new Point2d(partition.line2p.get(line2.get(j)).x, partition.line2p.get(line2.get(j)).y);
                double p12 = p1.distance(p2);
                if (minDistances12[i] > p12) {
                    minDistances12[i] = p12;
                    nearest12[i] = j;
                }
                if (minDistances21[j] > p12) {
                    minDistances21[j] = p12;
                    nearest21[j] = i;
                }
            }
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.INFO, "Line1 prefiltered {0}", new Object[]{line1});
            LOGGER.log(Level.INFO, "Line1 distances   {0}", new Object[]{toString(minDistances12)});
            LOGGER.log(Level.INFO, "Line2 prefiltered {0}", new Object[]{line2});
            LOGGER.log(Level.INFO, "Line2 distances   {0}", new Object[]{toString(minDistances21)});

        }
        if (inVicinity) {
            filterInVicinityIndexes(line1, minDistances12);
            filterInVicinityIndexes(line2, minDistances21);
            // Also check that angles are in same direction ??
            // We want a set of counterparts , ie element  i in line1 should correspond to element i in line 2, this implies we want the same size lists
            // If the lines are not equal , it implies that one of the lines have two points that is close to one point in the other.
            // We will remove one of them here , keep the nearest ?? 
            if (line1.size() != line2.size()) {
                java.util.Arrays.fill(nearest12, -1);
                java.util.Arrays.fill(nearest21, -1);
                java.util.Arrays.fill(minDistances12, Double.MAX_VALUE);
                java.util.Arrays.fill(minDistances21, Double.MAX_VALUE);
                for (int i = 0; i < line1.size(); i++) {
                    final Point2d p1 = new Point2d(partition.line1p.get(line1.get(i)).x, partition.line1p.get(line1.get(i)).y);
                    for (int j = 0; j < line2.size(); j++) {
                        final Point2d p2 = new Point2d(partition.line2p.get(line2.get(j)).x, partition.line2p.get(line2.get(j)).y);
                        double p12 = p1.distance(p2);
                        if (minDistances12[i] > p12) {
                            minDistances12[i] = p12;
                            nearest12[i] = j;
                        }
                        if (minDistances21[j] > p12) {
                            minDistances21[j] = p12;
                            nearest21[j] = i;
                        }
                    }
                }
                removeNonNearestIndexes(line1, nearest12);
                removeNonNearestIndexes(line2, nearest21);
            }
        } else {
            filterNoVicinityIndexes(line1, minDistances12);
            filterNoVicinityIndexes(line2, minDistances21);
        }

        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Line1 postfiltered {0}", new Object[]{line1});
            LOGGER.log(Level.FINER, "Line2 postfiltered {0}", new Object[]{line2});
        }
        // FIXME Make sure line1 and line2 have same number of entries.
        // FIXME Make sure for everty i in  nearest12[i] == i
    }

    /**
     * Remove from the line any point that is not nearest
     *
     * @param line
     * @param nearest
     */
    protected void removeNonNearestIndexes(final java.util.List<Integer> line, int[] nearest) {
        final java.util.Map<Integer, Integer> counter = new java.util.HashMap<>();
        for (int i = 0; i < line.size(); i++) {
            if (counter.containsKey(nearest[i])) {
                counter.put(nearest[i], counter.get(nearest[i]) + 1);
            } else {
                counter.put(nearest[i], 1);
            }
        }
        for (int i = line.size() - 1; i >= 0; i--) {
            if (counter.get(nearest[i]) > 1) {
                LOGGER.log(Level.FINER, "Removeing Line point of interest {0} from line[{1}]  corresponding to duplicate counterpart in vicinity ", new Object[]{i, line.size()});
                LOGGER.log(Level.FINER, "Removeing Line point of interest {0} corresponding to duplicate counterpart in vicinity ", String.format(" %d: %d  ==> %d x %d", i,  line.get(i), nearest[i], counter.get(nearest[i])));
                line.remove(i);
                counter.put(nearest[i], counter.get(nearest[i]) - 1);
            }
        }
    }

    /**
     * Remove entries in line that has no counter part knuckle in its vicinity
     *
     * @param line
     * @param minDistances
     */
    protected void filterNoVicinityIndexes(final java.util.List<Integer> line, double[] minDistances) {
        for (int i = minDistances.length - 1; i >= 0; i--) {
            if (minDistances[i] <= MAX_PAIRED_DISTANCE) {
                LOGGER.log(Level.FINER, "Removeing Line point of interest {0}   corresponding counterpart in vicinity ", String.format(" %d: %d  %.3f", i,  line.get(i), minDistances[i]));
                line.remove(i);
            }
        }
    }

    /**
     * Remove entries in line that has no counter part knuckle in its vicinity
     *
     * @param line
     * @param minDistances
     */
    private void filterInVicinityIndexes(final java.util.List<Integer> line, double[] minDistances) {
        for (int i = minDistances.length - 1; i >= 0; i--) {
            if (minDistances[i] > MAX_PAIRED_DISTANCE) {
                LOGGER.log(Level.FINER, "Removeing Line point of interest {0}  not corresponding counterpart in vicinity ", String.format(" %d: %d  %.3f", i, line.get(i), minDistances[i]));
                line.remove(i);
            }
        }
    }
}
