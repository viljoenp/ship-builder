/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import javax.vecmath.Point3d;
import org.rcwarships.model.Vertex;

/**
 *
 * @author viljoenp
 */
public class Partition {

    public Partition(final java.util.List<Vertex> line1, final java.util.List<Point3d> line1p, final java.util.List<Vertex> line2, final java.util.List<Point3d> line2p) {
        this.line1 = line1;
        this.line2 = line2;
        this.line1p = line1p;
        this.line2p = line2p;
    }

    public Partition subPartition(int line1Start, int line1End, int line2Start, int line2End) {
        return new Partition(
                line1.subList(line1Start, line1End),
                line1p.subList(line1Start, line1End),
                line2.subList(line2Start, line2End),
                line2p.subList(line2Start, line2End));
    }

    public final java.util.List<Vertex> line1;
    public final java.util.List<Vertex> line2;
    public final java.util.List<Point3d> line1p;
    public final java.util.List<Point3d> line2p;

}
