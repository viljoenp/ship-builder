/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import javax.vecmath.Point2d;

/**
 *
 * @author viljoenp
 */
public class PartitionInfo {

    public PartitionInfo(Partition partition) {
        this.minDistances12 = new double[partition.line1p.size()];
        this.nearest12 = new int[partition.line1p.size()];
        java.util.Arrays.fill(minDistances12, Double.MAX_VALUE);
        this.minDistances21 = new double[partition.line2p.size()];
        this.nearest21 = new int[partition.line2p.size()];
        java.util.Arrays.fill(minDistances21, Double.MAX_VALUE);
        for (int i = 0; i < partition.line1p.size(); i++) {
            final Point2d p1 = new Point2d(partition.line1p.get(i).x, partition.line1p.get(i).y);
            for (int j = 0; j < partition.line2p.size(); j++) {
                final Point2d p2 = new Point2d(partition.line2p.get(j).x, partition.line2p.get(j).y);
                double p12 = p1.distance(p2);
                if (minDistances12[i] > p12) {
                    minDistances12[i] = p12;
                    nearest12[i] = j;
                }
                if (minDistances21[j] > p12) {
                    minDistances21[j] = p12;
                    nearest21[j] = i;
                }
            }
        }
    }

    final double[] minDistances12;
    final int[] nearest12;
    final double[] minDistances21;
    final int[] nearest21;

}
