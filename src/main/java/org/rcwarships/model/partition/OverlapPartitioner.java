/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import org.rcwarships.model.ModelUtils;
import org.rcwarships.model.Vertex;

/**
 *
 * @author viljoenp
 */
public class OverlapPartitioner extends AbstractPartitioner {

    private final static Logger LOGGER = Logger.getLogger(OverlapPartitioner.class.getName());

    /**
     * if line1p and line2p has some points that is on top of each other (ie
     * only differs with z) then we can divide line set
     *
     * @param src
     * @return
     */
    @Override
    public java.util.List<Partition> partition(final Partition src) {
        final java.util.List<Vertex> line1 = src.line1;
        final java.util.List<Point3d> line1p = src.line1p;
        final java.util.List<Vertex> line2 = src.line2;
        final java.util.List<Point3d> line2p = src.line2p;
        final java.util.List<Partition> partitions = new java.util.ArrayList<>();
        if (line1p.size() > line2p.size()) {
            final Point3d[] minMax = ModelUtils.find_min_and_max_values(line1p);
            final SpacialPartionX<Point3d, Integer> partition = new SpacialPartionX<>(minMax[0].x - 1, minMax[1].x + 1, line1p.size(), minMax[0].y - 1, minMax[1].y + 1);
            partition.setEpsilon(0.001);
            for (int i = 0; i < line1p.size(); i++) {
                partition.add(line1p.get(i), i);
            }
            int line1Start = 0, line2Start = 0;
            for (int i = 1; i < line2p.size() - 1; i++) {
                final SpacialPartionX.Entry<Point3d, Integer> entry = partition.locate(line2p.get(i));
                if (entry != null) {
                    if (entry.value <= line1Start) {
                        LOGGER.log(Level.WARNING, "Partitioning failed new partition end ({0}) is index greater is less than line1Start {1}   position {2}", new Object[]{entry.value, line1Start, entry.value});
                        return java.util.Collections.singletonList(src);
                    }
                    if (entry.value >= line1p.size()) {
                        // End the partitioning
                        break;
                    } else {
                        partitions.add(new Partition(line1.subList(line1Start, entry.value + 1), line1p.subList(line1Start, entry.value + 1), line2.subList(line2Start, i + 1), line2p.subList(line2Start, i + 1)));
                        line1Start = entry.value;
                        line2Start = i;
                    }
                }
            }
            partitions.add(new Partition(line1.subList(line1Start, line1.size()), line1p.subList(line1Start, line1p.size()), line2.subList(line2Start, line2.size()), line2p.subList(line2Start, line2p.size())));

        } else {
            final Point3d[] minMax = ModelUtils.find_min_and_max_values(line2p);
            final SpacialPartionX<Point3d, Integer> partition = new SpacialPartionX<>(minMax[0].x - 1, minMax[1].x + 1, line2p.size(), minMax[0].y - 1, minMax[1].y + 1);
            partition.setEpsilon(0.001);
            for (int i = 1; i < line2p.size(); i++) {
                partition.add(line2p.get(i), i);
            }
            int line1Start = 0, line2Start = 0;
            for (int i = 1; i < line1p.size() - 1; i++) {
                final SpacialPartionX.Entry<Point3d, Integer> entry = partition.locate(line1p.get(i));
                if (entry != null) {
                    if (entry.value <= line2Start) {
                        LOGGER.log(Level.WARNING, "Partitioning failed new partition end ({0}) is index greater is less than line1Start {1}   position {2}", new Object[]{entry.value, line2Start, entry.value});
                        return java.util.Collections.singletonList(src);
                    }
                    if (entry.value >= line2p.size()) {
                        // End the partitioning
                        break;
                    } else {
                        partitions.add(new Partition(line1.subList(line1Start, i + 1), line1p.subList(line1Start, i + 1), line2.subList(line2Start, entry.value + 1), line2p.subList(line2Start, entry.value + 1)));
                        line1Start = i;
                        line2Start = entry.value;
                    }
                }
            }
            partitions.add(new Partition(line1.subList(line1Start, line1.size()), line1p.subList(line1Start, line1p.size()), line2.subList(line2Start, line2.size()), line2p.subList(line2Start, line2p.size())));
        }

        return partitions;
    }

}
