/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.rcwarships.model.Epsilon;
import org.rcwarships.model.Vertex;

/**
 *
 * @author viljoenp
 */
public class PartitionFactory {

    private final static Logger LOGGER = Logger.getLogger(PartitionFactory.class.getName());

    public static java.util.List<Partition> partition(final java.util.List<Vertex> line1, final java.util.List<Vertex> line2) {
        final TransformParallel transform = getParallelTransform(line1, line2);
        // Make a List of transformed point 
        final java.util.List<Point3d> line1p = transform.transformToPlane(line1);
        final java.util.List<Point3d> line2p = transform.transformToPlane(line2);

        return directionalPartition(
                unPairedKnuclePartition(
                        pairedKnuclePartition(
                                simpleOverlapPartition(new Partition(line1, line1p, line2, line2p))
                        )
                )
        );
    }

    private static java.util.List<Partition> simpleOverlapPartition(final Partition partition) {

        final Partitioner overlap = new OverlapPartitioner();
        final java.util.List<Partition> finerPartirions = overlap.partition(partition);
        LOGGER.log(Level.FINER, "partition subdivided into {0} simple partition(s) from {1},{2} segments", new Object[]{finerPartirions.size(), partition.line1.size(), partition.line2.size()});
        for (Partition finder : finerPartirions) {
            LOGGER.log(Level.FINER, "\tPartition {0},{1} segements ", new Object[]{finder.line1.size(), finder.line2.size()});
        }
        return finerPartirions;
    }

    private static java.util.List<Partition> pairedKnuclePartition(final java.util.List<Partition> partitions) {

        final Partitioner pairedKnucle = new PairedKnucklePartitioner();
        final java.util.List<Partition> finerPartirions = pairedKnucle.partition(partitions);
        LOGGER.log(Level.FINER, "partition subdivided into {0} Paired Knuckle partition(s) from {1}[ {2},{3} ] segments", new Object[]{finerPartirions.size(), partitions.size(), line1Count(partitions), line2Count(partitions)});
        for (Partition finder : finerPartirions) {
            LOGGER.log(Level.FINER, "\tPartition {0},{1} segements ", new Object[]{finder.line1.size(), finder.line2.size()});
        }
        return finerPartirions;
    }

    private static java.util.List<Partition> unPairedKnuclePartition(final java.util.List<Partition> partitions) {

        final Partitioner pairedKnucle = new UnPairedKnucklePartitioner();
        final java.util.List<Partition> finerPartirions = pairedKnucle.partition(partitions);
        LOGGER.log(Level.FINER, "partition subdivided into {0} UnPaired Knuckle partition(s) from {1}[ {2},{3} ] segments", new Object[]{finerPartirions.size(), partitions.size(), line1Count(partitions), line2Count(partitions)});
        for (Partition finder : finerPartirions) {
            LOGGER.log(Level.FINER, "\tPartition {0},{1} segements ", new Object[]{finder.line1.size(), finder.line2.size()});
        }
        return finerPartirions;
    }

    private static java.util.List<Partition> directionalPartition(final java.util.List<Partition> partitions) {

        final Partitioner pairedKnucle = new DirectionalPartitioner();
        final java.util.List<Partition> finerPartirions = pairedKnucle.partition(partitions);
        LOGGER.log(Level.FINER, "partition subdivided into {0} Directional partition(s) from {1}[ {2},{3} ] segments", new Object[]{finerPartirions.size(), partitions.size(), line1Count(partitions), line2Count(partitions)});
        for (Partition finder : finerPartirions) {
            LOGGER.log(Level.FINER, "\tPartition {0},{1} segements ", new Object[]{finder.line1.size(), finder.line2.size()});
        }
        return finerPartirions;
    }

    private static int line1Count(final java.util.List<Partition> partitions) {
        int total = 0;
        for (int i = 0; i < partitions.size(); i++) {
            total += partitions.get(i).line1.size() - (i == 0 ? 0 : 1);

        }
        return total;
    }

    private static int line2Count(final java.util.List<Partition> partitions) {
        int total = 0;
        for (int i = 0; i < partitions.size(); i++) {
            total += partitions.get(i).line2.size() - (i == 0 ? 0 : 1);
        }
        return total;
    }

    /**
     * line1 and line2 should be parallel to each other with respect of one of
     * the axis, Here we return a transform once it is applied to a vertex z
     * will represent the difference between the two planes. all the points of
     * line1 will be in a XY plane with z = 0 , all points in line2 will be a XY
     * plane with z = the distance between line1 and line2
     *
     * @param line1
     * @param line2
     * @return
     */
    private static TransformParallel getParallelTransform(final java.util.List<Vertex> line1, final java.util.List<Vertex> line2) throws IllegalArgumentException {
        // Try to figure out what axis the two lines are parallel with.
        final Vector3d line1dx = totalDiffenec(line1);
        final Vector3d line2dx = totalDiffenec(line2);
        double maxE = Epsilon.E * Math.max(line1.size(), line2.size());
        // All the x coordinates of line1 and line2 is in the same plane , but not y an z , so we transform  x=>z , y=>x  , z=>y    
        if (((Math.abs(line1dx.x) <= maxE) && (Math.abs(line2dx.x) <= maxE))
                && ((Math.abs(line1dx.y) > maxE) || (Math.abs(line2dx.y) > maxE))
                && ((Math.abs(line1dx.z) > maxE) || (Math.abs(line2dx.z) > maxE))) {
            LOGGER.log(Level.FINER, "Triangulation3d detected lines {0},{1} are parallel with regards to the X axis", new Object[]{line1.size(), line2.size()});
            return new TransformParallelXZYXZY(line1.get(0).coord.x);
        }
        // All the y coordinates of line1 and line2 is in the same plane , but not x an z , so we transform  x=>x , y=>z  , z=>y 
        if (((Math.abs(line1dx.y) <= maxE) && (Math.abs(line2dx.y) <= maxE))
                && ((Math.abs(line1dx.x) > maxE) || (Math.abs(line2dx.x) > maxE))
                && ((Math.abs(line1dx.z) > maxE) || (Math.abs(line2dx.z) > maxE))) {
            LOGGER.log(Level.FINER, "Triangulation3d detected lines {0},{1} are parallel with regards to the Y axis", new Object[]{line1.size(), line2.size()});
            return new TransformParallelXXYZZY(line1.get(0).coord.y);
        }

        // All the z coordinates of line1 and line2 is in the same plane , but not x an y , so we transform  x=>x , y=>y  , z=>z 
        if (((Math.abs(line1dx.z) <= maxE) && (Math.abs(line2dx.z) <= maxE))
                && ((Math.abs(line1dx.x) > maxE) || (Math.abs(line2dx.x) > maxE))
                && ((Math.abs(line1dx.y) > maxE) || (Math.abs(line2dx.y) > maxE))) {
            LOGGER.log(Level.FINER, "Triangulation3d detected lines {0},{1} are parallel with regards to the Z axis", new Object[]{line1.size(), line2.size()});
            return new TransformParallelXXYYZZ(line1.get(0).coord.z);
        }
        throw new IllegalArgumentException("The lines does not seem to be parallel with respect to one of the axis   diff1:" + line1dx + " , diff2:" + line2dx);
    }

    private static Vector3d totalDiffenec(final java.util.List<Vertex> line) {
//        final Vector3d diff = new Vector3d(0,0,0);
        double dx = 0, dy = 0, dz = 0;
        for (int i = 1; i < line.size(); i++) {
            final Vertex pre = line.get(i - 1);
            final Vertex cur = line.get(i);
            dx += pre.coord.x - cur.coord.x;
            dy += pre.coord.y - cur.coord.y;
            dz += pre.coord.z - cur.coord.z;
        }
        return new Vector3d(dx, dy, dz);
    }

}
