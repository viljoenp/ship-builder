/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Logger;

import static org.rcwarships.model.partition.PairedKnucklePartitioner.KNUCKLE_ANGLE;

/**
 *
 * @author viljoenp
 */
public class UnPairedKnucklePartitioner extends PairedKnucklePartitioner {

    private final static Logger LOGGER = Logger.getLogger(UnPairedKnucklePartitioner.class.getName());

    /**
     * Scan each line for Knuckles ( sharp edges ) ,Locate Knuckles in close
     * vicinity , and partition , only partition the line IF the opposite line
     * also has a knuckle in close vicinity
     *
     * @param partition
     * @return
     */
    @Override
    public java.util.List<Partition> partition(final Partition partition) {
        return partitionUnPairedKnuckleSegments(partition, KNUCKLE_ANGLE);
    }

    /**
     * Scan each line for Knuckles ( sharp edges ) ,Locate Knuckles in close
     * vicinity , and partition
     *
     * @param partition
     * @return
     */
    private java.util.List<Partition> partitionUnPairedKnuckleSegments(final Partition partition, final double knuckleAngle) {
        final java.util.List<Partition> partitions = new java.util.ArrayList<>();

        final java.util.List<Integer> line1 = scanKnuckles(partition.line1p, knuckleAngle);
        final java.util.List<Integer> line2 = scanKnuckles(partition.line2p, knuckleAngle);
        // Filter only knuckles that have a corresponding knuckle in the visinity
        if (line1.isEmpty() && line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        // Filter out any point that Have Countrer parts 
        filterVicinity(partition, line1, line2, false);
        if (line1.isEmpty() && line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        PartitionInfo partitionInfo = new PartitionInfo(partition);
        if (!line1.isEmpty() && !line2.isEmpty()) {
            if ((partitionInfo.nearest21[line2.get(0)] <= line1.get(0)) && (partitionInfo.nearest12[line1.get(0)] <= line2.get(0))) {  // if line2's knuckle nearest nabour is before line1's knuckle  AND  line1's knuckle nearest nabour is before line2's knuckle 
                // FIXME do we patrition the shappest knuckle 1st ??
                partitions.add(partition.subPartition(0, partitionInfo.nearest21[line2.get(0)] + 1, 0, line2.get(0) + 1));
                partitions.addAll(partitionUnPairedKnuckleSegments(partition.subPartition(partitionInfo.nearest21[line2.get(0)], partition.line1.size() , line2.get(0), partition.line2.size() ), knuckleAngle));
            } else if (partitionInfo.nearest21[line2.get(0)] <= line1.get(0)) { // if line2's knuckle nearest nabour is before line1's knuckle  
                partitions.add(partition.subPartition(0, partitionInfo.nearest21[line2.get(0)] + 1, 0, line2.get(0) + 1));
                partitions.addAll(partitionUnPairedKnuckleSegments(partition.subPartition(partitionInfo.nearest21[line2.get(0)], partition.line1.size() , line2.get(0), partition.line2.size() ), knuckleAngle));
            } else if (partitionInfo.nearest12[line1.get(0)] <= line2.get(0)) { // if line1's knuckle nearest nabour is before line2's knuckle 
                partitions.add(partition.subPartition(0, line1.get(0) + 1, 0, partitionInfo.nearest12[line1.get(0)] + 1));
                partitions.addAll(partitionUnPairedKnuckleSegments(partition.subPartition(line1.get(0), partition.line1.size() , partitionInfo.nearest12[line1.get(0)], partition.line2.size() ), knuckleAngle));
            } else { // both knucles nearest point is past the begining nuckles..
                // FIXME do we patrition the shappest knuckle 1st ??
                partitions.add(partition.subPartition(0, partitionInfo.nearest21[line2.get(0)] + 1, 0, line2.get(0) + 1));
                partitions.addAll(partitionUnPairedKnuckleSegments(partition.subPartition(partitionInfo.nearest21[line2.get(0)], partition.line1.size() , line2.get(0), partition.line2.size()), knuckleAngle));
            }
        } else if (!line1.isEmpty()) {
            int line1Start = 0;
            int line2Start = 0;
            for (int line1Knucke : line1) {
                partitions.add(partition.subPartition(line1Start, line1Knucke + 1, line2Start, partitionInfo.nearest12[line1Knucke] + 1));
                line1Start = line1Knucke;
                line2Start = partitionInfo.nearest12[line1Knucke];
            }
            partitions.add(partition.subPartition(line1Start, partition.line1.size(), line2Start, partition.line2.size()));

        } else if (!line2.isEmpty()) {
            int line1Start = 0;
            int line2Start = 0;
            for (int line2Knucke : line1) {
                partitions.add(partition.subPartition(line1Start, partitionInfo.nearest21[line2Knucke] + 1, line2Start, line2Knucke + 1));
                line1Start = partitionInfo.nearest21[line2Knucke];
                line2Start = line2Knucke;
            }
            partitions.add(partition.subPartition(line1Start, partition.line1.size(), line2Start, partition.line2.size()));
        }
        return partitions;
//            
//        if (line1.size() != line2.size()) {
//            // FIXME, we should try to find nearest Matches 
//            LOGGER.log(Level.WARNING, "The Knucle Partitions have un-even number of knucles  {0}, {1}", new Object[]{line1, line2});
//        }
//        for (int i = 0; i < Math.max(line1.size(), line2.size()); i++) { // Ensure we get Index out of bounds if line1.size != line2.size
//            final Partition tmp = partition.subPartition(line1Start, line1.get(i) + 1, line2Start, line2.get(i) + 1);
//            if (knuckleAngle > KNUCKLE_ANGLE / 2) {
//                final java.util.List<Partition> finer = partitionPairedKnuckleSegments(tmp, knuckleAngle / 2);
//                if (LOGGER.isLoggable(Level.FINER)) {
//                    LOGGER.log(Level.INFO, "Re-Partition {0} finer segements  ", new Object[]{finer.size()});
//                }
//                partitions.addAll(finer);
//            } else {
//                partitions.add(tmp);
//            }
//            line1Start = line1.get(i);
//            line2Start = line2.get(i);
//        }
//        final Partition tmp = partition.subPartition(line1Start, partition.line1.size(), line2Start, partition.line2.size());
//        if (knuckleAngle > KNUCKLE_ANGLE / 2) {
//            final java.util.List<Partition> finer = partitionPairedKnuckleSegments(tmp, knuckleAngle / 2);
//            if (LOGGER.isLoggable(Level.FINER)) {
//                LOGGER.log(Level.INFO, "Re-Partition {0} finer segements  ", new Object[]{finer.size()});
//            }
//            partitions.addAll(finer);
//        } else {
//            partitions.add(tmp);
//        }
//        return partitions;
    }

}
