/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector2d;

/**
 *
 * @author viljoenp
 */
public class PairedHorizontalKnucklePartitioner extends AbstractPartitioner {

    private final static Logger LOGGER = Logger.getLogger(PairedHorizontalKnucklePartitioner.class.getName()
    );
    // This SHOULD BE READ FROM SOME PROPERTY !!
    protected static double KNUCKLE_ANGLE = (Math.PI / 2) / 3.9;

    static {
        if (System.getProperty("knuckle-angle") != null) {
            KNUCKLE_ANGLE = Double.parseDouble(System.getProperty("knuckle-angle"));
        }
    }

    private final double knuckleAngle;

    public PairedHorizontalKnucklePartitioner() {
        this(KNUCKLE_ANGLE);
    }

    public PairedHorizontalKnucklePartitioner(double knuckleAngle) {
        this.knuckleAngle = knuckleAngle;
    }

    /**
     * Scan each line for Knuckles ( sharp edges ) ,Locate Knuckles in close
     * vicinity , and partition , only partition the line IF the opposite line
     * also has a knuckle in close vicinity
     *
     * @param partition
     * @return
     */
    @Override
    public java.util.List<Partition> partition(final Partition partition) {
        return partitionPairedKnuckleSegments(partition, knuckleAngle);
    }

    /**
     * Scan each line for Knuckles ( sharp edges ) ,Locate Knuckles in close
     * vicinity , and partition
     *
     * @param partition
     * @return
     */
    private java.util.List<Partition> partitionPairedKnuckleSegments(final Partition partition, final double knuckleAngle) {
        final java.util.List<Partition> partitions = new java.util.ArrayList<>();

        final java.util.List<Integer> line1 = scanVerticalKnuckles(partition.line1p, knuckleAngle);
        final java.util.List<Integer> line2 = scanVerticalKnuckles(partition.line2p, knuckleAngle);
        // Filter only knuckles that have a corresponding knuckle in the visinity
        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        filterVicinity(partition, line1, line2, true);
        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        if (line1.size() != line2.size()) {
            // FIXME, we should try to find nearest Matches 
            LOGGER.log(Level.WARNING, "The Knucle Partitions have un-even number of knucles  {0}, {1}", new Object[]{line1, line2});
        }
        int line1Start = 0, line2Start = 0;
        for (int i = 0; i < Math.max(line1.size(), line2.size()); i++) { // Ensure we get Index out of bounds if line1.size != line2.size
            final Partition tmp = partition.subPartition(line1Start, line1.get(i) + 1, line2Start, line2.get(i) + 1);
            partitions.add(tmp);
            line1Start = line1.get(i);
            line2Start = line2.get(i);
        }
        final Partition tmp = partition.subPartition(line1Start, partition.line1.size(), line2Start, partition.line2.size());

        partitions.add(tmp);

        return partitions;
    }

    Point2d to2D(final Tuple3d tuple) {
        return new Point2d(tuple.x, tuple.y);
    }

    Vector2d sub(final Point3d a, final Point3d b) {
        final Vector2d v01 = new Vector2d(to2D(a));
        v01.sub(to2D(b));
        return v01;
    }

    /**
     * return list of indexes to points that represent a knuckle in the polygon
     * line
     *
     * @param line
     * @return
     */
    protected java.util.List<Integer> scanVerticalKnuckles(final java.util.List<Point3d> line, final double knuckleAngle) {
        final java.util.List<Integer> knuckles = new java.util.ArrayList<>();
        Vector2d v01 = sub(line.get(0), line.get(1));

        for (int i = 1; i < line.size() - 1; i++) {
            final Vector2d v12 = sub(line.get(i), line.get(i + 1));
            if ((v01.lengthSquared() > 0) && (v12.lengthSquared() > 0)) {
                if (v01.angle(v12) > knuckleAngle) {
                    knuckles.add(i);
                    if (LOGGER.isLoggable(Level.FINER)) {
                        LOGGER.log(Level.INFO, "scanKnuckles {0}  {1} - {2}  {3} -- ADDED  {4}", new Object[]{i, v01, v12, v01.angle(v12), line.get(i)});
                    }
                }
            }
            if (v12.lengthSquared() > 0) {
                v01 = v12; // leave )
            }
        }
        return knuckles;
    }

}
