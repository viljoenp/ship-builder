/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.partition;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector2d;

/**
 *
 * @author viljoenp
 */
public class PairedVerticalKnucklePartitioner extends AbstractPartitioner {

    private final static Logger LOGGER = Logger.getLogger(PairedVerticalKnucklePartitioner.class.getName()
    );
    // This SHOULD BE READ FROM SOME PROPERTY !!
    protected static double KNUCKLE_ANGLE = (Math.PI / 2) / 3.9;

    static {
        if (System.getProperty("knuckle-angle") != null) {
            KNUCKLE_ANGLE = Double.parseDouble(System.getProperty("knuckle-angle"));
        }
    }

    private final double knuckleAngle;

    public PairedVerticalKnucklePartitioner() {
        this(KNUCKLE_ANGLE);
    }

    public PairedVerticalKnucklePartitioner(double knuckleAngle) {
        this.knuckleAngle = knuckleAngle;
    }

    /**
     * Scan each line for Knuckles ( sharp edges ) ,Locate Knuckles in close
     * vicinity , and partition , only partition the line IF the opposite line
     * also has a knuckle in close vicinity
     *
     * @param partition
     * @return
     */
    @Override
    public java.util.List<Partition> partition(final Partition partition) {
        return partitionPairedKnuckleSegments(partition, knuckleAngle);
    }

    /**
     * Scan each line for Knuckles ( sharp edges ) ,Locate Knuckles in close
     * vicinity , and partition
     *
     * @param partition
     * @return
     */
    private java.util.List<Partition> partitionPairedKnuckleSegments(final Partition partition, final double knuckleAngle) {
        final java.util.List<Partition> partitions = new java.util.ArrayList<>();

        final java.util.List<Integer> line1 = scanVerticalKnuckles(partition.line1p, knuckleAngle);
        final java.util.List<Integer> line2 = scanVerticalKnuckles(partition.line2p, knuckleAngle);
        if (partition.line1p.get(0).distance(new Point3d(25.53530761166048, 1654.1750259999997, 127.25629259373642)) < 0.00001) {
            LOGGER.log(Level.WARNING, "The this is where thing go wrong knucles  {0}, {1}", new Object[]{line1, line2});
        }
        // Filter only knuckles that have a corresponding knuckle in the visinity
        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        final java.util.List<Integer> linen1 = new java.util.ArrayList<>(line1);
        final java.util.List<Integer> linen2 = new java.util.ArrayList<>(line2);
        filterVicinity(partition, line1, line2, true);
        int line1Start = 0, line2Start = 0;

        final boolean refilter = ((linen1.size() != line1.size()) && (linen2.size() != line2.size()));
        if (line1.isEmpty() || line2.isEmpty()) {
            if (refilter) {
                return reFilterPartition(partition);
            } else {
                return java.util.Collections.singletonList(partition);
            }
        }
        if (line1.size() != line2.size()) {
            // FIXME, we should try to find nearest Matches 
            LOGGER.log(Level.WARNING, "The Knucle Partitions have un-even number of knucles  {0}, {1}", new Object[]{line1, line2});
        }
        for (int i = 0; i < Math.max(line1.size(), line2.size()); i++) { // Ensure we get Index out of bounds if line1.size != line2.size
            final Partition tmp = partition.subPartition(line1Start, line1.get(i) + 1, line2Start, line2.get(i) + 1);
            partitions.add(tmp);
            line1Start = line1.get(i);
            line2Start = line2.get(i);
        }
        final Partition tmp = partition.subPartition(line1Start, partition.line1.size(), line2Start, partition.line2.size());

        partitions.add(tmp);

        if (refilter) {
            filterVicinity(partition, linen1, linen2, false);
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, "We removed from both lines we refilter on aligment {0}, {1}", new Object[]{linen1, linen2});
            }
            return reFilterPartition(partitions);
        } else {
            return partitions;
        }
    }

    Point2d to2D(final Tuple3d tuple) {
        return new Point2d(tuple.x, tuple.y);
    }

    Vector2d sub(final Point3d a, final Point3d b) {
        final Vector2d v01 = new Vector2d(to2D(a));
        v01.sub(to2D(b));
        return v01;
    }

    /**
     * return list of indexes to points that represent a knuckle in the polygon
     * line
     *
     * @param line
     * @return
     */
    protected java.util.List<Integer> scanVerticalKnuckles(final java.util.List<Point3d> line, final double knuckleAngle) {
        final java.util.List<Integer> knuckles = new java.util.ArrayList<>();
        Vector2d v01 = sub(line.get(0), line.get(1));

        for (int i = 1; i < line.size() - 1; i++) {
            final Vector2d v12 = sub(line.get(i), line.get(i + 1));
            if ((v01.lengthSquared() > 0) && (v12.lengthSquared() > 0)) {
                if (v01.angle(v12) > knuckleAngle) {
                    knuckles.add(i);
                    if (LOGGER.isLoggable(Level.FINER)) {
                        LOGGER.log(Level.INFO, "scanKnuckles {0}  {1} - {2}  {3} -- ADDED  {4}", new Object[]{i, v01, v12, v01.angle(v12), line.get(i)});
                    }
                }
            }
            if (v12.lengthSquared() > 0) {
                v01 = v12; // leave )
            }
        }
        return knuckles;
    }

    public java.util.List<Partition> reFilterPartition(final java.util.List<Partition> partitions) {
        final java.util.List<Partition> results = new java.util.ArrayList<>();
        for (Partition partition : partitions) {
            results.addAll(reFilterPartition(partition));
        }
        return results;
    }

    private java.util.List<Partition> reFilterPartition(Partition partition) {
        final java.util.List<Partition> partitions = new java.util.ArrayList<>();

        final java.util.List<Integer> line1 = scanVerticalKnuckles(partition.line1p, knuckleAngle);
        final java.util.List<Integer> line2 = scanVerticalKnuckles(partition.line2p, knuckleAngle);
        if (partition.line1p.get(0).distance(new Point3d(25.53530761166048, 1654.1750259999997, 127.25629259373642)) < 0.00001) {
            LOGGER.log(Level.WARNING, "The this is where thing go wrong knucles  {0}, {1}", new Object[]{line1, line2});
        }
        // Filter only knuckles that have a corresponding knuckle in the visinity
        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        filterKnuckleAlignment(partition, line1, line2, true);
        int line1Start = 0, line2Start = 0;

        if (line1.isEmpty() || line2.isEmpty()) {
            return java.util.Collections.singletonList(partition);
        }
        if (line1.size() != line2.size()) {
            // FIXME, we should try to find nearest Matches 
            LOGGER.log(Level.WARNING, "The Knucle Partitions have un-even number of knucles  {0}, {1}", new Object[]{line1, line2});
        }
        for (int i = 0; i < Math.max(line1.size(), line2.size()); i++) { // Ensure we get Index out of bounds if line1.size != line2.size
            final Partition tmp = partition.subPartition(line1Start, line1.get(i) + 1, line2Start, line2.get(i) + 1);
            partitions.add(tmp);
            line1Start = line1.get(i);
            line2Start = line2.get(i);
        }
        final Partition tmp = partition.subPartition(line1Start, partition.line1.size(), line2Start, partition.line2.size());

        partitions.add(tmp);
        return partitions;

    }

    /**
     * Remove entries in line that has no counter part knuckle in its vicinity
     *
     * @param partition
     * @param line1
     * @param line2
     */
    protected void filterKnuckleAlignment(final Partition partition, final java.util.List<Integer> line1, final java.util.List<Integer> line2, final boolean inVicinity) {

        final double[] minDistances12 = new double[line1.size()];
        final double[] minDx12 = new double[line1.size()];
        final int[] nearest12 = new int[line1.size()];
        java.util.Arrays.fill(minDistances12, Double.MAX_VALUE);
        java.util.Arrays.fill(minDx12, Double.MAX_VALUE);
        final double[] minDistances21 = new double[line2.size()];
        final double[] minDx21 = new double[line2.size()];
        final int[] nearest21 = new int[line2.size()];
        java.util.Arrays.fill(minDistances21, Double.MAX_VALUE);
        java.util.Arrays.fill(minDx21, Double.MAX_VALUE);
        for (int i = 0; i < line1.size(); i++) {
            final Point2d p1 = new Point2d(partition.line1p.get(line1.get(i)).x, partition.line1p.get(line1.get(i)).y);
            for (int j = 0; j < line2.size(); j++) {
                final Vector2d p2 = new Vector2d(partition.line2p.get(line2.get(j)).x, partition.line2p.get(line2.get(j)).y);
                p2.sub(p1);
                double p12 = p2.length();
                if (minDistances12[i] > p12) {
                    minDistances12[i] = p12;
                    nearest12[i] = j;
                }
                if (minDistances21[j] > p12) {
                    minDistances21[j] = p12;
                    nearest21[j] = i;
                }
                double minDx = Math.min(Math.abs(p2.x), Math.abs(p2.y)); // ither X or Y must be the same
                if (minDx12[i] > minDx) {
                    minDx12[i] = minDx;
                }
                if (minDx21[j] > minDx) {
                    minDx21[j] = minDx;
                }
            }

        }
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.INFO, "Line1 prefiltered {0}", new Object[]{line1});
            LOGGER.log(Level.INFO, "Line1 distances   {0}", new Object[]{toString(minDistances12)});
            LOGGER.log(Level.INFO, "Line2 prefiltered {0}", new Object[]{line2});
            LOGGER.log(Level.INFO, "Line2 distances   {0}", new Object[]{toString(minDistances12)});

        }
        if (inVicinity) {
            filterInVicinityIndexes(line1, minDistances12, minDx12);
            filterInVicinityIndexes(line2, minDistances21, minDx21);
            // Also check that angles are in same direction ??
            // We want a set of counterparts , ie element  i in line1 should correspond to element i in line 2, this implies we want the same size lists
            // If the lines are not equal , it implies that one of the lines have two points that is close to one point in the other.
            // We will remove one of them here , keep the nearest ?? 
            if (line1.size() != line2.size()) {
                java.util.Arrays.fill(nearest12, -1);
                java.util.Arrays.fill(nearest21, -1);
                java.util.Arrays.fill(minDistances12, Double.MAX_VALUE);
                java.util.Arrays.fill(minDistances21, Double.MAX_VALUE);
                for (int i = 0; i < line1.size(); i++) {
                    final Point2d p1 = new Point2d(partition.line1p.get(line1.get(i)).x, partition.line1p.get(line1.get(i)).y);
                    for (int j = 0; j < line2.size(); j++) {
                        final Point2d p2 = new Point2d(partition.line2p.get(line2.get(j)).x, partition.line2p.get(line2.get(j)).y);
                        double p12 = p1.distance(p2);
                        if (minDistances12[i] > p12) {
                            minDistances12[i] = p12;
                            nearest12[i] = j;
                        }
                        if (minDistances21[j] > p12) {
                            minDistances21[j] = p12;
                            nearest21[j] = i;
                        }
                    }
                }
                removeNonNearestIndexes(line1, nearest12);
                removeNonNearestIndexes(line2, nearest21);
            }
        } else {
            filterNoVicinityIndexes(line1, minDistances12);
            filterNoVicinityIndexes(line2, minDistances21);
        }

        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Line1 postfiltered {0}", new Object[]{line1});
            LOGGER.log(Level.FINER, "Line2 postfiltered {0}", new Object[]{line2});
        }
        // FIXME Make sure line1 and line2 have same number of entries.
        // FIXME Make sure for everty i in  nearest12[i] == i
    }

    /**
     * Remove entries in line that has no counter part knuckle in its vicinity
     *
     * @param line
     * @param minDistances
     */
    private void filterInVicinityIndexes(final java.util.List<Integer> line, double[] minDistances, double[] minDx) {
        for (int i = minDistances.length - 1; i >= 0; i--) {
            if ((minDistances[i] > MAX_PAIRED_DISTANCE) && (minDx[i] > 0.1)) {
                LOGGER.log(Level.FINER, "Removeing Line point of interest {0}  not corresponding counterpart in vicinity ", String.format(" %d: %d  %.3f  dx:%.4f", i, line.get(i), minDistances[i], minDx[i]));
                line.remove(i);
            }
        }
    }
}
