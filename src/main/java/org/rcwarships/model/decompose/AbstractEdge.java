/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.decompose;

import org.rcwarships.model.Epsilon;

/**
 *
 * @author viljoenp
 */
public class AbstractEdge implements Decomposer.Edge {

    /**
     * The next edge in Counter-Clockwise order
     */
    protected Decomposer.Edge next;
    /**
     * The previous edge in Counter-Clockwise order
     */
    protected Decomposer.Edge prev;
    /**
     * The first vertex of the edge in Counter-Clockwise order
     */
    protected Decomposer.Vertex vertex0;
    /**
     * The second vertex of the edge in Counter-Clockwise order
     */
    protected Decomposer.Vertex vertex1;
    /**
     * The helper vertex of this edge
     */
    protected Decomposer.Vertex helper;

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AbstractEdge.Edge[V0=").append(this.vertex0).append("|V1=").append(this.vertex1).append("]");
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Decomposer.Edge o) {
        // check for reference equality first
        if (this == o) {
            return 0;
        }
        // first sort by the minimum x value
        double value = this.getMinX() - o.getMinX();
        if (Math.abs(value) <= Epsilon.E) {
            // if they are near zero sort by the minimum y
            value = o.getMinY() - this.getMinY();
        }
        return (int) Math.signum(value);
    }

    /**
     * Returns the minimum x value of this edge.
     *
     * @return double
     */
    @Override
    public double getMinX() {
        return Math.min(vertex0.getX(), vertex1.getX());
    }

    /**
     * Returns the maximum x value of this edge.
     *
     * @return double
     */
    @Override
    public double getMaxX() {
        return Math.max(vertex0.getX(), vertex1.getX());
    }

    /**
     * Returns the minimum y value of this edge.
     *
     * @return double
     */
    @Override
    public double getMinY() {
        return Math.min(vertex0.getY(), vertex1.getY());
    }

    /**
     * Returns the maximum y value of this edge.
     *
     * @return double
     */
    @Override
    public double getMaxY() {
        return Math.max(vertex0.getY(), vertex1.getY());
    }

    /**
     * Returns true if the interior of the polygon is to the right of this edge.
     * <p>
     * Given that the polygon's vertex winding is Counter- Clockwise, if the
     * vertices that make this edge decrease along the y axis then the interior
     * of the polygon is to the right, otherwise its to the left.
     *
     * @return boolean
     */
    @Override
    public boolean isInteriorRight() {
        double diff = vertex0.getY() - vertex1.getY();
        // check if the points have nearly the
        // same x value
        if (Math.abs(diff) <= Epsilon.E) {
            // if they do, is the vector of the
            // two points to the right or to the left
            return vertex0.getX() < vertex1.getX();
        } else {
            return diff > 0.0;
        }
    }

    @Override
    public Decomposer.Vertex getVertex0() {
        return vertex0;
    }

    @Override
    public Decomposer.Vertex getVertex1() {
        return vertex1;
    }

    @Override
    public void setVertex0(Decomposer.Vertex vertex0) {
        this.vertex0 = vertex0;
    }

    @Override
    public void setVertex1(Decomposer.Vertex vertex1) {
        this.vertex1 = vertex1;
    }

    @Override
    public Decomposer.Edge getNext() {
        return next;
    }

    @Override
    public void setNext(Decomposer.Edge next) {
        this.next = next;
    }

    @Override
    public Decomposer.Edge getPrev() {
        return prev;
    }

    @Override
    public void setPrev(Decomposer.Edge prev) {
        this.prev = prev;
    }

    @Override
    public Decomposer.Vertex getHelper() {
        return helper;
    }

    @Override
    public void setHelper(Decomposer.Vertex helper) {
        this.helper = helper;
    }

}
