/*
 * Copyright (c) 2011 William Bittle  http://www.dyn4j.org/
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 *   * Redistributions of source code must retain the above copyright notice, this list of conditions 
 *     and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *     and the following disclaimer in the documentation and/or other materials provided with the 
 *     distribution.
 *   * Neither the name of dyn4j nor the names of its contributors may be used to endorse or 
 *     promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.rcwarships.model.decompose;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Tuple2d;
import javax.vecmath.Vector2d;
import org.rcwarships.model.Convex;
import org.rcwarships.model.Epsilon;
import org.rcwarships.resources.Messages;

/**
 * Performs the Sweep Line algorithm to decompose the given polygon into
 * y-monotone pieces which are then used to triangulate the original polygon.
 * <p>
 * This algorithm is O(n log n) complexity in the y-monotone decomposition phase
 * and O(n) in the triangulation phase yielding a total complexity of O(n log
 * n).
 * <p>
 * After triangulation, the Hertel-Mehlhorn algorithm is used to reduce the
 * number of convex pieces. This is performed in O(n) time.
 * <p>
 * This algorithm total complexity is O(n log n).
 *
 * @author William Bittle
 * @version 3.0.2
 * @since 2.2.0
 */
public class SweepLine implements Decomposer {

    private static final Logger LOGGER = Logger.getLogger(SweepLine.class.getName());

//    /**
//     * Represents a vertex on a polygon that stores information about the left
//     * and right edges and left and right vertices.
//     *
//     * @author William Bittle
//     * @version 3.0.2
//     * @since 2.2.0
//     */
//    protected static class Vertex implements Comparable<Vertex> {
//
//        /**
//         * The vertex point
//         */
//        protected Vector2d point;
//        /**
//         * The vertex type
//         */
//        protected Type type;
//        /**
//         * The next vertex in Counter-Clockwise order
//         */
//        protected Vertex next;
//        /**
//         * The previous vertex in Counter-Clockwise order
//         */
//        protected Vertex prev;
//        /**
//         * The next edge in Counter-Clockwise order
//         */
//        protected Edge left;
//        /**
//         * The previous edge in Counter-Clockwise order
//         */
//        protected Edge right;
//        /**
//         * The reference to the vertex in the DCEL
//         */
//        protected DoublyConnectedEdgeList.Vertex dcelReference;
//
//        /* (non-Javadoc)
//         * @see java.lang.Comparable#compareTo(java.lang.Object)
//         */
//        @Override
//        public int compareTo(Vertex other) {
//            // sort by the y first then by x if the y's are equal
//            Vector2d p = this.point;
//            Vector2d q = other.point;
//            double diff = q.y - p.y;
//            if (Math.abs(diff) <= Epsilon.E) {
//                // if the difference is near equal then compare the x values
//                return (int) Math.signum(p.x - q.x);
//            } else {
//                return (int) Math.signum(diff);
//            }
//        }
//
//        /* (non-Javadoc)
//         * @see java.lang.Object#toString()
//         */
//        @Override
//        public String toString() {
//            StringBuilder sb = new StringBuilder();
//            sb.append("SweepLine.Vertex[Point=").append(this.point)
//                    .append("|Type=").append(this.type)
//                    .append("]");
//            return sb.toString();
//        }
//
//        /**
//         * Returns true if this {@link Vertex} is left of the given
//         * {@link Edge}.
//         *
//         * @param edge the {@link Edge}
//         * @return boolean true if this {@link Vertex} is to the left of the
//         * given {@link Edge}
//         */
//        public boolean isLeft(Edge edge) {
//            // attempt the simple comparison first
//            if (this.point.x < edge.getMinX()) {
//                return true;
//            } else if (this.point.x > edge.getMaxX()) {
//                return false;
//            }
//            // its in between the min and max x so we need to 
//            // do a side of line test
//            double location = Segment.getLocation(this.point, edge.v0.point, edge.v1.point);
//            if (location < 0.0) {
//                return true;
//            } else {
//                return false;
//            }
//        }
//
//        /**
//         * Returns true if the interior is to the right of this vertex.
//         * <p>
//         * The left edge of this vertex is used to determine where the interior
//         * of the polygon is.
//         *
//         * @return boolean
//         */
//        public boolean isInteriorRight() {
//            return this.left.isInteriorRight();
//        }
//    }
//    /**
//     * Represents an edge of a polygon storing the next and previous edges and
//     * the vertices that make up this edge.
//     * <p>
//     * The edge also stores a helper vertex which is used during y-monotone
//     * decomposition.
//     *
//     * @author William Bittle
//     * @version 3.0.2
//     * @since 2.2.0
//     */
//    protected static class Edge implements Comparable<Edge> {
//
//        /**
//         * The next edge in Counter-Clockwise order
//         */
//        protected Edge next;
//        /**
//         * The previous edge in Counter-Clockwise order
//         */
//        protected Edge prev;
//        /**
//         * The first vertex of the edge in Counter-Clockwise order
//         */
//        protected Vertex v0;
//        /**
//         * The second vertex of the edge in Counter-Clockwise order
//         */
//        protected Vertex v1;
//        /**
//         * The helper vertex of this edge
//         */
//        protected Vertex helper;
//
//        /* (non-Javadoc)
//         * @see java.lang.Object#toString()
//         */
//        @Override
//        public String toString() {
//            StringBuilder sb = new StringBuilder();
//            sb.append("SweepLine.Edge[V0=").append(this.v0)
//                    .append("|V1=").append(this.v1)
//                    .append("]");
//            return sb.toString();
//        }
//
//        /* (non-Javadoc)
//         * @see java.lang.Comparable#compareTo(java.lang.Object)
//         */
//        @Override
//        public int compareTo(Edge o) {
//            // check for reference equality first
//            if (this == o) {
//                return 0;
//            }
//            // first sort by the minimum x value
//            double value = this.getMinX() - o.getMinX();
//            if (Math.abs(value) <= Epsilon.E) {
//                // if they are near zero sort by the minimum y
//                value = o.getMinY() - this.getMinY();
//            }
//            return (int) Math.signum(value);
//        }
//
//        /**
//         * Returns the minimum x value of this edge.
//         *
//         * @return double
//         */
//        public double getMinX() {
//            return Math.min(v0.getX(), v1.getX());
//        }
//
//        /**
//         * Returns the maximum x value of this edge.
//         *
//         * @return double
//         */
//        public double getMaxX() {
//            return Math.max(v0.getX(), v1.getX());
//        }
//
//        /**
//         * Returns the minimum y value of this edge.
//         *
//         * @return double
//         */
//        public double getMinY() {
//            return Math.min(v0.getY(), v1.getY());
//        }
//
//        /**
//         * Returns the maximum y value of this edge.
//         *
//         * @return double
//         */
//        public double getMaxY() {
//            return Math.max(v0.getY(), v1.getY());
//        }
//
//        /**
//         * Returns true if the interior of the polygon is to the right of this
//         * edge.
//         * <p>
//         * Given that the polygon's vertex winding is Counter- Clockwise, if the
//         * vertices that make this edge decrease along the y axis then the
//         * interior of the polygon is to the right, otherwise its to the left.
//         *
//         * @return boolean
//         */
//        public boolean isInteriorRight() {
//            double diff = v0.getY() - v1.getY();
//            // check if the points have nearly the
//            // same x value
//            if (Math.abs(diff) <= Epsilon.E) {
//                // if they do, is the vector of the
//                // two points to the right or to the left
//                if (v0.getX() < v1.getX()) {
//                    return true;
//                } else {
//                    return false;
//                }
//                // otherwise just compare the y values
//            } else if (diff > 0.0) {
//                return true;
//            } else {
//                return false;
//            }
//        }
//    }
    /**
     * Class to extend the {@link BinarySearchTree} to add a method for finding
     * the {@link Edge} closest to a given {@link Vertex}.
     *
     * @author William Bittle
     * @version 2.2.0
     * @since 2.2.0
     */
    protected static class EdgeBinaryTree extends BinarySearchTree<Edge, EdgeComparator> {

        /**
         * Default constructor.
         */
        public EdgeBinaryTree() {
            super(new EdgeComparator(), true);
        }

        /**
         * Performs a search to find the right most {@link Edge} who is left of
         * the given {@link Vertex}.
         * <p>
         * If the tree is empty null is returned.
         *
         * @param vertex the {@link Vertex}
         * @return {@link Edge} the closest edge
         */
        public Edge findClosest(Vertex vertex) {
            // check for a null root node
            if (this.root == null) {
                Logger.getLogger(EdgeBinaryTree.class.getName()).log(Level.WARNING, "EdgeBinaryTree root is null, NPE?");
                return null;
            }
            // set the current node to the root
            BinarySearchTree<Edge, EdgeComparator>.Node<Edge> node = this.root;
            // initialize the best edge to the root
            Node<Edge> best = node;
            // loop until the current node is null
            while (node != null) {
                // get the left edge
                Edge edge = node.comparable;
                if (vertex.isLeft(edge)) {
                    // if e is left of the current edge then go left in the tree
                    node = node.left;
                } else {
                    // otherwise e is right of the current edge so go right
                    // and save the current edge as the best
                    best = node;
                    node = node.right;
                }
            }
            // return the best node's comparable (edge)
            return best.comparable;
        }
    }

    public static double cross(Tuple2d a, Tuple2d b) {
        return a.x * b.y - a.y * b.x;
    }

    /**
     * Returns the winding, Clockwise or Counter-Clockwise, for the given list
     * of points of a polygon.
     * <p>
     * This method determines the winding by computing a signed "area".
     *
     * @param points the points of a polygon
     * @return double negative for Clockwise winding; positive for
     * Counter-Clockwise winding
     * @throws NullPointerException if points is null or an element of points is
     * null
     * @throws IllegalArgumentException if points contains less than 2 elements
     * @since 2.2.0
     */
    public static double getWinding(java.util.List<Vertex> points) {
        // check for a null list
        if (points == null) {
            throw new NullPointerException(Messages.getString("geometry.nullPointList"));
        }
        // get the size
        int size = points.size();
        // the size must be larger than 1
        if (size < 2) {
            throw new IllegalArgumentException(Messages.getString("geometry.invalidSizePointList2"));
        }
        // determine the winding by computing a signed "area"
        double area = 0.0;
        for (int i = 0; i < size; i++) {
            // get the current point and the next point
            Vertex p1 = points.get(i);
            Vertex p2 = points.get(i + 1 == size ? 0 : i + 1);
            // check for null
            if (p1 == null || p2 == null) {
                throw new NullPointerException(Messages.getString("geometry.nullPointListElements"));
            }

            // add the signed area
            area += p1.cross(p2);
        }
        // return the area
        return area;
    }

    /**
     * Reverses the order of the polygon points within the given array.
     * <p>
     * This method performs a simple array reverse.
     *
     * @param points the polygon points
     * @throws NullPointerException if points is null
     * @since 2.2.0
     */
    public static void reverseWinding(java.util.List<Vertex> points) {
        // check for a null list
        if (points == null) {
            throw new NullPointerException(Messages.getString("geometry.nullPointArray"));
        }
        // get the length
        int size = points.size();
        // check for a length of 1
        if (size == 1 || size == 0) {
            return;
        }
        // otherwise perform the swapping loop
        Collections.reverse(points);
//        int i = 0;
//        int j = size - 1;
//        Vertex temp = null;
//        while (j > i) {
//            // swap
//            temp = points.get(j);
//            points.set(j, points.get(i));
//            points.set(i, temp);
//            // increment
//            j--;
//            i++;
//        }
    }


    /* (non-Javadoc)
     * @see org.rcwarships.geometry.decompose.Decomposer#decompose(org.rcwarships.geometry.Vector2d[])
     */
    @Override
    public List<Convex> decompose(java.util.List< Vertex> points) {
        // check for a null list
        if (points == null) {
            throw new NullPointerException(Messages.getString("geometry.decompose.nullArray"));
        }
        // get the number of points
        int size = points.size();
        // check the size
        if (size < 4) {
            throw new IllegalArgumentException(Messages.getString("geometry.decompose.invalidSize"));
        }

        // get the winding order
        double winding = getWinding(points);
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "winding {0}", winding);
        }
        // reverse the array if the points are in clockwise order
        if (winding < 0.0) {
            if (LOGGER.isLoggable(Level.FINEST)) {
                LOGGER.log(Level.FINEST, "reverseWinding {0}", winding);
            }
            reverseWinding(points);
            if (LOGGER.isLoggable(Level.FINEST)) {
                LOGGER.log(Level.FINEST, "post reverseWinding {0}", getWinding(points));
            }
        }

        // create a DCEL to efficiently store the resulting y-monotone polygons
        DoublyConnectedEdgeList dcel = new DoublyConnectedEdgeList(points);

        // create the priority queue (sorted queue by largest y value) and
        // the cyclical lists
        PriorityQueue<Vertex> queue = this.initialize(points, dcel);

        // Find all edges that need to be added to the polygon
        // to create a y-monotone decomposition
        EdgeBinaryTree tree = new EdgeBinaryTree();
        while (!queue.isEmpty()) {
            Vertex v = queue.poll();
            if (LOGGER.isLoggable(Level.FINEST)) {
                LOGGER.log(Level.FINEST, "Vertex {0} ", v);
            }
            if (v.getType() == Vertex.Type.START) {
                this.start(v, tree);
            } else if (v.getType() == Vertex.Type.END) {
                this.end(v, tree, dcel);
            } else if (v.getType() == Vertex.Type.SPLIT) {
                this.split(v, tree, dcel);
            } else if (v.getType() == Vertex.Type.MERGE) {
                this.merge(v, tree, dcel);
            } else if (v.getType() == Vertex.Type.REGULAR) {
                this.regular(v, tree, dcel);
            }
        }

        // the DCEL now contains a valid y-monotone polygon decomposition
        // next we need to triangulate all the y-monotone polygons
        List<MonotonePolygon<DoublyConnectedEdgeList.Vertex>> polygons = dcel.getYMonotonePolygons();

        // triangulate each monotone polygon
        int ympSize = polygons.size();
        for (int i = 0; i < ympSize; i++) {
            if (LOGGER.isLoggable(Level.FINEST)) {
                LOGGER.log(Level.FINEST, "triangulateMonotoneY {0} ", i);
                LOGGER.log(Level.FINEST, "triangulateMonotoneY={0} ", polygons.get(i));
            }
            dcel.triangulateMonotoneY(polygons.get(i));
        }

        // the DCEL now contains a valid triangulation
        // next we perform the Hertel-Mehlhorn algorithm to
        // remove unnecessary edges
        dcel.hertelMehlhorn();

        // the DCEL now contains a valid convex decompostion
        // convert the dcel into a list of convex shapes
        return dcel.getConvexDecomposition();
    }

    /**
     * Returns a priority queue of the points in the given array.
     * <p>
     * This method also builds the cyclic doubly-linked list of vertices and
     * edges used to neighbor features in constant time.
     *
     * @param points the array of polygon points
     * @param dcel the DCEL object to add references to the priority queue
     * vertices
     * @return PriorityQueue&lt;{@link Vertex}&gt;
     */
    protected PriorityQueue<Vertex> initialize(java.util.List<Vertex> points, DoublyConnectedEdgeList dcel) {
        // get the number points
        int size = points.size();
        if (size < 3) {
            throw new IllegalArgumentException("Polygon should have 3 or more vertexes");
        }
        // create a priority queue for the vertices
        PriorityQueue<Vertex> queue = new PriorityQueue<>(size);

        Vertex rootVertex = null;
        Vertex prevVertex = null;

        Edge rootEdge = null;
        Edge prevEdge = null;

        // build the vertices and edges
        for (int i = 0; i < size; i++) {
            // get this vertex point
            Vector2d point = points.get(i).getTuple2d();

            // NOTE sure if we really need thise clone
            // create the vertex for this point
            Vertex vertex = points.get(i).clone();
            // default the type to regular
            vertex.setType(Vertex.Type.REGULAR);
            vertex.setPrev(prevVertex);
            vertex.setDcelReference(dcel.vertices.get(i));

            // set the previous vertex's next pointer
            if (prevVertex != null) {
                prevVertex.setNext(vertex);
            }

            // make sure we save the first vertex so we
            // can wire up the last and first to create
            // a cyclic list
            if (rootVertex == null) {
                rootVertex = vertex;
            }

            // get the neighboring points
            Vector2d point1 = points.get(i == size - 1 ? 0 : i + 1).getTuple2d();
            Vector2d point0 = points.get(i == 0 ? size - 1 : i - 1).getTuple2d();

            // get the vertex type
            vertex.setType(this.getType(point0, point, point1));

            // set the previous vertex to this vertex
            prevVertex = vertex;
            // add the vertex to the priority queue
            queue.offer(vertex);

            // create the next edge
            Edge e = new AbstractEdge();
            e.setPrev(prevEdge);
            // the first vertex is this vertex
            e.setVertex0(vertex);

            // set the previous edge's end vertex and
            // next edge pointers
            if (prevEdge != null) {
                prevEdge.setVertex1(vertex);
                prevEdge.setNext(e);
            }

            // make sure we save the first edge so we
            // can wire up the last and first to create
            // a cyclic list
            if (rootEdge == null) {
                rootEdge = e;
            }

            // set the vertex's left and right edges
            vertex.setLeft(e);
            vertex.setRight(prevEdge);

            // set the previous edge to this edge
            prevEdge = e;
        }

        // finally complete the cyclical lists
        // connect the first edge's previous pointer
        // to the last edge we created
        rootEdge.setPrev(prevEdge);
        // set the last edge's next pointer to the
        // first edge
        prevEdge.setNext(rootEdge);
        // set the last edge's end vertex pointer to
        // the first edge's start vertex
        prevEdge.setVertex1(rootEdge.getVertex0());

        // set the previous edge of the first vertex
        rootVertex.setRight(prevEdge);
        // set the previous vertex of the first vertex
        rootVertex.setPrev(prevVertex);
        // set the last vertex's next pointer to the
        // first vertex
        prevVertex.setNext(rootVertex);

        // return the priority queue
        return queue;
    }

    /**
     * Returns the vertex type given the previous and next points.
     *
     * @param point0 the previous point
     * @param point the vertex point
     * @param point1 the next point
     * @return {@link Vertex.Type}
     */
    protected Vertex.Type getType(Vector2d point0, Vector2d point, Vector2d point1) {
        // create the edge vectors
        Vector2d v1 = Wound.to(point0, point);
        Vector2d v2 = Wound.to(point, point1);

        // check for coincident points
        if (Wound.isZero(v1) || Wound.isZero(v2)) {
            throw new IllegalArgumentException(Messages.getString("geometry.decompose.coincident") + " " + point0 + " " + point + " " + point1);
        }

        // get the angle between the two edges (we assume CCW winding)
        double cross = Wound.cross(v1, v2);

        boolean pBelowP0 = this.isBelow(point, point0);
        boolean pBelowP1 = this.isBelow(point, point1);

        // where is p relative to its neighbors?
        if (pBelowP0 && pBelowP1) {
            // then check if the 
            // if its below both of them then we need
            // to check the interior angle
            if (cross > 0.0) {
                // if the cross product is greater than zero
                // this indicates that the angle is < pi
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.FINEST, "Split END - pBelowP0:{0} , pBelowP1:{1} : cross:{2} \n\tpoint  :{3}\n\tpoint0 :{4}\n\tpoint1 :{5} ", new Object[]{pBelowP0, pBelowP1, cross, point0, point, point1});
                }

                // this vertex is an end vertex
                return Vertex.Type.END;
            } else {
                // this indicates that the angle is pi or greater
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.FINEST, "Split MERGE - pBelowP0:{0} , pBelowP1:{1} : cross:{2} \n\tpoint  :{3}\n\tpoint0 :{4}\n\tpoint1 :{5}", new Object[]{pBelowP0, pBelowP1, cross, point0, point, point1});
                }

                // this vertex is a merge vertex
                return Vertex.Type.MERGE;
            }
        } else if (!pBelowP0 && !pBelowP1) {
            // if its above both of them then we need
            // to check the interior angle
            if (cross > 0.0) {
                // if the cross product is greater than zero
                // this indicates that the angle is < pi
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.FINEST, "Split START - pBelowP0:{0} , pBelowP1:{1} : cross:{2} \n\tpoint  :{3}\n\tpoint0 :{4}\n\tpoint1 :{5}", new Object[]{pBelowP0, pBelowP1, cross, point0, point, point1});
                }

                // this vertex is a start vertex
                return Vertex.Type.START;
            } else {
                // this indicates that the angle is pi or greater
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.FINEST, "Split SPLIT - pBelowP0:{0} , pBelowP1:{1} : cross:{2} \n\tpoint  :{3}\n\tpoint0 :{4}\n\tpoint1 :{5}", new Object[]{pBelowP0, pBelowP1, cross, point0, point, point1});
                }
                // this vertex is a split vertex
                return Vertex.Type.SPLIT;
            }
        }
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "Split REGULAR - pBelowP0:{0} , pBelowP1:{1} : cross:{2} \n\tpoint  :{3}\n\tpoint0 :{4}\n\tpoint1 :{5}", new Object[]{pBelowP0, pBelowP1, cross, point0, point, point1});
        }

        return Vertex.Type.REGULAR;
    }

    /**
     * Returns true if the given point p is below the given point q.
     * <p>
     * If the point p and q form a horizontal line then p is considered below if
     * its x coordinate is greater than q's x coordinate.
     *
     * @param p the point
     * @param q another point
     * @return boolean true if p is below q; false if p is above q
     */
    protected boolean isBelow(Vector2d p, Vector2d q) {
        double diff = p.y - q.y;
        if (Math.abs(diff) <= Epsilon.E) {
            if (p.x > q.x) {
                return true;
            } else {
                return false;
            }
        } else {
            if (diff < 0.0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Handles a {@link Vertex.Type#START} event.
     *
     * @param vertex the vertex
     * @param tree the tree holding the current edges intersecting the sweep
     * line
     */
    protected void start(Vertex vertex, EdgeBinaryTree tree) {
        // we need to add the edge to the left to the tree
        // since the line in the next event may be intersecting it
        Edge leftEdge = vertex.getLeft();
        tree.insert(leftEdge);
        // set the left edge's helper to this vertex
        leftEdge.setHelper(vertex);
    }

    /**
     * Handles a {@link Vertex.Type#END} event.
     *
     * @param vertex the vertex
     * @param tree the tree holding the current edges intersecting the sweep
     * line
     * @param dcel the DCEL object to add edges to if necessary
     */
    protected void end(Vertex vertex, EdgeBinaryTree tree, DoublyConnectedEdgeList dcel) {
        // if the vertex type is an end vertex then we
        // know that we need to remove the right edge
        // since the sweep line no longer intersects it
        Edge rightEdge = vertex.getRight();
        // before we remove the edge we need to make sure
        // that we don't forget to link up MERGE vertices
        if (rightEdge.getHelper().getType() == Vertex.Type.MERGE) {
            // connect v to v.right.helper
            dcel.addHalfEdges(vertex.getDcelReference(), rightEdge.getHelper().getDcelReference());
        }
        // remove v.right from T
        tree.remove(rightEdge);
    }

    /**
     * Handles a {@link Vertex.Type#SPLIT} event.
     *
     * @param vertex the vertex
     * @param tree the tree holding the current edges intersecting the sweep
     * line
     * @param dcel the DCEL object to add edges to if necessary
     */
    protected void split(Vertex vertex, EdgeBinaryTree tree, DoublyConnectedEdgeList dcel) {
        // if we have a split vertex then we can find
        // the closest edge to the left side of the vertex
        // and attach its helper to this vertex
        Edge ej = tree.findClosest(vertex);

        // connect v to ej.helper
        dcel.addHalfEdges(vertex.getDcelReference(), ej.getHelper().getDcelReference());

        // set the new helper for the edge
        ej.setHelper(vertex);
        // insert the edge to the left of this vertex
        tree.insert(vertex.getLeft());
        // set the left edge's helper
        vertex.getLeft().setHelper(vertex);
    }

    /**
     * Handles a {@link Vertex.Type#MERGE} event.
     *
     * @param vertex the vertex
     * @param tree the tree holding the current edges intersecting the sweep
     * line
     * @param dcel the DCEL object to add edges to if necessary
     */
    protected void merge(Vertex vertex, EdgeBinaryTree tree, DoublyConnectedEdgeList dcel) {
        // get the previous edge
        Edge eiPrev = vertex.getRight();
        // check if its helper is a merge vertex
        if (eiPrev.getHelper().getType() == Vertex.Type.MERGE) {
            // connect v to v.right.helper
            dcel.addHalfEdges(vertex.getDcelReference(), eiPrev.getHelper().getDcelReference());
        }
        // remove the previous edge since the sweep 
        // line no longer intersects with it
        tree.remove(eiPrev);
        // find the edge closest to the given vertex
        Edge ej = tree.findClosest(vertex);
        // is the edge's helper a merge vertex
        if (ej.getHelper().getType() == Vertex.Type.MERGE) {
            // connect v to ej.helper
            dcel.addHalfEdges(vertex.getDcelReference(), ej.getHelper().getDcelReference());
        }
        // set the closest edge's helper to this vertex
        ej.setHelper(vertex);
    }

    /**
     * Handles a {@link Vertex.Type#MERGE} event.
     *
     * @param vertex the vertex
     * @param tree the tree holding the current edges intersecting the sweep
     * line
     * @param dcel the DCEL object to add edges to if necessary
     */
    protected void regular(Vertex vertex, EdgeBinaryTree tree, DoublyConnectedEdgeList dcel) {

        // check if the interior is to the right of this vertex
        if (vertex.isInteriorRight()) {
            // if so, check the previous edge's helper to see
            // if its a merge vertex
            if (vertex.getRight().getHelper().getType() == Vertex.Type.MERGE) {
                // connect v to v.right.helper
                dcel.addHalfEdges(vertex.getDcelReference(), vertex.getRight().getHelper().getDcelReference());
            }
            // remove the previous edge since the sweep 
            // line no longer intersects with it
            tree.remove(vertex.getRight());
            // add the next edge
            tree.insert(vertex.getLeft());
            // set the helper
            vertex.getLeft().setHelper(vertex);
        } else {
            // otherwise find the closest edge
            Edge ej = tree.findClosest(vertex);
            // check the helper type
            if (ej.getHelper().getType() == Vertex.Type.MERGE) {
                // connect v to ej.helper
                dcel.addHalfEdges(vertex.getDcelReference(), ej.getHelper().getDcelReference());
            }
            // set the new helper
            ej.setHelper(vertex);
        }
    }

    public static class EdgeComparator implements Comparator<Edge>, java.io.Serializable {

        private static final long serialVersionUID = 20140226001L;
        /* (non-Javadoc)
         * @see java.util.Comparator#compareTo(java.lang.Object)
         */

        @Override
        public int compare(final Edge ed1, final Edge ed2) {
            if (ed1 == ed2) {
                return 0;
            }
            // first sort by the minimum x value
            double value = ed1.getMinX() - ed2.getMinX();
            if (Math.abs(value) <= Epsilon.E) {
                // if they are near zero sort by the minimum y
                value = ed2.getMinY() - ed1.getMinY();
            }
            return (int) Math.signum(value);

        }
    }
}
