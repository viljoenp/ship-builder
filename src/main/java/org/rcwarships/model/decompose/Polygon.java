/*
 * Copyright (c) 2010-2012 William Bittle  http://www.dyn4j.org/
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 *   * Redistributions of source code must retain the above copyright notice, this list of conditions 
 *     and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *     and the following disclaimer in the documentation and/or other materials provided with the 
 *     distribution.
 *   * Neither the name of dyn4j nor the names of its contributors may be used to endorse or 
 *     promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.rcwarships.model.decompose;
//
//import org.rcwarships.geometry.*;
import javax.vecmath.Tuple2d;
import javax.vecmath.Vector2d;
import org.rcwarships.model.Convex;
import org.rcwarships.model.Epsilon;
import org.rcwarships.resources.Messages;

/**
 * Represents a {@link Convex} {@link Polygon}.
 * <p>
 * A {@link Polygon} must have at least 3 vertices where one of which is not
 * colinear with the other two simultaneously. A {@link Polygon} must also be
 * {@link Convex} and have anti-clockwise winding of points.
 * <p>
 * A polygon cannot have coincident vertices.
 *
 * @author William Bittle
 * @version 3.0.2
 * @since 1.0.0
 */
public class Polygon extends Wound implements Convex {

    /**
     * Default constructor for sub classes.
     */
    protected Polygon() {
        super();
    }
//        	/**
//	 * Returns the cross product of the this {@link Vector2} and the given {@link Vector2}.
//	 * @param vector the {@link Vector2}
//	 * @return double
//	 */
//	public double cross(Vector2 vector) {
//		return this.x * vector.y - this.y * vector.x;
//	}


    /**
     * Full constructor.
     * <p>
     * Creates a new {@link Polygon} using the given vertices. The center of the
     * polygon is calculated using an area weighted method.
     * <p>
     * A polygon must have 3 or more vertices, of which one is not colinear with
     * the other two.
     * <p>
     * A polygon must also be convex and have anti-clockwise winding.
     *
     * @param vertices the array of vertices
     * @throws NullPointerException if vertices is null or contains a null
     * element
     * @throws IllegalArgumentException if vertices contains less than 3 points,
     * contains coincident points, is not convex, or has clockwise winding
     */
    public Polygon(SweepLine.Vertex... vertices) {
        super();
        // check the vertex array
        if (vertices == null) {
            throw new NullPointerException(Messages.getString("geometry.polygon.nullArray"));
        }
        // get the size
        int size = vertices.length;
        // check the size
        if (size < 3) {
            throw new IllegalArgumentException(Messages.getString("geometry.polygon.lessThan3Vertices"));
        }
        // check for null vertices
        for (int i = 0; i < size; i++) {
            if (vertices[i] == null) {
                throw new NullPointerException(Messages.getString("geometry.polygon.nullVertices"));
            }
        }
        // check for convex
        double area = 0.0;
        double sign = 0.0;
        for (int i = 0; i < size; i++) {
            SweepLine.Vertex p0 = (i - 1 < 0) ? vertices[size - 1] : vertices[i - 1];
            SweepLine.Vertex p1 = vertices[i];
            SweepLine.Vertex p2 = (i + 1 == size) ? vertices[0] : vertices[i + 1];


            // check the cross product for CCW winding
            area += p1.cross(p2);
            // check for coincident vertices
            if (p1.equals(p2)) {
                throw new IllegalArgumentException(Messages.getString("geometry.polygon.coincidentVertices"));
            }
            double cross = Math.signum(cross(p0.to(p1),p1.to(p2)));
            // check for colinear points (for now its allowed)
            if (Math.abs(cross) > Epsilon.E) {
                // check for convexity
                if (sign != 0.0 && cross != sign) {
                    throw new IllegalArgumentException(Messages.getString("geometry.polygon.nonConvex"));
                }
            }
            sign = cross;
        }
        // check for CCW
        if (area < 0.0) {
            throw new IllegalArgumentException(Messages.getString("geometry.polygon.invalidWinding"));
        }
        // set the vertices
        this.vertices = vertices;
        // create the normals
        this.normals = new Vector2d[size];
        for (int i = 0; i < size; i++) {
            // get the edge points
            SweepLine.Vertex p1 = vertices[i];
            SweepLine.Vertex p2 = (i + 1 == size) ? vertices[0] : vertices[i + 1];
            // create the edge and get its left perpedicular vector
            Vector2d n = left(p1.to(p2));
            // normalize it
            n.normalize();
            this.normals[i] = n;
        }
        // perform the area weighted center to otain the center
        this.center = getAreaWeightedCenter(this.vertices);
        // find the maximum radius from the center
        double r2 = 0.0;
        for (int i = 0; i < size; i++) {
            double r2t = distanceSquared(this.center, vertices[i].getX(), vertices[i].getY());
            // keep the largest
            r2 = Math.max(r2, r2t);
        }
        // set the radius
        this.radius = Math.sqrt(r2);
    }

    /**
     * Returns the area weighted centroid for the given points.
     *
     * @see #getAreaWeightedCenter(List)
     * @param points the {@link Polygon} points
     * @return {@link Tuple2d} the area weighted centroid
     * @throws NullPointerException if points is null or an element of points is
     * null
     * @throws IllegalArgumentException if points is empty
     */
    public static final Vector2d getAreaWeightedCenter(SweepLine.Vertex... points) {
        // check for null array
        if (points == null) {
            throw new NullPointerException(Messages.getString("geometry.nullPointArray"));
        }
        // get the size
        int size = points.length;
        // check for empty array
        if (size == 0) {
            throw new IllegalArgumentException(Messages.getString("geometry.invalidSizePointArray1"));
        }
        // check for array of one point
        if (size == 1) {
            SweepLine.Vertex p = points[0];
            // check for null
            if (p == null) {
                throw new NullPointerException(Messages.getString("geometry.nullPointArrayElements"));
            }
            return new Vector2d(p.getX(), p.getY());
        }
        // otherwise perform the computation
        Vector2d center = new Vector2d();
        double area = 0.0;
        // loop through the vertices
        for (int i = 0; i < size; i++) {
            // get two verticies
            SweepLine.Vertex p1 = points[i];
            SweepLine.Vertex p2 = i + 1 < size ? points[i + 1] : points[0];
            // check for null
            if (p1 == null || p2 == null) {
                throw new NullPointerException(Messages.getString("geometry.nullPointArrayElements"));
            }
            // perform the cross product (yi * x(i+1) - y(i+1) * xi)
            double d = p1.cross(p2);
            // multiply by half
            double triangleArea = 0.5 * d;
            // add it to the total area
            area += triangleArea;

            // area weighted centroid
            // (p1 + p2) * (D / 3)
            // = (x1 + x2) * (yi * x(i+1) - y(i+1) * xi) / 3
            // we will divide by the total area later
            // 
            // center.add(new Vector2d(p1.getX(), p1.getY()).sum(new Vector2d(p2.getX(), p2.getY())).multiply(INV_3).multiply(triangleArea));
            Vector2d p1_p2 = new Vector2d(p1.getX()+p2.getX(), p1.getY()+p2.getY());
            p1_p2.scale(INV_3);
            p1_p2.scale(triangleArea);
            center.add(p1_p2);            
        }
        // check for zero area
        if (Math.abs(area) <= Epsilon.E) {
            // zero area can only happen if all the points are the same point
            // in which case just return a copy of the first
            return new Vector2d(points[0].getX(), points[0].getY());
        }
        // finish the centroid calculation by dividing by the total area
        center.scale(1.0 / area);
        // return the center
        return center;
    }
    /**
     * The value of 1/3
     */
    private static final double INV_3 = 1.0 / 3.0;
    /* (non-Javadoc)
     * @see org.rcwarships.geometry.Wound#toString()
     */

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Polygon[").append(super.toString())
                .append("|UserData=").append(this.userData)
                .append("]");
        return sb.toString();
    }
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.Convex#getAxes(java.util.List, org.rcwarships.geometry.Transform)
//     */
//    @Override
//    public Tuple2d[] getAxes(Tuple2d[] foci, Transform transform) {
//        // get the size of the foci list
//        int fociSize = foci != null ? foci.length : 0;
//        // get the number of vertices this polygon has
//        int size = this.vertices.length;
//        // the axes of a polygon are created from the normal of the edges
//        // plus the closest point to each focus
//        Tuple2d[] axes = new Tuple2d[size + fociSize];
//        int n = 0;
//        // loop over the edge normals and put them into world space
//        for (int i = 0; i < size; i++) {
//            // create references to the current points
//            Tuple2d v = this.normals[i];
//            // transform it into world space and add it to the list
//            axes[n++] = transform.getTransformedR(v);
//        }
//        // loop over the focal points and find the closest
//        // points on the polygon to the focal points
//        for (int i = 0; i < fociSize; i++) {
//            // get the current focus
//            Tuple2d f = foci[i];
//            // create a place for the closest point
//            Tuple2d closest = transform.getTransformed(this.vertices[0]);
//            double d = f.distanceSquared(closest);
//            // find the minimum distance vertex
//            for (int j = 1; j < size; j++) {
//                // get the vertex
//                Tuple2d p = this.vertices[j];
//                // transform it into world space
//                p = transform.getTransformed(p);
//                // get the squared distance to the focus
//                double dt = f.distanceSquared(p);
//                // compare with the last distance
//                if (dt < d) {
//                    // if its closer then save it
//                    closest = p;
//                    d = dt;
//                }
//            }
//            // once we have found the closest point create 
//            // a vector from the focal point to the point
//            Tuple2d axis = f.to(closest);
//            // normalize it
//            axis.normalize();
//            // add it to the array
//            axes[n++] = axis;
//        }
//        // return all the axes
//        return axes;
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.Convex#getFoci(org.rcwarships.geometry.Transform)
//     */
//    @Override
//    public Tuple2d[] getFoci(Transform transform) {
//        return null;
//    }
//
    /* (non-Javadoc)
     * @see org.rcwarships.geometry.Shape#contains(org.rcwarships.geometry.Vector, org.rcwarships.geometry.Transform)
     */
//
//    @Override
//    public boolean contains(Vector2d point, Transform transform) {
//        // if the polygon is convex then do a simple inside test
//        // if the the sign of the location of the point on the side of an edge (or line)
//        // is always the same and the polygon is convex then we know that the
//        // point lies inside the polygon
//        // This method doesn't care about vertex winding
//        // inverse transform the point to put it in local coordinates
//        Tuple2d p = transform.getInverseTransformed(point);
//        Tuple2d p1 = new Vector2d( this.vertices[0].getX() , this.vertices[0].getY() );
//        Tuple2d p2 = new Vector2d( this.vertices[1].getX() , this.vertices[1].getY() );;
//        // get the location of the point relative to the first two vertices
//        double last = getLocation(p, p1, p2);
//        int size = this.vertices.length;
//        // loop through the rest of the vertices
//        for (int i = 1; i < size; i++) {
//            // p1 is now p2
//            p1 = p2;
//            // p2 is the next point
//            p2 = this.vertices[(i + 1) == size ? 0 : i + 1].getTuple2d();
//            // check if they are equal (one of the vertices)
//            if (p.equals(p1)) {
//                return true;
//            }
//            // do side of line test
//            // multiply the last location with this location
//            // if they are the same sign then the opertation will yield a positive result
//            // -x * -y = +xy, x * y = +xy, -x * y = -xy, x * -y = -xy
//            if (last * getLocation(p, p1, p2) < 0) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.AbstractShape#rotate(double, double, double)
//     */
//    @Override
//    public void rotate(double theta, double x, double y) {
//        super.rotate(theta, x, y);
//        int size = this.vertices.length;
//        for (int i = 0; i < size; i++) {
//            this.vertices[i].rotate(theta, x, y);
//            this.normals[i].rotate(theta, x, y);
//        }
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.AbstractShape#translate(double, double)
//     */
//    @Override
//    public void translate(double x, double y) {
//        super.translate(x, y);
//        int size = this.vertices.length;
//        for (int i = 0; i < size; i++) {
//            this.vertices[i].add(x, y);
//        }
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.Shape#project(org.rcwarships.geometry.Vector, org.rcwarships.geometry.Transform)
//     */
//    @Override
//    public Interval project(Tuple2d n, Transform transform) {
//        double v = 0.0;
//        // get the first point
//        Tuple2d p = transform.getTransformed(this.vertices[0]);
//        // project the point onto the vector
//        double min = n.dot(p);
//        double max = min;
//        // loop over the rest of the vertices
//        int size = this.vertices.length;
//        for (int i = 1; i < size; i++) {
//            // get the next point
//            p = transform.getTransformed(this.vertices[i]);
//            // project it onto the vector
//            v = n.dot(p);
//            if (v < min) {
//                min = v;
//            } else if (v > max) {
//                max = v;
//            }
//        }
//        return new Interval(min, max);
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.Convex#getFarthestFeature(org.rcwarships.geometry.Vector, org.rcwarships.geometry.Transform)
//     */
//    @Override
//    public Edge getFarthestFeature(Tuple2d n, Transform transform) {
//        // transform the normal into local space
//        Tuple2d localn = transform.getInverseTransformedR(n);
//        Tuple2d maximum = new Tuple2d();
//        double max = -Double.MAX_VALUE;
//        int index = 0;
//        // find the vertex on the polygon that is further along on the penetration axis
//        int count = this.vertices.length;
//        for (int i = 0; i < count; i++) {
//            // get the current vertex
//            Tuple2d v = this.vertices[i];
//            // get the scalar projection of v onto axis
//            double projection = localn.dot(v);
//            // keep the maximum projection point
//            if (projection > max) {
//                // set the max point
//                maximum.set(v);
//                // set the new maximum
//                max = projection;
//                // save the index
//                index = i;
//            }
//        }
//
//        // once we have the point of maximum
//        // see which edge is most perpendicular
//        int l = index + 1 == count ? 0 : index + 1;
//        int r = index - 1 < 0 ? count - 1 : index - 1;
//        Tuple2d leftN = this.normals[index == 0 ? count - 1 : index - 1];
//        Tuple2d rightN = this.normals[index];
//        // create the maximum point for the feature (transform the maximum into world space)
//        transform.transform(maximum);
//        Vertex vm = new Vertex(maximum, index);
//        // is the left or right edge more perpendicular?
//        if (leftN.dot(localn) < rightN.dot(localn)) {
//            Tuple2d left = transform.getTransformed(this.vertices[l]);
//            Vertex vl = new Vertex(left, l);
//            // make sure the edge is the right winding
//            return new Edge(vm, vl, vm, maximum.to(left), index + 1);
//        } else {
//            Tuple2d right = transform.getTransformed(this.vertices[r]);
//            Vertex vr = new Vertex(right, r);
//            // make sure the edge is the right winding
//            return new Edge(vr, vm, vm, right.to(maximum), index);
//        }
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.Convex#getFarthestPoint(org.rcwarships.geometry.Vector, org.rcwarships.geometry.Transform)
//     */
//    @Override
//    public Tuple2d getFarthestPoint(Tuple2d n, Transform transform) {
//        // transform the normal into local space
//        Tuple2d localn = transform.getInverseTransformedR(n);
//        Tuple2d point = new Tuple2d();
//        // set the farthest point to the first one
//        point.set(this.vertices[0]);
//        // prime the projection amount
//        double max = localn.dot(this.vertices[0]);
//        // loop through the rest of the vertices to find a further point along the axis
//        int size = this.vertices.length;
//        for (int i = 1; i < size; i++) {
//            // get the current vertex
//            Tuple2d v = this.vertices[i];
//            // project the vertex onto the axis
//            double projection = localn.dot(v);
//            // check to see if the projection is greater than the last
//            if (projection > max) {
//                // otherwise this point is the farthest so far so clear the array and add it
//                point.set(v);
//                // set the new maximum
//                max = projection;
//            }
//        }
//        // transform the point into world space
//        transform.transform(point);
//        return point;
//    }
//
//    /**
//     * Creates a {@link Mass} object using the geometric properties of this
//     * {@link Polygon} and the given density.
//     * <p>
//     * A {@link Polygon}'s centroid must be computed by the area weighted method
//     * since the average method can be bias to one side if there are more points
//     * on that one side than another.
//     * <p>
//     * Finding the area of a {@link Polygon} can be done by using the following
//     * summation:
//     * <pre>
//     * 0.5 * &sum;(x<sub>i</sub> * y<sub>i + 1</sub> - x<sub>i + 1</sub> * y<sub>i</sub>)
//     * </pre> Finding the area weighted centroid can be done by using the
//     * following summation:
//     * <pre>
//     * 1 / (6 * A) * &sum;(p<sub>i</sub> + p<sub>i + 1</sub>) * (x<sub>i</sub> * y<sub>i + 1</sub> - x<sub>i + 1</sub> * y<sub>i</sub>)
//     * </pre> Finding the inertia tensor can by done by using the following
//     * equation:
//     * <pre>
//     *          &sum;(p<sub>i + 1</sub> x p<sub>i</sub>) * (p<sub>i</sub><sup>2</sup> + p<sub>i</sub> &middot; p<sub>i + 1</sub> + p<sub>i + 1</sub><sup>2</sup>)
//     * m / 6 * -------------------------------------------
//     *                        &sum;(p<sub>i + 1</sub> x p<sub>i</sub>)
//     * </pre> Where the mass is computed by:
//     * <pre>
//     * d * area
//     * </pre>
//     *
//     * @param density the density in kg/m<sup>2</sup>
//     * @return {@link Mass} the {@link Mass} of this {@link Polygon}
//     */
//    @Override
//    public Mass createMass(double density) {
//        // can't use normal centroid calculation since it will be weighted towards sides
//        // that have larger distribution of points.
//        Tuple2d center = new Tuple2d();
//        double area = 0.0;
//        double I = 0.0;
//        int n = this.vertices.length;
//        // calculate inverse three once
//        final double inv3 = 1.0 / 3.0;
//        // loop through the vertices
//        for (int i = 0; i < n; i++) {
//            // get two vertices
//            Tuple2d p1 = this.vertices[i];
//            Tuple2d p2 = i + 1 < n ? this.vertices[i + 1] : this.vertices[0];
//            // perform the cross product (yi * x(i+1) - y(i+1) * xi)
//            double D = p1.cross(p2);
//            // multiply by half
//            double triangleArea = 0.5 * D;
//            // add it to the total area
//            area += triangleArea;
//
//            // area weighted centroid
//            // (p1 + p2) * (D / 6)
//            // = (x1 + x2) * (yi * x(i+1) - y(i+1) * xi) / 6
//            // we will divide by the total area later
//            center.add(p1.sum(p2).multiply(inv3).multiply(triangleArea));
//
//            // (yi * x(i+1) - y(i+1) * xi) * (p2^2 + p2 . p1 + p1^2)
//            I += triangleArea * (p2.dot(p2) + p2.dot(p1) + p1.dot(p1));
//            // we will do the m / 6A = (d / 6) when we have the area summed up
//        }
//        // compute the mass
//        double m = density * area;
//        // finish the centroid calculation by dividing by the total area
//        center.multiply(1.0 / area);
//        // finish the inertia tensor by dividing by the total area and multiplying by d / 6
//        I *= (density / 6.0);
//        return new Mass(center, m, I);
//    }
//
//    /* (non-Javadoc)
//     * @see org.rcwarships.geometry.Shape#createAABB(org.rcwarships.geometry.Transform)
//     */
//    @Override
//    public AABB createAABB(Transform transform) {
//        double vx = 0.0;
//        double vy = 0.0;
//        // get the first point
//        Tuple2d p = transform.getTransformed(this.vertices[0]);
//        // project the point onto the vector
//        double minX = Tuple2d.X_AXIS.dot(p);
//        double maxX = minX;
//        double minY = Tuple2d.Y_AXIS.dot(p);
//        double maxY = minY;
//        // loop over the rest of the vertices
//        int size = this.vertices.length;
//        for (int i = 1; i < size; i++) {
//            // get the next point
//            p = transform.getTransformed(this.vertices[i]);
//            // project it onto the vector
//            vx = Tuple2d.X_AXIS.dot(p);
//            vy = Tuple2d.Y_AXIS.dot(p);
//            // compare the x values
//            minX = Math.min(minX, vx);
//            maxX = Math.max(maxX, vx);
//            minY = Math.min(minY, vy);
//            maxY = Math.max(maxY, vy);
//        }
//        // create the aabb
//        return new AABB(minX, minY, maxX, maxY);
//    }

    public void rotateAboutCenter(double theta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getRadius(Tuple2d center) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



}
