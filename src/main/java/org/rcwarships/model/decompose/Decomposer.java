/*
 * Copyright (c) 2011 William Bittle  http://www.dyn4j.org/
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 *   * Redistributions of source code must retain the above copyright notice, this list of conditions 
 *     and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
 *     and the following disclaimer in the documentation and/or other materials provided with the 
 *     distribution.
 *   * Neither the name of dyn4j nor the names of its contributors may be used to endorse or 
 *     promote products derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.rcwarships.model.decompose;

import java.util.List;
import javax.vecmath.Vector2d;
import org.rcwarships.model.Convex;

/**
 * Represents an algorithm to decompose a given list of points representing a
 * polygon into a list of convex pieces.
 *
 * @author William Bittle
 * @version 2.2.3
 * @since 2.2.0
 */
public interface Decomposer {

    public interface Vertex extends Comparable<Vertex> {

        /**
         * Enumeration of the {@link Vertex} types.
         *
         * @author William Bittle
         * @version 2.2.0
         * @since 2.2.0
         */
        enum Type {

            /**
             * Vertex above both its neighbors and the internal angle is less
             * than &pi;
             */
            START,
            /**
             * Vertex above both its neighbors and the internal angle is greater
             * than &pi;
             */
            SPLIT,
            /**
             * Vertex below both its neighbors and the internal angle is less
             * than &pi;
             */
            END,
            /**
             * Vertex below both its neighbors and the internal angle is greater
             * than &pi;
             */
            MERGE,
            /**
             * Vertex between both of its neighbors
             */
            REGULAR,
        }

        boolean isLeft(SweepLine.Edge edge);

        boolean isInteriorRight();

        Type getType();

        void setType(Type type);

        double getX();

        double getY();

        Vector2d getTuple2d();

        Vertex getNext();

        void setNext(Vertex next);

        Vertex getPrev();

        void setPrev(Vertex next);

        Edge getLeft();

        void setLeft(SweepLine.Edge left);

        Edge getRight();

        void setRight(SweepLine.Edge right);

        Vector2d to(Vertex dest);

        DoublyConnectedEdgeList.Vertex getDcelReference();

        void setDcelReference(DoublyConnectedEdgeList.Vertex dcelReference);

        double cross(Vertex dest);

        Vertex clone() ;
    }

//    /**
//     * Represents a vertex on a polygon that stores information about the left
//     * and right edges and left and right vertices.
//     *
//     * @author William Bittle
//     * @version 3.0.2
//     * @since 2.2.0
//     */
//    protected static class Vertex implements Comparable<Vertex> {
//
//        /**
//         * The vertex point
//         */
//        protected Vector2d point;
//        /**
//         * The vertex type
//         */
//        protected Type type;
//        /**
//         * The next vertex in Counter-Clockwise order
//         */
//        protected Vertex next;
//        /**
//         * The previous vertex in Counter-Clockwise order
//         */
//        protected Vertex prev;
//        /**
//         * The next edge in Counter-Clockwise order
//         */
//        protected Edge left;
//        /**
//         * The previous edge in Counter-Clockwise order
//         */
//        protected Edge right;
//        /**
//         * The reference to the vertex in the DCEL
//         */
//        protected DoublyConnectedEdgeList.Vertex dcelReference;
//
//        /* (non-Javadoc)
//         * @see java.lang.Comparable#compareTo(java.lang.Object)
//         */
//        @Override
//        public int compareTo(Vertex other) {
//            // sort by the y first then by x if the y's are equal
//            Vector2d p = this.point;
//            Vector2d q = other.point;
//            double diff = q.y - p.y;
//            if (Math.abs(diff) <= Epsilon.E) {
//                // if the difference is near equal then compare the x values
//                return (int) Math.signum(p.x - q.x);
//            } else {
//                return (int) Math.signum(diff);
//            }
//        }
//
//        /* (non-Javadoc)
//         * @see java.lang.Object#toString()
//         */
//        @Override
//        public String toString() {
//            StringBuilder sb = new StringBuilder();
//            sb.append("SweepLine.Vertex[Point=").append(this.point)
//                    .append("|Type=").append(this.type)
//                    .append("]");
//            return sb.toString();
//        }
//
//        /**
//         * Returns true if this {@link Vertex} is left of the given
//         * {@link Edge}.
//         *
//         * @param edge the {@link Edge}
//         * @return boolean true if this {@link Vertex} is to the left of the
//         * given {@link Edge}
//         */
//        public boolean isLeft(Edge edge) {
//            // attempt the simple comparison first
//            if (this.point.x < edge.getMinX()) {
//                return true;
//            } else if (this.point.x > edge.getMaxX()) {
//                return false;
//            }
//            // its in between the min and max x so we need to 
//            // do a side of line test
//            double location = Segment.getLocation(this.point, edge.v0.point, edge.v1.point);
//            if (location < 0.0) {
//                return true;
//            } else {
//                return false;
//            }
//        }
//
//        /**
//         * Returns true if the interior is to the right of this vertex.
//         * <p>
//         * The left edge of this vertex is used to determine where the interior
//         * of the polygon is.
//         *
//         * @return boolean
//         */
//        public boolean isInteriorRight() {
//            return this.left.isInteriorRight();
//        }
//    }
    /**
     * Represents an edge of a polygon storing the next and previous edges and
     * the vertices that make up this edge.
     * <p>
     * The edge also stores a helper vertex which is used during y-monotone
     * decomposition.
     *
     * @author William Bittle
     * @version 3.0.2
     * @since 2.2.0
     */
    public interface Edge extends Comparable<Edge> {

        /**
         * Returns the minimum x value of this edge.
         *
         * @return double
         */
        public double getMinX();

        /**
         * Returns the maximum x value of this edge.
         *
         * @return double
         */
        public double getMaxX();

        /**
         * Returns the minimum y value of this edge.
         *
         * @return double
         */
        public double getMinY();

        /**
         * Returns the maximum y value of this edge.
         *
         * @return double
         */
        public double getMaxY();

        public Vertex getVertex0();

        public Vertex getVertex1();

        public Decomposer.Edge getNext();

        public void setNext(Decomposer.Edge next);

        public Decomposer.Edge getPrev();

        public void setPrev(Decomposer.Edge prev);

        public Decomposer.Vertex getHelper();

        public void setHelper(Decomposer.Vertex helper);

        public void setVertex0(Decomposer.Vertex vertex0);

        public void setVertex1(Decomposer.Vertex vertex1);

        /**
         * Returns true if the interior of the polygon is to the right of this
         * edge.
         * <p>
         * Given that the polygon's vertex winding is Counter- Clockwise, if the
         * vertices that make this edge decrease along the y axis then the
         * interior of the polygon is to the right, otherwise its to the left.
         *
         * @return boolean
         */
        public boolean isInteriorRight();
    }

    /**
     * Performs the decomposition on the given list of polygon points returning
     * a list of convex shapes.
     *
     * @param points the list of points
     * @return List&lt;{@link Convex}&gt;
     * @throws NullPointerException if points is null or contains null points
     * @throws IllegalArgumentException if points contains less than 4 points
     */
    public List<Convex> decompose(java.util.List< Vertex> points);
}
