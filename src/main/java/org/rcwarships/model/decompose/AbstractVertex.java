/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.decompose;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Vector2d;
import org.rcwarships.model.Epsilon;

/**
 *
 * @author viljoenp
 */
public abstract class AbstractVertex implements Decomposer.Vertex, java.lang.Cloneable {

    /**
     * The vertex type
     */
    protected Decomposer.Vertex.Type type;
    /**
     * The next vertex in Counter-Clockwise order
     */
    protected Decomposer.Vertex next;
    /**
     * The previous vertex in Counter-Clockwise order
     */
    protected Decomposer.Vertex prev;
    /**
     * The next edge in Counter-Clockwise order
     */
    protected Decomposer.Edge left;
    /**
     * The previous edge in Counter-Clockwise order
     */
    protected Decomposer.Edge right;
    /**
     * The reference to the vertex in the DCEL
     */
    protected DoublyConnectedEdgeList.Vertex dcelReference;

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Decomposer.Vertex other) {
        // sort by the y first then by x if the y's are equal
        double diff = other.getY() - getY();
        if (Math.abs(diff) <= Epsilon.E) {
            // if the difference is near equal then compare the x values
            return (int) Math.signum(getX() - other.getX());
        } else {
            return (int) Math.signum(diff);
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SweepLine.Vertex[Point=[").append(getX()).append(",").append(getY()).append("]").append("|  Type=").append(this.type).append("]");
        return sb.toString();
    }

    /**
     * Returns true if this {@link Vertex} is left of the given {@link Edge}.
     *
     * @param edge the {@link Edge}
     * @return boolean true if this {@link Vertex} is to the left of the given
     * {@link Edge}
     */
    @Override
    public boolean isLeft(Decomposer.Edge edge) {
        // attempt the simple comparison first
        if (this.getX() < edge.getMinX()) {
            return true;
        } else if (this.getX() > edge.getMaxX()) {
            return false;
        }
        // its in between the min and max x so we need to 
        // do a side of line test
        double location = Wound.getLocation(this.getTuple2d(), edge.getVertex0().getTuple2d(), edge.getVertex1().getTuple2d());
        return location < 0.0;
    }

    /**
     * Returns true if the interior is to the right of this vertex.
     * <p>
     * The left edge of this vertex is used to determine where the interior of
     * the polygon is.
     *
     * @return boolean
     */
    @Override
    public boolean isInteriorRight() {
        return this.left.isInteriorRight();
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public Decomposer.Vertex getNext() {
        return next;
    }

    @Override
    public void setNext(Decomposer.Vertex next) {
        this.next = next;
    }

    @Override
    public Decomposer.Vertex getPrev() {
        return prev;
    }

    @Override
    public void setPrev(Decomposer.Vertex prev) {
        this.prev = prev;
    }

    @Override
    public Decomposer.Edge getLeft() {
        return left;
    }

    @Override
    public void setLeft(Decomposer.Edge left) {
        this.left = left;
    }

    @Override
    public Decomposer.Edge getRight() {
        return right;
    }

    @Override
    public void setRight(Decomposer.Edge right) {
        this.right = right;
    }

    @Override
    public DoublyConnectedEdgeList.Vertex getDcelReference() {
        return dcelReference;
    }

    @Override
    public void setDcelReference(DoublyConnectedEdgeList.Vertex dcelReference) {
        this.dcelReference = dcelReference;
    }

    public static double cross(Vector2d a, Vector2d b) {
        return a.x * b.y - a.y * b.x;
    }

    @Override
    public Vector2d to(Decomposer.Vertex dest) {
        return new Vector2d(dest.getX() - getX(), dest.getY() - getY());
    }

    @Override
    public double cross(Decomposer.Vertex dest) {
        return getX() * dest.getY() - getY() * dest.getX();
    }

    @Override
    public AbstractVertex clone() {
        try {
            return (AbstractVertex) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(AbstractVertex.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }
}
