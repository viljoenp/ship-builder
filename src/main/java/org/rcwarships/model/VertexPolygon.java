/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model;

public class VertexPolygon {

}
/*

import org.zou.triangulation.Point3d;
import org.zou.triangulation.Polygon;

public class VertexPolygon extends java.util.ArrayList<Point3d<Vertex>> implements Polygon<Vertex> {

    public VertexPolygon() {
        super();
    }

    public VertexPolygon(final java.util.List<Vertex> vertexList) {
        super(vertexList.size());
        addVertexes(vertexList);
    }

    public VertexPolygon(int capacity) {
        super(capacity);
    }

    public final void addVertexes(final  java.util.List<Vertex> vertexList) {
        for (Vertex v : vertexList) {
            add(new VertexPoint(v));
        }
    }

    public static class VertexPoint implements Point3d<Vertex> {

        final Vertex data;

        public VertexPoint(final Vertex data) {
            this.data = data;
        }

        @Override
        public double getX() {
            return data.coord.x;
        }

        @Override
        public double getY() {
            return data.coord.y;
        }

        @Override
        public double getZ() {
            return data.coord.z;
        }

        @Override
        public Vector3d getNormal() {
            return data.normal;
        }

        @Override
        public Vertex getData() {
            return data;
        }

    }

}


*/
