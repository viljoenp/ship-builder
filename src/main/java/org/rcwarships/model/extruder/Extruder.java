/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.extruder;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple2d;
import javax.vecmath.Tuple3d;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import org.rcwarships.loaders.svg.SvgFile;
import org.rcwarships.model.Epsilon;
import org.rcwarships.model.Plane;
import org.rcwarships.model.Transform3dTo2d;

/**
 *
 * @author viljoenp
 */
public class Extruder {

    private static final Logger LOGGER = Logger.getLogger(Extruder.class.getName());

    /**
     * Create a list of Point3d that represents the projection of the
     * originalPoints using the transform, the returned points will form the
     * opposite edges so that originalPoints + the extruded(reversed) points
     * form a closed loop that together forms a 2d ribbon of average width. The
     * Extrusion will be limited in the Y axis to the max value of the Y
     * coordinates at the start and end of the list of originalPoints BUT NOTE
     * in the X plane
     *
     * @param originalPoints The Ordered list of points that form a
     * non-intersecting polygon line.
     * @param transform The Transform used to transform a Point3d to a Point2d
     * for this extrusion.
     * @param width Average width of the extrusion.
     * @param maxCollisionDepth number of vertexes to backtrack in search of
     * collisions after the extrusion phase
     * @return list of Point3d that form the opposite side of the polygon loop
     * for the extrusion.
     */
    public static java.util.List<Point3d> extrude(final java.util.List<? extends Tuple3d> originalPoints, final Transform3dTo2d transform, final double width, final int maxCollisionDepth) {

        java.util.List<Vector2d> yznormals = new java.util.ArrayList<>(originalPoints.size());
        yznormals.add(transform.getNormal(originalPoints.get(0), originalPoints.get(1)));
        for (int i = 1; i < originalPoints.size(); i++) {
            yznormals.add(transform.getNormal(originalPoints.get(i - 1), originalPoints.get(i)));
        }

        java.util.List<Vector2d> avgnormals = new java.util.ArrayList<>(originalPoints.size());
        for (int i = 0; i < yznormals.size(); i++) {
            Vector2d curr = findNext(yznormals, i);
            Vector2d prev = findPrev(yznormals, i);
            if (curr.dot(prev) > 0.5) {
                avgnormals.add(avagrage(curr, prev));
            } else {
                avgnormals.add(add(curr, prev));
            }
        }

        //  1. First we build a list of pure projections.
        java.util.List<ExtrudeEdge> extruded_vectors = new java.util.LinkedList<>();

        for (int i = 0; i < originalPoints.size() - 1; i++) {
            final ExtrudeEdge projection = project(transform, width, avgnormals.get(i), originalPoints.get(i), originalPoints.get(i + 1));
            extruded_vectors.add(projection);
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(extruded_vectors, "Transform");
        }

        removeUnMergedCounterWinding(extruded_vectors);
        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(extruded_vectors, "removeUnMergedCounterWinding-1");
        }
        // 2. Sweep through the edges and merge adjasent edges
        // The List of extruded edges will not join correctly , when the angle between edges is > 180 degrees , the two points will not join ( ie a gap ) , 
        // if the angle between edges is < 180 degrees the two edges will intersect. 
        mergeEdgePoints(extruded_vectors, width);
        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(extruded_vectors, "mergeEdgePoints");
        }

        removeZeroSizedEdges(extruded_vectors);
        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(extruded_vectors, "removeZeroSizedEdges-1");
        }
        // 3. Sweep through the edges and check for any collisions with non adjasent edges
        final java.util.List<ExtrudeEdge> checked = new java.util.LinkedList<>();
        for (ExtrudeEdge e1 : extruded_vectors) {
            int size = checked.size();
            while (removeCollisions(checked, e1, maxCollisionDepth)) {
                if (LOGGER.isLoggable(Level.FINER)) {
                    LOGGER.log(Level.WARNING, "removed forward collision  for   {0} - {1} and {2} - {3}  was {4}  now {5}", new Object[]{checked.get(checked.size() - 1).getP1(), checked.get(checked.size() - 1).getP2(), e1.getP1(), e1.getP2(), size, checked.size()});
                }
            }
            checked.add(e1);
        }
        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(checked, "removeZeroSizedEdges");
        }
        removeCounterWinding(checked);
        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(checked, "removeCounterWinding-2");
        }
        removeZeroSizedEdges(checked);

        removePinchedEdges(checked);
        removeZeroSizedEdges(checked);

        if (LOGGER.isLoggable(Level.FINER)) {
            reportZero(checked, "removeZeroSizedEdges-2");
        }

        // 5 Pack the result by reversing the transform
        final java.util.List<Point3d> result = new java.util.ArrayList<>(checked.size() + 1);
        result.add(transform.get3dCoord(checked.get(0).getP1()));
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.log(Level.FINER, "Extrution : startIdx:{0} - {1}     ==    {2}", new Object[]{0, checked.get(0).getP1(), transform.get3dCoord(checked.get(0).getP1())});
        }
        for (int i = 0; i < checked.size(); i++) {
            ExtrudeEdge edge = checked.get(i);

            result.add(transform.get3dCoord(edge.getP2()));
            if (LOGGER.isLoggable(Level.FINER)) {
                LOGGER.log(Level.FINER, "Extrution :      Idx:{0} - {1}     ==    {2}", new Object[]{i, edge.getP2(), transform.get3dCoord(edge.getP2())});
            }
        }

        // FIXME Verify that every point in result is at least width distance away from every other point in originalPoints
//        double distances[][] = new double[originalPoints.size()][result.size()];
        for (int i = 0; i < result.size(); i++) {
            Point3d res = result.get(i);
            for (int j = 0; j < originalPoints.size(); j++) {
                Tuple3d orig = originalPoints.get(j);
                final double dist = res.distance(new Point3d(orig));
//                distances[i][j] = res.distance(new Point3d(orig));
                if (dist < width * 0.99) {
                    LOGGER.log(Level.WARNING, "Extrution did not comply to width Extrution#{0}:{1} is {2} distance from Point#{3}:{4} ", new Object[]{i, res, dist, j, orig});
                }
            }
        }

        return result;
    }

    private static Vector2d add(Vector2d a, Vector2d b) {
        return new Vector2d(a.x + b.x, a.y + b.y);
    }

    private static Vector2d avagrage(Vector2d a, Vector2d b) {
        final Vector2d sum = add(a, b);
        sum.normalize();
        return sum;
    }

    private static Vector2d findPrev(java.util.List<Vector2d> yznormals, int idx) {
        for (int i = idx; i > 0; i--) {
            Vector2d vec = yznormals.get(i);
            if ((vec.lengthSquared() > 0) && (vec.x > -.5)) {
                return vec;
            }
        }
        return yznormals.get(0);

    }

    private static Vector2d findNext(java.util.List<Vector2d> yznormals, int idx) {
        for (int i = idx + 1; i < yznormals.size(); i++) {
            Vector2d vec = yznormals.get(i);
            if ((vec.lengthSquared() > 0) && (vec.x > -.5)) {
                return vec;
            }
        }
        return yznormals.get(yznormals.size() - 1);

    }

    public static void reportZero(final java.util.List<ExtrudeEdge> checked, final String step) {
        for (ExtrudeEdge e1 : checked) {
            if (((Math.abs(e1.getP1().y) <= 0.001) && (Math.abs(e1.getP1().x) <= 0.001))
                    || ((Math.abs(e1.getP2().y) <= 0.001) && (Math.abs(e1.getP2().x) <= 0.001))) {
                LOGGER.log(Level.WARNING, "ZERO : found zero Point after step {0}  ==  {1}, {2} ", new Object[]{step, e1.getP1(), e1.getP2()});
            }

            if ((e1.getP1().y < 0.0) || (e1.getP2().y < 0.0)) {
                LOGGER.log(Level.WARNING, "NEG : found neg Y Point after step {0}  ==  {1}, {2} ", new Object[]{step, e1.getP1(), e1.getP2()});
            }
        }
    }

    private static void mergeEdgePoints(final java.util.List<ExtrudeEdge> checked, final double width) {
        boolean changes;
        do {
            changes = false;
            for (java.util.ListIterator<ExtrudeEdge> iter = checked.listIterator(); iter.hasNext();) {
                final ExtrudeEdge e1 = iter.next();
                if (!iter.hasNext()) {
                    break;
                }
                final ExtrudeEdge e2 = iter.next();

                if (e1.getP2().distance(e1.getP1()) < width * 0.1) {
                    Point2d merge = new Point2d(e1.getP2());
                    merge.add(e2.getP1());
                    merge.scale(0.5);
                    e1.getP2().set(merge);
                    e2.getP1().set(merge);
                } else {

                    final Tuple2d intersection = findIntersection(e1, e2, false);
                    if (intersection == null) {
                        if (LOGGER.isLoggable(Level.FINER)) {
                            LOGGER.log(Level.FINER, "Merge no intersection for {0} - {1}   and {2} - {3} ", new Object[]{e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2()});
                        }
                        // These are usually parrallell lines , to fix we merge e1 and e2 into a new Edge(e1.p1, e2.p2);
                        if (iter.hasNext()) {
                            iter.previous();
                            iter.remove(); // remove e2
                            e1.setP2(e2.getP2());
                            e1.setOrigin2(e2.getOrigin2());
                            changes = true;
                        } else {
                            iter.previous();
                            iter.previous(); // remove e1
                            iter.remove();
                            e2.setP1(e1.getP1());
                            e2.setOrigin1(e1.getOrigin1());
                            iter.next();
                            changes = true;
                        }
                    } else {
                        if (intersection.y < 0.01) {
                            if (LOGGER.isLoggable(Level.FINER)) {
                                LOGGER.log(Level.FINER, "Merge intersection with a NEG Y {0} - {1}   and {2} - {3} ", new Object[]{e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2()});
                            }
                            if (iter.hasNext()) {
                                iter.previous();
                                iter.remove(); // remove e2
                                e1.setP2(e2.getP2());
                                e1.setOrigin2(e2.getOrigin2());
                                changes = true;
                            } else {
                                iter.previous();
                                iter.previous(); // remove e1
                                iter.remove();
                                e2.setP1(e1.getP1());
                                e2.setOrigin1(e1.getOrigin1());
                                iter.next();
                                changes = true;
                            }
                        } else {
                            double d1 = e1.getP2().distance(new Point2d(intersection));
                            double d2 = e2.getP1().distance(new Point2d(intersection));
                            double de = e1.getP2().distance(e2.getP1());
                            if (de > 0.0000001 && (5 * de < d1) && (5 * de < d2)) {
                                if (LOGGER.isLoggable(Level.FINER)) {
                                    LOGGER.log(Level.FINER, "Merge intersection is to large  d1:{0}  d2:{1}  de:{2}   {3}={4}  --  {5} ", new Object[]{d1, d2, de, e1.getP2(), e2.getP1(), intersection});
                                }
                                Point2d merge = new Point2d(e1.getP2());
                                merge.add(e2.getP1());
                                merge.scale(0.5);
                                e1.getP2().set(merge);
                                e2.getP1().set(merge);
                            } else {
                                e1.getP2().set(intersection);
                                e2.getP1().set(intersection);
                            }
                        }
                    }
                }
                iter.previous();
            }
        } while (changes);

    }

    private static void removePinchedEdges(final java.util.List<ExtrudeEdge> checked) {
        boolean changes;
        do {
            changes = false;
            for (java.util.ListIterator<ExtrudeEdge> iter = checked.listIterator(); iter.hasNext();) {
                final ExtrudeEdge e1 = iter.next();
                if (!iter.hasNext()) {
                    break;
                }
                final ExtrudeEdge e2 = iter.next();
                if (e1.angle(e2) <= Epsilon.E) {
                    LOGGER.log(Level.WARNING, "Remove pinched Edges Angle={0}      {1} - {2}   and {3} - {4}  ", new Object[]{e1.angle(e2), e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2()});
                    if (iter.hasNext()) {
                        iter.previous();
                        iter.remove(); // remove e2
                        e1.setP2(e2.getP2());
                        changes = true;
                    } else {
                        iter.previous();
                        iter.previous(); // remove e1
                        iter.remove();
                        e2.setP1(e1.getP1());
                        iter.next();
                        changes = true;
                    }

                }
                iter.previous();
            }
        } while (changes);
    }

    private static void removeUnMergedCounterWinding(final java.util.List<ExtrudeEdge> checked) {
        boolean changes;
        int min = findMinima(checked);
        do {
            changes = false;
            for (java.util.ListIterator<ExtrudeEdge> iter = checked.listIterator(); iter.hasNext();) {
                int e1Idx = iter.nextIndex();
//                final ExtrudeEdge e1 = iter.next();
                iter.next();
                if (!iter.hasNext()) {
                    break;
                }
                int e2Idx = iter.nextIndex();
                final ExtrudeEdge e2 = iter.next();
                if (!iter.hasNext()) {
                    break;
                }
                //int e3Idx = iter.nextIndex();
//                final ExtrudeEdge e3 = iter.next();
                iter.next();
                iter.previous();
                iter.previous();
                if (e2Idx < min) {
                    if (e2.getP2().y > e2.getP1().y) {
                        iter.remove(); // remove e2
                        min--;
                        changes = true;
                    }

                } else if (e1Idx > min) {
                    if (e2.getP1().y > e2.getP2().y) {
                        iter.remove(); // remove e2
                        changes = true;
                    }
                }
            }
        } while (changes);
    }

    public static void removeCounterWinding(final java.util.List<ExtrudeEdge> checked) {
        boolean changes;
        int min = findMinima(checked);
        do {
            changes = false;
            for (java.util.ListIterator<ExtrudeEdge> iter = checked.listIterator(); iter.hasNext();) {
                int e1Idx = iter.nextIndex();
                final ExtrudeEdge e1 = iter.next();
                if (!iter.hasNext()) {
                    break;
                }
                int e2Idx = iter.nextIndex();
                final ExtrudeEdge e2 = iter.next();
                if (e2Idx < min) {
                    if ((e2.getP2().y > e1.getP1().y) || (e2.getP2().y > e1.getP2().y)) {
                        if (LOGGER.isLoggable(Level.FINER)) {
                            LOGGER.log(Level.FINER, "Remove incline prior to minima {0} - {1}   and {2} - {3} ", new Object[]{e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2()});
                        }
                        iter.previous();
                        if (iter.hasNext()) {
                            iter.remove(); // remove e2
                            e1.setP2(e2.getP2());
                        } else {
                            iter.previous(); // remove e1
                            iter.remove();
                            e2.setP1(e1.getP1());
                            iter.next();
                        }
                        min--;
                        changes = true;
                    }
                } else if (e1Idx > min) {
                    if ((e1.getP1().y > e2.getP1().y) || (e1.getP1().y > e2.getP2().y)) {
                        if (LOGGER.isLoggable(Level.FINER)) {
                            LOGGER.log(Level.FINER, "Remove decline post to minima {0} - {1}   and {2} - {3} ", new Object[]{e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2()});
                        }
                        iter.previous();
                        if (iter.hasPrevious()) {
                            iter.previous(); // remove e1
                            iter.remove();
                            e2.setP1(e1.getP1());
                            iter.next();
                        } else {
                            iter.remove(); // remove e2
                            e1.setP2(e2.getP2());
                        }
                        changes = true;
                    }
                }
                iter.previous();
            }
        } while (changes);
    }

    private static void removeZeroSizedEdges(final java.util.List<ExtrudeEdge> checked) {
        boolean changes;
        do {
            changes = false;
            for (java.util.ListIterator<ExtrudeEdge> iter = checked.listIterator(); iter.hasNext();) {
                final ExtrudeEdge e1 = iter.next();
                if (!iter.hasNext()) {
                    break;
                }
                final ExtrudeEdge e2 = iter.next();
                if (e1.getP1().equals(e1.getP2())) {
                    iter.previous();
                    iter.previous(); // remove e1
                    iter.remove();
                    e2.setP1(e1.getP1());
                    iter.next();
                    changes = true;
                } else if (e2.getP1().equals(e2.getP2())) {
                    iter.previous();
                    iter.remove(); // remove e2
                    e1.setP2(e2.getP2());
                    changes = true;
                }
                iter.previous();
            }
        } while (changes);
    }

    private static int findMinima(final java.util.List<ExtrudeEdge> checked) {
        double curmin = Double.MAX_VALUE;
        int curidx = 0;
        for (int i = 0; i < checked.size(); i++) {
            if (checked.get(i).getP1().y < curmin) {
                curmin = checked.get(i).getP1().y;
                curidx = i;
            }
        }
        return curidx;
    }

    public static java.util.List<Point3d> mirrorOnY(final java.util.List<Point3d> original_line) {
        final java.util.List<Point3d> mirrored_line = new java.util.ArrayList<>(original_line.size());
        for (Point3d point : original_line) {
            mirrored_line.add(new Point3d(-point.x, point.y, point.z));
        }
        return mirrored_line;
    }

    /**
     * Check if a point is between two planes.
     *
     * @param plane0
     * @param plane1
     * @param planeDistance the distance separating the two planes
     * @param point
     * @return
     */
    private static boolean between(Plane plane0, Plane plane1, double planeDistance, Point3d point) {
        double dist0 = Math.abs(plane0.parametricDistance(point));
        double dist1 = Math.abs(plane1.parametricDistance(point));
        return ((dist0 < planeDistance) && (dist1 < planeDistance));
    }

    public static Point3d project(Point3d point, Vector3d projection) {
        final Point3d newPoint = new Point3d(point);
        if (Math.abs(projection.x) > Epsilon.E) {
            newPoint.x = projection.x;
        }
        if (Math.abs(projection.y) > Epsilon.E) {
            newPoint.y = projection.y;
        }
        if (Math.abs(projection.z) > Epsilon.E) {
            newPoint.z = projection.z;
        }

        return newPoint;
    }

    public static double cutNotch(final java.util.List<Point3d> originalPoints, Plane plane0, Plane plane1, Vector3d projection) {
        double planeDistance = Math.abs(plane0.distance(plane1));
        if (planeDistance < Epsilon.E) {
            LOGGER.log(Level.WARNING, "The two planes {0}  &  {1}  either intersects or they conincide", new Object[]{plane0, plane1});
            return Double.NaN;
        }
        // ensure the normals if the two planes pount to each other  , ie to the middle of the two planes
        if (plane0.distance(plane1) > 0 && plane1.distance(plane0) < 0) {
            LOGGER.log(Level.WARNING, "Flip plane0 {0}  & Flip plane1 {1} ", new Object[]{plane0, plane1});
            return cutNotch(originalPoints, plane0.flip(), plane1.flip(), projection);
        } else if (plane0.distance(plane1) > 0 && plane1.distance(plane0) > 0) {
            LOGGER.log(Level.WARNING, "Flip plane0 {0} not plane1 {1}", new Object[]{plane0, plane1});
            return cutNotch(originalPoints, plane0.flip(), plane1, projection);
        } else if (plane0.distance(plane1) < 0 && plane1.distance(plane0) < 0) {
            LOGGER.log(Level.WARNING, "Flip plane1 {0} not plane0 {1}", new Object[]{plane1, plane0});
            return cutNotch(originalPoints, plane0, plane1.flip(), projection);
        }
        // if plane0 is further from the origin than plane1 , we spwap planes
        if (Math.abs(plane0.parametricDistance(new Point3d(0, 0, 0))) > Math.abs(plane1.parametricDistance(new Point3d(0, 0, 0)))) {
            LOGGER.log(Level.WARNING, "Swap AND flip plane0 {0} and plane1 {1}", new Object[]{plane0, plane1});
            return cutNotch(originalPoints, plane0.flip(), plane1.flip(), projection);
        }

//        final boolean reverse = !order(plane0, originalPoints);
//        if (reverse) {
//            java.util.Collections.reverse(originalPoints);
//        }
        Point3d last_vert = originalPoints.get(0);
        Point3d last_co = new Point3d(last_vert);
        double last_d0 = plane0.parametricDistance(last_co);
        double last_d1 = plane1.parametricDistance(last_co);
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.INFO, "Start 0:  startPoint {0} last_d0:{3}, last_d1:{4} isBetween:{5} Planes\n\t\tplane0:{1}\n\t\tplane1:{2} ", new Object[]{last_co, plane0, plane1, last_d0, last_d1, between(plane0, plane1, planeDistance, last_co)});
        }
        if (between(plane0, plane1, planeDistance, last_co)) {
            last_vert.set(project(last_vert, projection));
        }
        int i = 1;
        boolean processed = false;
        while (i < originalPoints.size()) {
            Point3d curr_vert = originalPoints.get(i);
            Point3d curr_co = new Point3d(curr_vert);
            double curr_d0 = plane0.parametricDistance(curr_vert);
            double curr_d1 = plane1.parametricDistance(curr_vert);

            boolean last_between = between(plane0, plane1, planeDistance, last_co);
            boolean curr_between = between(plane0, plane1, planeDistance, curr_co);
            boolean intersect0 = plane0.isIntersected(last_co, curr_co, true);
            boolean intersect1 = plane1.isIntersected(last_co, curr_co, true);

            if (last_between && curr_between) {
                //   | . . |             
                //   Case 0 :   Both Points are between the two Planes
                final int lastidx = i - 1;
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 0: {12} - planeDistance {13} last_between:{0}, curr_between:{1}, intersect0:{2}, intersect1:{3}  last_d0:{4}, last_d1:{5} , curr_d0:{6}, curr_d1:{7}\n\t\tlast:{8} - curr:{9}\n\t\tplane0:{10}\n\t\tplane1:{11}", new Object[]{last_between, curr_between, intersect0, intersect1, last_d0, last_d1, curr_d0, curr_d1, last_co, curr_co, plane0, plane1, i, planeDistance});
                }
                if (Math.abs(curr_d1) > Epsilon.E * 3) {
                    // Modify curr_vert
                    if (LOGGER.isLoggable(Level.FINEST)) {
                        LOGGER.log(Level.INFO, "Case 0:1 Setting projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(curr_vert, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                    }
                    curr_vert.set(project(curr_vert, projection));
                } else {
                    //   | .   !  
                    //   Case 0.1:  curr_vert is on the line
                    if (LOGGER.isLoggable(Level.FINEST)) {
                        LOGGER.log(Level.INFO, "Case 0:1 Adding projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(curr_co, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                    }
                    originalPoints.add(i, project(curr_co, projection));
                    i++;
                }
                if (LOGGER.isLoggable(Level.FINEST)) {
                    StringBuilder sb = new StringBuilder();
                    for (int j = lastidx; j < Math.min(i + 1, originalPoints.size()); j++) {
                        sb.append("\tidx:").append(j).append("  ").append(originalPoints.get(j)).append("\n");
                    }
                    LOGGER.log(Level.INFO, "Case 0: from {0} dump\n{1}", new Object[]{lastidx, sb});
                }
                processed = true;
            } else if (intersect0 && intersect1) {
                //   . | | .
                // Case 1 :   Both Planes are between the two Points
                // We need to insert the entire notch , four points
                final int lastidx = i - 1;
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 1: {12} last_between:{0}, curr_between:{1}, intersect0:{2}, intersect1:{3}  last_d0:{4}, last_d1:{5} , curr_d0:{6}, curr_d1:{7}\n\t\tlast:{8} - curr:{9}\n\t\tplane0:{10}\n\t\tplane1:{11}", new Object[]{last_between, curr_between, intersect0, intersect1, last_d0, last_d1, curr_d0, curr_d1, last_co, curr_co, plane0, plane1, i});
                }
                final Point3d intersectp0 = new Point3d(plane0.intersectSegment(last_co, curr_co, true));
                if (Math.abs(last_d0) > Epsilon.E * 3) {
                    if (LOGGER.isLoggable(Level.FINEST)) {
                        LOGGER.log(Level.INFO, "Case 1:1 Adding intersect point at {0}  P: {1}  between {2} and {3}", new Object[]{i, intersectp0, originalPoints.get(i - 1), originalPoints.get(i)});
                    }
                    originalPoints.add(i, intersectp0);
                    i++;
                }   // else  we have   !   | .    last is on the plane0 so we dont need to insert the intersection
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 1:2 Adding projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(intersectp0, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                }
                originalPoints.add(i, project(intersectp0, projection));
                i++;
                final Point3d intersectp1 = new Point3d(plane1.intersectSegment(last_co, curr_co, true));
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 1:3 Adding projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(intersectp1, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                }
                originalPoints.add(i, project(intersectp1, projection));
                i++;
                if (Math.abs(curr_d1) > Epsilon.E * 3) {
                    if (LOGGER.isLoggable(Level.FINEST)) {
                        LOGGER.log(Level.INFO, "Case 1:4 Adding intersect point at {0}  P: {1}  between {2} and {3}", new Object[]{i, intersectp1, originalPoints.get(i - 1), originalPoints.get(i)});
                    }
                    originalPoints.add(i, intersectp1);
                    i++;
                }   // else  we have   . |   !   curr is on plane1 se we dont need to insert the intersecrion 
                if (LOGGER.isLoggable(Level.FINEST)) {
                    StringBuilder sb = new StringBuilder();
                    for (int j = lastidx; j < Math.min(i + 1, originalPoints.size()); j++) {
                        sb.append("\tidx:").append(j).append("  ").append(originalPoints.get(j)).append("\n");
                    }
                    LOGGER.log(Level.INFO, "Case 1: from {0} dump\n{1}", new Object[]{lastidx, sb});

                }
                processed = true;
            } else if (intersect0) {
                //   . | . |
                // Case 2 :   Plane0 intercect the line segement between last and curr
                final int lastidx = i - 1;
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 2: {12} last_between:{0}, curr_between:{1}, intersect0:{2}, intersect1:{3}  last_d0:{4}, last_d1:{5} , curr_d0:{6}, curr_d1:{7}\n\t\tlast:{8} - curr:{9}\n\t\tplane0:{10}\n\t\tplane1:{11}", new Object[]{last_between, curr_between, intersect0, intersect1, last_d0, last_d1, curr_d0, curr_d1, last_co, curr_co, plane0, plane1, i});
                }
                final Point3d intersectp0 = new Point3d(plane0.intersectSegment(last_co, curr_co, true));
                if ((Math.abs(last_d0) > Epsilon.E * 3) || (Math.abs(curr_d0) > Epsilon.E * 3)) {
                    if (LOGGER.isLoggable(Level.FINEST)) {
                        LOGGER.log(Level.INFO, "Case 2:1 Adding intersect point at {0}  P: {1}  between {2} and {3}", new Object[]{i, intersectp0, originalPoints.get(i - 1), originalPoints.get(i)});
                    }
                    originalPoints.add(i, intersectp0);
                    i++;
                }   // else  we have   !  . |   OR   .   !   |   last is on the plane0 or curr is on plane0  so we dont need to insert the intersection
                // No matter the border case, we must add the projected point
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 2:2 Adding projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(intersectp0, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                }
                originalPoints.add(i, project(intersectp0, projection));
                i++;
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 2:3 Setting projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(curr_vert, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                }
                curr_vert.set(project(curr_vert, projection));
                if (LOGGER.isLoggable(Level.FINEST)) {
                    StringBuilder sb = new StringBuilder();
                    for (int j = lastidx; j < Math.min(i + 1, originalPoints.size()); j++) {
                        sb.append("\tidx:").append(j).append("  ").append(originalPoints.get(j)).append("\n");
                    }
                    LOGGER.log(Level.INFO, "Case 2: from {0} dump\n{1}", new Object[]{lastidx, sb});

                }
                processed = true;
            } else if (intersect1) {
                //   | . | . 
                // Case 3 :   Plane1 intercect the line segement between last and curr
                final int lastidx = i - 1;
//                StringBuilder sb0 = new StringBuilder();
//                for (int j = lastidx; j < Math.min(i + 1, originalPoints.size()); j++) {
//                    sb0.append("\tidx:").append(j).append("  ").append(originalPoints.get(j)).append("\n");
//                }
//                LOGGER.log(Level.INFO, "Case 3: from {0} predump\n{1}", new Object[]{lastidx, sb0});
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 3: {12} last_between:{0}, curr_between:{1}, intersect0:{2}, intersect1:{3}  last_d0:{4}, last_d1:{5} , curr_d0:{6}, curr_d1:{7}\n\t\tlast:{8} - curr:{9}\n\t\tplane0:{10}\n\t\tplane1:{11}", new Object[]{last_between, curr_between, intersect0, intersect1, last_d0, last_d1, curr_d0, curr_d1, last_co, curr_co, plane0, plane1, i});
                }
                // No matter the border case, we must add the projected point
                final Point3d intersectp1 = new Point3d(plane1.intersectSegment(last_co, curr_co, true));
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 3:1 Adding projection point at {0}  P: {1}  between {2} and {3}", new Object[]{i, project(intersectp1, projection), originalPoints.get(i - 1), originalPoints.get(i)});
                }
                originalPoints.add(i, project(intersectp1, projection));
                i++;
                if ((Math.abs(last_d1) > Epsilon.E * 3) || (Math.abs(curr_d1) > Epsilon.E * 3)) {
                    if (LOGGER.isLoggable(Level.FINEST)) {
                        LOGGER.log(Level.INFO, "Case 3:2 Adding intersect point at {0}  P: {1}  between {2} and {3}", new Object[]{i, intersectp1, originalPoints.get(i - 1), originalPoints.get(i)});
                    }
                    originalPoints.add(i, intersectp1);
                    i++;
                }   // else  we have   |   !  .    OR   |   .   !     last is on the plane1 or curr is on plane1  so we dont need to insert the intersection
                if (LOGGER.isLoggable(Level.FINEST)) {
                    StringBuilder sb = new StringBuilder();
                    for (int j = lastidx; j < Math.min(i + 1, originalPoints.size()); j++) {
                        sb.append("\tidx:").append(j).append("  ").append(originalPoints.get(j)).append("\n");
                    }
                    LOGGER.log(Level.INFO, "Case 3: from {0} dump\n{1}", new Object[]{lastidx, sb});

                }
                processed = true;
            } else {
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.INFO, "Case 4: {12} - planeDistance {13}  last_between:{0}, curr_between:{1}, intersect0:{2}, intersect1:{3}  last_d0:{4}, last_d1:{5} , curr_d0:{6}, curr_d1:{7}\n\t\tlast:{8} - curr:{9}\n\t\tplane0:{10}\n\t\tplane1:{11}", new Object[]{last_between, curr_between, intersect0, intersect1, last_d0, last_d1, curr_d0, curr_d1, last_co, curr_co, plane0, plane1, i, planeDistance});
                }
            }
            i++;
            last_co = curr_co;
            last_d0 = curr_d0;
            last_d1 = curr_d1;
        }
//        if (reverse) {
//            java.util.Collections.reverse(originalPoints);
//        }

        if (processed) {
            if (Math.abs(projection.x) > Epsilon.E) {
                return projection.x;
            }
            if (Math.abs(projection.y) > Epsilon.E) {
                return projection.y;
            }
            if (Math.abs(projection.z) > Epsilon.E) {
                return projection.z;
            } else {
                return 0;
            }
        } else {
            return Double.NaN;
        }
//        return originalPoints;
    }

    /* ==========================================================================
     Notch  utilities
     =========================================================================== */
    public static double xDelta(final java.util.List<? extends Tuple3d> originalPoints, final Transform3dTo2d transform) {
        double delta = 0;
        for (int i = 1; i < originalPoints.size() - 1; i++) {
            delta += transform.getX(originalPoints.get(i - 1)) - transform.getX(originalPoints.get(i));
        }
        return delta;
    }


    /* ==========================================================================
     Extrusion  utilities
     =========================================================================== */
    private static boolean removeCollisions(final java.util.List<ExtrudeEdge> checked, ExtrudeEdge e2, final int maxCollisionDepth) {

        for (int i = checked.size() - 2; i > 0 && i > checked.size() - maxCollisionDepth; i--) {
            final ExtrudeEdge e1 = checked.get(i);
            Tuple2d intersection = findIntersection(e1, e2, true);
            if (intersection != null) {
                LOGGER.log(Level.FINEST, "Found collision  for  {0} - {1} and {2} - {3}  -- collision {4}  at {5}  size {6} ", new Object[]{e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2(), intersection, i, checked.size()});
                if (LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.log(Level.WARNING, "Found collision  for  {0} - {1} and {2} - {3}  -- collision {4}  at {5}  size {6} ", new Object[]{e1.getP1(), e1.getP2(), e2.getP1(), e2.getP2(), intersection, i, checked.size()});
                    try {
                        SvgFile keelCenter = new SvgFile("collision-err" + (nonintersects) + ".svg");
                        keelCenter.export_extrude_error(1.0 / 144.0 / 10, 10, 360, e1, e2);
                        keelCenter.close();
                    } catch (java.io.IOException ex) {
                        LOGGER.log(Level.FINER, "Failed to dump collision-err" + (nonintersects) + ".svg", ex);
                    }
                }
                for (int j = checked.size() - 1; j > i; j--) {
                    checked.remove(j);
//                    ExtrudeEdge rem = checked.remove(j);
//                    LOGGER.log(Level.WARNING, "remove collision loop {0} - {1} ", new Object[]{j, rem});
                }
                e1.getP2().set(intersection);
                e2.getP1().set(intersection);
                if (LOGGER.isLoggable(Level.FINEST)) {
                    try {
                        SvgFile keelCenter = new SvgFile("merge-err" + (nonintersects++) + ".svg");
                        keelCenter.export_extrude_error(1.0 / 144.0 / 10, 10, 360, e1, e2);
                        keelCenter.close();
                    } catch (java.io.IOException ex) {
                        LOGGER.log(Level.FINER, "Failed to dump merge-err" + (nonintersects - 1) + ".svg", ex);
                    }
                }
                return true;
            }
        }
        return false;
    }

    private static ExtrudeEdge project(final Transform3dTo2d transform, final double width, Vector2d direction, final Tuple3d p1, final Tuple3d p2) {
        final Point2d proj = projectNormal(direction, width);
        Point2d p12d = transform.get2dCoord(p1);
        Point2d p22d = transform.get2dCoord(p2);
        p12d.add(proj);
        p22d.add(proj);
        return new ExtrudeEdge(p12d, p22d, transform.get2dCoord(p1), transform.get2dCoord(p2));
    }

    private static Point2d projectNormal(Vector2d n, double width) {
        if (n.length() > 0.000001) {
            n.scale(width);
            return new Point2d(-n.y, n.x);
        } else {
            return new Point2d(0.0, 1.0);
        }
    }

    private static double cross(Tuple2d p1, Tuple2d p2) {
        return p1.x * p2.y - p1.y * p2.x;
    }

    private static Tuple2d findIntersection(ExtrudeEdge e1, ExtrudeEdge e2, boolean collisionOnly) {
        final Point2d v1 = new Point2d(e1.getP2());
        v1.sub(e1.getP1());
        final Point2d v2 = new Point2d(e2.getP2());
        v2.sub(e2.getP1());

        final double denom = cross(v1, v2);
        if (denom == 0) {
            return null; // Collinear
        }
        boolean denomPositive = denom > 0;

        final Point2d v3 = new Point2d(e1.getP1());
        v3.sub(e2.getP1());

        final double s_numer = cross(v1, v3);
        if (collisionOnly && (s_numer < 0) == denomPositive) {
            return null; // No collision
        }
        final double t_numer = cross(v2, v3);
        if (collisionOnly && (t_numer < 0) == denomPositive) {
            return null; // No collision
        }
        if (collisionOnly && (((s_numer > denom) == denomPositive) || ((t_numer > denom) == denomPositive))) {
            return null; // No collision
        }
        // Collision detected
        final double t = t_numer / denom;
        final Point2d result = new Point2d(v1);
        result.scaleAdd(t, e1.getP1());
        return result;
    }

    private static int nonintersects = 0;
}
