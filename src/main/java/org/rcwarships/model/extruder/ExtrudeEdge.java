/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.model.extruder;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

/**
 *
 * @author viljoenp
 */
public class ExtrudeEdge  implements Cloneable{

    public ExtrudeEdge() {
    }

    public ExtrudeEdge(final Point2d p1, final Point2d p2, Point2d origin1, Point2d origin2) {
        this();
        this.p1 = p1;
        this.p2 = p2;
        this.origin1 = origin1;
        this.origin2 = origin2;
    }

    public Point2d getP1() {
        return p1;
    }

    public void setP1(Point2d p1) {
        this.p1 = p1;
    }

    public Point2d getP2() {
        return p2;
    }

    public void setP2(Point2d p2) {
        this.p2 = p2;
    }

    public Point2d getOrigin1() {
        return origin1;
    }

    public void setOrigin1(Point2d origin1) {
        this.origin1 = origin1;
    }

    public Point2d getOrigin2() {
        return origin2;
    }

    public void setOrigin2(Point2d origin2) {
        this.origin2 = origin2;
    }

    private Point2d p1;
    private Point2d p2;
    private Point2d origin1;
    private Point2d origin2;

    @Override
    public ExtrudeEdge clone() {
        return new ExtrudeEdge(new Point2d(p1), new Point2d(p2), origin1, origin2);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.p1 != null ? this.p1.hashCode() : 0);
        hash = 43 * hash + (this.p2 != null ? this.p2.hashCode() : 0);
        return hash;
    }

    public double angle(ExtrudeEdge e2) {
        // p1 - p2
        final Vector2d p2_p1 = new Vector2d(p1.x - p2.x, p1.y - p2.y);
        // p1 - e2.p2
        final Vector2d p2_p3 = new Vector2d(e2.p2.x - p2.x, e2.p2.y - p2.y);
        // p2 - e2.p2
//        final Vector2d v1_2 = new Vector2d(p1.x - e2.p2.x, p1.y - e2.p2.y);
//        Vector2d e1 = new Vector2d(p1);

        return p2_p1.angle(p2_p3);
    }

    public double angle1(ExtrudeEdge e2) {
        // p1 - p2
        final Vector2d p1_p2 = new Vector2d(p2.x - p1.x, p2.y - p1.y);
        // p1 - e2.p2
        final Vector2d p1_p3 = new Vector2d(e2.p2.x - p1.x, e2.p2.y - p1.y);
        // p2 - e2.p2
//        final Vector2d v1_2 = new Vector2d(p1.x - e2.p2.x, p1.y - e2.p2.y);
//        Vector2d e1 = new Vector2d(p1);

        return p1_p2.angle(p1_p3);
    }

    public double angle2(ExtrudeEdge e2) {
        // p1 - p2
        final Vector2d p3_p2 = new Vector2d(p2.x - e2.p2.x, p2.y - e2.p2.y);
        // p1 - e2.p2
        final Vector2d p3_p1 = new Vector2d(p1.x - e2.p2.x, p1.y - e2.p2.y);
        // p2 - e2.p2
//        final Vector2d v1_2 = new Vector2d(p1.x - e2.p2.x, p1.y - e2.p2.y);
//        Vector2d e1 = new Vector2d(p1);

        return p3_p2.angle(p3_p1);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExtrudeEdge other = (ExtrudeEdge) obj;
        if (this.p1 != other.p1 && (this.p1 == null || !this.p1.equals(other.p1))) {
            return false;
        }
        return this.p2 == other.p2 || (this.p2 != null && this.p2.equals(other.p2));
    }

}
