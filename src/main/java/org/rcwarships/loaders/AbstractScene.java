/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.loaders;

import org.rcwarships.loaders.Scene;
import org.rcwarships.model.Mesh;

/**
 *
 * @author viljoenp
 */
public class AbstractScene implements Scene {

    private final java.util.Map<String, Mesh> namedObjects = new java.util.HashMap<>();

    /**
     * Adds the given String/Object pair to the table of named objects.
     *
     * @param name
     * @param mesh
     */
    @Override
    public void addNamedObject(String name, Mesh mesh) {
        if (namedObjects.get(name) == null) {
            namedObjects.put(name, mesh);
        } else {
            // key already exists - append a unique integer to end of name
            int nameIndex = 1;
            boolean done = false;
            while (!done) {
                // Iterate starting from "[1]" until we find a unique key
                String tempName = name + "[" + nameIndex + "]";
                if (namedObjects.get(tempName) == null) {
                    namedObjects.put(tempName, mesh);
                    done = true;
                }
                nameIndex++;
            }
        }
    }

    /**
     * This method returns a Hashtable which contains a list of all named
     * objects in the file and their associated scene graph objects. The naming
     * scheme for file objects is file-type dependent, but may include such
     * names as the DEF names of Vrml or filenames of subjects (as in Lightwave
     * 3D).
     *
     * @return an unmodifiable map of all the internal objects
     */
    @Override
    public java.util.Map<String, Mesh> getNamedObjects() {
        return java.util.Collections.unmodifiableMap(namedObjects);
    }
}
