/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.loaders.obj;

/**
 * NOTE this is a modified version of the original file to load the model in to
 * a custom org.rcwarships.model.Mesh model.
 *
 * @author viljoenp
 */
public class ParsingErrorException extends Exception {

    public ParsingErrorException(Throwable thrwbl) {
        super(thrwbl);
    }

    public ParsingErrorException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public ParsingErrorException(String string) {
        super(string);
    }

    public ParsingErrorException() {
        super();
    }

    @Override
    public ParsingErrorException initCause(Throwable thrwbl) {
        return (ParsingErrorException) super.initCause(thrwbl);
    }
    private static final long serialVersionUID = 23243232323L;
}
