/*
 * $RCSfile: SceneBase.java,v $
 *
 * Copyright (c) 2007 Sun Microsystems, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistribution of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistribution in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in
 *   the documentation and/or other materials provided with the
 *   distribution.
 *
 * Neither the name of Sun Microsystems, Inc. or the names of
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY
 * EXCLUDED. SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR
 * ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
 * CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND
 * REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR
 * INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 *
 * You acknowledge that this software is not designed, licensed or
 * intended for use in the design, construction, operation or
 * maintenance of any nuclear facility.
 *
 * $Revision: 1.4 $
 * $Date: 2007/02/09 17:20:06 $
 * $State: Exp $
 */
package org.rcwarships.loaders.obj;

import org.rcwarships.loaders.AbstractScene;
import javax.media.j3d.Background;
import javax.media.j3d.Behavior;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Fog;
import javax.media.j3d.Light;
import javax.media.j3d.Sound;
import javax.media.j3d.TransformGroup;
import org.rcwarships.model.Mesh;

/**
 * NOTE this is a modified version of the original file to load the model in to
 * a custom org.rcwarships.model.Mesh model.
 *
 * This class implements the Scene interface and extends it to incorporate
 * utilities that could be used by loaders. There should be little need for
 * future loaders to subclass this, or to implement Scene directly, as the
 * functionality of a SceneBase is fairly straightforward. This class is
 * responsible for both the storage and retrieval of data from the Scene. The
 * storage methods (used only by Loader writers) are all of the add* routines.
 * The retrieval methods (used primarily by Loader users) are all of the get*
 * routines.
 */
public class ObjectFileScene extends AbstractScene {

    BranchGroup sceneGroup = null;
    BranchGroup behaviorGroup = null;
    String description = null;
    java.util.List<TransformGroup> viewVector = new java.util.ArrayList<>();
    java.util.List<Float> hfovVector = new java.util.ArrayList<>();
    java.util.List<Behavior> behaviorVector = new java.util.ArrayList<>();
    java.util.List<Light> lightVector = new java.util.ArrayList<>();
    java.util.List<Fog> fogVector = new java.util.ArrayList<>();
    java.util.List<Background> backgroundVector = new java.util.ArrayList<>();
    java.util.List<Sound> soundVector = new java.util.ArrayList<>();

    // Add methods
    /**
     * Sets the sceneGroup to be the group that is passed in.
     */
    public void setSceneGroup(BranchGroup scene) {
        sceneGroup = scene;
    }

    /**
     * Adds the given group to the list of view groups.
     */
    public void addViewGroup(TransformGroup tg) {
        viewVector.add(tg);
    }

    /**
     * Adds the given field of view value to the list of field of view values.
     */
    public void addHorizontalFOV(float hfov) {
        hfovVector.add(new Float(hfov));
    }

    /**
     * Adds the given behavior to a list of behaviors
     */
    public void addBehaviorNode(Behavior b) {
        behaviorVector.add(b);
    }

    /**
     * Adds the given Light node to the list of lights.
     */
    public void addLightNode(Light light) {
        lightVector.add(light);
    }

    /**
     * Adds the given Background node to the list of backgrounds.
     */
    public void addBackgroundNode(Background background) {
        backgroundVector.add(background);
    }

    /**
     * Adds the given Sound node to the list of sounds.
     */
    public void addSoundNode(Sound sound) {
        soundVector.add(sound);
    }

    /**
     * Adds the given Fog node to the list of fog nodes.
     */
    public void addFogNode(Fog fog) {
        fogVector.add(fog);
    }

    /**
     * Sets the text description of the scene to the passed in String.
     */
    public void addDescription(String descriptionString) {
        description = descriptionString;
    }

    /**
     * This method returns the BranchGroup containing the overall scene loaded
     * by the loader.
     */
    public BranchGroup getSceneGroup() {
        return sceneGroup;
    }

    /**
     * This method returns an array of all View Groups defined in the file. A
     * View Group is defined as a TransformGroup which contains a ViewPlatform.
     * The TransformGroup holds the position/orientation information for the
     * given ViewPlatform and the ViewPlatform holds an view-specific
     * information, such as Field of View.
     */
    public TransformGroup[] getViewGroups() {
        if (viewVector.isEmpty()) {
            return null;
        }
        return viewVector.toArray(new TransformGroup[]{});
    }

    /**
     * This method returns an array of floats that contains the horizontal field
     * of view values for each corresponding entry in the array of view groups
     * returned by the method getViewGroups.
     */
    public float[] getHorizontalFOVs() {
        if (hfovVector.isEmpty()) {
            return null;
        }

        int arraySize = hfovVector.size();
        float[] hfovs = new float[arraySize];
        Float[] tmpFovs = hfovVector.toArray(new Float[]{});

        // copy to array of floats and delete Floats
        for (int i = 0; i < arraySize; i++) {
            hfovs[i] = tmpFovs[i].floatValue();
            tmpFovs[i] = null;
        }
        return hfovs;
    }

    /**
     * This method returns an array of all Lights defined in the file.
     */
    public Light[] getLightNodes() {
        if (lightVector.isEmpty()) {
            return null;
        }
        return lightVector.toArray(new Light[]{});
    }

    /**
     * This method returns an array of all Background nodes defined in the file.
     */
    public Background[] getBackgroundNodes() {
        if (backgroundVector.isEmpty()) {
            return null;
        }
        return backgroundVector.toArray(new Background[]{});
    }

    /**
     * This method returns an array of all Fog nodes defined in the file.
     */
    public Fog[] getFogNodes() {
        if (fogVector.isEmpty()) {
            return null;
        }
        return fogVector.toArray(new Fog[]{});
    }

    /**
     * This method returns a group containing all of the Behavior nodes in the
     * scene.
     */
    public Behavior[] getBehaviorNodes() {
        if (behaviorVector.isEmpty()) {
            return null;
        }
        return behaviorVector.toArray(new Behavior[]{});
    }

    /**
     * This method returns an array of all of the Sound nodes defined in the
     * file.
     */
    public Sound[] getSoundNodes() {
        if (soundVector.isEmpty()) {
            return null;
        }
        return soundVector.toArray(new Sound[]{});
    }

    /**
     * This method returns the text description of the file. If no such
     * description exists, this method should return null.
     */
    public String getDescription() {
        return description;
    }
}
