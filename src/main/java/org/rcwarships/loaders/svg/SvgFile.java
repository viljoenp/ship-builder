/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.loaders.svg;

import java.io.File;
import java.io.IOException;
import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;
import org.rcwarships.model.Mesh;
import org.rcwarships.model.ModelUtils;
import org.rcwarships.model.Vertex;
import org.rcwarships.model.extruder.ExtrudeEdge;

/**
 * Used for debugging.
 *
 * @author viljoenp
 */
public class SvgFile {

    private final File file;
    private final java.io.FileWriter fileWriter;

    public SvgFile(String filename) throws IOException {
        file = new File(filename);
        fileWriter = new java.io.FileWriter(file);
        start();
    }

    private void start() throws IOException {
        fileWriter.write("<?xml version=\"1.0\"?>");
        fileWriter.write("<svg width=\"200cm\" height=\"260cm\" viewBox=\"0 0 2000 2600\" xmlns=\"http://www.w3.org/2000/svg\" version=\"1.2\" baseProfile=\"tiny\">\n");
        fileWriter.write("    <desc>Export from blender</desc>\n");
        fileWriter.write("    <!-- Object dump -->\n");
        fileWriter.write("    <rect x=\"1\" y=\"1\" width=\"1998\" height=\"2598\" fill=\"none\" stroke=\"blue\" stroke-width=\"2\" />\n");

    }

    public void export_yz_svg(Mesh meshOBJ, double scale, int offsetx, int offsety, boolean mirror_y, boolean mirror_z) throws IOException {
        fileWriter.write("<g xml:id=\"" + meshOBJ.name + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + meshOBJ.name + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_YZ(meshOBJ, scale, scale) + "\" />\n");
        if (mirror_y) {
            fileWriter.write("<polygon points=\"" + coords_YZ(meshOBJ, -scale, scale) + "\" />\n");
        }
        if (mirror_z) {
            fileWriter.write("<polygon points=\"" + coords_YZ(meshOBJ, scale, -scale) + "\" />\n");
        }
        if ((mirror_y) && (mirror_z)) {
            fileWriter.write("<polygon points=\"" + coords_YZ(meshOBJ, -scale, -scale) + "\" />\n");
        }
        fileWriter.write("<polygon points=\"" + coords_YZ(meshOBJ, scale, scale) + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_yx_svg(Mesh meshOBJ, final String poligonName, double scale, int offsetx, int offsety, java.util.List<Vertex>... points) throws IOException {
        fileWriter.write("<g xml:id=\"" + poligonName + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + poligonName + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_YX(meshOBJ, scale, scale, points) + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_yz_svg(final String poligonName, String color, int offsetx, int offsety, java.util.List<Point3d>... points) throws IOException {
        fileWriter.write("<g xml:id=\"" + poligonName + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"" + color + "\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + poligonName + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_YZ(points) + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_yz_svg(Mesh meshOBJ, final String poligonName, double scale, int offsetx, int offsety, java.util.List<Vertex>... points) throws IOException {
        fileWriter.write("<g xml:id=\"" + poligonName + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + poligonName + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_YZ(meshOBJ, scale, scale, points) + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_xz_svg(final String poligonName, String color, int offsetx, int offsety, java.util.List<Point3d>... points) throws IOException {
        fileWriter.write("<g xml:id=\"" + poligonName + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"" + color + "\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + poligonName + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_XZ(points) + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_yx_svg(final String poligonName, String color, int offsetx, int offsety, java.util.List<Point3d>... points) throws IOException {
        fileWriter.write("<g xml:id=\"" + poligonName + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"" + color + "\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + poligonName + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_YX(points) + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_yx_svg(Mesh meshOBJ, double scale, int offsetx, int offsety, boolean mirror_y, boolean mirror_x) throws IOException {
        fileWriter.write("<g xml:id=\"" + meshOBJ.name + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + meshOBJ.name + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_YX(meshOBJ, scale, scale) + "\" />\n");
        if (mirror_y) {
            fileWriter.write("<polygon points=\"" + coords_YX(meshOBJ, -scale, scale) + "\" />\n");
        }
        if (mirror_x) {
            fileWriter.write("<polygon points=\"" + coords_YX(meshOBJ, scale, -scale) + "\" />\n");
        }
        if ((mirror_y) && (mirror_x)) {
            fileWriter.write("<polygon points=\"" + coords_YX(meshOBJ, -scale, -scale) + "\" />\n");
        }
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

    public void export_xz_svg(Mesh meshOBJ, double scale, int offsetx, int offsety, boolean mirror_x, boolean mirror_z) throws IOException {
        fileWriter.write("<g xml:id=\"" + meshOBJ.name + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + meshOBJ.name + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\"" + coords_XZ(meshOBJ, scale, scale) + "\" />\n");
        if (mirror_x) {
            fileWriter.write("<polygon points=\"" + coords_XZ(meshOBJ, -scale, scale) + "\" />\n");
        }
        if (mirror_z) {
            fileWriter.write("<polygon points=\"" + coords_XZ(meshOBJ, scale, -scale) + "\" />\n");
        }
        if ((mirror_x) && (mirror_z)) {
            fileWriter.write("<polygon points=\"" + coords_XZ(meshOBJ, -scale, -scale) + "\" />\n");
        }
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }

//
//    private String coords_XY(Mesh meshOBJ, double scaleX,double scaleY) {
//        StringBuilder POINTSSTR = new StringBuilder();
//        java.util.List<Vertex> sorted_list = meshOBJ.traverseEdges(meshOBJ.edges,meshOBJ.verts.get(0),meshOBJ.verts.get(0));
//        for(Vertex lastVertex : sorted_list) {
//            if (POINTSSTR.length() > 0) {
//                POINTSSTR.append(String.format(" %.3f,%.3f", model_m_to_scale_mm(lastVertex.coord.x, scaleX), model_m_to_scale_mm(lastVertex.coord.y, scaleY)));
//            } else {
//                POINTSSTR.append(String.format("%.3f,%.3f", model_m_to_scale_mm(lastVertex.coord.x, scaleX), model_m_to_scale_mm(lastVertex.coord.y, scaleY)));
//            }
//        }
//        return POINTSSTR.toString();
//    }
    private String coords_YX(Mesh meshOBJ, double scaleY, double scaleX, java.util.List<Vertex>... points) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<Vertex> sorted_list = ModelUtils.makeList(meshOBJ, points);
        for (Vertex lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.coord.y, lastVertex.coord.x));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.coord.y, lastVertex.coord.x));
            }
        }
        return POINTSSTR.toString();
    }

    private String coords_YX(Mesh meshOBJ, double scaleY, double scaleX) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<Vertex> sorted_list = meshOBJ.traverseEdges(meshOBJ.edges, meshOBJ.verts.get(0), meshOBJ.verts.get(0));
        for (Vertex lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.coord.y, lastVertex.coord.x));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.coord.y, lastVertex.coord.x));
            }
        }
        return POINTSSTR.toString();
    }

    private String coords_XZ(Mesh meshOBJ, double scaleX, double scaleZ) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<Vertex> sorted_list = meshOBJ.traverseEdges(meshOBJ.edges, meshOBJ.verts.get(0), meshOBJ.verts.get(0));
        for (Vertex lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.coord.x, lastVertex.coord.z));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.coord.x, lastVertex.coord.z));
            }
        }
        return POINTSSTR.toString();
    }

    private String coords_YZ(Mesh meshOBJ, double scaleY, double scaleZ) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<Vertex> sorted_list = meshOBJ.traverseEdges(meshOBJ.edges, meshOBJ.verts.get(0), meshOBJ.verts.get(0));
        for (Vertex lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.coord.y, lastVertex.coord.z));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.coord.y, lastVertex.coord.z));
            }
        }
        return POINTSSTR.toString();
    }

    public <F extends Tuple3d> java.util.List<F> makeList(java.util.List<F>... points) {
        int size = 0;
        for (java.util.List<F> list : points) {
            size += list.size();
        }
        final java.util.List<F> list = new java.util.ArrayList<>(size);
        for (java.util.List<F> pointList : points) {
            list.addAll(pointList);
        }
        return list;
    }

    private <F extends Tuple3d> String coords_XZ(java.util.List<F>... points) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<F> sorted_list = makeList(points);
        for (F lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.x, lastVertex.z));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.x, lastVertex.z));
            }
        }
        return POINTSSTR.toString();
    }

    private <F extends Tuple3d> String coords_YZ(java.util.List<F>... points) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<F> sorted_list = makeList(points);
        for (F lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.y, lastVertex.z));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.y, lastVertex.z));
            }
        }
        return POINTSSTR.toString();
    }

    private <F extends Tuple3d> String coords_YX(java.util.List<F>... points) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<F> sorted_list = makeList(points);
        for (F lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.y, lastVertex.x));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.y, lastVertex.x));
            }
        }
        return POINTSSTR.toString();
    }

    private String coords_YZ(Mesh meshOBJ, double scaleY, double scaleZ, java.util.List<Vertex>... points) {
        StringBuilder POINTSSTR = new StringBuilder();
        java.util.List<Vertex> sorted_list = ModelUtils.makeList(meshOBJ, points);
        for (Vertex lastVertex : sorted_list) {
            if (POINTSSTR.length() > 0) {
                POINTSSTR.append(String.format(" %.3f,%.3f", lastVertex.coord.y, lastVertex.coord.z));
            } else {
                POINTSSTR.append(String.format("%.3f,%.3f", lastVertex.coord.y, lastVertex.coord.z));
            }
        }
        return POINTSSTR.toString();
    }

    public void close() throws IOException {
        fileWriter.write("</svg>\n");
        fileWriter.close();
    }

    public void export_extrude_error(double scale, int offsetx, int offsety, ExtrudeEdge e1, ExtrudeEdge e2) throws IOException {
        fileWriter.write("<g xml:id=\"" + "err-intersect-e1" + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + "err-intersect-e1" + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\""
                + String.format("%.3f,%.3f %.3f,%.3f",
                        e1.getP1().x, e1.getP1().y,
                        e1.getP2().x, e1.getP2().y)
                + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");

        fileWriter.write("<g xml:id=\"" + "err-intersect-e2" + "\" transform=\"translate(" + offsetx + "," + offsety + ")\" >\n");
        fileWriter.write("<g fill=\"none\" stroke=\"black\" stroke-width=\"0.25\">\n");
        fileWriter.write("<text x=\"0\" y=\"-20\" font-family=\"Verdana\" font-size=\"8\" fill=\"blue\">" + "err-intersect-e2" + "</text>\n");
        fileWriter.write("<g transform=\"scale(1 -1)\" >\n");
        fileWriter.write("<polygon points=\""
                + String.format("%.3f,%.3f %.3f,%.3f",
                        e2.getP1().x, e2.getP1().y,
                        e2.getP2().x, e2.getP2().y)
                + "\" />\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
        fileWriter.write("</g>\n");
    }
}
