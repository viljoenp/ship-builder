/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.loaders;

import com.sun.j3d.loaders.IncorrectFormatException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.rcwarships.loaders.archive.ArchiveFile;
import org.rcwarships.loaders.obj.ObjectFile;
import org.rcwarships.loaders.obj.ParsingErrorException;

/**
 *
 * @author viljoenp
 */
public class SceneUtil {

    private static final Logger LOGGER = Logger.getLogger(SceneUtil.class.getName());

    public static Scene loadFromURI(URL url, int flags, float creaseAngle) throws IncorrectFormatException, ParsingErrorException, IOException, FileNotFoundException, URISyntaxException {
        switch (url.getProtocol()) {
            case "file":
            case "http":
                String name = url.getFile();
                if (name.toLowerCase(Locale.UK).endsWith(".obj")) {

                    final ObjectFile loader = new ObjectFile(flags, creaseAngle);
                    return loader.load(new BufferedReader(new InputStreamReader(url.openConnection().getInputStream())));
                } else if (name.toLowerCase(Locale.UK).endsWith(".stl")) {
                    LOGGER.log(Level.WARNING, "Skipped zip entry {0}, dont know how to load a scene from it", name);
                    throw new IllegalArgumentException("know how to load a scene from " + name);
                } else {
                    LOGGER.log(Level.WARNING, "Skipped zip entry {0}, dont know how to load a scene from it", name);
                    throw new IllegalArgumentException("know how to load a scene from " + name);
                }
            case "jar":
                ArchiveFile loader = new ArchiveFile(flags, creaseAngle);
                return loader.load(url);
            default:
                throw new IllegalArgumentException("Unknown URL protocal " + url.getProtocol());
        }

    }
}
