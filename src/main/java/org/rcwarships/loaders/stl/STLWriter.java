package org.rcwarships.loaders.stl;

/*
 *   __               .__       .__  ._____.           
 * _/  |_  _______  __|__| ____ |  | |__\_ |__   ______
 * \   __\/  _ \  \/  /  |/ ___\|  | |  || __ \ /  ___/
 *  |  | (  <_> >    <|  \  \___|  |_|  || \_\ \\___ \ 
 *  |__|  \____/__/\_ \__|\___  >____/__||___  /____  >
 *                   \/       \/             \/     \/ 
 *
 * Copyright (c) 2006-2011 Karsten Schmidt
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * http://creativecommons.org/licenses/LGPL/2.1/
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Tuple3d;
import org.rcwarships.model.Face;
import org.rcwarships.model.Mesh;

/**
 * A simple, but flexible and memory efficient exporter for binary STL files.
 * Custom color support is implemented via the STLcolorModel interface and the
 * exporter comes with the 2 most common format variations defined by the
 * DEFAULT and MATERIALISE constants.
 *
 * The minimal design of this exporter means it does not build an extra list of
 * faces in RAM and so is able to easily export models with millions of faces.
 *
 * http://en.wikipedia.org/wiki/STL_(file_format)
 */
public class STLWriter {

    protected static final Logger logger = Logger.getLogger(STLWriter.class.getName());
    public static final int DEFAULT_RGB = -1;
    public static final STLColorModel DEFAULT = new DefaultSTLColorModel();
    public static final STLColorModel MATERIALISE = new MaterialiseSTLColorModel(0xffffffff);
    public static final int DEFAULT_BUFFER = 0x10000;
    protected OutputStream ds;
    protected byte[] buf = new byte[4];
    protected int bufferSize;
    protected javax.vecmath.Point3d scale = new javax.vecmath.Point3d(1, 1, 1);
    protected boolean useInvertedNormals = false;
    protected STLColorModel colorModel;
    private String fileName = "STLWriter.stl";

    private final boolean ascii;

    public STLWriter(final String fileName) {
        this();
        this.fileName = fileName;
    }

    public STLWriter() {
        this(DEFAULT, DEFAULT_BUFFER, true);
    }

    public STLWriter(STLColorModel cm, int bufSize, final boolean ascii) {
        colorModel = cm;
        this.bufferSize = bufSize;
        this.ascii = ascii;
    }

    public void beginBinarySave(OutputStream stream, int numFaces) throws IOException {
        logger.info("starting to save STL binary data to output stream...");
        ds = new BufferedOutputStream(new DataOutputStream(stream), bufferSize);
        writeBinaryHeader(numFaces);
    }

    public void beginAsciiSave(OutputStream stream, String name) throws IOException {
        logger.info("starting to save STL ASCII data to output stream...");
        ds = new BufferedOutputStream(new DataOutputStream(stream), bufferSize);
        writeAsciiHeader(name);
    }

    public void beginSave(String fn, int numFaces) throws IOException {
        logger.log(Level.INFO, "saving mesh to: {0}", fn);
        if (ascii) {
            beginAsciiSave(new FileOutputStream(fn), fn.toUpperCase(Locale.UK).replaceAll(".STL", ""));
        } else {
            beginBinarySave(new FileOutputStream(fn), numFaces);
        }
    }

    public void endSave() throws IOException {
        if (ds != null) {
            ds.flush();
            ds.close();
        }
    }

    public void face(final javax.vecmath.Point3d a, final javax.vecmath.Point3d b, final javax.vecmath.Point3d c) throws IOException {
        face(a, b, c, DEFAULT_RGB);
    }

    /**
     * Calculates cross-product with vector v. The resulting vector is
     * perpendicular to both the current and supplied vector and overrides the
     * current.
     *
     * @param v the v
     *
     * @return itself
     */
    public final javax.vecmath.Point3d crossSelf(final javax.vecmath.Point3d a, javax.vecmath.Point3d v) {
        final double cx = a.y * v.z - v.y * a.z;
        final double cy = a.z * v.x - v.z * a.x;
        a.z = a.x * v.y - v.x * a.y;
        a.x = cx;
        a.y = cy;
        return a;
    }

    /**
     * Calculates cross-product with vector v. The resulting vector is
     * perpendicular to both the current and supplied vector and overrides the
     * current.
     *
     * @param v the v
     *
     * @return itself
     */
    public final javax.vecmath.Point3d sub(final javax.vecmath.Point3d a, final javax.vecmath.Point3d v) {
        return new javax.vecmath.Point3d(a.x - v.x, a.y - v.y, a.z - v.z);
    }

    public void face(javax.vecmath.Point3d a, javax.vecmath.Point3d b, javax.vecmath.Point3d c, int rgb) throws IOException {
        //        Vec3D normal = b.sub(a).crossSelf(c.sub(a)).normalize();
        //        if (useInvertedNormals) {
        //            normal.invert();
        //        }
        final javax.vecmath.Vector3d normal = new javax.vecmath.Vector3d(crossSelf(sub(b, a), sub(c, a)));
        normal.normalize();
        if (useInvertedNormals) {
            normal.scale(-1);
        }
        face(a, b, c, new javax.vecmath.Point3d(normal), rgb);
    }

    public void face(javax.vecmath.Point3d a, javax.vecmath.Point3d b, javax.vecmath.Point3d c, javax.vecmath.Point3d normal, int rgb) throws IOException {
        if (ascii) {
            writeAsciiVector("facet normal", normal);
            wrireAsciiString("\touter loop\n");
            writeAsciiVector("\t\tvertex", a);
            writeAsciiVector("\t\tvertex", b);
            writeAsciiVector("\t\tvertex", c);
            wrireAsciiString("\tendloop\n");
            wrireAsciiString("endfacet\n");
        } else {
            writeBinaryVector(normal);
            // vertices
            writeBinaryScaledVector(a);
            writeBinaryScaledVector(b);
            writeBinaryScaledVector(c);
            // vertex attrib (color)
            if (rgb != DEFAULT_RGB) {
                writeBinaryShort(colorModel.formatRGB(rgb));
            } else {
                writeBinaryShort(colorModel.getDefaultRGB());
            }
        }
    }

    private void prepareBuffer(int a) {
        buf[3] = (byte) (a >>> 24);
        buf[2] = (byte) (a >> 16 & 0xff);
        buf[1] = (byte) (a >> 8 & 0xff);
        buf[0] = (byte) (a & 0xff);
    }

    public void setScale(float s) {
        scale.set(s, s, s);
    }

    public void setScale(javax.vecmath.Point3d s) {
        scale.set(s);
    }

    public void useInvertedNormals(boolean state) {
        useInvertedNormals = state;
    }

    protected void writeBinaryFloat(float a) throws IOException {
        prepareBuffer(Float.floatToRawIntBits(a));
        ds.write(buf, 0, 4);
    }

    protected void writeAsciiHeader(String name) throws IOException {
        byte[] header = String.format("solid %s\n", name).getBytes(Charset.forName("UTF-8"));
        ds.write(header, 0, header.length);
    }

    protected void writeBinaryHeader(int num) throws IOException {
        byte[] header = new byte[80];
        colorModel.formatHeader(header);
        ds.write(header, 0, 80);
        writeBinaryInt(num);
    }

    protected void writeBinaryInt(int a) throws IOException {
        prepareBuffer(a);
        ds.write(buf, 0, 4);
    }

    protected void writeBinaryScaledVector(javax.vecmath.Point3d v) throws IOException {
        writeBinaryFloat((float) (v.x * scale.x));
        writeBinaryFloat((float) (v.y * scale.y));
        writeBinaryFloat((float) (v.z * scale.z));
    }

    protected void writeBinaryShort(int a) throws IOException {
        buf[0] = (byte) (a & 0xff);
        buf[1] = (byte) (a >> 8 & 0xff);
        ds.write(buf, 0, 2);
    }

    protected void writeBinaryVector(javax.vecmath.Point3d v) throws IOException {
        writeBinaryFloat((float) v.x);
        writeBinaryFloat((float) v.y);
        writeBinaryFloat((float) v.z);

    }

    public void writeAsciiVector(String str, Tuple3d tuple) throws IOException {
        byte[] buffer = String.format("%s %e %e %e\n", str, tuple.x, tuple.y, tuple.z).getBytes(Charset.forName("UTF-8"));
        ds.write(buffer, 0, buffer.length);
    }

    public void wrireAsciiString(String str) throws IOException {
        byte[] buffer = str.getBytes(Charset.forName("UTF-8"));
        ds.write(buffer, 0, buffer.length);
    }

    public void exportMesh(final Mesh meshOBJ) throws IOException {

        try {
            if (ascii) {
                beginAsciiSave(new FileOutputStream(fileName), meshOBJ.name);
            } else {
                beginBinarySave(new FileOutputStream(fileName), meshOBJ.faces.size());
            }
            for (final Face face : meshOBJ.faces) {
                face(meshOBJ.verts.get(face.verts[0]).coord, meshOBJ.verts.get(face.verts[1]).coord, meshOBJ.verts.get(face.verts[2]).coord);
            }
        } finally {
            if (ascii) {
                byte[] footer = String.format("endsolid %s", meshOBJ.name).getBytes(Charset.forName("UTF-8"));
                ds.write(footer, 0, footer.length);
            }
            endSave();
        }
    }
}
