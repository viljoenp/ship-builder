/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.loaders.archive;

import com.sun.j3d.loaders.IncorrectFormatException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.Locale;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.rcwarships.loaders.Scene;
import org.rcwarships.loaders.obj.ObjectFile;
import org.rcwarships.loaders.obj.ObjectFileScene;
import org.rcwarships.loaders.obj.ParsingErrorException;
import org.rcwarships.model.Mesh;

/**
 *
 * @author viljoenp
 */
public class ArchiveFile {

    private static final Logger LOGGER = Logger.getLogger(ArchiveFile.class.getName());

    private final int flags;
    private final float creaseAngle;

    public ArchiveFile(final int flags, final float creaseAngle) {
        this.flags = flags;
        this.creaseAngle = creaseAngle;
    }

    public Scene load(URL url) throws FileNotFoundException, IncorrectFormatException, ParsingErrorException, IOException, URISyntaxException {

        final JarURLConnection jarConnection = (JarURLConnection) url.openConnection();
        final JarEntry jentry = jarConnection.getJarEntry();
        if (jentry == null) {
            final JarFile archive = new JarFile(new File(url.toURI()));
            final ArchivedScene scene = new ArchivedScene();
            for (Enumeration<JarEntry> entries = archive.entries(); entries.hasMoreElements();) {
                final JarEntry entry = entries.nextElement();
                if (entry.getName().toLowerCase(Locale.UK).endsWith(".obj")) {
                    final ObjectFile loader = new ObjectFile(flags, creaseAngle);
                    final Scene entryScene = loader.load(new BufferedReader(new InputStreamReader(archive.getInputStream(entry))));
                    for (java.util.Map.Entry<String, Mesh> meshEntry : entryScene.getNamedObjects().entrySet()) {
                        scene.addNamedObject(meshEntry.getKey(), meshEntry.getValue());
                    }
                } else if (entry.getName().toLowerCase(Locale.UK).endsWith(".stl")) {
                    LOGGER.log(Level.WARNING, "Skipped zip entry {0}, dont know how to load a scene from it", entry.getName());
                } else {
//                    throw new IllegalArgumentException("Dont wh")
                    LOGGER.log(Level.WARNING, "Skipped zip entry {0}, dont know how to load a scene from it", entry.getName());
                }
            }
            return scene;
        } else {
            if (jentry.getName().toLowerCase(Locale.UK).endsWith(".obj")) {
                final ObjectFile loader = new ObjectFile(flags, creaseAngle);
                return loader.load(new BufferedReader(new InputStreamReader(jarConnection.getInputStream())));
            } else if (jentry.getName().toLowerCase(Locale.UK).endsWith(".stl")) {
                LOGGER.log(Level.WARNING, "Skipped zip entry {0}, dont know how to load a scene from it", jentry.getName());
                throw new IllegalArgumentException("know how to load a scene from " + jentry.getName());
            } else {
                LOGGER.log(Level.WARNING, "Skipped zip entry {0}, dont know how to load a scene from it", jentry.getName());
                throw new IllegalArgumentException("know how to load a scene from " + jentry.getName());
            }
        }

    }
}
