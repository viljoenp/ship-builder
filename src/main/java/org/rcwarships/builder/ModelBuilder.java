/**
 * Copyright (C) 2010 Petrus Viljoen. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.rcwarships.loaders.Scene;
import org.rcwarships.loaders.SceneUtil;
import org.rcwarships.loaders.obj.ObjectFile;
import org.rcwarships.loaders.obj.ObjectFileScene;
import org.rcwarships.loaders.stl.STLWriter;
import org.rcwarships.model.CaprailFactory;
import org.rcwarships.model.KeelFactory;
import org.rcwarships.model.Mesh;
import org.rcwarships.model.ModelUtils;
import org.rcwarships.model.RibFactory;
import org.rcwarships.model.Vertex;

/**
 * import a HULL mesh ( usually from an .obj export from delftship )
 *
 * @author viljoenp
 */
public class ModelBuilder {

    private static final Logger LOGGER = Logger.getLogger(ModelBuilder.class.getName());
    //# The intended scale of the resulting model
    static final double model_scale = 1.0 / 144.0;
    //# if we intend the ribs te be 1cm wide
    //# the width (as seen from font) of rib , keel and deck plates
    static final double plate_width = 10;
    //# the thicknet (as seen from side) of rib , keel and deck plates
    static final double plate_thickness = ModelUtils.RIB_WIDTH_PER_INCH_SPACING * 2;

//    private boolean spin = false;
    private boolean noTriangulate = false;
    private boolean noStripify = false;
    private double creaseAngle = 60.0;
    private URL filename = null;

    private String output = "";
    private ModelInput input = null;
//    private SimpleUniverse univ = null;

    public ModelBuilder(String args[]) {
        if (args.length != 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].startsWith("-")) {
                    if (args[i].equals("-c")) {
                        if (i < args.length - 1) {
                            creaseAngle = (new Double(args[++i])).doubleValue();
                        } else {
                            usage();
                        }
                    } else if (args[i].equals("-o")) {
                        if (i < args.length - 1) {
                            output = args[++i].trim();
                            if (!output.endsWith("/")) {
                                output += "/";
                            }
                        } else {
                            usage();
                        }
                    } else if (args[i].equals("-i")) {
                        if (i < args.length - 1) {
                            try {
                                ++i;
                                final URL inputFile;
                                if ((args[i].indexOf("file:") == 0) || (args[i].indexOf("http") == 0)) {
                                    inputFile = new URL(args[i]);
                                } else if (args[i].charAt(0) != '/') {
                                    inputFile = new URL("file:./" + args[i]);
                                } else {
                                    inputFile = new URL("file:" + args[i]);
                                }

                                final JAXBContext jc = JAXBContext.newInstance(ModelInput.class);
                                Unmarshaller unmarshaller = jc.createUnmarshaller();
                                System.out.println("Loading model input parameters from " + inputFile);
                                input = (ModelInput) unmarshaller.unmarshal(inputFile);
                            } catch (MalformedURLException | JAXBException e) {
                                System.err.println(e);
                                System.exit(1);
                            }
                        } else {
                            usage();
                        }
                    } else {
                        usage();
                    }
                } else {
                    try {
                        if ((args[i].indexOf("file:") == 0) || (args[i].indexOf("http") == 0) || (args[i].indexOf("jar") == 0)) {
                            filename = new URL(args[i]);
                        } else if (args[i].charAt(0) != '/') {
                            filename = new URL("file:./" + args[i]);
                        } else {
                            filename = new URL("file:" + args[i]);
                        }
                    } catch (MalformedURLException e) {
                        System.err.println(e);
                        System.exit(1);
                    }
                }
            }
        }

        if (filename == null) {
            System.err.println("no file specified in arguments");
            System.exit(1);
        }
    }

    public Scene createSceneGraph() throws Exception {
        int flags = ObjectFile.RESIZE;
        if (!noTriangulate) {
            flags |= ObjectFile.TRIANGULATE;
        }
        if (!noStripify) {
            flags |= ObjectFile.STRIPIFY;
        }
        return SceneUtil.loadFromURI(filename, flags, (float) (creaseAngle * Math.PI / 180.0));
    }

    private void usage() {
        System.out.println("Usage: java ModelBuilder [-c degrees] [ -i <input.xml> ] -o <output-directory> file>");
        System.out.println("  -c Set crease angle for normal generation (default is 60 without");
        System.out.println("     smoothing group info, otherwise 180 within smoothing groups)");
        System.out.println("  -i input parameters for the model generation, see ModelInfo.xsd");
        System.out.println("  -o output direcotry where output will be saved");
        System.exit(0);
    } // End of usage

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here

        final ModelBuilder instance = new ModelBuilder(args);
        final double start_load = System.currentTimeMillis();

        final Scene scene = instance.createSceneGraph();
        LOGGER.log(Level.INFO, "Loaded Object scene  {0} secs", (((double) System.currentTimeMillis()) - start_load) / 1000.f);
        for (String name : scene.getNamedObjects().keySet()) {
            System.out.printf("Found named Object %s : %s   with %d Vertexes, %d Edges, %d Faces\n", name, scene.getNamedObjects().get(name).getClass().getSimpleName(), scene.getNamedObjects().get(name).verts.size(), scene.getNamedObjects().get(name).edges.size(), scene.getNamedObjects().get(name).faces.size());
            final Mesh hull_MESH = scene.getNamedObjects().get(name);
            hull_MESH.scale(new Point3d(1000 * model_scale, 1000 * model_scale, 1000 * model_scale)); // Scale the model the size we want it (1 unit was 1m) and  1:144 but we want units in mm,  from now on we work in mm not meters..

            final double start_edge_detect = System.currentTimeMillis();
            hull_MESH.detectManifoldEdges();
            LOGGER.log(Level.INFO, "Detected manifold Edges in {0} secs", (((double) System.currentTimeMillis()) - start_edge_detect) / 1000.f);

            final double start_minmax_detect = System.currentTimeMillis();
            final Vertex[] minMax = hull_MESH.findMinAndMaxVertices_verts(hull_MESH.verts, new Vector3d(0, 1, 0));
            LOGGER.log(Level.INFO, "Detected MinMax vertexes in {0} secs:  {1}:( {2}, {3}, {4})  -  {5}:( {6}, {7}, {8})", new Object[]{(((double) System.currentTimeMillis()) - start_minmax_detect) / 1000.f, minMax[0].index, minMax[0].coord.x, minMax[0].coord.y, minMax[0].coord.z, minMax[1].index, minMax[1].coord.x, minMax[1].coord.y, minMax[1].coord.z});

            if (instance.input == null) {
                System.out.println("Generating default Input parameters for Model");
                instance.input = ModelUtils.generateModelInput(hull_MESH);
            }
            //# we will populate this list with the y Coordinates of where we want to place ribs. this allows us to costomize rib placement
            //# to customise the rib positions. print out the values and initialize it here
            //# we default to have ribs separated by 50.8 mm on the final scaled mode. note the first 3 sections and last 3 sections has more detail            
            final java.util.List<ModelUtils.RibPosition> rib_positions = new java.util.ArrayList<>();
            for (Rib rib : instance.input.getRibs()) {
                rib_positions.add(new ModelUtils.RibPosition(rib.getPosition(), rib.getWidth(), rib.getThickness()));
            }

            final double start_keel_cutter = System.currentTimeMillis();
            ModelUtils.KeelInfo keelInfo = new ModelUtils.KeelInfo(instance.input.getKeelInfo().getWidth(), instance.input.getKeelInfo().getThickness());
            //# construct a keel MESH from the hull, the keel plate will have a width of plate_width, rib gaps will be of plate_thickness at every rib_positions (centre)
            Mesh keelObject = KeelFactory.make_keel(hull_MESH, keelInfo, rib_positions, 64);
            LOGGER.log(Level.INFO, "Created keel mesh in {0} secs   start:{1} , stop:{2}   width:{3}  extrusion:{4}", new Object[]{(((double) System.currentTimeMillis()) - start_keel_cutter) / 1000.f, keelInfo.min_y, keelInfo.max_y, keelInfo.width, keelInfo.thickness});

            scene.addNamedObject(keelObject.name, keelObject);

            // # construct each rib , at the specified position MESH from the hull, the rib plate will have a width of plate_width , the gap for the keel , and notches for the deck will be of plate_thickness
            java.util.List<String> ribNames = new java.util.ArrayList<>();
            for (ModelUtils.RibPosition pos : rib_positions) {
                try {
                    Mesh ribjObject = RibFactory.makeRib(hull_MESH, plate_width, plate_thickness, pos, 64, true);
                    scene.addNamedObject(ribjObject.name, ribjObject);
                    ribNames.add(ribjObject.name);
                } catch (Exception e) {
                    LOGGER.log(Level.WARNING, "Could not complete rib " + pos.y + " will try without the keel notch", e);
                    try {
                        Mesh ribjObject = RibFactory.makeRib(hull_MESH, plate_width, plate_thickness, pos, 64, false);
                        scene.addNamedObject(ribjObject.name, ribjObject);
                        ribNames.add(ribjObject.name);
                    } catch (Exception ex) {
                        LOGGER.log(Level.WARNING, "Could not complete rib " + pos.y + " will substitue it with a solid rib", ex);
                        try {
                            Mesh ribjObject = RibFactory.makeSolidRib(hull_MESH, plate_width, plate_thickness, pos, 64);
                            scene.addNamedObject(ribjObject.name, ribjObject);
                            ribNames.add(ribjObject.name);
                        } catch (RuntimeException exx) {
                            LOGGER.log(Level.WARNING, "Could not complete subtitude rib " + pos.y + " ", exx);
                        }
                    }
                }

            }

            //# construct a keel Deck from the hull, the keel plate will have a width of plate_width, rib gaps will be of plate_thickness at every rib_positions (centre)
            ModelUtils.CapRailInfo capRailInfo = new ModelUtils.CapRailInfo(instance.input.getCaprailInfo().getWidth(), instance.input.getCaprailInfo().getL1Extrusion(), instance.input.getCaprailInfo().getL2Extrusion());
            Mesh caprailObject = CaprailFactory.makeCaprail(hull_MESH, capRailInfo, keelInfo, rib_positions, 64);
            scene.addNamedObject(caprailObject.name, caprailObject);

            //# construct a keel Deck from the hull, the keel plate will have a width of plate_width, rib gaps will be of plate_thickness at every rib_positions (centre)
            ModelUtils.DeckInfo deckInfo = new ModelUtils.DeckInfo(instance.input.getDeckPlate().getThickness(), instance.input.getDeckPlate().getExtrusion());
            Mesh deckObject = CaprailFactory.makeDeckPlate(hull_MESH, deckInfo, keelInfo, 64);
            scene.addNamedObject(deckObject.name, deckObject);

            STLWriter stl = new STLWriter(instance.output + "KEEL-MESH.stl");
            stl.exportMesh(scene.getNamedObjects().get("KEEL-MESH"));
            STLWriter cstl = new STLWriter(instance.output + "CAPRAIL-MESH.stl");
            cstl.exportMesh(scene.getNamedObjects().get("CAPRAIL-MESH"));
            STLWriter dstl = new STLWriter(instance.output + "DECK-MESH.stl");
            dstl.exportMesh(scene.getNamedObjects().get("DECK-MESH"));
            for (String rname : ribNames) {
                STLWriter rstl = new STLWriter(instance.output + rname + ".stl");
                rstl.exportMesh(scene.getNamedObjects().get(rname));
            }

            dumpOpenScadScript(instance.output, ribNames);
        }

    }

    private static void dumpOpenScadScript(String output, java.util.List<String> ribNames) throws IOException {
//                        import("/Users/viljoenp/proj-private/ship-builder/target/KEEL-MESH.stl");

        try (Writer writer = new BufferedWriter(new FileWriter(new File(output + "merge-parts.scad")))) {
            writer.write(String.format("union() {\n"));
            File capFile = new File(output + "CAPRAIL-MESH.stl");
            writer.write(String.format("\timport(\"%s\");\n", capFile.getAbsolutePath()));
            for (String ribName : ribNames) {
                File ribFile = new File(output + ribName + ".stl");
                writer.write(String.format("\timport(\"%s\");\n", ribFile.getAbsolutePath()));
            }
            writer.write(String.format("}\n"));
        }
    }
}
