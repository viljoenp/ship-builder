/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * Input data needed for the generation of the a deck plate (fits on top of
 * caprail ).
 *
 * @author viljoenp
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deck")
public class Deck implements java.io.Serializable {

    private static final long serialVersionUID = 20140226001L;
    /**
     * Distance the Deck-plate will be extruded inwards from the hull before it
     * starts, NOTE this should be slightly larger (0.5mm) than the
     * Cap-rail.thickness
     */
    @XmlAttribute(name = "extrusion", required = true)
    private double extrusion;
    /**
     * The thickness of the Deck-plate , NOTE this should be the same or
     * slightly less (0.1mm) than the Cap-rail.width
     */
    @XmlAttribute(name = "thickness", required = true)
    private double thickness;

    public double getExtrusion() {
        return extrusion;
    }

    public void setExtrusion(double extrusion) {
        this.extrusion = extrusion;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

}
