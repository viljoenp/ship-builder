/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * Input data needed for the generation of the a keel.
 *
 * @author viljoenp
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "keel")
public class Keel implements java.io.Serializable {

    private static final long serialVersionUID = 20140226001L;
    /**
     * Distance the Keel will be extruded inwards from the hull to make it a
     * solid, units is after scaling.
     *
     * Note that this extrusion is not applied from the center of the hull, but
     * rather from side profile of the keel (width units in from the center ).
     */
    @XmlAttribute(name = "thickness", required = true)
    private double thickness;
    /**
     * The width/thickness of the rib ( profile exposed to the hull ), units is
     * after scaling.
     */
    @XmlAttribute(name = "width", required = true)
    private double width;

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

}
