/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import org.rcwarships.loaders.obj.ObjectFile;
import org.rcwarships.loaders.obj.ObjectFileScene;
import org.rcwarships.model.Mesh;
import org.rcwarships.model.Vertex;

import static org.rcwarships.builder.ModelBuilder.model_scale;

/**
 *
 * @author viljoenp
 */
public class ObjInfo {

    private static final Logger LOGGER = Logger.getLogger(RibCalculator.class.getName());
    private double creaseAngle = 60.0;
    private URL filename = null;

    public ObjInfo(String[] args) {
        if (args.length != 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].startsWith("-")) {
                    if (args[i].equals("-c")) {
                        if (i < args.length - 1) {
                            creaseAngle = (new Double(args[++i])).doubleValue();
                        } else {
                            usage();
                        }
                    } else {
                        usage();
                    }
                } else {
                    try {
                        if ((args[i].indexOf("file:") == 0)
                                || (args[i].indexOf("http") == 0)) {
                            filename = new URL(args[i]);
                        } else if (args[i].charAt(0) != '/') {
                            filename = new URL("file:./" + args[i]);
                        } else {
                            filename = new URL("file:" + args[i]);
                        }
                    } catch (MalformedURLException e) {
                        System.err.println(e);
                        System.exit(1);
                    }
                }
            }
        }

    }

    private void usage() {
        System.out.println("Usage: java ObjInfo [-c degrees] <.obj file>");
        System.out.println("  -c Set crease angle for normal generation (default is 60 without");
        System.out.println("     smoothing group info, otherwise 180 within smoothing groups)");
        System.exit(0);
    } // End of usage   

    private boolean noTriangulate = false;
    private boolean noStripify = false;

    public ObjectFileScene createSceneGraph() throws Exception {

        int flags = ObjectFile.RESIZE;
        if (!noTriangulate) {
            flags |= ObjectFile.TRIANGULATE;
        }
        if (!noStripify) {
            flags |= ObjectFile.STRIPIFY;
        }
        ObjectFile f = new ObjectFile(flags, (float) (creaseAngle * Math.PI / 180.0));
        ObjectFileScene s = f.load(filename);

        return s;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here

        final RibCalculator instance = new RibCalculator(args);
        final double start_load = System.currentTimeMillis();

        final ObjectFileScene scene = instance.createSceneGraph();
        LOGGER.log(Level.INFO, "Loaded Object scene  {0} secs", (((double) System.currentTimeMillis()) - start_load) / 1000.f);
        for (String name : scene.getNamedObjects().keySet()) {
            System.out.printf("Found named Object %s : %s   with %d Vertexes, %d Edges, %d Faces\n", name, scene.getNamedObjects().get(name).getClass().getSimpleName(), scene.getNamedObjects().get(name).verts.size(), scene.getNamedObjects().get(name).edges.size(), scene.getNamedObjects().get(name).faces.size());
            final Mesh tmpMesh = scene.getNamedObjects().get(name);
            tmpMesh.scale(new Point3d(1000 * model_scale, 1000 * model_scale, 1000 * model_scale)); // Scale the model the size we want it (1 unit was 1m) and  1:144 but we want units in mm,  from now on we work in mm not meters..

            final double start_edge_detect = System.currentTimeMillis();
            tmpMesh.detectManifoldEdges();
            LOGGER.log(Level.INFO, "Detected manifold Edges in {0} secs", (((double) System.currentTimeMillis()) - start_edge_detect) / 1000.f);

            final double start_minmax_detect = System.currentTimeMillis();
            final Vertex[] minMax = tmpMesh.findMinAndMaxVertices_verts(tmpMesh.verts, new Vector3d(0, 1, 0));
            LOGGER.log(Level.INFO, "Detected MinMax vertexes in {0} secs:  {1}:( {2}, {3}, {4})  -  {5}:( {6}, {7}, {8})", new Object[]{(((double) System.currentTimeMillis()) - start_minmax_detect) / 1000.f, minMax[0].index, minMax[0].coord.x, minMax[0].coord.y, minMax[0].coord.z, minMax[1].index, minMax[1].coord.x, minMax[1].coord.y, minMax[1].coord.z});
        }
    }
}
