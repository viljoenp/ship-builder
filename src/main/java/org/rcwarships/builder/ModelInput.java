/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Input needed to generate the entire model.
 *
 * @author viljoenp
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "modelInput", propOrder = {
    "caprailInfo",
    "deckPlate",
    "keelInfo",
    "ribs"
})
public class ModelInput implements java.io.Serializable {

    private static final long serialVersionUID = 20140226001L;

    @XmlElement(required = true)
    private Keel keelInfo;
    @XmlElement(required = true)
    private Caprail caprailInfo;
    @XmlElement(required = true)
    private Deck deckPlate;
    @XmlElement(required = true)
    private java.util.List< Rib> ribs;

    public Keel getKeelInfo() {
        return keelInfo;
    }

    public void setKeelInfo(Keel keelInfo) {
        this.keelInfo = keelInfo;
    }

    public Caprail getCaprailInfo() {
        return caprailInfo;
    }

    public void setCaprailInfo(Caprail caprailInfo) {
        this.caprailInfo = caprailInfo;
    }

    public Deck getDeckPlate() {
        return deckPlate;
    }

    public void setDeckPlate(Deck deckPlate) {
        this.deckPlate = deckPlate;
    }

    public java.util.List<Rib> getRibs() {
        return ribs;
    }

    public void setRibs(java.util.List<Rib> ribs) {
        if (ribs == null) {
            ribs = new ArrayList<>();
        }
        this.ribs = ribs;
    }

}
