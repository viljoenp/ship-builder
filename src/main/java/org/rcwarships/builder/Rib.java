/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * Input data needed for the generation of the a rib, location , thickness ,
 * width.
 *
 * @author viljoenp
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rib")
public class Rib implements java.io.Serializable {

    private static final long serialVersionUID = 20140226001L;
    /**
     * The position on the Y axis ( after Scaling ).
     */
    @XmlAttribute(name = "position", required = true)
    private double position;
    /**
     * Distance the rib will be extruded inwards from the hull to make it a
     * solid, units is after scaling.
     */
    @XmlAttribute(name = "thickness", required = true)
    private double thickness;
    /**
     * The width/thickness of the rib ( impenetrable area exposed to the hull ),
     * units is after scaling.
     */
    @XmlAttribute(name = "width", required = true)
    private double width;

    public double getPosition() {
        return position;
    }

    public void setPosition(double position) {
        this.position = position;
    }

    public double getThickness() {
        return thickness;
    }

    public void setThickness(double thickness) {
        this.thickness = thickness;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

}
