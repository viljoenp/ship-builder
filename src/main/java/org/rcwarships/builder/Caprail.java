/*
 * 
 *   Copyright (C) 2010 Petrus Viljoen. All rights reserved. 
 *   
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as 
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *   
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.rcwarships.builder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * Input data needed for the generation of the a keel.
 *
 * @author viljoenp
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "caprail")
public class Caprail implements java.io.Serializable {

    private static final long serialVersionUID = 20140226001L;

    /**
     * Distance the Cap-Rail will be extruded inwards from the hull to make it a
     * solid, units is after scaling. Note the the Cap-Rail will be done in two
     * layers this is the inwards extrusion of top layer.
     */
    @XmlAttribute(name = "l1Extrusion", required = true)
    private double l1Extrusion;

    /**
     * Distance the Cap-Rail will be extruded inwards from the hull to make the
     * layer2 ( ledge for the deck to plate to rest on) .
     */
    @XmlAttribute(name = "l2Extrusion", required = true)
    private double l2Extrusion;

    /**
     * The width/thickness of the Cap-Rail ( impenetrable area exposed to the
     * hull ), units is after scaling.
     */
    @XmlAttribute(name = "width", required = true)
    private double width;

    public double getL1Extrusion() {
        return l1Extrusion;
    }

    public void setL1Extrusion(double l1Extrusion) {
        this.l1Extrusion = l1Extrusion;
    }

    public double getL2Extrusion() {
        return l2Extrusion;
    }

    public void setL2Extrusion(double l2Extrusion) {
        this.l2Extrusion = l2Extrusion;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

}
