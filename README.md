

Tools to build 3d printable models from a hull manifold ( export as .obj from Delfship www.delftship.net )


Example usage:
 
* java -d64 -Xmx16G -Xms2G  org.rcwarships.builder.ModelBuilder -o target -i src/test/resources/vittorio-veneto.xml "jar:file:src/test/resources/test-data.zip!/Vittorio Veneto wavefront (v2.5)-hull.obj"
 
* java -d64 -Xmx16G -Xms2G  org.rcwarships.builder.ModelBuilder -o target -i src/test/resources/jean-bart.xml "jar:file:src/test/resources/test-data.zip!/Jean Bart-2013-08-05.obj" 
 
* java -d64 -Xmx16G -Xms2G  org.rcwarships.builder.ModelBuilder -o target -i src/test/resources/bismark.xml "jar:file:src/test/resources/test-data.zip!/HULL.obj"

A default input file can be generated with 
 
* java -d64 -Xmx16G -Xms4G  org.rcwarships.builder.RibCalculator  "Vittorio Veneto wavefront (v2.5)-hull.obj"

Sample .obj files can be found at 

https://drive.google.com/folderview?id=0B7Bi8GMXyKZPa2NUWEZSc2hTdnM&usp=sharing


The source is a bit nasty at the moment ( not exactly a stellar example of a good clean Java Project) , this was originally a Python addon for Blender. I'm still in the process of removing unused code, and refactoring some of the factories..
